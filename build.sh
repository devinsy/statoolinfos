#/bin/bash

#
# Display help.
#
function help
{
	echo "Build script."
	echo "Usage: build.sh [ -h | -help | --help | -snapshot | -local | -full ]"
	echo "     -h, -help, --help            display this help."
	echo "     -snapshot, --snapshot        build a snapshot."
	echo "     -local, --local              build a new version without tag nor version number commit nor Git push."
	echo "     -tagandpush, --tagandpush    build a new version with tag, version number commit and Git push."
	echo ""
}

#
# Check env.
#
function check_env
{
	okCount=0
	
	# Ant check.
	antCheck=`which ant`
	if [[ "$antCheck" =~ ^/.* ]]; then
		echo "Ant requirement................ OK"
		let "okCount+=1"
	else
		echo "Ant requirement................ MISSING"
	fi
	
	# Javac check.
	javacCheck=`which javac`
	if [[ "$javacCheck" =~ ^/.* ]]; then
		echo "Javac requirement.............. OK"
		let "okCount+=1"
	else
		echo "Javac requirement.............. MISSING"
	fi
	
	# Java version check.
    JAVA_VERSION=17;
	javaVersionCheck=`javac -version 2>&1`
	if [[ "$javaVersionCheck" =~ ^.*\ ${JAVA_VERSION}. ]]; then
		echo "Java ${JAVA_VERSION} version requirement..... OK"
		let "okCount+=1"
	else
		echo "Java ${JAVA_VERSION} version requirement..... MISSING"
	fi
	
	# Git check.
	gitCheck=`which git 2>&1`
	if [[ "$gitCheck" =~ ^/.* ]]; then
		echo "GIT requirement................ OK"
		let "okCount+=1"
	else
		echo "GIT requirement................ MISSING"
	fi

	if [ "$okCount" == 4 ]; then
	    echo "Requirement OK"
	else
	    echo "Requirement MISSING, build abort"
	    exit -1
	fi
}


#
# Main.
#
if [ "$#" -eq 0 ] || [ "$1" == "-h" ]  || [ "$1" == "-help" ]  || [ "$1" == "--help" ]; then
    help
else
	check_env
	if [ "$1" == "-snapshot" ] || [ "$1" == "--snapshot" ] ; then
	    ant -f build-snapshot.xml
	elif [ "$1" == "-local" ] || [ "$1" == "--local" ] ; then
	    ant -f build-local.xml
	elif [ "$1" == "-tagandpush" ] || [ "$1" == "--tagandpush" ] ; then
	    ant -f build-tagandpush.xml
	else
    	echo "Invalid parameters."
    	help
    fi
fi
