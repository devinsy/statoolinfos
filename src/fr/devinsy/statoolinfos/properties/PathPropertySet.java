/*
 * Copyright (C) 2020 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.properties;

import java.util.Hashtable;

import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringSet;

/**
 * The Class StatoolsInfosProperties.
 */
public class PathPropertySet
{
    private Hashtable<String, String> data;

    /**
     * Instantiates a new path property set.
     */
    public PathPropertySet()
    {
        this.data = new Hashtable<String, String>();
    }

    /**
     * Instantiates a new path property set.
     *
     * @param initialCapacity
     *            the initial capacity
     */
    public PathPropertySet(final int initialCapacity)
    {
        this.data = new Hashtable<String, String>(initialCapacity);
    }

    /**
     * Instantiates a new path properties.
     *
     * @param source
     *            the source
     */
    public PathPropertySet(final PathPropertySet source)
    {
        this.data = new Hashtable<String, String>(source.size());
        for (String path : source.getPaths())
        {
            this.data.put(path, source.get(path));
        }
    }

    /**
     * Adds the.
     *
     * @param key
     *            the key
     * @param value
     *            the value
     */
    public void add(final String key, final String value)
    {
        this.data.put(key, value);
    }

    /**
     * Gets the.
     *
     * @param path
     *            the path
     * @return the string
     */
    public String get(final String path)
    {
        String result;

        result = this.data.get(path);

        //
        return result;
    }

    /**
     * Gets the keys.
     *
     * @return the keys
     */
    public StringList getPaths()
    {
        StringList result;

        result = new StringList();

        for (Object key : this.data.keySet())
        {
            result.add((String) key);
        }

        //
        return result;
    }

    /**
     * Gets the prefixes.
     *
     * @return the prefixes
     */
    public StringSet getPrefixes()
    {
        StringSet result;

        result = new StringSet();

        for (Object key : this.data.keySet())
        {
            result.add(getPrefix((String) key));
        }

        //
        return result;
    }

    /**
     * Gets the prefix list.
     *
     * @return the prefix list
     */
    public StringList getPrefixList()
    {
        StringList result;

        result = new StringList();

        for (Object key : this.data.keySet())
        {
            result.add(getPrefix((String) key));
        }

        //
        return result;
    }

    /**
     * Put.
     *
     * @param key
     *            the key
     * @param value
     *            the value
     */
    public void put(final String key, final String value)
    {
        this.data.put(key, value);
    }

    /**
     * Removes the path.
     *
     * @param path
     *            the path
     * @return the string
     */
    public String removePath(final String path)
    {
        String result;

        result = this.data.remove(path);

        //
        return result;
    }

    /**
     * Size.
     *
     * @return the int
     */
    public int size()
    {
        int result;

        result = this.data.size();

        //
        return result;
    }

    /**
     * To string list.
     *
     * @return the string list
     */
    public StringList toStringList()
    {
        StringList result;

        result = new StringList();

        for (String path : getPaths())
        {
            String value = get(path);
            result.add(path + "=" + value);
        }

        //
        return result;
    }

    /**
     * To string list formatted.
     *
     * @return the string list
     */
    public StringList toStringListFormatted()
    {
        StringList result;

        result = new StringList();
        StringList lines = toStringList();

        StringList prefixes = getPrefixList();

        // sort prefixes.
        for (String prefix : prefixes)
        {
            result.add("# [" + prefix + "]");

            StringList sectionLines = lines.filter("^" + prefix + "\\..*");
            result.addAll(sectionLines);

            result.add("");
        }

        //
        return result;
    }

    /**
     * Gets the prefix.
     *
     * @param source
     *            the source
     * @return the prefix
     */
    private static String getPrefix(final String source)
    {
        String result;

        result = source.substring(source.indexOf("."));

        //
        return result;
    }
}
