/*
 * Copyright (C) 2020-2023 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.properties;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringsUtils;

/**
 * The Class StatoolsInfosProperties.
 */
public class PathPropertyUtils
{
    /**
     * Checks if is property line.
     *
     * @param line
     *            the line
     * @return true, if is property line
     */
    public static boolean isPropertyLine(final String line)
    {
        boolean result;

        if (StringUtils.isAllBlank(line))
        {
            result = false;
        }
        else
        {
            result = line.matches("^[^#].*[^\\s].*=.*$");
        }

        //
        return result;
    }

    /**
     * Load.
     *
     * @param file
     *            the file
     * @return the path property list
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static PathPropertyList load(final File file) throws IOException
    {
        PathPropertyList result;

        result = load(file, StandardCharsets.UTF_8);

        //
        return result;
    }

    /**
     * Load.
     *
     * @param file
     *            the file
     * @param charset
     *            the charset name
     * @return the path properties
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static PathPropertyList load(final File file, final Charset charset) throws IOException
    {
        PathPropertyList result;

        if (file == null)
        {
            throw new IllegalArgumentException("File parameter is null.");
        }
        else
        {
            result = new PathPropertyList();

            BufferedReader in = null;
            try
            {
                in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charset));
                result = read(in);
            }
            finally
            {
                IOUtils.closeQuietly(in);
            }
        }

        //
        return result;
    }

    /**
     * Load.
     *
     * @param url
     *            the url
     * @return the path property list
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static PathPropertyList load(final URL url) throws IOException
    {
        PathPropertyList result;

        StringList lines = StringsUtils.load(url);

        result = read(lines);

        //
        return result;
    }

    /**
     * Read.
     *
     * @param in
     *            the in
     * @return the path property list
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static PathPropertyList read(final BufferedReader in) throws IOException
    {
        PathPropertyList result;

        result = new PathPropertyList();

        boolean ended = false;
        while (!ended)
        {
            PathProperty property = readPathProperty(in);

            if (property == null)
            {
                ended = true;
            }
            else
            {
                result.add(property);
            }
        }

        //
        return result;
    }

    /**
     * Read.
     *
     * @param source
     *            the source
     * @return the path property list
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static PathPropertyList read(final StringList source) throws IOException
    {
        PathPropertyList result;

        result = new PathPropertyList();

        for (String line : source)
        {
            if (isPropertyLine(line))
            {
                PathProperty property = valueOf(line);

                result.add(property);
            }
        }

        //
        return result;
    }

    /**
     * Read not empty line.
     *
     * @param in
     *            the in
     * @return the string
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static String readActiveLine(final BufferedReader in) throws IOException
    {
        String result;

        boolean ended = false;
        result = null;
        while (!ended)
        {
            String line = in.readLine();

            if (line == null)
            {
                ended = true;
                result = null;
            }
            else if ((StringUtils.isNotBlank(line)) && (!line.startsWith("#")) && (line.contains("=")))
            {
                ended = true;
                result = line;
            }
        }

        //
        return result;
    }

    /**
     * Read path property.
     *
     * @param in
     *            the in
     * @return the path property
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static PathProperty readPathProperty(final BufferedReader in) throws IOException
    {
        PathProperty result;

        String line = readActiveLine(in);

        if (line == null)
        {
            result = null;
        }
        else
        {
            result = valueOf(line);
        }

        //
        return result;
    }

    /**
     * Save.
     *
     * @param file
     *            the file
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void save(final File file, final PathProperties source) throws IOException
    {
        PrintWriter out = null;
        try
        {
            out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8));
            write(out, source);
        }
        finally
        {
            //
            IOUtils.closeQuietly(out);
        }
    }

    /**
     * Value of.
     *
     * @param line
     *            the line
     * @return the path property
     */
    public static PathProperty valueOf(final String line)
    {
        PathProperty result;

        if (line == null)
        {
            result = null;
        }
        else
        {
            String[] tokens = line.split("=", 2);
            result = new PathProperty(tokens[0].trim(), tokens[1].trim());
        }

        //
        return result;
    }

    /**
     * Write.
     *
     * @param out
     *            the out
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void write(final PrintWriter out, final PathProperties source) throws IOException
    {
        if (source != null)
        {
            StringList lines = source.toStringListFormatted();

            for (String string : lines)
            {
                out.write(string);
                out.write("\n");
            }
        }
    }
}
