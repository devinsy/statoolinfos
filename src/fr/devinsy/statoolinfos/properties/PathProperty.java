/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.properties;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class PathProperty.
 */
public class PathProperty
{
    public static final Pattern METRIC_LABEL_PATTERN = Pattern.compile("^metrics\\.(?<label>\\S+)\\.(name|description|\\d{4}(\\.(months|weeks|days))?)$");
    public static final Pattern METRIC_NAME_PATTERN = Pattern.compile("^(?<name>metrics\\.\\S+)\\.(name|description|\\d{4}(\\.(months|weeks|days))?)$");
    public static final Pattern METRIC_YEAR_PATTERN = Pattern.compile("^metrics\\.\\S+\\.(?<year>\\d{4})(\\.(months|weeks|days))?$");

    private String path;
    private String value;

    /**
     * Instantiates a new path property.
     *
     * @param path
     *            the key
     * @param value
     *            the value
     */
    public PathProperty(final String path, final String value)
    {
        if (StringUtils.isBlank(path))
        {
            throw new IllegalArgumentException("Path is blank.");
        }
        else
        {
            this.path = path;
            this.value = value;
        }
    }

    /**
     * Gets the leaf.
     *
     * @return the leaf
     */
    public String getLeaf()
    {
        String result;

        result = this.path.substring(this.path.lastIndexOf('.') + 1);

        //
        return result;
    }

    /**
     * Gets the metric name.
     * 
     * <pre>
     * ("service.name")                       = null
     * ("metrics.https.visitors.name")        = "https.visitors"
     * ("metrics.https.visitors.2020")        = "https.visitors"
     * ("metrics.https.visitors.2020.months") = "https.visitors"
     * </pre>
     * 
     * @return the metric name
     */
    public String getMetricLabel()
    {
        String result;

        Matcher matcher = METRIC_LABEL_PATTERN.matcher(this.path);
        if (matcher.matches())
        {
            result = matcher.group("label");
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Gets the metric name.
     * 
     * <pre>
     * ("service.name")                       = null
     * ("metrics.https.visitors.name")        = "metrics.https.visitors"
     * ("metrics.https.visitors.2020")        = "metrics.https.visitors"
     * ("metrics.https.visitors.2020.months") = "metrics.https.visitors"
     * </pre>
     * 
     * @return the metric name
     */
    public String getMetricName()
    {
        String result;

        Matcher matcher = METRIC_NAME_PATTERN.matcher(this.path);
        if (matcher.matches())
        {
            result = matcher.group("name");
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Gets the metric year.
     * 
     * <pre>
     * ("service.name")                       = null
     * ("metrics.https.visitors.name")        = null
     * ("metrics.https.visitors.2020")        = "2020"
     * ("metrics.https.visitors.2020.months") = "2020"
     * </pre>
     * 
     * @return the metric name
     */
    public String getMetricYear()
    {
        String result;

        Matcher matcher = METRIC_YEAR_PATTERN.matcher(this.path);
        if (matcher.matches())
        {
            result = matcher.group("year");
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Gets the path.
     *
     * @return the path
     */
    public String getPath()
    {
        return this.path;
    }

    /**
     * Gets the prefix.
     *
     * @return the prefix
     */
    public String getPrefix()
    {
        String result;

        int index = this.path.indexOf(".");
        if (index == -1)
        {
            result = null;
        }
        else
        {
            result = this.path.substring(0, index);
        }

        //
        return result;
    }

    /**
     * Gets the sub prefix.
     *
     * @return the sub prefix
     */
    public String getSubPrefix()
    {
        String result;

        if (StringUtils.countMatches(this.path, '.') > 1)
        {
            result = this.path.substring(0, this.path.indexOf(".", this.path.indexOf(".") + 1));
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue()
    {
        return this.value;
    }

    /**
     * Sets the path.
     *
     * @param path
     *            the new path
     */
    public void setPath(final String path)
    {
        this.path = path;
    }

    /**
     * Sets the value.
     *
     * @param value
     *            the new value
     */
    public void setValue(final String value)
    {
        this.value = value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString()
    {
        String result;

        result = "[" + this.path + "=" + this.value + "]";

        //
        return result;
    }
}
