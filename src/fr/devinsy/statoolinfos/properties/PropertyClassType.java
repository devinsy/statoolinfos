/*
 * Copyright (C) 2021-2023 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.properties;

import org.apache.commons.lang3.StringUtils;

public enum PropertyClassType
{
    FEDERATION,
    ORGANIZATION,
    SERVICE,
    HOSTING,
    METRICS;

    /**
     * Checks if is child of.
     *
     * @param parent
     *            the parent
     * @return true, if is child of
     */
    public boolean isChildOf(final PropertyClassType parent)
    {
        boolean result;

        switch (this)
        {
            case FEDERATION:
                if (parent == null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            break;
            case ORGANIZATION:
                if (parent == FEDERATION)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            break;
            case SERVICE:
                if (parent == ORGANIZATION)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            break;
            case HOSTING:
                if (parent == ORGANIZATION)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            break;
            case METRICS:
                if (parent == METRICS)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            break;
            default:
                result = false;
        }

        //
        return result;
    }

    /**
     * Of.
     *
     * @param value
     *            the value
     * @return the property class type
     */
    public static PropertyClassType of(final String value)
    {
        PropertyClassType result;

        String target = StringUtils.trim(StringUtils.toRootLowerCase(value));

        if (target == null)
        {
            result = null;
        }
        else if (StringUtils.equals(target, "federation"))
        {
            result = FEDERATION;
        }
        else if (StringUtils.equals(target, "organization"))
        {
            result = ORGANIZATION;
        }
        else if (StringUtils.equals(target, "service"))
        {
            result = SERVICE;
        }
        else if (StringUtils.equals(target, "hosting"))
        {
            result = HOSTING;
        }
        else if (StringUtils.equals(target, "metrics"))
        {
            result = METRICS;
        }
        else
        {
            result = null;
        }

        //
        return result;
    }
}