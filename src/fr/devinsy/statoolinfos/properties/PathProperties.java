/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.properties;

import java.net.URL;
import java.util.Iterator;

import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringSet;

/**
 * The Interface PathProperties.
 */
public interface PathProperties extends Iterable<PathProperty>
{

    /**
     * Adds the.
     *
     * @param source
     *            the source
     */
    void add(PathProperties source);

    /**
     * Adds the.
     *
     * @param property
     *            the property
     * @return true, if successful
     */
    boolean add(PathProperty property);

    /**
     * Gets the.
     *
     * @param path
     *            the path
     * @return the string
     */
    String get(final String path);

    /**
     * Gets the by prefix.
     *
     * @param prefix
     *            the prefix
     * @return the by prefix
     */
    PathProperties getByPrefix(String prefix);

    /**
     * Gets the keys.
     *
     * @return the keys
     */
    StringList getPaths();

    /**
     * Gets the prefixes.
     *
     * @return the prefixes
     */
    StringSet getPrefixes();

    /**
     * Gets the prefix list.
     *
     * @return the prefix list
     */
    StringList getPrefixList();

    /**
     * Gets the property.
     *
     * @param path
     *            the path
     * @return the property
     */
    PathProperty getProperty(final String path);

    /**
     * Gets the sub prefixes.
     *
     * @return the sub prefixes
     */
    StringSet getSubPrefixes();

    /**
     * Gets the url.
     *
     * @param path
     *            the path
     * @return the url
     */
    URL getURL(String path);

    /**
     * Iterator.
     *
     * @return the iterator
     */
    @Override
    Iterator<PathProperty> iterator();

    /**
     * Put.
     *
     * @param key
     *            the key
     * @param value
     *            the value
     */
    void put(final String key, final long value);

    /**
     * Put.
     *
     * @param key
     *            the key
     * @param value
     *            the value
     */
    void put(final String key, final String value);

    /**
     * Delete path.
     *
     * @param path
     *            the path
     */
    void removePath(final String path);

    /**
     * Removes the prefix.
     *
     * @param prefix
     *            the prefix
     */
    void removeSection(final String prefix);

    /**
     * Size.
     *
     * @return the int
     */
    int size();

    /**
     * To string list.
     *
     * @return the string list
     */
    StringList toStringList();

    /**
     * To string list formatted.
     *
     * @return the string list
     */
    StringList toStringListFormatted();

    /**
     * Gets the prefix.
     *
     * @param source
     *            the source
     * @return the prefix
     */
    static String getPrefix(final String source)
    {
        String result;

        result = source.substring(source.indexOf("."));

        //
        return result;
    }
}
