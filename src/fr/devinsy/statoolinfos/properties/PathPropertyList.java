/*
 * Copyright (C) 2020-2023 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.properties;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.Year;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.threeten.extra.YearWeek;

import fr.devinsy.statoolinfos.core.StatoolInfosUtils;
import fr.devinsy.statoolinfos.metrics.Metric;
import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringSet;

/**
 * The Class PathPropertyList.
 */
public class PathPropertyList extends ArrayList<PathProperty> implements PathProperties
{
    private static final long serialVersionUID = -8491243013973609457L;
    private static final Logger logger = LoggerFactory.getLogger(PathPropertyList.class);

    /**
     * Instantiates a new path property list.
     */
    public PathPropertyList()
    {
        super();
    }

    /**
     * Instantiates a new path property list.
     *
     * @param initialCapacity
     *            the initial capacity
     */
    public PathPropertyList(final int initialCapacity)
    {
        super(initialCapacity);
    }

    /**
     * Instantiates a new path property list.
     *
     * @param source
     *            the source
     */
    public PathPropertyList(final PathProperties source)
    {
        super();
        if (source != null)
        {
            this.ensureCapacity(source.size());

            for (PathProperty property : source)
            {
                this.add(property);
            }
        }
    }

    /**
     * Adds the.
     *
     * @param source
     *            the source
     */
    @Override
    public void add(final PathProperties source)
    {
        for (PathProperty property : source)
        {
            add(property);
        }
    }

    /**
     * Gets the.
     *
     * @param path
     *            the path.
     * @return the string
     */
    @Override
    public String get(final String path)
    {
        String result;

        PathProperty property = getProperty(path);

        if (property == null)
        {
            result = null;
        }
        else
        {
            result = property.getValue();
        }

        //
        return result;
    }

    /**
     * Gets the.
     *
     * @param paths
     *            the paths
     * @return the string
     */
    public String get(final String... paths)
    {
        String result;

        boolean ended = false;
        result = null;
        int index = 0;
        while (!ended)
        {
            if (index < paths.length)
            {
                String path = paths[index];
                PathProperty property = getProperty(path);
                if (property == null)
                {
                    index += 1;
                }
                else
                {
                    ended = true;
                    result = property.getValue();
                }
            }
            else
            {
                ended = true;
                result = null;
            }
        }

        //
        return result;
    }

    /**
     * Gets the as file.
     *
     * @param path
     *            the path
     * @return the as file
     */
    public File getAsFile(final String path)
    {
        File result;

        String value = get(path);

        if (value == null)
        {
            result = null;
        }
        else
        {
            result = new File(value);
        }

        //
        return result;
    }

    /**
     * Gets the by pattern.
     *
     * @param regex
     *            the regex
     * @return the by pattern
     */
    public PathPropertyList getByPattern(final String regex)
    {
        PathPropertyList result;

        result = new PathPropertyList();

        if (StringUtils.isNotBlank(regex))
        {
            Pattern pattern = Pattern.compile(regex);

            for (PathProperty property : this)
            {
                Matcher matcher = pattern.matcher(property.getPath());
                if (matcher.matches())
                {
                    result.add(property);
                }
            }
        }

        //
        return result;
    }

    /**
     * Gets the by prefix.
     *
     * @param prefix
     *            the prefix
     * @return the by prefix
     */
    @Override
    public PathPropertyList getByPrefix(final String prefix)
    {
        PathPropertyList result;

        result = new PathPropertyList();

        if (StringUtils.isNotBlank(prefix))
        {
            String pattern;
            if (prefix.endsWith("."))
            {
                pattern = prefix;
            }
            else
            {
                pattern = prefix + ".";
            }

            for (PathProperty current : this)
            {
                if (StringUtils.startsWith(current.getPath(), pattern))
                {
                    result.add(current);
                }

            }
        }

        //
        return result;
    }

    /**
     * Gets the year leafs.
     *
     * @param prefix
     *            the prefix
     * @return the year leafs
     */
    public PathPropertyList getByYearPrefix(final String prefix)
    {
        PathPropertyList result;

        result = new PathPropertyList();

        if (StringUtils.isNotBlank(prefix))
        {
            String pattern;
            if (prefix.endsWith("."))
            {
                pattern = "^" + prefix + "\\d\\d\\d\\d$";
            }
            else
            {
                pattern = "^" + prefix + "\\.\\d\\d\\d\\d$";
            }

            result = getByPattern(pattern);
        }

        //
        return result;
    }

    /**
     * Gets the date.
     *
     * @param path
     *            the path
     * @return the date
     */
    public LocalDate getDate(final String path)
    {
        LocalDate result;

        result = StatoolInfosUtils.parseDate(get(path));

        //
        return result;
    }

    /**
     * Gets the file class.
     *
     * @return the file class
     */
    public PropertyClassType getClassType()
    {
        PropertyClassType result;

        result = PropertyClassType.of(get("file.class"));

        //
        return result;
    }

    /**
     * Gets the index property.
     *
     * @param path
     *            the path
     * @return the index property
     */
    public int getIndexProperty(final String path)
    {
        int result;

        boolean ended = false;
        int index = 0;
        result = -1;
        while (!ended)
        {
            if (index < size())
            {
                PathProperty current = this.get(index);

                if (StringUtils.equalsIgnoreCase(current.getPath(), path))
                {
                    ended = true;
                    result = index;
                }
                else
                {
                    index += 1;
                }
            }
            else
            {
                ended = true;
                result = -1;
            }
        }

        //
        return result;
    }

    /**
     * Gets the metric.
     *
     * @param path
     *            the path
     * @return the metric
     */
    public Metric getMetric(final String path)
    {
        Metric result;

        String metricName = StringUtils.defaultIfBlank(get(path + ".name"), RegExUtils.removeFirst(path, "^metrics\\."));
        String metricDescription = StringUtils.defaultIfBlank(get(path + ".description"), metricName);

        StringList years = getMetricYears(path).sort();

        if (years.isEmpty())
        {
            result = null;
        }
        else
        {
            result = new Metric(path, metricName, metricDescription, years.get(0));

            for (String year : years)
            {
                result.getYearValues().add(get(path + "." + year));
                result.getMonthValues().addAll(StatoolInfosUtils.splitMonthValues(get(path + "." + year + ".months")));
                result.getWeekValues().addAll(StatoolInfosUtils.splitWeekValues(get(path + "." + year + ".weeks")));
                result.getDayValues().addAll(StatoolInfosUtils.splitDayValues(get(path + "." + year + ".weeks")));
            }
        }

        //
        return result;
    }

    /**
     * Gets the metric month values.
     *
     * @param path
     *            the path
     * @return the metric month values
     */
    public MonthValues getMetricMonthValues(final String path)
    {
        MonthValues result;

        String metricName = StringUtils.defaultIfBlank(get(path + ".name"), RegExUtils.removeFirst(path, "^metrics\\."));
        String metricDescription = StringUtils.defaultIfBlank(get(path + ".description"), metricName);

        StringList years = getMetricYears(path).sort();

        result = new MonthValues(metricName, metricDescription);

        for (String year : years)
        {
            String line = get(path + "." + year + ".months");
            StringList values = StatoolInfosUtils.splitMonthValues(line);
            int monthIndex = 1;
            for (String value : values)
            {
                if (!StringUtils.isBlank(value))
                {
                    YearMonth timestamp = YearMonth.of(Integer.valueOf(year), monthIndex);

                    result.put(timestamp, Double.valueOf(value));
                }

                monthIndex += 1;
            }
        }

        //
        return result;
    }

    /**
     * Gets the metric prefixes.
     *
     * @return the metric prefixes
     */
    public StringList getMetricPrefixes()
    {
        StringList result;

        StringSet prefixes = new StringSet();

        Pattern pattern = Pattern.compile("^(?<target>metrics(\\.\\S+)+)\\.(name|description|\\d{4}).*$");

        for (String path : getPaths())
        {
            Matcher matcher = pattern.matcher(path);
            if (matcher.matches())
            {
                if (matcher.start("target") != -1)
                {
                    prefixes.add(matcher.group("target"));
                }
            }
        }

        result = new StringList(prefixes).sort();

        //
        return result;
    }

    /**
     * Gets the metric specific prefixes.
     *
     * @return the metric specific prefixes
     */
    public StringList getMetricSpecificPrefixes()
    {
        StringList result;

        result = new StringList();
        for (String metricPath : getMetricPrefixes())
        {
            {
                if ((!StringUtils.startsWithAny(metricPath, "metrics.http.", "metrics.service.", "mertics.moderation.")))
                {
                    result.add(metricPath);
                }
            }
        }

        //
        return result;
    }

    /**
     * Gets the metric.
     *
     * @param path
     *            the path
     * @return the metric
     */
    public WeekValues getMetricWeekValues(final String path)
    {
        WeekValues result;

        String metricName = StringUtils.defaultIfBlank(get(path + ".name"), RegExUtils.removeFirst(path, "^metrics\\."));
        String metricDescription = StringUtils.defaultIfBlank(get(path + ".description"), metricName);

        StringList years = getMetricYears(path).sort();

        result = new WeekValues(metricName, metricDescription);

        for (String year : years)
        {
            String line = get(path + "." + year + ".weeks");
            StringList values = StatoolInfosUtils.splitWeekValues(line);
            int weekIndex = 1;
            for (String value : values)
            {
                if (!StringUtils.isBlank(value))
                {
                    YearWeek timestamp = YearWeek.of(Integer.valueOf(year), weekIndex);

                    result.put(timestamp, Double.valueOf(value));
                }

                weekIndex += 1;
            }
        }

        //
        return result;
    }

    /**
     * Gets the metric years.
     *
     * @param path
     *            the path
     * @return the metric years
     */
    public StringList getMetricYears(final String path)
    {
        StringList result;

        StringSet years = new StringSet();

        Pattern pattern = Pattern.compile("^" + path + "\\.(?<year>\\d{4}).*$");

        for (PathProperty property : getByPrefix(path))
        {
            String subPath = property.getPath();
            Matcher matcher = pattern.matcher(subPath);
            if (matcher.matches())
            {
                if (matcher.start("year") != -1)
                {
                    years.add(matcher.group("year"));
                }
            }
        }

        result = new StringList(years);

        //
        return result;
    }

    /**
     * Gets the metric year values.
     *
     * @param path
     *            the path
     * @return the metric year values
     */
    public YearValues getMetricYearValues(final String path)
    {
        YearValues result;

        String metricName = StringUtils.defaultIfBlank(get(path + ".name"), RegExUtils.removeFirst(path, "^metrics\\."));
        String metricDescription = StringUtils.defaultIfBlank(get(path + ".description"), metricName);

        StringList years = getMetricYears(path).sort();

        result = new YearValues(metricName, metricDescription);

        if (!years.isEmpty())
        {
            Year start = Year.of(Integer.parseInt(years.getFirst()));
            Year end = Year.of(Integer.parseInt(years.getLast()));

            for (Year timestamp = start; !timestamp.isAfter(end); timestamp = timestamp.plusYears(1))
            {
                String value = get(path + "." + timestamp.getValue());

                if (StringUtils.isNumeric(value))
                {
                    result.put(timestamp, Double.valueOf(value));
                }
                else
                {
                    result.put(timestamp, 0.0);
                }
            }
        }

        //
        return result;
    }

    /**
     * Gets the keys.
     *
     * @return the keys
     */
    @Override
    public StringList getPaths()
    {
        StringList result;

        result = new StringList();

        for (PathProperty property : this)
        {
            result.add(property.getPath());
        }

        //
        return result;
    }

    /**
     * Gets the prefixes.
     *
     * @return the prefixes
     */
    @Override
    public StringSet getPrefixes()
    {
        StringSet result;

        result = new StringSet();

        for (PathProperty property : this)
        {
            result.add(property.getSubPrefix());
        }

        //
        return result;
    }

    /**
     * Gets the prefix list.
     *
     * @return the prefix list
     */
    @Override
    public StringList getPrefixList()
    {
        StringList result;

        result = new StringList();

        for (PathProperty property : this)
        {
            String prefix = getPrefix(property.getPath());

            if (!result.contains(prefix))
            {
                result.add(prefix);
            }
        }

        //
        return result;
    }

    /**
     * Gets the property.
     *
     * @param path
     *            the path
     * @return the property
     */
    @Override
    public PathProperty getProperty(final String path)
    {
        PathProperty result;

        boolean ended = false;
        Iterator<PathProperty> iterator = this.iterator();
        result = null;
        while (!ended)
        {
            if (iterator.hasNext())
            {
                PathProperty current = iterator.next();

                if (StringUtils.equalsIgnoreCase(current.getPath(), path))
                {
                    ended = true;
                    result = current;
                }
            }
            else
            {
                ended = true;
                result = null;
            }
        }

        //
        return result;
    }

    /**
     * Gets the sub prefixes.
     *
     * @return the sub prefixes
     */
    @Override
    public StringSet getSubPrefixes()
    {
        StringSet result;

        result = new StringSet();

        for (PathProperty property : this)
        {
            result.add(property.getSubPrefix());
        }

        //
        return result;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.statoolinfos.properties.PathProperties#getURL(java.lang.String)
     */
    @Override
    public URL getURL(final String path)
    {
        URL result;

        try
        {
            String value = get(path);
            if ((path == null) || (!StringUtils.startsWith(value, "http")))
            {
                result = null;
            }
            else
            {
                result = new URL(value.trim());
            }
        }
        catch (MalformedURLException exception)
        {
            logger.error("Error getURL with [" + path + "]");
            exception.printStackTrace();
            result = null;
        }

        //
        return result;
    }

    /**
     * Checks for filled metrics.
     *
     * @return true, if successful
     */
    public boolean hasFilledValue()
    {
        boolean result;

        boolean ended = false;
        Iterator<PathProperty> iterator = this.iterator();
        result = false;
        while (!ended)
        {
            if (iterator.hasNext())
            {
                PathProperty property = iterator.next();

                if (StringUtils.isNotBlank(property.getValue()))
                {
                    ended = true;
                    result = true;
                }
            }
            else
            {
                ended = true;
                result = false;
            }
        }

        //
        return result;
    }

    @Override
    public void put(final String key, final long value)
    {
        put(key, "" + value);
    }

    /**
     * Put.
     *
     * @param path
     *            the path
     * @param value
     *            the value
     */
    @Override
    public void put(final String path, final String value)
    {
        if (StringUtils.isNotBlank(path))
        {
            PathProperty property = getProperty(path);
            if (property == null)
            {
                add(new PathProperty(path, value));
            }
            else
            {
                property.setValue(property.getValue());
            }
        }
    }

    /**
     * Delete path.
     *
     * @param path
     *            the path
     */
    @Override
    public void removePath(final String path)
    {
        boolean ended = false;
        Iterator<PathProperty> iterator = this.iterator();
        while (!ended)
        {
            if (iterator.hasNext())
            {
                PathProperty current = iterator.next();

                if (StringUtils.equalsIgnoreCase(current.getPath(), path))
                {
                    ended = true;
                    iterator.remove();
                }
            }
        }
    }

    /**
     * Removes the section.
     *
     * @param prefix
     *            the prefix
     */
    @Override
    public void removeSection(final String prefix)
    {
        if (StringUtils.isNotBlank(prefix))
        {
            String pattern;
            if (prefix.endsWith("."))
            {
                pattern = prefix;
            }
            else
            {
                pattern = prefix + ".";
            }

            boolean ended = false;
            Iterator<PathProperty> iterator = iterator();
            while (!ended)
            {
                if (iterator.hasNext())
                {
                    PathProperty current = iterator.next();
                    if (StringUtils.startsWith(current.getPath(), pattern))
                    {
                        iterator.remove();
                    }
                }
                else
                {
                    ended = true;
                }
            }
        }
    }

    /**
     * Shrink to leaf.
     *
     * @return the path property list
     */
    public PathPropertyList shrinkToLeaf()
    {
        PathPropertyList result;

        for (PathProperty property : this)
        {
            property.setPath(property.getLeaf());
        }

        result = this;

        //
        return result;
    }

    /**
     * Sort.
     *
     * @param sorting
     *            the sorting
     * @return the path property list
     */
    public PathPropertyList sort(final PathPropertyComparator.Sorting sorting)
    {
        PathPropertyList result;

        sort(new PathPropertyComparator(sorting));

        result = this;

        //
        return result;
    }

    /**
     * Sort by path.
     *
     * @return the path property list
     */
    public PathPropertyList sortByPath()
    {
        PathPropertyList result;

        result = sort(PathPropertyComparator.Sorting.PATH);

        //
        return result;
    }

    /**
     * Sort by value.
     *
     * @return the path property list
     */
    public PathPropertyList sortByValue()
    {
        PathPropertyList result;

        result = sort(PathPropertyComparator.Sorting.VALUE);

        //
        return result;
    }

    /**
     * To string list.
     *
     * @return the string list
     */
    @Override
    public StringList toStringList()
    {
        StringList result;

        result = new StringList();

        for (String path : getPaths())
        {
            String value = get(path);
            result.add(path + "=" + value);
        }

        //
        return result;
    }

    /**
     * To string list formatted.
     *
     * @return the string list
     */
    @Override
    public StringList toStringListFormatted()
    {
        StringList result;

        result = new StringList();

        StringList prefixes = getPrefixList();

        // sort prefixes.
        for (String prefix : prefixes)
        {
            result.add("# [" + StringUtils.capitalize(prefix) + "]");

            PathProperties section = getByPrefix(prefix);
            result.addAll(section.toStringList());

            result.add("");
        }

        //
        return result;
    }

    /**
     * Gets the prefix.
     *
     * @param source
     *            the source
     * @return the prefix
     */
    private static String getPrefix(final String source)
    {
        String result;

        result = source.substring(0, source.indexOf("."));

        //
        return result;
    }
}
