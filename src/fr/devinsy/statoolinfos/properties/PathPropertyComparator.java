/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.properties;

import java.util.Comparator;

import fr.devinsy.statoolinfos.util.CompareUtils;

/**
 * The Class PathPropertyComparator.
 */
public class PathPropertyComparator implements Comparator<PathProperty>
{
    public enum Sorting
    {
        PATH,
        VALUE
    }

    private Sorting sorting;

    /**
     * Instantiates a new path property comparator.
     *
     * @param sorting
     *            the sorting
     */
    public PathPropertyComparator(final Sorting sorting)
    {
        //
        this.sorting = sorting;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    @Override
    public int compare(final PathProperty alpha, final PathProperty bravo)
    {
        int result;

        result = compare(alpha, bravo, this.sorting);

        //
        return result;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @param sorting
     *            the sorting
     * @return the int
     */
    public static int compare(final PathProperty alpha, final PathProperty bravo, final Sorting sorting)
    {
        int result;

        if (sorting == null)
        {
            result = 0;
        }
        else
        {
            switch (sorting)
            {
                default:
                case PATH:
                    result = CompareUtils.compare(getPath(alpha), getPath(bravo));
                break;

                case VALUE:
                    result = CompareUtils.compare(getValue(alpha), getValue(bravo));
                break;
            }
        }

        //
        return result;
    }

    /**
     * Gets the path.
     *
     * @param source
     *            the source
     * @return the path
     */
    public static String getPath(final PathProperty source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.getPath();
        }

        //
        return result;
    }

    /**
     * Gets the value.
     *
     * @param source
     *            the source
     * @return the value
     */
    public static String getValue(final PathProperty source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.getValue();
        }

        //
        return result;
    }
}
