/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.properties;

import java.time.YearMonth;
import java.util.HashMap;

import fr.devinsy.strings.StringList;

/**
 * The Class MonthValues.
 */
public class MonthValues extends HashMap<YearMonth, Double>
{
    private static final long serialVersionUID = 1868850913480770744L;

    private String label;
    private String description;

    /**
     * Instantiates a new month values.
     */
    public MonthValues()
    {
        this(null, null);
    }

    /**
     * Instantiates a new month values.
     *
     * @param label
     *            the label
     * @param description
     *            the description
     */
    public MonthValues(final String label, final String description)
    {
        super();
        this.label = label;
        this.description = description;
    }

    /**
     * Adds the.
     *
     * @param timestamp
     *            the timestamp
     * @param value
     *            the value
     */
    public void add(final YearMonth timestamp, final double value)
    {
        put(timestamp, getValue(timestamp) + value);
    }

    /**
     * Adds the all.
     *
     * @param source
     *            the source
     */
    public void addAll(final MonthValues source)
    {
        if (source != null)
        {
            for (YearMonth timestamp : source.keySet())
            {
                add(timestamp, source.getValue(timestamp));
            }
        }
    }

    /**
     * Dec.
     *
     * @param timestamp
     *            the timestamp
     */
    public void dec(final YearMonth timestamp)
    {
        put(timestamp, getValue(timestamp) - 1);
    }

    /**
     * Extract.
     *
     * @param start
     *            the start
     * @param end
     *            the end
     * @return the month value map
     */
    public MonthValues extract(final YearMonth start, final YearMonth end)
    {
        MonthValues result;

        YearMonth startTarget = normalizeStart(start);
        YearMonth endTarget = normalizeEnd(end);

        result = new MonthValues();
        for (YearMonth timestamp : this.keySet())
        {
            if ((!timestamp.isBefore(startTarget)) &&
                    (!timestamp.isAfter(endTarget)))
            {
                result.put(timestamp, get(timestamp));
            }
        }

        //
        return result;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        return this.description;
    }

    /**
     * Gets the label.
     *
     * @return the label
     */
    public String getLabel()
    {
        return this.label;
    }

    /**
     * Gets the normalized timestamps.
     *
     * @param start
     *            the start
     * @param end
     *            the end
     * @return the normalized timestamps
     */
    public StringList getNormalizedTimestamps(final YearMonth start, final YearMonth end)
    {
        StringList result;

        YearMonth startTarget = normalizeStart(start);
        YearMonth endTarget = normalizeEnd(end);

        result = new StringList();
        for (YearMonth timestamp = startTarget; endTarget.isAfter(timestamp); timestamp = timestamp.plusMonths(1))
        {
            result.append(timestamp);
        }

        //
        return result;
    }

    /**
     * Gets the oldest timestamp.
     *
     * @return the oldest timestamp
     */
    public YearMonth getOldestTimestamp()
    {
        YearMonth result;

        result = null;

        for (YearMonth timestamp : this.keySet())
        {
            if ((result == null) || (timestamp.isBefore(result)))
            {
                result = timestamp;
            }
        }

        //
        return result;
    }

    /**
     * Gets the value.
     *
     * @param timestamp
     *            the timestamp
     * @return the value
     */
    public double getValue(final YearMonth timestamp)
    {
        double result;

        Double value = get(timestamp);
        if (value == null)
        {
            result = 0.0;
        }
        else
        {
            result = value;
        }

        //
        return result;
    }

    /**
     * Inc.
     *
     * @param timestamp
     *            the timestamp
     */
    public void inc(final YearMonth timestamp)
    {
        put(timestamp, getValue(timestamp) + 1);
    }

    /**
     * Normalize end.
     *
     * @param end
     *            the end
     * @return the year month
     */
    public YearMonth normalizeEnd(final YearMonth end)
    {
        YearMonth result;

        if (end == null)
        {
            result = YearMonth.now();
        }
        else
        {
            result = end;
        }

        //
        return result;
    }

    /**
     * Normalize start.
     *
     * @param start
     *            the start
     * @return the year month
     */
    public YearMonth normalizeStart(final YearMonth start)
    {
        YearMonth result;

        if (start == null)
        {
            result = getOldestTimestamp();
        }
        else
        {
            result = start;
        }

        //
        return result;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the new description
     */
    public void setDescription(final String description)
    {
        this.description = description;
    }

    /**
     * Sets the label.
     *
     * @param label
     *            the new label
     */
    public void setLabel(final String label)
    {
        this.label = label;
    }

    /**
     * Sum.
     *
     * @return the double
     */
    public double sum()
    {
        double result;

        result = 0;
        for (Double value : this.values())
        {
            result += value;
        }

        //
        return result;
    }

    /**
     * Gets the normalized values.
     *
     * @param start
     *            the start
     * @param end
     *            the end
     * @return the normalized values
     */
    public StringList toNormalizedValues(final YearMonth start, final YearMonth end)
    {
        StringList result;

        YearMonth startTarget = normalizeStart(start);
        YearMonth endTarget = normalizeEnd(end);

        result = new StringList();
        for (YearMonth timestamp = startTarget; endTarget.isAfter(timestamp); timestamp = timestamp.plusMonths(1))
        {
            result.append(getValue(timestamp));
        }

        //
        return result;
    }
}
