/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.properties;

import java.util.HashMap;

import org.threeten.extra.YearWeek;

import fr.devinsy.strings.StringList;

/**
 * The Class WeekValues.
 */
public class WeekValues extends HashMap<YearWeek, Double>
{
    private static final long serialVersionUID = -211990850389225574L;

    private String label;
    private String description;

    /**
     * Instantiates a new week values.
     */
    public WeekValues()
    {
        this(null, null);
    }

    /**
     * Instantiates a new week values.
     *
     * @param label
     *            the label
     * @param description
     *            the description
     */
    public WeekValues(final String label, final String description)
    {
        super();
        this.label = label;
        this.description = description;
    }

    /**
     * Adds the.
     *
     * @param timestamp
     *            the timestamp
     * @param value
     *            the value
     */
    public void add(final YearWeek timestamp, final double value)
    {
        put(timestamp, getValue(timestamp) + value);
    }

    /**
     * Adds the all.
     *
     * @param source
     *            the source
     */
    public void addAll(final WeekValues source)
    {
        if (source != null)
        {
            for (YearWeek timestamp : source.keySet())
            {
                add(timestamp, source.getValue(timestamp));
            }
        }
    }

    /**
     * Dec.
     *
     * @param timestamp
     *            the timestamp
     */
    public void dec(final YearWeek timestamp)
    {
        put(timestamp, getValue(timestamp) - 1);
    }

    /**
     * Extract.
     *
     * @param start
     *            the start
     * @param end
     *            the end
     * @return the month value map
     */
    public WeekValues extract(final YearWeek start, final YearWeek end)
    {
        WeekValues result;

        YearWeek startTarget = normalizeStart(start);
        YearWeek endTarget = normalizeEnd(end);

        result = new WeekValues();
        for (YearWeek timestamp : this.keySet())
        {
            if ((!timestamp.isBefore(startTarget)) &&
                    (!timestamp.isAfter(endTarget)))
            {
                result.put(timestamp, get(timestamp));
            }
        }

        //
        return result;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        return this.description;
    }

    /**
     * Gets the label.
     *
     * @return the label
     */
    public String getLabel()
    {
        return this.label;
    }

    /**
     * Gets the normalized timestamps.
     *
     * @param start
     *            the start
     * @param end
     *            the end
     * @return the normalized timestamps
     */
    public StringList getNormalizedTimestamps(final YearWeek start, final YearWeek end)
    {
        StringList result;

        YearWeek startTarget = normalizeStart(start);
        YearWeek endTarget = normalizeEnd(end);

        result = new StringList();
        for (YearWeek timestamp = startTarget; endTarget.isAfter(timestamp); timestamp = timestamp.plusWeeks(1))
        {
            result.append(timestamp);
        }

        //
        return result;
    }

    /**
     * Gets the oldest timestamp.
     *
     * @return the oldest timestamp
     */
    public YearWeek getOldestTimestamp()
    {
        YearWeek result;

        result = null;

        for (YearWeek timestamp : this.keySet())
        {
            if ((result == null) || (timestamp.isBefore(result)))
            {
                result = timestamp;
            }
        }

        //
        return result;
    }

    /**
     * Gets the value.
     *
     * @param timestamp
     *            the timestamp
     * @return the value
     */
    public double getValue(final YearWeek timestamp)
    {
        double result;

        Double value = get(timestamp);
        if (value == null)
        {
            result = 0.0;
        }
        else
        {
            result = value;
        }

        //
        return result;
    }

    /**
     * Inc.
     *
     * @param timestamp
     *            the timestamp
     */
    public void inc(final YearWeek timestamp)
    {
        put(timestamp, getValue(timestamp) + 1);
    }

    /**
     * Normalize end.
     *
     * @param end
     *            the end
     * @return the year month
     */
    public YearWeek normalizeEnd(final YearWeek end)
    {
        YearWeek result;

        if (end == null)
        {
            result = YearWeek.now();
        }
        else
        {
            result = end;
        }

        //
        return result;
    }

    /**
     * Normalize start.
     *
     * @param start
     *            the start
     * @return the year month
     */
    public YearWeek normalizeStart(final YearWeek start)
    {
        YearWeek result;

        if (start == null)
        {
            result = getOldestTimestamp();
        }
        else
        {
            result = start;
        }

        //
        return result;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the new description
     */
    public void setDescription(final String description)
    {
        this.description = description;
    }

    /**
     * Sets the label.
     *
     * @param label
     *            the new label
     */
    public void setLabel(final String label)
    {
        this.label = label;
    }

    /**
     * Gets the normalized values.
     *
     * @param start
     *            the start
     * @param end
     *            the end
     * @return the normalized values
     */
    public StringList toNormalizedValues(final YearWeek start, final YearWeek end)
    {
        StringList result;

        YearWeek startTarget = normalizeStart(start);
        YearWeek endTarget = normalizeEnd(end);

        result = new StringList();
        for (YearWeek timestamp = startTarget; endTarget.isAfter(timestamp); timestamp = timestamp.plusWeeks(1))
        {
            result.append(getValue(timestamp));
        }

        //
        return result;
    }
}
