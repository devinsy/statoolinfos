/*
 * Copyright (C) 2020 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.build;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.statoolinfos.core.Configuration;
import fr.devinsy.statoolinfos.core.Factory;
import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.properties.PathProperties;
import fr.devinsy.statoolinfos.properties.PathPropertyList;
import fr.devinsy.statoolinfos.properties.PathPropertyUtils;

/**
 * The Class StatoolInfos.
 */
public class Builder
{
    private static Logger logger = LoggerFactory.getLogger(Builder.class);

    /**
     * Builds the.
     *
     * @param configurationFile
     *            the input
     * @throws StatoolInfosException
     *             the statool infos exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void build(final File configurationFile) throws StatoolInfosException, IOException
    {
        logger.info("Build {}", configurationFile.getAbsolutePath());

        Configuration configuration = Factory.loadConfiguration(configurationFile);
        logger.info("Build input setting: {}", configuration.getBuildInput());
        logger.info("Build directory setting: {}", configuration.getBuildDirectoryPath());

        File inputFile = configuration.getBuildInput();
        File buildDirectory = configuration.getBuildDirectory();
        if (inputFile == null)
        {
            throw new StatoolInfosException("Input is undefined.");
        }
        else if (!inputFile.exists())
        {
            throw new StatoolInfosException("Input does not exist.");
        }
        else if (!inputFile.isFile())
        {
            throw new StatoolInfosException("Input is not a file.");
        }
        else if (buildDirectory == null)
        {
            throw new StatoolInfosException("Build directory is undefined.");
        }
        else if (!buildDirectory.exists())
        {
            throw new StatoolInfosException("Build directory does not exist.");
        }
        else if (!buildDirectory.isDirectory())
        {
            throw new StatoolInfosException("Build directory is not a directory.");
        }
        else
        {
            // Build file section.
            PathProperties target = new PathPropertyList();
            target.put("file.class", configuration.get("conf.class"));
            target.put("file.generator", "StatoolInfos");
            target.put("file.datetime", LocalDateTime.now().toString());
            target.put("file.protocol", configuration.get("conf.protocol"));

            // Load input properties.
            PathProperties input = PathPropertyUtils.load(inputFile);

            // Add input properties with file section ones.
            target.add(input);

            // Save the build properties.
            File targetFile = new File(buildDirectory, configuration.getBuildInput().getName());
            PathPropertyUtils.save(targetFile, target);
        }
    }

    /**
     * Clear.
     *
     * @param configuration
     *            the configuration
     * @throws StatoolInfosException
     *             the statool infos exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void clear(final Configuration configuration) throws StatoolInfosException, IOException
    {
        logger.info("Build directory setting: {}", configuration.getBuildDirectoryPath());

        String path = configuration.getBuildDirectoryPath();
        if (StringUtils.isBlank(path))
        {
            logger.warn("Undefined build directory.");
        }
        else if (!new File(path).exists())
        {
            logger.warn("Build directory does not exist: {}.", path);
        }
        else
        {
            File file = new File(configuration.getBuildDirectory(), configuration.getBuildInput().getName());

            if ((file.isFile()) && (StringUtils.endsWith(file.getName(), ".properties")))
            {
                logger.info("BUILDER : Deleting {}", file.getAbsolutePath());
                file.delete();
            }
        }
    }
}
