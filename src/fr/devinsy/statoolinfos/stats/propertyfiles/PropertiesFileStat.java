/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.propertyfiles;

import java.net.URL;
import java.time.LocalDateTime;

import fr.devinsy.statoolinfos.core.Organization;
import fr.devinsy.statoolinfos.core.Service;

/**
 * The Class PropertiesFileStat.
 */
public class PropertiesFileStat
{
    private String urlName;
    private URL url;
    private Organization organization;
    private Service service;
    private int lineCount;
    private int activeLineCount;
    private int blankPropertyCount;
    private int filledPropertyCount;
    private int warningCount;
    private int alertCount;
    private int errorCount;
    private int voidCount;
    private LocalDateTime updateDate;

    /**
     * Instantiates a new properties file stat.
     */
    public PropertiesFileStat()
    {
    }

    /**
     * Gets the active line count.
     *
     * @return the active line count
     */
    public int getActiveLineCount()
    {
        return this.activeLineCount;
    }

    /**
     * Gets the alert count.
     *
     * @return the alert count
     */
    public int getAlertCount()
    {
        return this.alertCount;
    }

    /**
     * Gets the blank property count.
     *
     * @return the blank property count
     */
    public int getBlankPropertyCount()
    {
        return this.blankPropertyCount;
    }

    /**
     * Gets the error count.
     *
     * @return the error count
     */
    public int getErrorCount()
    {
        return this.errorCount;
    }

    /**
     * Gets the filled property count.
     *
     * @return the filled property count
     */
    public int getFilledPropertyCount()
    {
        return this.filledPropertyCount;
    }

    /**
     * Gets the line count.
     *
     * @return the line count
     */
    public int getLineCount()
    {
        return this.lineCount;
    }

    /**
     * Gets the organization.
     *
     * @return the organization
     */
    public Organization getOrganization()
    {
        return this.organization;
    }

    /**
     * Gets the property count.
     *
     * @return the property count
     */
    public int getPropertyCount()
    {
        return this.activeLineCount;
    }

    /**
     * Gets the service.
     *
     * @return the service
     */
    public Service getService()
    {
        return this.service;
    }

    /**
     * Gets the update date.
     *
     * @return the update date
     */
    public LocalDateTime getUpdateDate()
    {
        return this.updateDate;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public URL getURL()
    {
        return this.url;
    }

    /**
     * Gets the URL name.
     *
     * @return the URL name
     */
    public String getURLName()
    {
        return this.urlName;
    }

    /**
     * Gets the void count.
     *
     * @return the void count
     */
    public int getVoidCount()
    {
        return this.voidCount;
    }

    /**
     * Gets the warning count.
     *
     * @return the warning count
     */
    public int getWarningCount()
    {
        return this.warningCount;
    }

    /**
     * Inc active line count.
     */
    public void incActiveLineCount()
    {
        this.activeLineCount += 1;
    }

    /**
     * Inc alert count.
     */
    public void incAlertCount()
    {
        this.alertCount += 1;
    }

    /**
     * Inc blank property count.
     */
    public void incBlankPropertyCount()
    {
        this.blankPropertyCount += 1;
    }

    /**
     * Inc error count.
     */
    public void incErrorCount()
    {
        this.errorCount += 1;
    }

    /**
     * Inc filled property count.
     */
    public void incFilledPropertyCount()
    {
        this.filledPropertyCount += 1;
    }

    /**
     * Inc line count.
     */
    public void incLineCount()
    {
        this.lineCount += 1;
    }

    /**
     * Inc void count.
     */
    public void incVoidCount()
    {
        this.voidCount += 1;
    }

    /**
     * Inc warning count.
     */
    public void incWarningCount()
    {
        this.warningCount += 1;
    }

    /**
     * Sets the active line count.
     *
     * @param activeLineCount
     *            the new active line count
     */
    public void setActiveLineCount(final int activeLineCount)
    {
        this.activeLineCount = activeLineCount;
    }

    /**
     * Sets the alert count.
     *
     * @param alertCount
     *            the new alert count
     */
    public void setAlertCount(final int alertCount)
    {
        this.alertCount = alertCount;
    }

    /**
     * Sets the blank property count.
     *
     * @param blankPropertyCount
     *            the new blank property count
     */
    public void setBlankPropertyCount(final int blankPropertyCount)
    {
        this.blankPropertyCount = blankPropertyCount;
    }

    /**
     * Sets the error count.
     *
     * @param errorCount
     *            the new error count
     */
    public void setErrorCount(final int errorCount)
    {
        this.errorCount = errorCount;
    }

    /**
     * Sets the filled property count.
     *
     * @param filledPropertyCount
     *            the new filled property count
     */
    public void setFilledPropertyCount(final int filledPropertyCount)
    {
        this.filledPropertyCount = filledPropertyCount;
    }

    /**
     * Sets the line count.
     *
     * @param lineCount
     *            the new line count
     */
    public void setLineCount(final int lineCount)
    {
        this.lineCount = lineCount;
    }

    /**
     * Sets the organization.
     *
     * @param organization
     *            the new organization
     */
    public void setOrganization(final Organization organization)
    {
        this.organization = organization;
    }

    /**
     * Sets the service.
     *
     * @param service
     *            the new service
     */
    public void setService(final Service service)
    {
        this.service = service;
    }

    /**
     * Sets the update date.
     *
     * @param updateDate
     *            the new update date
     */
    public void setUpdateDate(final LocalDateTime updateDate)
    {
        this.updateDate = updateDate;
    }

    /**
     * Sets the url.
     *
     * @param url
     *            the new url
     */
    public void setURL(final URL url)
    {
        this.url = url;
    }

    /**
     * Sets the URL name.
     *
     * @param localName
     *            the new URL name
     */
    public void setURLName(final String localName)
    {
        this.urlName = localName;
    }

    /**
     * Sets the void count.
     *
     * @param voidCount
     *            the new void count
     */
    public void setVoidCount(final int voidCount)
    {
        this.voidCount = voidCount;
    }

    /**
     * Sets the warning count.
     *
     * @param warningCount
     *            the new warning count
     */
    public void setWarningCount(final int warningCount)
    {
        this.warningCount = warningCount;
    }
}
