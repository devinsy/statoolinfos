/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.propertyfiles;

import java.util.Comparator;

import fr.devinsy.statoolinfos.util.CompareUtils;

/**
 * The Class PropertyStatComparator.
 */
public class PropertiesFileStatComparator implements Comparator<PropertiesFileStat>
{
    public enum Sorting
    {
        LOCAL_NAME,
        ORGANIZATION,
        LINE_COUNT,
        ACTIVE_LINE_COUNT,
        BLANK_PROPERTY_COUNT,
        FILLED_PROPERTY_COUNT,
        ERROR_COUNT
    }

    private Sorting sorting;

    /**
     * Instantiates a new project comparator.
     *
     * @param sorting
     *            the sorting
     */
    public PropertiesFileStatComparator(final Sorting sorting)
    {
        //
        this.sorting = sorting;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    @Override
    public int compare(final PropertiesFileStat alpha, final PropertiesFileStat bravo)
    {
        int result;

        result = compare(alpha, bravo, this.sorting);

        //
        return result;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @param sorting
     *            the sorting
     * @return the int
     */
    public static int compare(final PropertiesFileStat alpha, final PropertiesFileStat bravo, final Sorting sorting)
    {
        int result;

        if (sorting == null)
        {
            result = 0;
        }
        else
        {
            switch (sorting)
            {
                default:
                case LOCAL_NAME:
                    result = CompareUtils.compare(getLocalName(alpha), getLocalName(bravo));
                break;

                case ORGANIZATION:
                    result = CompareUtils.compare(getOrganizationName(alpha), getOrganizationName(bravo));
                break;

                case LINE_COUNT:
                    result = CompareUtils.compare(getLineCount(alpha), getLineCount(bravo));
                break;

                case ACTIVE_LINE_COUNT:
                    result = CompareUtils.compare(getActiveCount(alpha), getActiveCount(bravo));
                break;

                case FILLED_PROPERTY_COUNT:
                    result = CompareUtils.compare(getFilledCount(alpha), getFilledCount(bravo));
                break;

                case BLANK_PROPERTY_COUNT:
                    result = CompareUtils.compare(getBlankCount(alpha), getBlankCount(bravo));
                break;

                case ERROR_COUNT:
                    result = CompareUtils.compare(getErrorCount(alpha), getErrorCount(bravo));
                break;
            }
        }

        //
        return result;
    }

    /**
     * Gets the active count.
     *
     * @param source
     *            the source
     * @return the active count
     */
    public static Long getActiveCount(final PropertiesFileStat source)
    {
        Long result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = (long) source.getActiveLineCount();
        }

        //
        return result;
    }

    /**
     * Gets the id.
     *
     * @param source
     *            the source
     * @return the id
     */
    public static Long getBlankCount(final PropertiesFileStat source)
    {
        Long result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = (long) source.getBlankPropertyCount();
        }

        //
        return result;
    }

    /**
     * Gets the error count.
     *
     * @param source
     *            the source
     * @return the error count
     */
    public static Long getErrorCount(final PropertiesFileStat source)
    {
        Long result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = (long) source.getErrorCount();
        }

        //
        return result;
    }

    /**
     * Gets the filled count.
     *
     * @param source
     *            the source
     * @return the filled count
     */
    public static Long getFilledCount(final PropertiesFileStat source)
    {
        Long result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = (long) source.getFilledPropertyCount();
        }

        //
        return result;
    }

    /**
     * Gets the line count.
     *
     * @param source
     *            the source
     * @return the line count
     */
    public static Long getLineCount(final PropertiesFileStat source)
    {
        Long result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = (long) source.getLineCount();
        }

        //
        return result;
    }

    /**
     * Gets the local name.
     *
     * @param source
     *            the source
     * @return the local name
     */
    public static String getLocalName(final PropertiesFileStat source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.getURLName();
        }

        //
        return result;
    }

    /**
     * Gets the organization name.
     *
     * @param source
     *            the source
     * @return the organization name
     */
    public static String getOrganizationName(final PropertiesFileStat source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.getOrganization().getName();
        }

        //
        return result;
    }
}
