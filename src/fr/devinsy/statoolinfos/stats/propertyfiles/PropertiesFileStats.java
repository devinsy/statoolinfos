/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.propertyfiles;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.statoolinfos.checker.PropertyChecker;
import fr.devinsy.statoolinfos.checker.PropertyChecks;
import fr.devinsy.statoolinfos.core.Metrics;
import fr.devinsy.statoolinfos.core.Organization;
import fr.devinsy.statoolinfos.core.Service;
import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringsUtils;

/**
 * The Class PropertiesFileStats.
 */
public class PropertiesFileStats extends ArrayList<PropertiesFileStat>
{
    private static final long serialVersionUID = 8828667177906801801L;

    /**
     * Instantiates a new properties file stats.
     */
    public PropertiesFileStats()
    {
        super();
    }

    /**
     * Gets the active line count.
     *
     * @return the active line count
     */
    public long getActiveLineCount()
    {
        long result;

        result = 0;
        for (PropertiesFileStat stat : this)
        {
            result += stat.getActiveLineCount();
        }

        //
        return result;
    }

    /**
     * Gets the blank property count.
     *
     * @return the blank property count
     */
    public long getBlankPropertyCount()
    {
        long result;

        result = 0;
        for (PropertiesFileStat stat : this)
        {
            result += stat.getBlankPropertyCount();
        }

        //
        return result;
    }

    /**
     * Gets the error count.
     *
     * @return the error count
     */
    public long getErrorCount()
    {
        long result;

        result = 0;
        for (PropertiesFileStat stat : this)
        {
            result += stat.getErrorCount();
        }

        //
        return result;
    }

    /**
     * Gets the filled property count.
     *
     * @return the filled property count
     */
    public long getFilledPropertyCount()
    {
        long result;

        result = 0;
        for (PropertiesFileStat stat : this)
        {
            result += stat.getFilledPropertyCount();
        }

        //
        return result;
    }

    /**
     * Gets the line count.
     *
     * @return the line count
     */
    public long getLineCount()
    {
        long result;

        result = 0;
        for (PropertiesFileStat stat : this)
        {
            result += stat.getLineCount();
        }

        //
        return result;
    }

    /**
     * Gets the property count.
     *
     * @return the property count
     */
    public long getPropertyCount()
    {
        long result;

        result = getActiveLineCount();

        //
        return result;
    }

    /**
     * Gets the void count.
     *
     * @return the void count
     */
    public long getVoidCount()
    {
        long result;

        result = 0;
        for (PropertiesFileStat stat : this)
        {
            result += stat.getVoidCount();
        }

        //
        return result;
    }

    /**
     * Gets the warning count.
     *
     * @return the warning count
     */
    public long getWarningCount()
    {
        long result;

        result = 0;
        for (PropertiesFileStat stat : this)
        {
            result += stat.getWarningCount();
        }

        //
        return result;
    }

    /**
     * Sort.
     *
     * @param sorting
     *            the sorting
     * @return the properties file stats
     */
    public PropertiesFileStats sort(final PropertiesFileStatComparator.Sorting sorting)
    {
        PropertiesFileStats result;

        sort(new PropertiesFileStatComparator(sorting));

        result = this;

        //
        return result;
    }

    /**
     * Sort by blank count.
     *
     * @return the property stat list
     */
    public PropertiesFileStats sortByName()
    {
        PropertiesFileStats result;

        result = sort(PropertiesFileStatComparator.Sorting.LOCAL_NAME);

        //
        return result;
    }

    /**
     * Stat.
     *
     * @param inputFile
     *            the input file
     * @return the properties file stat
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public PropertiesFileStat stat(final File inputFile) throws IOException
    {
        PropertiesFileStat result;

        result = new PropertiesFileStat();

        StringList lines = StringsUtils.load(inputFile);
        for (String line : lines)
        {
            result.incLineCount();
            if ((StringUtils.isNotBlank(line)) && (!line.trim().startsWith("#")))
            {
                if (line.matches("^.+=.+$"))
                {
                    result.incActiveLineCount();
                    result.incFilledPropertyCount();
                }
                else if (line.matches("^.+=.*$"))
                {
                    result.incActiveLineCount();
                    result.incBlankPropertyCount();
                }
                else
                {
                    result.incErrorCount();
                }
            }
        }

        //
        return result;
    }

    /**
     * Stat.
     *
     * @param metrics
     *            the metrics
     * @param organization
     *            the organization
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void stat(final Metrics metrics, final Organization organization) throws IOException
    {
        PropertiesFileStat stat = stat(metrics.getInputFile());

        stat.setURL(metrics.getInputURL());
        stat.setURLName(metrics.getLocalFileName());
        stat.setOrganization(organization);
        stat.setUpdateDate(metrics.getCrawledDate());

        PropertyChecker checker = new PropertyChecker();
        PropertyChecks checks = checker.checkMetrics(metrics.getInputFile());
        stat.setAlertCount(checks.getAlertCount());
        stat.setErrorCount(checks.getErrorCount());
        stat.setVoidCount(checks.getVoidCount());
        stat.setWarningCount(checks.getWarningCount());

        add(stat);
    }

    /**
     * Stat.
     *
     * @param organization
     *            the organization
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void stat(final Organization organization) throws IOException
    {
        PropertiesFileStat stat = stat(organization.getInputFile());

        stat.setURL(organization.getInputURL());
        stat.setURLName("/federation/organizations/" + organization.getTechnicalName() + "/" + organization.getTechnicalName() + ".properties");
        stat.setOrganization(organization);
        stat.setService(null);
        stat.setUpdateDate(organization.getCrawledDate());

        PropertyChecker checker = new PropertyChecker();
        PropertyChecks checks = checker.checkOrganization(organization.getInputFile());
        stat.setWarningCount(checks.getWarningCount());
        stat.setAlertCount(checks.getAlertCount());
        stat.setErrorCount(checks.getErrorCount());
        stat.setVoidCount(checks.getVoidCount());

        add(stat);
    }

    /**
     * Stat.
     *
     * @param service
     *            the service
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void stat(final Service service) throws IOException
    {
        PropertiesFileStat stat = stat(service.getInputFile());

        stat.setURL(service.getInputURL());
        stat.setURLName("/federation/organizations/" + service.getOrganization().getTechnicalName() + "/services/" + service.getTechnicalName() + "/" + service.getTechnicalName() + ".properties");
        stat.setOrganization(service.getOrganization());
        stat.setService(service);
        stat.setUpdateDate(service.getCrawledDate());

        PropertyChecker checker = new PropertyChecker();
        PropertyChecks checks = checker.checkService(service.getInputFile());
        stat.setAlertCount(checks.getAlertCount());
        stat.setErrorCount(checks.getErrorCount());
        stat.setVoidCount(checks.getVoidCount());
        stat.setWarningCount(checks.getWarningCount());

        add(stat);
    }
}
