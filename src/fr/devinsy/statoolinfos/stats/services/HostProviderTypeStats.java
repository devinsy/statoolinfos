/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.services;

import fr.devinsy.statoolinfos.core.Service.HostProviderType;

/**
 * The Class HostProviderTypeStats.
 */
public class HostProviderTypeStats
{
    private long homeCount;
    private long hostedBayCount;
    private long hostedServerCount;
    private long outsourcedCount;
    private long unknownCount;

    /**
     * Instantiates a new host provider type stats.
     */
    public HostProviderTypeStats()
    {
        this.homeCount = 0;
        this.hostedBayCount = 0;
        this.hostedServerCount = 0;
        this.outsourcedCount = 0;
        this.unknownCount = 0;
    }

    /**
     * Gets the home count.
     *
     * @return the home count
     */
    public long getHomeCount()
    {
        return this.homeCount;
    }

    /**
     * Gets the hosted bay count.
     *
     * @return the hosted bay count
     */
    public long getHostedBayCount()
    {
        return this.hostedBayCount;
    }

    /**
     * Gets the hosted server count.
     *
     * @return the hosted server count
     */
    public long getHostedServerCount()
    {
        return this.hostedServerCount;
    }

    /**
     * Gets the outsourced count.
     *
     * @return the outsourced count
     */
    public long getOutsourcedCount()
    {
        return this.outsourcedCount;
    }

    /**
     * Gets the unknown count.
     *
     * @return the unknown count
     */
    public long getUnknownCount()
    {
        return this.unknownCount;
    }

    /**
     * Inc.
     *
     * @param type
     *            the type
     */
    public void inc(final HostProviderType type)
    {
        switch (type)
        {
            case HOME:
                this.homeCount += 1;
            break;
            case HOSTEDBAY:
                this.hostedBayCount += 1;
            break;
            case HOSTEDSERVER:
                this.hostedServerCount += 1;
            break;
            case OUTSOURCED:
                this.outsourcedCount += 1;
            break;
            default:
                this.unknownCount += 1;
        }
    }
}
