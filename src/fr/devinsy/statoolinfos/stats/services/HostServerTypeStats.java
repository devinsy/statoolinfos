/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.services;

import fr.devinsy.statoolinfos.core.Service.HostServerType;

/**
 * The Class HostServerTypeStats.
 */
public class HostServerTypeStats
{
    private long nanoCount;
    private long physicalCount;
    private long virtualCount;
    private long sharedCount;
    private long cloudCount;
    private long unknownCount;

    /**
     * Instantiates a new host server type stats.
     */
    public HostServerTypeStats()
    {
        this.nanoCount = 0;
        this.physicalCount = 0;
        this.virtualCount = 0;
        this.sharedCount = 0;
        this.cloudCount = 0;
        this.unknownCount = 0;
    }

    /**
     * Gets the cloud count.
     *
     * @return the cloud count
     */
    public long getCloudCount()
    {
        return this.cloudCount;
    }

    /**
     * Gets the nano count.
     *
     * @return the nano count
     */
    public long getNanoCount()
    {
        return this.nanoCount;
    }

    /**
     * Gets the physical count.
     *
     * @return the physical count
     */
    public long getPhysicalCount()
    {
        return this.physicalCount;
    }

    /**
     * Gets the shared count.
     *
     * @return the shared count
     */
    public long getSharedCount()
    {
        return this.sharedCount;
    }

    /**
     * Gets the unknown count.
     *
     * @return the unknown count
     */
    public long getUnknownCount()
    {
        return this.unknownCount;
    }

    /**
     * Gets the virtual count.
     *
     * @return the virtual count
     */
    public long getVirtualCount()
    {
        return this.virtualCount;
    }

    /**
     * Inc.
     *
     * @param type
     *            the type
     */
    public void inc(final HostServerType type)
    {
        switch (type)
        {
            case NANO:
                this.nanoCount += 1;
            break;
            case PHYSICAL:
                this.physicalCount += 1;
            break;
            case VIRTUAL:
                this.virtualCount += 1;
            break;
            case SHARED:
                this.sharedCount += 1;
            break;
            case CLOUD:
                this.cloudCount += 1;
            break;
            default:
                this.unknownCount += 1;
        }
    }
}
