/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.services;

import fr.devinsy.statoolinfos.core.Service.ServiceInstallType;

/**
 * The Class ServiceInstallTypeStats.
 */
public class ServiceInstallTypeStats
{
    private long distributionCount;
    private long providerCount;
    private long packageCount;
    private long toolingCount;
    private long clonerepoCount;
    private long archiveCount;
    private long sourcesCount;
    private long containerCount;
    private long unknownCount;

    /**
     * Instantiates a new host provider type stats.
     */
    public ServiceInstallTypeStats()
    {
        this.distributionCount = 0;
        this.providerCount = 0;
        this.packageCount = 0;
        this.toolingCount = 0;
        this.clonerepoCount = 0;
        this.archiveCount = 0;
        this.sourcesCount = 0;
        this.containerCount = 0;
        this.unknownCount = 0;
    }

    /**
     * Gets the archive count.
     *
     * @return the archive count
     */
    public long getArchiveCount()
    {
        return this.archiveCount;
    }

    /**
     * Gets the clonerepo count.
     *
     * @return the clonerepo count
     */
    public long getClonerepoCount()
    {
        return this.clonerepoCount;
    }

    /**
     * Gets the container count.
     *
     * @return the container count
     */
    public long getContainerCount()
    {
        return this.containerCount;
    }

    /**
     * Gets the distribution count.
     *
     * @return the distribution count
     */
    public long getDistributionCount()
    {
        return this.distributionCount;
    }

    /**
     * Gets the package count.
     *
     * @return the package count
     */
    public long getPackageCount()
    {
        return this.packageCount;
    }

    /**
     * Gets the provider count.
     *
     * @return the provider count
     */
    public long getProviderCount()
    {
        return this.providerCount;
    }

    /**
     * Gets the sources count.
     *
     * @return the sources count
     */
    public long getSourcesCount()
    {
        return this.sourcesCount;
    }

    /**
     * Gets the tooling count.
     *
     * @return the tooling count
     */
    public long getToolingCount()
    {
        return this.toolingCount;
    }

    /**
     * Gets the unknown count.
     *
     * @return the unknown count
     */
    public long getUnknownCount()
    {
        return this.unknownCount;
    }

    /**
     * Inc.
     *
     * @param type
     *            the type
     */
    public void inc(final ServiceInstallType type)
    {
        switch (type)
        {
            case DISTRIBUTION:
                this.distributionCount += 1;
            break;
            case PROVIDER:
                this.providerCount += 1;
            break;
            case PACKAGE:
                this.packageCount += 1;
            break;
            case TOOLING:
                this.toolingCount += 1;
            break;
            case CLONEREPO:
                this.clonerepoCount += 1;
            break;
            case ARCHIVE:
                this.archiveCount += 1;
            break;
            case SOURCES:
                this.sourcesCount += 1;
            break;
            case CONTAINER:
                this.containerCount += 1;
            break;
            default:
                this.unknownCount += 1;
        }
    }
}
