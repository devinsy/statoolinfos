/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.services;

import fr.devinsy.statoolinfos.core.Service.RegistrationType;

/**
 * The Class RegistrationStats.
 */
public class RegistrationStats
{
    private long noneCount;
    private long freeCount;
    private long memberCount;
    private long clientCount;
    private long unknownCount;

    /**
     * Instantiates a new host provider type stats.
     */
    public RegistrationStats()
    {
        this.noneCount = 0;
        this.freeCount = 0;
        this.memberCount = 0;
        this.clientCount = 0;
        this.unknownCount = 0;
    }

    /**
     * Gets the client count.
     *
     * @return the client count
     */
    public long getClientCount()
    {
        return this.clientCount;
    }

    /**
     * Gets the count.
     *
     * @return the count
     */
    public long getCount()
    {
        long result;

        result = this.noneCount + this.freeCount + this.memberCount + this.clientCount + this.unknownCount;

        //
        return result;
    }

    /**
     * Gets the free count.
     *
     * @return the free count
     */
    public long getFreeCount()
    {
        return this.freeCount;
    }

    /**
     * Gets the member count.
     *
     * @return the member count
     */
    public long getMemberCount()
    {
        return this.memberCount;
    }

    /**
     * Gets the none count.
     *
     * @return the none count
     */
    public long getNoneCount()
    {
        return this.noneCount;
    }

    /**
     * Gets the unknown count.
     *
     * @return the unknown count
     */
    public long getUnknownCount()
    {
        return this.unknownCount;
    }

    /**
     * Inc.
     *
     * @param type
     *            the type
     */
    public void inc(final RegistrationType type)
    {
        switch (type)
        {
            case NONE:
                this.noneCount += 1;
            break;
            case FREE:
                this.freeCount += 1;
            break;
            case MEMBER:
                this.memberCount += 1;
            break;
            case CLIENT:
                this.clientCount += 1;
            break;
            default:
                this.unknownCount += 1;
        }
    }
}
