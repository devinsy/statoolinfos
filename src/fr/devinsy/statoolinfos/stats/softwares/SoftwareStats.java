/*
 * Copyright (C) 2020 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.softwares;

import java.util.ArrayList;
import java.util.Collections;

/**
 * The Class SoftwareStats.
 */
public class SoftwareStats extends ArrayList<SoftwareStat>
{
    private static final long serialVersionUID = 3512825968551889090L;

    /**
     * Instantiates a new software stats.
     */
    public SoftwareStats()
    {
        super();
    }

    /**
     * Reverse.
     *
     * @return the software stats
     */
    public SoftwareStats reverse()
    {
        SoftwareStats result;

        Collections.reverse(this);

        result = this;

        //
        return result;
    }

    /**
     * Sort.
     *
     * @param sorting
     *            the sorting
     * @return the issues
     */
    public SoftwareStats sort(final SoftwareStatComparator.Sorting sorting)
    {
        SoftwareStats result;

        sort(new SoftwareStatComparator(sorting));

        result = this;

        //
        return result;
    }

    /**
     * Sort by name.
     *
     * @return the software stats
     */
    public SoftwareStats sortByName()
    {
        SoftwareStats result;

        result = sort(SoftwareStatComparator.Sorting.NAME);

        //
        return result;
    }

    /**
     * Sort by organization count.
     *
     * @return the software stats
     */
    public SoftwareStats sortByOrganizationCount()
    {
        SoftwareStats result;

        result = sort(SoftwareStatComparator.Sorting.ORGANIZATION_COUNT);

        //
        return result;
    }

    /**
     * Sort by service count.
     *
     * @return the category stats
     */
    public SoftwareStats sortByServiceCount()
    {
        SoftwareStats result;

        result = sort(SoftwareStatComparator.Sorting.SERVICE_COUNT);

        //
        return result;
    }

    /**
     * Sort by user count.
     *
     * @return the category stats
     */
    public SoftwareStats sortByUserCount()
    {
        SoftwareStats result;

        result = sort(SoftwareStatComparator.Sorting.USER_COUNT);

        //
        return result;
    }
}
