/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.ip;

import java.util.ArrayList;
import java.util.Collections;

/**
 * The Class IpStats.
 */
public final class IpStats extends ArrayList<IpStat>
{
    private static final long serialVersionUID = -2725108375443481335L;

    /**
     * Instantiates a new ip user agent stats.
     */
    public IpStats()
    {
        super();
    }

    /**
     * Instantiates a new ip stats.
     *
     * @param capacity
     *            the capacity
     */
    public IpStats(final int capacity)
    {
        super(capacity);
    }

    /**
     * Reverse.
     *
     * @return the ip stats
     */
    public IpStats reverse()
    {
        IpStats result;

        Collections.reverse(this);

        result = this;

        //
        return result;
    }

    /**
     * Sort.
     *
     * @param sorting
     *            the sorting
     * @return the ip stats
     */
    public IpStats sort(final IpStatComparator.Sorting sorting)
    {
        IpStats result;

        sort(new IpStatComparator(sorting));

        result = this;

        //
        return result;
    }

    /**
     * Sort count.
     *
     * @return the ip stats
     */
    public IpStats sortByCount()
    {
        IpStats result;

        result = sort(IpStatComparator.Sorting.COUNT);

        //
        return result;
    }

    /**
     * Sort by ip.
     *
     * @return the ip stats
     */
    public IpStats sortByIp()
    {
        IpStats result;

        result = sort(IpStatComparator.Sorting.IP);

        //
        return result;
    }
}