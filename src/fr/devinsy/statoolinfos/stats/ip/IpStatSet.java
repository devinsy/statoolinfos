/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.ip;

import java.util.HashMap;

/**
 * The Class IpStatSet.
 */
public final class IpStatSet extends HashMap<String, IpStat>
{
    private static final long serialVersionUID = -8411746796941831991L;

    /**
     * Instantiates a new ip stats.
     */
    public IpStatSet()
    {
        super();
    }

    /**
     * Put.
     *
     * @param ip
     *            the ip
     */
    public void put(final String ip)
    {
        IpStat stat = get(ip);
        if (stat == null)
        {
            stat = new IpStat(ip);
            this.put(ip, stat);
        }

        stat.inc();
    }

    /**
     * To list.
     *
     * @return the ip stats
     */
    public IpStats toList()
    {
        IpStats result;

        result = new IpStats(this.size());

        for (IpStat stat : this.values())
        {
            result.add(stat);
        }

        //
        return result;
    }
}