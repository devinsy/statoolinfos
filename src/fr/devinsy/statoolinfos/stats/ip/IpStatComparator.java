/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.ip;

import java.util.Comparator;

import fr.devinsy.statoolinfos.util.CompareUtils;

/**
 * The Class IpStatComparator.
 */
public class IpStatComparator implements Comparator<IpStat>
{
    public enum Sorting
    {
        IP,
        COUNT
    }

    private Sorting sorting;

    /**
     * Instantiates a new category stat comparator.
     *
     * @param sorting
     *            the sorting
     */
    public IpStatComparator(final Sorting sorting)
    {
        this.sorting = sorting;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    @Override
    public int compare(final IpStat alpha, final IpStat bravo)
    {
        int result;

        result = compare(alpha, bravo, this.sorting);

        //
        return result;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @param sorting
     *            the sorting
     * @return the int
     */
    public static int compare(final IpStat alpha, final IpStat bravo, final Sorting sorting)
    {
        int result;

        if (sorting == null)
        {
            result = 0;
        }
        else
        {
            switch (sorting)
            {
                default:
                case IP:
                    result = CompareUtils.compare(getIp(alpha), getIp(bravo));
                break;

                case COUNT:
                    result = CompareUtils.compare(getCount(alpha), getCount(bravo));
                break;
            }
        }

        //
        return result;
    }

    /**
     * Gets the user count.
     *
     * @param source
     *            the source
     * @return the user count
     */
    public static Long getCount(final IpStat source)
    {
        Long result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = (long) source.getCount();
        }

        //
        return result;
    }

    /**
     * Gets the name.
     *
     * @param source
     *            the source
     * @return the name
     */
    public static String getIp(final IpStat source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.getValue();
        }

        //
        return result;
    }
}
