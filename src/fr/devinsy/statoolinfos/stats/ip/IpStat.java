/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.ip;

/**
 * The Class IpStat.
 */
public final class IpStat
{
    private String value;
    private long count;

    /**
     * Instantiates a new ip stat.
     *
     * @param ip
     *            the ip
     */
    public IpStat(final String ip)
    {
        this.value = ip;
        this.count = 0;
    }

    /**
     * Gets the count.
     *
     * @return the count
     */
    public long getCount()
    {
        return this.count;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue()
    {
        return this.value;
    }

    /**
     * Inc.
     */
    public void inc()
    {
        this.count += 1;
    }
}