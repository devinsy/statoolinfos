/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.ip;

import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLog;

/**
 * The Class IpStator.
 */
public final class IpStator
{
    private long logCount;
    private IpStatSet ips;

    /**
     * Instantiates a new user agent stator.
     */
    public IpStator()
    {
        this.logCount = 0;
        this.ips = new IpStatSet();
    }

    /**
     * Gets the ips.
     *
     * @return the ips
     */
    public IpStats getIps()
    {
        IpStats result;

        result = this.ips.toList();

        //
        return result;
    }

    /**
     * Gets the log count.
     *
     * @return the log count
     */
    public long getLogCount()
    {
        return this.logCount;
    }

    /**
     * Adds the log.
     *
     * @param log
     *            the log
     */
    public void putLog(final HttpAccessLog log)
    {
        this.logCount += 1;
        this.ips.put(log.getIp());
    }
}