/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.useragent;

import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLog;
import fr.devinsy.statoolinfos.metrics.httpaccess.HttpStatusCategory;
import fr.devinsy.statoolinfos.stats.visitor.VisitorStat;
import fr.devinsy.statoolinfos.stats.visitor.VisitorStatSet;

/**
 * The Class UserAgentStat.
 */
public final class UserAgentStat
{
    private String userAgent;
    private long logCount;
    private IpSet ips;
    private VisitorStatSet visitors;

    /**
     * Instantiates a new user agent stat.
     *
     * @param userAgent
     *            the user agent
     */
    public UserAgentStat(final String userAgent)
    {
        this.userAgent = userAgent;
        this.logCount = 0;
        this.ips = new IpSet();
        this.visitors = new VisitorStatSet();
    }

    /**
     * Gets the ip count.
     *
     * @return the ip count
     */
    public long getIpCount()
    {
        return this.ips.size();
    }

    /**
     * Gets the log count.
     *
     * @return the log count
     */
    public long getLogCount()
    {
        return this.logCount;
    }

    /**
     * Gets the user agent.
     *
     * @return the user agent
     */
    public String getUserAgent()
    {
        return this.userAgent;
    }

    /**
     * Gets the visit count.
     *
     * @return the visit count
     */
    public long getVisitCount()
    {
        long result;

        result = this.visitors.getVisitCount();

        //
        return result;
    }

    /**
     * Inc log count.
     */
    public void incLogCount()
    {
        this.logCount += 1;
    }

    /**
     * Put log.
     *
     * @param log
     *            the log
     */
    public void putLog(final HttpAccessLog log)
    {
        incLogCount();
        this.ips.add(log.getIp());

        if (log.getStatus().getCategory() == HttpStatusCategory.SUCCESS)
        {
            VisitorStat stat = this.visitors.put(log.getIp(), log.getUserAgent().toString());
            stat.getVisits().add(log.getTime());
        }
    }
}