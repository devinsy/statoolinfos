/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.useragent;

import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLog;

/**
 * The Class UserAgentStator.
 */
public final class UserAgentStator
{
    private long logCount;
    private UserAgentStatSet userAgents;

    /**
     * Instantiates a new user agent stator.
     */
    public UserAgentStator()
    {
        this.logCount = 0;
        this.userAgents = new UserAgentStatSet();
    }

    /**
     * Gets the log count.
     *
     * @return the log count
     */
    public long getLogCount()
    {
        return this.logCount;
    }

    /**
     * Gets the user agent stats.
     *
     * @return the user agent stats
     */
    public UserAgentStats getUserAgentStats()
    {
        UserAgentStats result;

        result = this.userAgents.toList();

        //
        return result;
    }

    /**
     * Adds the log.
     *
     * @param log
     *            the log
     */
    public void putLog(final HttpAccessLog log)
    {
        this.logCount += 1;
        this.userAgents.put(log);
    }
}
