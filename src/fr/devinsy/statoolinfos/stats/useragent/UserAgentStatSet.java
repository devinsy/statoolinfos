/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.useragent;

import java.util.HashMap;

import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLog;

/**
 * The Class UserAgents.
 */
public final class UserAgentStatSet extends HashMap<String, UserAgentStat>
{
    private static final long serialVersionUID = -7943808966632477322L;

    /**
     * Instantiates a new user agents.
     */
    public UserAgentStatSet()
    {
        super();
    }

    /**
     * Put.
     *
     * @param log
     *            the log
     */
    public void put(final HttpAccessLog log)
    {
        String key = log.getUserAgent().toString().trim();

        UserAgentStat stat = get(key);
        if (stat == null)
        {
            stat = new UserAgentStat(log.getUserAgent().toString());
            this.put(key, stat);
        }

        stat.putLog(log);
    }

    /**
     * To list.
     *
     * @return the user agent stats
     */
    public UserAgentStats toList()
    {
        UserAgentStats result;

        result = new UserAgentStats(this.size());

        for (UserAgentStat stat : this.values())
        {
            result.add(stat);
        }

        //
        return result;
    }
}