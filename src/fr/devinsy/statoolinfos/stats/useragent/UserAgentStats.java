/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.useragent;

import java.util.ArrayList;
import java.util.Collections;

/**
 * The Class UserAgentStats.
 */
public final class UserAgentStats extends ArrayList<UserAgentStat>
{
    private static final long serialVersionUID = -6389473662022565639L;

    /**
     * Instantiates a new user agent stats.
     */
    public UserAgentStats()
    {
        super();
    }

    /**
     * Instantiates a new user agent stats.
     *
     * @param capacity
     *            the capacity
     */
    public UserAgentStats(final int capacity)
    {
        super(capacity);
    }

    /**
     * Reverse.
     *
     * @return the user agent stats
     */
    public UserAgentStats reverse()
    {
        UserAgentStats result;

        Collections.reverse(this);

        result = this;

        //
        return result;
    }

    /**
     * Sort.
     *
     * @param sorting
     *            the sorting
     * @return the ip stats
     */
    public UserAgentStats sort(final UserAgentStatComparator.Sorting sorting)
    {
        UserAgentStats result;

        sort(new UserAgentStatComparator(sorting));

        result = this;

        //
        return result;
    }

    /**
     * Sort by count.
     *
     * @return the user agent stats
     */
    public UserAgentStats sortByCount()
    {
        UserAgentStats result;

        result = sort(UserAgentStatComparator.Sorting.COUNT);

        //
        return result;
    }

    /**
     * Sort by ip.
     *
     * @return the user agent stats
     */
    public UserAgentStats sortByLabel()
    {
        UserAgentStats result;

        result = sort(UserAgentStatComparator.Sorting.USERAGENT);

        //
        return result;
    }
}