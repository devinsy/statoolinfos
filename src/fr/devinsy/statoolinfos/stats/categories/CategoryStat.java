/*
 * Copyright (C) 2020-2023 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.categories;

import fr.devinsy.statoolinfos.core.Category;

/**
 * The Class CategoryStat.
 */
public class CategoryStat
{
    private Category category;
    private int serviceCount;
    private int organizationCount;
    private int userCount;
    private int visitCount;

    /**
     * Instantiates a new category stat.
     *
     * @param category
     *            the category
     */
    public CategoryStat(final Category category)
    {
        this.category = category;
        this.serviceCount = 0;
        this.organizationCount = 0;
        this.userCount = 0;
        this.visitCount = 0;
    }

    /**
     * Gets the category.
     *
     * @return the category
     */
    public Category getCategory()
    {
        return this.category;
    }

    /**
     * Gets the organization count.
     *
     * @return the organization count
     */
    public int getOrganizationCount()
    {
        return this.organizationCount;
    }

    /**
     * Gets the service count.
     *
     * @return the service count
     */
    public int getServiceCount()
    {
        return this.serviceCount;
    }

    /**
     * Gets the user count.
     *
     * @return the user count
     */
    public int getUserCount()
    {
        return this.userCount;
    }

    /**
     * Gets the visit count.
     *
     * @return the visit count
     */
    public int getVisitCount()
    {
        return this.visitCount;
    }

    /**
     * Inc organization count.
     */
    public void incOrganizationCount()
    {
        this.organizationCount += 1;
    }

    /**
     * Inc service count.
     */
    public void incServiceCount()
    {
        this.serviceCount += 1;
    }

    /**
     * Inc user count.
     */
    public void incUserCount()
    {
        this.userCount += 1;
    }

    /**
     * Inc user count.
     *
     * @param value
     *            the value
     */
    public void incUserCount(final long value)
    {
        this.userCount += value;
    }

    /**
     * Inc visit count.
     */
    public void incVisitCount()
    {
        this.visitCount += 1;
    }

    /**
     * Inc visit count.
     *
     * @param value
     *            the value
     */
    public void incVisitCount(final long value)
    {
        this.visitCount += value;
    }

    /**
     * Sets the organization count.
     *
     * @param organizationCount
     *            the new organization count
     */
    public void setOrganizationCount(final int organizationCount)
    {
        this.organizationCount = organizationCount;
    }

    /**
     * Sets the service count.
     *
     * @param serviceCount
     *            the new service count
     */
    public void setServiceCount(final int serviceCount)
    {
        this.serviceCount = serviceCount;
    }

    /**
     * Sets the user count.
     *
     * @param userCount
     *            the new user count
     */
    public void setUserCount(final int userCount)
    {
        this.userCount = userCount;
    }

    /**
     * Sets the visit count.
     *
     * @param visitCount
     *            the new visit count
     */
    public void setVisitCount(final int visitCount)
    {
        this.visitCount = visitCount;
    }
}
