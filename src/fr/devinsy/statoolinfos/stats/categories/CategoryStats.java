/*
 * Copyright (C) 2020 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.categories;

import java.util.ArrayList;
import java.util.Collections;

/**
 * The Class CategoryStats.
 */
public class CategoryStats extends ArrayList<CategoryStat>
{
    private static final long serialVersionUID = 4864900171648399075L;

    /**
     * Instantiates a new property stat list.
     */
    public CategoryStats()
    {
        super();
    }

    /**
     * Reverse.
     *
     * @return the property stat list
     */
    public CategoryStats reverse()
    {
        CategoryStats result;

        Collections.reverse(this);

        result = this;

        //
        return result;
    }

    /**
     * Sort.
     *
     * @param sorting
     *            the sorting
     * @return the issues
     */
    public CategoryStats sort(final CategoryStatComparator.Sorting sorting)
    {
        CategoryStats result;

        sort(new CategoryStatComparator(sorting));

        result = this;

        //
        return result;
    }

    /**
     * Sort by name.
     *
     * @return the category stats
     */
    public CategoryStats sortByName()
    {
        CategoryStats result;

        result = sort(CategoryStatComparator.Sorting.NAME);

        //
        return result;
    }

    /**
     * Sort by organization count.
     *
     * @return the category stats
     */
    public CategoryStats sortByOrganizationCount()
    {
        CategoryStats result;

        result = sort(CategoryStatComparator.Sorting.ORGANIZATION_COUNT);

        //
        return result;
    }

    /**
     * Sort by service count.
     *
     * @return the category stats
     */
    public CategoryStats sortByServiceCount()
    {
        CategoryStats result;

        result = sort(CategoryStatComparator.Sorting.SERVICE_COUNT);

        //
        return result;
    }

    /**
     * Sort by user count.
     *
     * @return the category stats
     */
    public CategoryStats sortByUserCount()
    {
        CategoryStats result;

        result = sort(CategoryStatComparator.Sorting.USER_COUNT);

        //
        return result;
    }
}
