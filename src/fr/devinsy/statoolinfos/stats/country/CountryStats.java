/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.country;

import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class CountryStats.
 */
public class CountryStats extends HashMap<String, Integer>
{
    private static final long serialVersionUID = -8177123413128556290L;

    public static final String UNKNOWN_LABEL = "Inconnu";

    /**
     * Instantiates a new country stats.
     */
    public CountryStats()
    {
        super();
        put(UNKNOWN_LABEL, 0);
    }

    /**
     * Gets the.
     *
     * @param country
     *            the country
     * @return the integer
     */
    public int get(final String country)
    {
        int result;

        Integer count = super.get(country);

        if (count == null)
        {
            result = 0;
        }
        else
        {
            result = count;
        }

        //
        return result;
    }

    /**
     * Gets the total count.
     *
     * @return the total count
     */
    public long getTotalCount()
    {
        long result;

        result = 0;
        for (long value : this.values())
        {
            result += value;
        }

        //
        return result;
    }

    /**
     * Gets the unknown.
     *
     * @return the unknown
     */
    public int getUnknown()
    {
        int result;

        result = get(UNKNOWN_LABEL);

        //
        return result;
    }

    /**
     * Inc.
     *
     * @param country
     *            the country
     */
    public void inc(final String country)
    {
        if (StringUtils.isBlank(country))
        {
            incUnknown();
        }
        else
        {
            put(country, get(country) + 1);
        }
    }

    /**
     * Inc unknown.
     */
    public void incUnknown()
    {
        inc(UNKNOWN_LABEL);
    }
}
