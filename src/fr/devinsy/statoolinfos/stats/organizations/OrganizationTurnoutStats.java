/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.organizations;

/**
 * The Class OrganizationTurnoutStats.
 */
public class OrganizationTurnoutStats
{
    private long withSelfFileCount;
    private long withServiceFileCount;
    private long withServiceMetricCount;
    private long passiveCount;

    /**
     * Instantiates a new organization turnout stats.
     */
    public OrganizationTurnoutStats()
    {
    }

    /**
     * Gets the passive count.
     *
     * @return the passive count
     */
    public long getPassiveCount()
    {
        return this.passiveCount;
    }

    /**
     * Gets the total count.
     *
     * @return the total count
     */
    public long getTotalCount()
    {
        long result;

        result = this.withSelfFileCount + this.withServiceFileCount + this.withServiceMetricCount + this.passiveCount;

        //
        return result;
    }

    /**
     * Gets the with self file count.
     *
     * @return the with self file count
     */
    public long getWithSelfFileCount()
    {
        return this.withSelfFileCount;
    }

    /**
     * Gets the with service file count.
     *
     * @return the with service file count
     */
    public long getWithServiceFileCount()
    {
        return this.withServiceFileCount;
    }

    /**
     * Gets the with service metric count.
     *
     * @return the with service metric count
     */
    public long getWithServiceMetricCount()
    {
        return this.withServiceMetricCount;
    }

    /**
     * Inc passive count.
     */
    public void incPassiveCount()
    {
        this.passiveCount += 1;
    }

    /**
     * Inc with self file count.
     */
    public void incWithSelfFileCount()
    {
        this.withSelfFileCount += 1;
    }

    /**
     * Inc with service file count.
     */
    public void incWithServiceFileCount()
    {
        this.withServiceFileCount += 1;
    }

    /**
     * Inc with service metric count.
     */
    public void incWithServiceMetricCount()
    {
        this.withServiceMetricCount += 1;
    }

    /**
     * Sets the passive count.
     *
     * @param passiveCount
     *            the new passive count
     */
    public void setPassiveCount(final long passiveCount)
    {
        this.passiveCount = passiveCount;
    }
}
