/*
 * Copyright (C) 2022-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.organizations;

import fr.devinsy.statoolinfos.core.Organization;

/**
 * The Class OrganizationTypeStats.
 */
public class OrganizationTypeStats
{
    private long associationCount;
    private long informalCount;
    private long cooperativeCount;
    private long microcompanyCount;
    private long companyCount;
    private long individualCount;
    private long otherCount;
    private long unknownCount;

    /**
     * Instantiates a new organization type stats.
     */
    public OrganizationTypeStats()
    {
        this.associationCount = 0;
        this.informalCount = 0;
        this.cooperativeCount = 0;
        this.microcompanyCount = 0;
        this.companyCount = 0;
        this.individualCount = 0;
        this.otherCount = 0;
        this.unknownCount = 0;
    }

    /**
     * Gets the association count.
     *
     * @return the association count
     */
    public long getAssociationCount()
    {
        return this.associationCount;
    }

    /**
     * Gets the company count.
     *
     * @return the company count
     */
    public long getCompanyCount()
    {
        return this.companyCount;
    }

    /**
     * Gets the cooperative count.
     *
     * @return the cooperative count
     */
    public long getCooperativeCount()
    {
        return this.cooperativeCount;
    }

    /**
     * Gets the individual count.
     *
     * @return the individual count
     */
    public long getIndividualCount()
    {
        return this.individualCount;
    }

    /**
     * Gets the informal count.
     *
     * @return the informal count
     */
    public long getInformalCount()
    {
        return this.informalCount;
    }

    /**
     * Gets the microcompany count.
     *
     * @return the microcompany count
     */
    public long getMicrocompanyCount()
    {
        return this.microcompanyCount;
    }

    /**
     * Gets the other count.
     *
     * @return the other count
     */
    public long getOtherCount()
    {
        return this.otherCount;
    }

    /**
     * Gets the unknown count.
     *
     * @return the unknown count
     */
    public long getUnknownCount()
    {
        return this.unknownCount;
    }

    /**
     * Inc.
     *
     * @param type
     *            the type
     */
    public void inc(final Organization.Type type)
    {
        if (type == null)
        {
            this.unknownCount += 1;
        }
        else
        {
            switch (type)
            {
                case ASSOCIATION:
                    this.associationCount += 1;
                break;
                case COMPANY:
                    this.companyCount += 1;
                break;
                case COOPERATIVE:
                    this.cooperativeCount += 1;
                break;
                case INDIVIDUAL:
                    this.individualCount += 1;
                break;
                case INFORMAL:
                    this.informalCount += 1;
                break;
                case MICROCOMPANY:
                    this.microcompanyCount += 1;
                break;
                case OTHER:
                    this.otherCount += 1;
                break;
                default:
                    this.unknownCount += 1;
            }
        }
    }
}
