/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats;

import java.io.IOException;
import java.net.MalformedURLException;
import java.time.YearMonth;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.statoolinfos.core.Categories;
import fr.devinsy.statoolinfos.core.Category;
import fr.devinsy.statoolinfos.core.Federation;
import fr.devinsy.statoolinfos.core.Metrics;
import fr.devinsy.statoolinfos.core.Organization;
import fr.devinsy.statoolinfos.core.Organizations;
import fr.devinsy.statoolinfos.core.Service;
import fr.devinsy.statoolinfos.core.Service.RegistrationType;
import fr.devinsy.statoolinfos.core.Services;
import fr.devinsy.statoolinfos.core.Software;
import fr.devinsy.statoolinfos.core.Softwares;
import fr.devinsy.statoolinfos.crawl.CrawlCache;
import fr.devinsy.statoolinfos.properties.PathPropertyList;
import fr.devinsy.statoolinfos.stats.categories.CategoryStat;
import fr.devinsy.statoolinfos.stats.categories.CategoryStats;
import fr.devinsy.statoolinfos.stats.country.CountryStats;
import fr.devinsy.statoolinfos.stats.organizations.OrganizationTurnoutStats;
import fr.devinsy.statoolinfos.stats.organizations.OrganizationTypeStats;
import fr.devinsy.statoolinfos.stats.properties.PropertyStats;
import fr.devinsy.statoolinfos.stats.propertyfiles.PropertiesFileStats;
import fr.devinsy.statoolinfos.stats.services.HostProviderTypeStats;
import fr.devinsy.statoolinfos.stats.services.HostServerTypeStats;
import fr.devinsy.statoolinfos.stats.services.RegistrationStats;
import fr.devinsy.statoolinfos.stats.services.ServiceInstallTypeStats;
import fr.devinsy.statoolinfos.stats.softwares.SoftwareStat;
import fr.devinsy.statoolinfos.stats.softwares.SoftwareStats;
import fr.devinsy.strings.StringSet;

/**
 * The Class StatAgent.
 */
public class StatAgent
{
    private static Logger logger = LoggerFactory.getLogger(StatAgent.class);

    /**
     * Instantiates a new stat agent.
     */
    private StatAgent()
    {
    }

    /**
     * Stat all categories.
     *
     * @param federation
     *            the federation
     * @param categories
     *            the categories
     * @return the property stats
     */
    public static CategoryStats statAllCategories(final Federation federation, final Categories categories)
    {
        CategoryStats result;

        result = new CategoryStats();

        for (Category category : categories)
        {
            CategoryStat stat = new CategoryStat(category);
            StringSet organizations = new StringSet();

            for (Service service : federation.getServices())
            {
                String softwareName = service.getSoftwareName();
                if (category.matchesSoftware(softwareName))
                {
                    stat.incServiceCount();
                    YearMonth month = YearMonth.now().minusMonths(1);
                    stat.incUserCount(service.getMonthUserCount(month));
                    stat.incVisitCount(service.getMonthVisitCount(month));
                    organizations.add(service.getOrganization().getName());
                }
            }
            stat.setOrganizationCount(organizations.size());

            result.add(stat);
        }

        //
        return result;
    }

    /**
     * Stat all properties.
     *
     * @param federation
     *            the federation
     * @return the property stats
     */
    public static PropertyStats statAllProperties(final Federation federation)
    {
        PropertyStats result;

        result = new PropertyStats();

        result.stat(federation);
        for (Organization organization : federation.getOrganizations())
        {
            result.stat(organization);

            for (Service service : organization.getServices())
            {
                result.stat(service);

                for (Metrics metrics : service.getMetrics())
                {
                    result.stat(metrics);
                }
            }
        }

        //
        return result;
    }

    /**
     * Stat all properties files.
     *
     * @param federation
     *            the federation
     * @param cache
     *            the cache
     * @return the properties file stats
     * @throws MalformedURLException
     *             the malformed URL exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static PropertiesFileStats statAllPropertiesFiles(final Federation federation, final CrawlCache cache) throws MalformedURLException, IOException
    {
        PropertiesFileStats result;

        result = new PropertiesFileStats();

        for (Organization organization : federation.getOrganizations())
        {
            result.stat(organization);

            for (Service service : organization.getServices())
            {
                result.stat(service);

                for (Metrics metrics : service.getMetrics())
                {
                    result.stat(metrics, organization);
                }
            }
        }

        //
        return result;
    }

    /**
     * Stat all softwares.
     *
     * @param federation
     *            the federation
     * @param categories
     *            the categories
     * @return the software stats
     */
    public static SoftwareStats statAllSoftwares(final Federation federation, final Categories categories)
    {
        SoftwareStats result;

        // Build catalog.
        Softwares catalog = federation.getSoftwares();
        logger.info("CATALOG={}", catalog.size());

        // Stat.
        result = new SoftwareStats();
        for (Software software : catalog.values())
        {
            SoftwareStat stat = new SoftwareStat(software.getName());
            stat.getCategories().addAll(categories.findBySoftware(software.getName()));
            StringSet organizations = new StringSet();
            for (Service service : federation.getServices())
            {
                Software current = catalog.get(service.getSoftwareName());
                if (current == software)
                {
                    stat.incServiceCount();
                    YearMonth month = YearMonth.now().minusMonths(1);
                    stat.incUserCount(service.getMonthUserCount(month));
                    stat.incVisitCount(service.getMonthVisitCount(month));
                    organizations.add(service.getOrganization().getName());
                }
            }
            stat.setOrganizationCount(organizations.size());
            result.add(stat);
        }

        //
        return result;
    }

    /**
     * Stat federation properties.
     *
     * @param federation
     *            the federation
     * @return the property stats
     */
    public static PropertyStats statFederationProperties(final Federation federation)
    {
        PropertyStats result;

        result = new PropertyStats();

        result.stat(federation);

        //
        return result;
    }

    /**
     * Stat host provider type.
     *
     * @param services
     *            the services
     * @return the host provider type stats
     */
    public static HostProviderTypeStats statHostProviderType(final Services services)
    {
        HostProviderTypeStats result;

        result = new HostProviderTypeStats();

        //
        for (Service service : services)
        {
            result.inc(service.getHostProviderType());
        }

        //
        return result;
    }

    /**
     * Stat host server distribution.
     *
     * @param services
     *            the services
     * @return the host server type stats
     */
    public static HostServerTypeStats statHostServerDistribution(final Services services)
    {
        HostServerTypeStats result;

        result = new HostServerTypeStats();

        //
        for (Service service : services)
        {
            result.inc(service.getHostServerType());
        }

        //
        return result;
    }

    /**
     * Stat host server type.
     *
     * @param services
     *            the services
     * @return the host server type stats
     */
    public static HostServerTypeStats statHostServerType(final Services services)
    {
        HostServerTypeStats result;

        result = new HostServerTypeStats();

        //
        for (Service service : services)
        {
            result.inc(service.getHostServerType());
        }

        //
        return result;
    }

    /**
     * Stat organizations properties.
     *
     * @param organizations
     *            the organizations
     * @return the property stats
     */
    public static PropertyStats statOrganizationsProperties(final Organizations organizations)
    {
        PropertyStats result;

        result = new PropertyStats();

        for (Organization organization : organizations)
        {
            result.stat(organization);
        }

        //
        return result;
    }

    /**
     * Stat organization type.
     *
     * @param organizations
     *            the organizations
     * @return the organization type stats
     */
    public static OrganizationTypeStats statOrganizationType(final Organizations organizations)
    {
        OrganizationTypeStats result;

        result = new OrganizationTypeStats();

        //
        for (Organization organization : organizations)
        {
            result.inc(organization.getType());
        }

        //
        return result;
    }

    /**
     * Stat registration types.
     *
     * @param services
     *            the services
     * @return the registration stats
     */
    public static RegistrationStats statRegistrationTypes(final Services services)
    {
        RegistrationStats result;

        result = new RegistrationStats();

        //
        for (Service service : services)
        {
            if (service.isRegistrationNone())
            {
                result.inc(RegistrationType.NONE);
            }

            if (service.isRegistrationFree())
            {
                result.inc(RegistrationType.FREE);
            }

            if (service.isRegistrationMember())
            {
                result.inc(RegistrationType.MEMBER);
            }

            if (service.isRegistrationClient())
            {
                result.inc(RegistrationType.CLIENT);
            }

            if (service.isRegistrationUnknown())
            {
                result.inc(RegistrationType.UNKNOWN);
            }
        }

        //
        return result;
    }

    /**
     * Stats country.
     *
     * @param organizations
     *            the organizations
     * @return the country stats
     */
    public static CountryStats statsCountry(final Organizations organizations)
    {
        CountryStats result;

        result = new CountryStats();

        //
        for (Organization organization : organizations)
        {
            result.inc(organization.getCountryCode());
        }

        //
        return result;
    }

    /**
     * Stats country.
     *
     * @param services
     *            the services
     * @return the country stats
     */
    public static CountryStats statsCountry(final Services services)
    {
        CountryStats result;

        result = new CountryStats();

        //
        for (Service service : services)
        {
            result.inc(service.getCountryCode());
        }

        //
        return result;
    }

    /**
     * Stat service install type.
     *
     * @param services
     *            the services
     * @return the service install type stats
     */
    public static ServiceInstallTypeStats statServiceInstallType(final Services services)
    {
        ServiceInstallTypeStats result;

        result = new ServiceInstallTypeStats();

        //
        for (Service service : services)
        {
            result.inc(service.getServiceInstallType());
        }

        //
        return result;
    }

    /**
     * Stat services properties.
     *
     * @param services
     *            the services
     * @return the property stats
     */
    public static PropertyStats statServicesProperties(final Services services)
    {
        PropertyStats result;

        result = new PropertyStats();

        for (Service service : services)
        {
            result.stat(service);

            for (Metrics metrics : service.getMetrics())
            {
                result.stat(metrics);
            }
        }

        //
        return result;
    }

    /**
     * Stats organization turnout.
     *
     * @param organizations
     *            the organizations
     * @return the organization turnout stats
     */
    public static OrganizationTurnoutStats statsOrganizationTurnout(final Organizations organizations)
    {
        OrganizationTurnoutStats result;

        result = new OrganizationTurnoutStats();

        //
        for (Organization organization : organizations)
        {
            if (StringUtils.isBlank(organization.getStartDateValue()))
            {
                result.incPassiveCount();
            }
            else if (organization.getServiceCount() == 0)
            {
                result.incWithSelfFileCount();
            }
            else
            {
                boolean ended = false;
                Iterator<Service> iterator = organization.getServices().iterator();
                while (!ended)
                {
                    if (iterator.hasNext())
                    {
                        Service service = iterator.next();

                        PathPropertyList lines = service.getByPattern("metrics\\..*\\.\\d{4}\\.(months|weeks|days)");

                        if (lines.hasFilledValue())
                        {
                            ended = true;
                            result.incWithServiceMetricCount();
                        }
                    }
                    else
                    {
                        ended = true;
                        result.incWithServiceFileCount();
                    }
                }
            }
        }

        //
        return result;
    }

    /**
     * Stats organization turnout.
     *
     * @param service
     *            the service
     * @return the organization turnout stats
     */
    public static OrganizationTurnoutStats statsOrganizationTurnout(final Service service)
    {
        OrganizationTurnoutStats result;

        result = new OrganizationTurnoutStats();

        //
        PathPropertyList lines = service.getByPattern("metrics\\..*\\.\\d{4}\\.(months|weeks|days)");

        if (lines.hasFilledValue())
        {
            result.incWithServiceMetricCount();
        }
        else
        {
            result.incWithServiceFileCount();
        }

        //
        return result;
    }
}
