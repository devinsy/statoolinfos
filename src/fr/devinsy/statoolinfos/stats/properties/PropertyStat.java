/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.properties;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class PropertyStat.
 */
public class PropertyStat
{
    private String path;
    private int blankCount;
    private int filledCount;

    /**
     * Instantiates a new property stat.
     */
    public PropertyStat(final String path)
    {
        if (StringUtils.isBlank(path))
        {
            throw new IllegalArgumentException("Null parameter.");
        }
        else
        {
            this.path = path;
            this.blankCount = 0;
            this.filledCount = 0;
        }
    }

    /**
     * Gets the blank count.
     *
     * @return the blank count
     */
    public int getBlankCount()
    {
        return this.blankCount;
    }

    /**
     * Gets the filled count.
     *
     * @return the filled count
     */
    public int getFilledCount()
    {
        return this.filledCount;
    }

    /**
     * Gets the path.
     *
     * @return the path
     */
    public String getPath()
    {
        return this.path;
    }

    /**
     * Inc blank.
     */
    public void incBlank()
    {
        this.blankCount += 1;
    }

    /**
     * Inc filled.
     */
    public void incFilled()
    {
        this.filledCount += 1;
    }

    /**
     * Sets the blank count.
     *
     * @param blankCount
     *            the new blank count
     */
    public void setBlankCount(final int blankCount)
    {
        this.blankCount = blankCount;
    }

    /**
     * Sets the filled count.
     *
     * @param filledCount
     *            the new filled count
     */
    public void setFilledCount(final int filledCount)
    {
        this.filledCount = filledCount;
    }
}
