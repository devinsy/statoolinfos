/*
 * Copyright (C) 2020 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.properties;

import java.util.ArrayList;
import java.util.Collections;

/**
 * The Class PropertyStatList.
 */
public class PropertyStatList extends ArrayList<PropertyStat>
{
    private static final long serialVersionUID = -3654806428144379451L;

    /**
     * Instantiates a new property stat list.
     */
    public PropertyStatList()
    {
        super();
    }

    /**
     * Reverse.
     *
     * @return the property stat list
     */
    public PropertyStatList reverse()
    {
        PropertyStatList result;

        Collections.reverse(this);

        result = this;

        //
        return result;
    }

    /**
     * Sort.
     *
     * @param sorting
     *            the sorting
     * @return the issues
     */
    public PropertyStatList sort(final PropertyStatComparator.Sorting sorting)
    {
        PropertyStatList result;

        sort(new PropertyStatComparator(sorting));

        result = this;

        //
        return result;
    }

    /**
     * Sort by blank count.
     *
     * @return the property stat list
     */
    public PropertyStatList sortByBlankCount()
    {
        PropertyStatList result;

        result = sort(PropertyStatComparator.Sorting.BLANK_COUNT);

        //
        return result;
    }

    /**
     * Sort by filled count.
     *
     * @return the property stat list
     */
    public PropertyStatList sortByFilledCount()
    {
        PropertyStatList result;

        result = sort(PropertyStatComparator.Sorting.FILLED_COUNT);

        //
        return result;
    }

    /**
     * Sort by created on.
     *
     * @return the issues
     */
    public PropertyStatList sortByPath()
    {
        PropertyStatList result;

        result = sort(PropertyStatComparator.Sorting.PATH);

        //
        return result;
    }

    /**
     * Sort by reverse filled count.
     *
     * @return the property stat list
     */
    public PropertyStatList sortByReverseFilledCount()
    {
        PropertyStatList result;

        result = sort(PropertyStatComparator.Sorting.REVERSE_FILLED_COUNT);

        //
        return result;
    }
}
