/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.properties;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.statoolinfos.properties.PathProperty;
import fr.devinsy.statoolinfos.properties.PathPropertyList;

/**
 * The Class PropertyStat.
 */
public class PropertyStats
{
    private int fileCount;
    private PropertyStatSet stats;

    /**
     * Instantiates a new property stat.
     */
    public PropertyStats()
    {
        this.fileCount = 0;
        this.stats = new PropertyStatSet();
    }

    /**
     * Gets the.
     *
     * @param path
     *            the path
     * @return the property stat
     */
    public PropertyStat get(final String path)
    {
        PropertyStat result;

        result = this.stats.get(path);

        //
        return result;
    }

    /**
     * Gets the file count.
     *
     * @return the file count
     */
    public int getFileCount()
    {
        return this.fileCount;
    }

    /**
     * Gets the not metric property stats.
     *
     * @return the not metric property stats
     */
    public PropertyStats getGeneralPropertyStats()
    {
        PropertyStats result;

        result = new PropertyStats();

        for (PropertyStat stat : this.stats.values())
        {
            if (!StringUtils.startsWithAny(stat.getPath(), "metrics.", "subs."))
            {
                result.stats.put(stat);
            }
        }

        //
        return result;
    }

    /**
     * Gets the list.
     *
     * @return the list
     */
    public PropertyStatList getList()
    {
        PropertyStatList result;

        result = new PropertyStatList();

        for (PropertyStat stat : this.stats.values())
        {
            result.add(stat);
        }

        //
        return result;
    }

    /**
     * Gets the metric property stats.
     *
     * @return the metric property stats
     */
    public PropertyStats getMetricPropertyStats()
    {
        PropertyStats result;

        result = new PropertyStats();

        for (PropertyStat stat : this.stats.values())
        {
            if (StringUtils.startsWithAny(stat.getPath(), "metrics.", "subs."))
            {
                result.stats.put(stat);
            }
        }

        //
        return result;
    }

    /**
     * Gets the property count.
     *
     * @return the property count
     */
    public int getPropertyCount()
    {
        return this.stats.size();
    }

    /**
     * Gets the stats.
     *
     * @return the stats
     */
    public PropertyStatSet getStats()
    {
        return this.stats;
    }

    /**
     * Stat.
     *
     * @param properties
     *            the properties
     */
    public void stat(final PathPropertyList properties)
    {
        this.fileCount += 1;

        for (PathProperty property : properties)
        {
            String path = property.getPath().toLowerCase();
            PropertyStat stat = this.stats.get(path);

            if (stat == null)
            {
                stat = new PropertyStat(path);
            }

            if (StringUtils.isBlank(property.getValue()))
            {
                stat.incBlank();
            }
            else
            {
                stat.incFilled();
            }

            this.stats.put(stat);
        }
    }
}
