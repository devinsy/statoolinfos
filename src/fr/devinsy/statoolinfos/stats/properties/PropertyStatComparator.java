/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.properties;

import java.util.Comparator;

import fr.devinsy.statoolinfos.util.CompareUtils;

/**
 * The Class PropertyStatComparator.
 */
public class PropertyStatComparator implements Comparator<PropertyStat>
{
    public enum Sorting
    {
        PATH,
        BLANK_COUNT,
        FILLED_COUNT,
        REVERSE_FILLED_COUNT
    }

    private Sorting sorting;

    /**
     * Instantiates a new project comparator.
     *
     * @param sorting
     *            the sorting
     */
    public PropertyStatComparator(final Sorting sorting)
    {
        //
        this.sorting = sorting;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    @Override
    public int compare(final PropertyStat alpha, final PropertyStat bravo)
    {
        int result;

        result = compare(alpha, bravo, this.sorting);

        //
        return result;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @param sorting
     *            the sorting
     * @return the int
     */
    public static int compare(final PropertyStat alpha, final PropertyStat bravo, final Sorting sorting)
    {
        int result;

        if (sorting == null)
        {
            result = 0;
        }
        else
        {
            switch (sorting)
            {
                default:
                case PATH:
                    result = CompareUtils.compare(getPath(alpha), getPath(bravo));
                break;

                case BLANK_COUNT:
                    result = CompareUtils.compare(getBlankCount(alpha), getBlankCount(bravo));
                break;

                case FILLED_COUNT:
                    result = CompareUtils.compare(getFilledCount(alpha), getFilledCount(bravo));
                break;

                case REVERSE_FILLED_COUNT:
                    result = -CompareUtils.compare(getFilledCount(alpha), getFilledCount(bravo));
                    if (result == 0)
                    {
                        result = CompareUtils.compare(getPath(alpha), getPath(bravo));
                    }
                break;
            }
        }

        //
        return result;
    }

    /**
     * Gets the id.
     *
     * @param source
     *            the source
     * @return the id
     */
    public static Long getBlankCount(final PropertyStat source)
    {
        Long result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = (long) source.getBlankCount();
        }

        //
        return result;
    }

    /**
     * Gets the filled count.
     *
     * @param source
     *            the source
     * @return the filled count
     */
    public static Long getFilledCount(final PropertyStat source)
    {
        Long result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = (long) source.getFilledCount();
        }

        //
        return result;
    }

    /**
     * Gets the path.
     *
     * @param source
     *            the source
     * @return the path
     */
    public static String getPath(final PropertyStat source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.getPath();
        }

        //
        return result;
    }
}
