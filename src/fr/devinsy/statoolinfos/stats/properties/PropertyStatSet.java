/*
 * Copyright (C) 2020 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.properties;

import java.util.HashMap;

/**
 * The Class PropertyStatSet.
 */
public class PropertyStatSet extends HashMap<String, PropertyStat>
{
    private static final long serialVersionUID = -728150929356133285L;

    /**
     * Instantiates a new property stats.
     */
    public PropertyStatSet()
    {
        super();
    }

    /**
     * Put.
     *
     * @param stat
     *            the stat
     */
    void put(final PropertyStat stat)
    {
        put(stat.getPath(), stat);
    }
}
