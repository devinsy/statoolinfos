/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.visitor;

import java.util.Comparator;

import fr.devinsy.statoolinfos.util.CompareUtils;

/**
 * The Class VisitorStatComparator.
 */
public class VisitorStatComparator implements Comparator<VisitorStat>
{
    public enum Sorting
    {
        IP,
        USERAGENT,
        LOGCOUNT,
        VISITCOUNT
    }

    private Sorting sorting;

    /**
     * Instantiates a new visitor stat comparator.
     *
     * @param sorting
     *            the sorting
     */
    public VisitorStatComparator(final Sorting sorting)
    {
        this.sorting = sorting;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    @Override
    public int compare(final VisitorStat alpha, final VisitorStat bravo)
    {
        int result;

        result = compare(alpha, bravo, this.sorting);

        //
        return result;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @param sorting
     *            the sorting
     * @return the int
     */
    public static int compare(final VisitorStat alpha, final VisitorStat bravo, final Sorting sorting)
    {
        int result;

        if (sorting == null)
        {
            result = 0;
        }
        else
        {
            switch (sorting)
            {
                case IP:
                    result = CompareUtils.compare(getIp(alpha), getIp(bravo));
                break;

                default:
                case USERAGENT:
                    result = CompareUtils.compare(getUserAgent(alpha), getUserAgent(bravo));
                break;

                case LOGCOUNT:
                    result = CompareUtils.compare(getLogCount(alpha), getLogCount(bravo));
                break;

                case VISITCOUNT:
                    result = CompareUtils.compare(getVisitCount(alpha), getVisitCount(bravo));
                break;
            }
        }

        //
        return result;
    }

    /**
     * Gets the ip.
     *
     * @param source
     *            the source
     * @return the ip
     */
    public static String getIp(final VisitorStat source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.getIp();
        }

        //
        return result;
    }

    /**
     * Gets the count.
     *
     * @param source
     *            the source
     * @return the count
     */
    public static Long getLogCount(final VisitorStat source)
    {
        Long result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = (long) source.getLogCount();
        }

        //
        return result;
    }

    /**
     * Gets the user agent.
     *
     * @param source
     *            the source
     * @return the user agent
     */
    public static String getUserAgent(final VisitorStat source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.getUserAgent();
        }

        //
        return result;
    }

    public static Long getVisitCount(final VisitorStat source)
    {
        Long result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = (long) source.getVisitCount();
        }

        //
        return result;
    }
}
