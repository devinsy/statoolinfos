/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.visitor;

import java.util.ArrayList;
import java.util.Collections;

/**
 * The Class VisitorStats.
 */
public final class VisitorStats extends ArrayList<VisitorStat>
{
    private static final long serialVersionUID = 8866450468875949301L;

    /**
     * Instantiates a new visitor stats.
     */
    public VisitorStats()
    {
        super();
    }

    /**
     * Instantiates a new visitor stats.
     *
     * @param capacity
     *            the capacity
     */
    public VisitorStats(final int capacity)
    {
        super(capacity);
    }

    /**
     * Reverse.
     *
     * @return the visitor stats
     */
    public VisitorStats reverse()
    {
        VisitorStats result;

        Collections.reverse(this);

        result = this;

        //
        return result;
    }

    /**
     * Sort.
     *
     * @param sorting
     *            the sorting
     * @return the visitor stats
     */
    public VisitorStats sort(final VisitorStatComparator.Sorting sorting)
    {
        VisitorStats result;

        sort(new VisitorStatComparator(sorting));

        result = this;

        //
        return result;
    }

    /**
     * Sort by ip.
     *
     * @return the visitor stats
     */
    public VisitorStats sortByIp()
    {
        VisitorStats result;

        result = sort(VisitorStatComparator.Sorting.IP);

        //
        return result;
    }

    /**
     * Sort by count.
     *
     * @return the visitor stats
     */
    public VisitorStats sortByLogCount()
    {
        VisitorStats result;

        result = sort(VisitorStatComparator.Sorting.LOGCOUNT);

        //
        return result;
    }

    /**
     * Sort by label.
     *
     * @return the visitor stats
     */
    public VisitorStats sortByUserAgent()
    {
        VisitorStats result;

        result = sort(VisitorStatComparator.Sorting.USERAGENT);

        //
        return result;
    }

    /**
     * Sort by visit count.
     *
     * @return the visitor stats
     */
    public VisitorStats sortByVisitCount()
    {
        VisitorStats result;

        result = sort(VisitorStatComparator.Sorting.VISITCOUNT);

        //
        return result;
    }
}