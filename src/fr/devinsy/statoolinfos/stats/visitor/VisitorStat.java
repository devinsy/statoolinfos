/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.visitor;

import fr.devinsy.statoolinfos.metrics.httpaccess.Visits;

/**
 * The Class VisitorStat.
 */
public final class VisitorStat
{
    private String ip;
    private String userAgent;
    private long logCount;
    private Visits visits;

    /**
     * Instantiates a new visitor stat.
     *
     * @param ip
     *            the ip
     * @param userAgent
     *            the user agent
     */
    public VisitorStat(final String ip, final String userAgent)
    {
        this.ip = ip;
        this.userAgent = userAgent;
        this.logCount = 0;
        this.visits = new Visits();
    }

    /**
     * Gets the ip.
     *
     * @return the ip
     */
    public String getIp()
    {
        return this.ip;
    }

    /**
     * Gets the log count.
     *
     * @return the log count
     */
    public long getLogCount()
    {
        return this.logCount;
    }

    /**
     * Gets the user agent.
     *
     * @return the user agent
     */
    public String getUserAgent()
    {
        return this.userAgent;
    }

    /**
     * Gets the visit count.
     *
     * @return the visit count
     */
    public long getVisitCount()
    {
        long result;

        result = this.visits.size();

        //
        return result;
    }

    /**
     * Gets the visits.
     *
     * @return the visits
     */
    public Visits getVisits()
    {
        return this.visits;
    }

    /**
     * Inc.
     */
    public void inc()
    {
        this.logCount += 1;
    }
}