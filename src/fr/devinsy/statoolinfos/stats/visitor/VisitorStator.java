/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.visitor;

import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLog;
import fr.devinsy.statoolinfos.metrics.httpaccess.HttpStatusCategory;

/**
 * The Class VisitorStator.
 */
public final class VisitorStator
{
    private long logCount;
    private VisitorStatSet visitors;

    /**
     * Instantiates a new visitor stator.
     */
    public VisitorStator()
    {
        this.logCount = 0;
        this.visitors = new VisitorStatSet();
    }

    public long getLogCount()
    {
        return this.logCount;
    }

    /**
     * Gets the visitors.
     *
     * @return the visitors
     */
    public VisitorStats getVisitorStats()
    {
        VisitorStats result;

        result = this.visitors.toList();

        //
        return result;
    }

    /**
     * Adds the log.
     *
     * @param log
     *            the log
     */
    public void putLog(final HttpAccessLog log)
    {
        if (log != null)
        {
            this.logCount += 1;
            VisitorStat stat = this.visitors.put(log.getIp(), log.getUserAgent().toString());

            if (log.getStatus().getCategory() == HttpStatusCategory.SUCCESS)
            {
                stat.getVisits().add(log.getTime());
            }
        }
    }
}