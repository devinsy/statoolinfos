/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.stats.visitor;

import java.util.HashMap;

import fr.devinsy.statoolinfos.stats.useragent.UserAgentStat;

/**
 * The Class VisitorStatSet.
 */
public final class VisitorStatSet extends HashMap<String, VisitorStat>
{
    private static final long serialVersionUID = -3123113539922160408L;

    /**
     * Instantiates a new visitor stat set.
     */
    public VisitorStatSet()
    {
        super();
    }

    /**
     * Compute key.
     *
     * @param ip
     *            the ip
     * @param userAgent
     *            the user agent
     * @return the string
     */
    private String computeKey(final String ip, final String userAgent)
    {
        String result;

        result = ip + userAgent.trim();

        //
        return result;
    }

    /**
     * Gets the visit count.
     *
     * @return the visit count
     */
    public long getVisitCount()
    {
        long result;

        result = 0;

        for (VisitorStat stat : this.values())
        {
            result += stat.getVisitCount();
        }

        //
        return result;
    }

    /**
     * Put.
     *
     * @param ip
     *            the ip
     * @param userAgent
     *            the user agent
     * @return the visitor stat
     */
    public VisitorStat put(final String ip, final String userAgent)
    {
        VisitorStat result;

        String key = computeKey(ip, userAgent);

        result = get(key);
        if (result == null)
        {
            result = new VisitorStat(ip, userAgent);
            this.put(key, result);
        }

        result.inc();

        //
        return result;
    }

    /**
     * Put.
     *
     * @param ip
     *            the ip
     * @param userAgent
     *            the user agent
     */
    public void put(final String ip, final UserAgentStat userAgent)
    {
        this.put(userAgent.getUserAgent(), userAgent);
    }

    /**
     * To list.
     *
     * @return the user agent stats
     */
    public VisitorStats toList()
    {
        VisitorStats result;

        result = new VisitorStats(this.size());

        for (VisitorStat stat : this.values())
        {
            result.add(stat);
        }

        //
        return result;
    }
}