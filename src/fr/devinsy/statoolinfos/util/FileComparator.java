/*
 * Copyright (C) 2020-2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.util;

import java.io.File;
import java.util.Comparator;

/**
 * The Class FileComparator.
 */
public class FileComparator implements Comparator<File>
{
    public enum Sorting
    {
        NAME
    }

    private Sorting sorting;

    /**
     * Instantiates a new organization comparator.
     *
     * @param sorting
     *            the sorting
     */
    public FileComparator(final Sorting sorting)
    {
        this.sorting = sorting;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    @Override
    public int compare(final File alpha, final File bravo)
    {
        int result;

        result = compare(alpha, bravo, this.sorting);

        //
        return result;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @param sorting
     *            the sorting
     * @return the int
     */
    public static int compare(final File alpha, final File bravo, final Sorting sorting)
    {
        int result;

        if (sorting == null)
        {
            result = 0;
        }
        else
        {
            switch (sorting)
            {
                default:
                case NAME:
                    result = CompareUtils.compareNatural(getName(alpha), getName(bravo));
                break;
            }
        }

        //
        return result;
    }

    /**
     * Gets the name.
     *
     * @param source
     *            the source
     * @return the name
     */
    public static String getName(final File source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.getName();
        }

        //
        return result;
    }
}
