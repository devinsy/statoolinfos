/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.util;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.strings.StringList;

/**
 * The Class FilesUtils.
 */
public class FilesUtils
{

    /**
     * Instantiates a new files utils.
     */
    private FilesUtils()
    {
    }

    /**
     * List recursively.
     *
     * @param source
     *            the source
     * @return the files
     */
    public static Files listRecursively(final File source)
    {
        Files result;

        result = new Files();
        if ((source != null) && (source.exists()) && (source.canRead()))
        {
            if (source.isFile())
            {
                result.add(source);
            }
            else
            {
                for (File file : source.listFiles())
                {
                    if (file.isDirectory())
                    {
                        result.addAll(listRecursively(file));
                    }
                    else
                    {
                        result.add(file);
                    }
                }
            }
        }

        //
        return result;
    }

    /**
     * Read first line.
     *
     * @param file
     *            the file
     * @return the string
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static String readFirstLine(final File file) throws IOException
    {
        String result;

        result = readFirstLine(new Files(file));

        //
        return result;
    }

    /**
     * Read first line.
     *
     * @param files
     *            the file
     * @return the string
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static String readFirstLine(final Files files) throws IOException
    {
        String result;

        FilesLineIterator iterator = null;
        try
        {
            iterator = new FilesLineIterator(files);
            if (iterator.hasNext())
            {
                result = iterator.next();
            }
            else
            {
                result = "";
            }
        }
        finally
        {
            if (iterator != null)
            {
                iterator.close();
            }
        }

        //
        return result;
    }

    /**
     * Read first line not blank.
     *
     * @param files
     *            the files
     * @return the string
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static String readFirstLineNotBlank(final Files files) throws IOException
    {
        String result;

        FilesLineIterator iterator = null;
        result = null;
        try
        {
            iterator = new FilesLineIterator(files);
            iterator.setPrintOff();

            boolean ended = false;
            while (!ended)
            {
                if (iterator.hasNext())
                {
                    result = iterator.next();

                    if (!StringUtils.isBlank(result))
                    {
                        ended = true;
                    }
                }
                else
                {
                    result = null;
                    ended = true;
                }
            }
        }
        finally
        {
            if (iterator != null)
            {
                iterator.close();
            }
        }

        //
        return result;
    }

    /**
     * Search recursively.
     *
     * @param source
     *            the source
     * @param regex
     *            the regex
     * @return the files
     */
    public static Files search(final File source, final String regex)
    {
        Files result;

        result = new Files();

        Pattern pattern = Pattern.compile(regex);

        Files full = listRecursively(source);
        for (File file : full)
        {
            if (pattern.matcher(file.getName()).matches())
            {
                result.add(file);
            }
        }

        //
        return result;
    }

    /**
     * Search by wildcard.
     *
     * @param source
     *            the source. Example: /var/log/apache2/www.*.org/*access??
     * @return the files
     */
    public static Files searchByWildcard(final String source)
    {
        Files result;

        result = new Files();

        if (StringUtils.contains(source, ','))
        {
            result.addAll(searchByWildcardFromMany(source));
        }
        else if (StringUtils.isNotBlank(source))
        {
            // Get the first parent without wildcard.
            File parent = null;
            File path = new File(source).getAbsoluteFile();
            File current = path;

            boolean ended = false;
            while (!ended)
            {
                if (current.getParentFile().equals(current))
                {
                    ended = true;
                    parent = current;
                }
                else
                {
                    if (StringUtils.containsAny(current.getAbsolutePath(), '?', '*'))
                    {
                        current = current.getParentFile();
                    }
                    else
                    {
                        ended = true;
                        parent = current;
                    }
                }
            }

            //
            System.out.println("parent=" + parent);
            String regex = path.toString().replace(".", "\\.").replace("?", ".").replace("*", ".*");
            result = listRecursively(parent).keepPath(regex);
        }

        //
        return result;
    }

    /**
     * Search by wildcard.
     *
     * @param sources
     *            the sources
     * @return the files
     */
    public static Files searchByWildcard(final StringList sources)
    {
        Files result;

        result = new Files();

        if (sources != null)
        {
            for (String source : sources)
            {
                result.addAll(searchByWildcard(source));
            }
        }

        //
        return result;
    }

    /**
     * Search by wildcard from many.
     *
     * @param source
     *            the source
     * @return the files
     */
    public static Files searchByWildcardFromMany(final String source)
    {
        Files result;

        result = new Files();

        if (StringUtils.isNotBlank(source))
        {
            String[] paths = source.split(",");

            for (String path : paths)
            {
                if (StringUtils.isNotBlank(path))
                {
                    result.addAll(searchByWildcard(StringUtils.trim(path)));
                }
            }
        }

        //
        return result;
    }

    /**
     * List recursively.
     *
     * @param source
     *            the source
     * @param extensions
     *            the extensions
     * @return the files
     */
    public static Files searchEndingWith(final File source, final String... extensions)
    {
        Files result;

        result = new Files();

        Files full = listRecursively(source);
        for (File file : full)
        {
            if (StringUtils.endsWithAny(file.getName(), extensions))
            {
                result.add(file);
            }
        }

        //
        return result;
    }

    /**
     * Search starting with.
     *
     * @param source
     *            the source
     * @param extensions
     *            the extensions
     * @return the files
     */
    public static Files searchStartingWith(final File source, final String... extensions)
    {
        Files result;

        result = new Files();

        Files full = listRecursively(source);
        for (File file : full)
        {
            if (StringUtils.startsWithAny(file.getName(), extensions))
            {
                result.add(file);
            }
        }

        //
        return result;
    }
}
