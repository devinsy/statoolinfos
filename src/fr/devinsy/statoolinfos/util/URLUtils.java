/*
 * Copyright (C) 2020 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.util;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.statoolinfos.core.StatoolInfosUtils;

/**
 * The Class URLUtils.
 */
public final class URLUtils
{
    private static Logger logger = LoggerFactory.getLogger(URLUtils.class);

    /**
     * Instantiates a new URL utils.
     */
    private URLUtils()
    {
    }

    /**
     * Copy resources recursively.
     *
     * @param source
     *            the origin url
     * @param target
     *            the destination
     * @return true, if successful
     */
    public static boolean copyDeeply(final URL source, final File target)
    {
        boolean result;

        try
        {
            final URLConnection urlConnection = source.openConnection();
            if (urlConnection instanceof JarURLConnection)
            {
                result = copyURL(target, (JarURLConnection) urlConnection);
                result = true;
            }
            else
            {
                FileUtils.copyDirectory(new File(source.getPath()), target);
                result = true;
            }
        }
        catch (final IOException exception)
        {
            exception.printStackTrace();
            result = false;
        }

        //
        return result;
    }

    /**
     * Copy resource.
     *
     * @param source
     *            the source
     * @param target
     *            the target
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void copyResource(final String source, final File target) throws IOException
    {
        URL url = StatoolInfosUtils.class.getResource(source);

        File finalTarget;
        if (target.isDirectory())
        {
            String fileName = FilenameUtils.getName(source);
            finalTarget = new File(target, fileName);
        }
        else
        {
            finalTarget = target;
        }

        FileUtils.copyURLToFile(url, finalTarget);
    }

    /**
     * Copy jar resources recursively.
     *
     * @param destDir
     *            the dest dir
     * @param jarConnection
     *            the jar connection
     * @return true, if successful
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private static boolean copyURL(final File target, final JarURLConnection jarConnection) throws IOException
    {
        boolean result;

        JarFile jarFile = jarConnection.getJarFile();

        boolean ended = false;
        Enumeration<JarEntry> iterator = jarFile.entries();
        result = false;
        while (!ended)
        {
            if (iterator.hasMoreElements())
            {
                JarEntry entry = iterator.nextElement();

                if (entry.getName().startsWith(jarConnection.getEntryName()))
                {
                    String fileName = StringUtils.removeStart(entry.getName(), jarConnection.getEntryName());
                    File file = new File(target, fileName);
                    if (entry.isDirectory())
                    {
                        if (!file.exists())
                        {
                            logger.info("MKDIR [{}]", file);
                            file.mkdirs();
                        }
                    }
                    else
                    {
                        URL url = URLUtils.class.getResource("/" + entry.getName());
                        File finalTarget = new File(target, fileName);

                        logger.info("COPY [{}][{}]", url, finalTarget);
                        FileUtils.copyURLToFile(url, finalTarget);
                    }
                }
            }
            else
            {
                ended = true;
                result = true;
            }
        }

        //
        return result;
    }

    /**
     * Equals.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return true, if successful
     */
    public static boolean equals(final URL alpha, final URL bravo)
    {
        boolean result;

        String alphaValue;
        if (alpha == null)
        {
            alphaValue = null;
        }
        else
        {
            alphaValue = alpha.toString();
        }

        String bravoValue;
        if (bravo == null)
        {
            bravoValue = null;
        }
        else
        {
            bravoValue = bravo.toString();
        }

        result = StringUtils.equals(alphaValue, bravoValue);

        //
        return result;
    }

    /**
     * Checks if is valid.
     *
     * @param value
     *            the value
     * @return true, if is valid
     */
    public static boolean isValid(final String value)
    {
        boolean result;

        if ((StringUtils.isBlank(value)) || (!StringUtils.startsWith(value, "http")))
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /**
     * Of.
     *
     * @param path
     *            the path
     * @return the url
     */
    public static URL of(final String path)
    {
        URL result;

        try
        {
            if ((StringUtils.isBlank(path)) || (!StringUtils.startsWith(path, "http")))
            {
                result = null;
            }
            else
            {
                result = new URL(path);
            }
        }
        catch (MalformedURLException exception)
        {
            result = null;
        }

        //
        return result;
    }
}