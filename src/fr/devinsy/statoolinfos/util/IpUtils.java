/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.util;

import java.util.regex.Pattern;

/**
 * The Class IpUtils.
 */
public class IpUtils
{
    public static final Pattern IPV4_PATTERN = Pattern.compile("\\d{0,3}\\.\\d{0,3}\\.\\d{0,3}\\.\\d{0,3}");
    public static final Pattern IPV6_PATTERN = Pattern.compile("([0-9a-f]{1,4}:{1,2}){4,7}([0-9a-f]){1,4}", Pattern.CASE_INSENSITIVE);

    private IpUtils()
    {
    }

    /**
     * Checks if is ipv4.
     *
     * @param value
     *            the value
     * @return true, if is ipv4
     */
    public static boolean isIpv4(final String value)
    {
        boolean result;

        if (value == null)
        {
            result = false;
        }
        else if (value.startsWith("::ffff:"))
        {
            result = true;
        }
        else
        {
            result = value.contains(".");
        }

        //
        return result;
    }

    /**
     * Checks if is ipv6.
     *
     * @param value
     *            the value
     * @return true, if is ipv6
     */
    public static boolean isIpv6(final String value)
    {
        boolean result;

        if (value == null)
        {
            result = false;
        }
        else if (value.startsWith("::ffff:"))
        {
            result = false;
        }
        else
        {
            result = value.contains(":");
        }

        //
        return result;
    }

    /**
     * Checks if is valid ipv4.
     *
     * @param input
     *            the input
     * @return true, if is valid ipv4
     */
    public static boolean isValidIpv4(final String input)
    {
        boolean result;

        result = IPV4_PATTERN.matcher(input).matches();

        //
        return result;
    }

    /**
     * Checks if is ipv6.
     *
     * @param input
     *            the input
     * @return true, if is ipv6
     */
    public static boolean isValidIpv6(final String input)
    {
        boolean result;

        result = IPV6_PATTERN.matcher(input).matches();

        //
        return result;
    }
}