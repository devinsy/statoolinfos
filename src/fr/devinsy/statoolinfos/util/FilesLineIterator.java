/*
 * Copyright (C) 2022-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.util;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class FilesLineIterator.
 */
public class FilesLineIterator implements Iterator<String>
{
    private static Logger logger = LoggerFactory.getLogger(FilesLineIterator.class);

    private Iterator<File> fileIterator;
    private LineIterator lineIterator;
    private boolean newFile;
    private boolean print;

    /**
     * Instantiates a new files line iterator.
     *
     * @param source
     *            the source
     */
    public FilesLineIterator(final File source)
    {
        this(new Files(source));
    }

    /**
     * Instantiates a new files line iterator.
     *
     * @param source
     *            the source
     */
    public FilesLineIterator(final Files source)
    {
        this.fileIterator = source.iterator();
        this.lineIterator = null;
        this.newFile = true;
        setPrintOn();
    }

    /**
     * Close.
     */
    public void close()
    {
        if (this.lineIterator != null)
        {
            this.lineIterator.close();
        }
    }

    /**
     * Forward.
     */
    private void forward()
    {
        boolean ended = false;
        while (!ended)
        {
            try
            {
                if (this.lineIterator == null)
                {
                    if (this.fileIterator.hasNext())
                    {
                        File file = this.fileIterator.next();
                        if (this.print)
                        {
                            System.out.println("Iterating file [" + file.getAbsolutePath() + "]");
                        }
                        this.lineIterator = new LineIterator(file);
                        this.newFile = true;
                    }
                    else
                    {
                        ended = true;
                    }
                }
                else if (this.lineIterator.hasNext())
                {
                    ended = true;
                }
                else
                {
                    this.lineIterator.close();
                    if (this.fileIterator.hasNext())
                    {
                        File file = this.fileIterator.next();
                        if (this.print)
                        {
                            System.out.println("Iterating file [" + file.getAbsolutePath() + "]");
                        }
                        this.lineIterator = new LineIterator(file);
                        this.newFile = true;
                    }
                    else
                    {
                        this.lineIterator = null;
                        ended = true;
                    }
                }
            }
            catch (IOException exception)
            {
                logger.error("ERROR READING.", exception);
                if (this.lineIterator != null)
                {
                    this.lineIterator.close();
                    this.lineIterator = null;
                }
            }
        }
    }

    /**
     * Gets the current file.
     *
     * @return the current file
     */
    public File getCurrentFile()
    {
        File result;

        forward();
        result = this.lineIterator.getFile();

        //
        return result;
    }

    /* (non-Javadoc)
     * @see java.util.Iterator#hasNext()
     */
    @Override
    public boolean hasNext()
    {
        boolean result;

        forward();

        if ((this.lineIterator == null) && (!this.fileIterator.hasNext()))
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /**
     * Checks if is new file.
     *
     * @return true, if is new file
     */
    public boolean isNewFile()
    {
        boolean result;

        result = this.newFile;

        //
        return result;
    }

    /* (non-Javadoc)
     * @see java.util.Iterator#next()
     */
    @Override
    public String next()
    {
        String result;

        try
        {
            forward();

            if (this.lineIterator == null)
            {
                result = null;
            }
            else
            {
                result = this.lineIterator.next();
                this.newFile = false;
            }
        }
        catch (IOException exception)
        {
            throw new IllegalArgumentException("ERROR READING.", exception);
        }

        //
        return result;
    }

    /**
     * Sets the print off.
     */
    public void setPrintOff()
    {
        this.print = false;
    }

    /**
     * Sets the print on.
     */
    public void setPrintOn()
    {
        this.print = true;
    }
}
