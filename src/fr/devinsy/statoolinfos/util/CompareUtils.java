/*
 * Copyright (C) 2020-2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.util;

import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class CompareUtils.
 */
public class CompareUtils
{
    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    public static int compare(final Boolean alpha, final Boolean bravo)
    {
        int result;

        if ((alpha == null) && (bravo == null))
        {
            result = 0;
        }
        else if (alpha == null)
        {
            result = -1;
        }
        else if (bravo == null)
        {
            result = +1;
        }
        else
        {
            result = alpha.compareTo(bravo);
        }

        //
        return result;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    public static int compare(final Double alpha, final Double bravo)
    {
        int result;

        if ((alpha == null) && (bravo == null))
        {
            result = 0;
        }
        else if (alpha == null)
        {
            result = -1;
        }
        else if (bravo == null)
        {
            result = +1;
        }
        else
        {
            result = alpha.compareTo(bravo);
        }

        //
        return result;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    public static int compare(final Integer alpha, final Integer bravo)
    {
        int result;

        if ((alpha == null) && (bravo == null))
        {
            result = 0;
        }
        else if (alpha == null)
        {
            result = -1;
        }
        else if (bravo == null)
        {
            result = +1;
        }
        else
        {
            result = alpha.compareTo(bravo);
        }

        //
        return result;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    public static int compare(final LocalDateTime alpha, final LocalDateTime bravo)
    {
        int result;

        if ((alpha == null) && (bravo == null))
        {
            result = 0;
        }
        else if (alpha == null)
        {
            result = -1;
        }
        else if (bravo == null)
        {
            result = +1;
        }
        else
        {
            result = alpha.compareTo(bravo);
        }

        //
        return result;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    public static int compare(final Long alpha, final Long bravo)
    {
        int result;

        if ((alpha == null) && (bravo == null))
        {
            result = 0;
        }
        else if (alpha == null)
        {
            result = -1;
        }
        else if (bravo == null)
        {
            result = +1;
        }
        else
        {
            result = alpha.compareTo(bravo);
        }

        //
        return result;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    public static int compare(final String alpha, final String bravo)
    {
        int result;

        result = StringUtils.compare(alpha, bravo);

        //
        return result;
    }

    /**
     * Compare ignore case.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    public static int compareIgnoreCase(final String alpha, final String bravo)
    {
        int result;

        result = StringUtils.compareIgnoreCase(alpha, bravo);

        //
        return result;
    }

    /**
     * Compare natural.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    public static int compareNatural(final String alpha, final String bravo)
    {
        int result;

        Pattern pattern = Pattern.compile("(?<left>.*)(?<digits>\\d+)(?<right>.*)");

        Matcher matcherA = pattern.matcher(alpha);
        Matcher matcherB = pattern.matcher(bravo);
        if ((matcherA.find()) && (matcherB.find()))
        {
            result = StringUtils.compare(matcherA.group("left"), matcherB.group("left"));

            if (result == 0)
            {
                Long a = Long.valueOf(matcherA.group("digits"));
                Long b = Long.valueOf(matcherB.group("digits"));

                result = compare(a, b);

                if (result == 0)
                {
                    result = compareNatural(matcherA.group("right"), matcherB.group("right"));
                }
            }
        }
        else
        {
            result = StringUtils.compare(alpha, bravo);
        }

        //
        return result;
    }

    /**
     * Compare reverse.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    public static int compareReverse(final Integer alpha, final Integer bravo)
    {
        int result;

        if ((alpha == null) && (bravo == null))
        {
            result = 0;
        }
        else if (alpha == null)
        {
            result = +1;
        }
        else if (bravo == null)
        {
            result = -1;
        }
        else
        {
            result = alpha.compareTo(bravo);
        }

        //
        return result;
    }

}