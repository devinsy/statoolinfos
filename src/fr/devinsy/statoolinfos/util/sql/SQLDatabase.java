/*
 * Copyright (C) 2013-2022 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.util.sql;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class SQLDatabase.
 */
public class SQLDatabase
{
    private static final Logger logger = LoggerFactory.getLogger(SQLDatabase.class);;

    public enum Driver
    {
        HSQL,
        MARIADB,
        MYSQL,
        NONE,
        POSTGRES,
        SQLITE
    };

    public enum Status
    {
        OPENED,
        CLOSED
    };

    private Status status;
    private Driver driver;
    private String url;
    private String login;
    private String password;
    private Connection connection;

    /**
     * Instantiates a new SQL database.
     *
     * @param url
     *            the url
     * @param login
     *            the login
     * @param password
     *            the password
     */
    public SQLDatabase(final String url, final String login, final String password)
    {
        if (StringUtils.isBlank(url))
        {
            throw new IllegalArgumentException("url is null.");
        }
        else if (StringUtils.isBlank(login))
        {
            throw new IllegalArgumentException("login is null.");
        }
        else
        {
            this.status = Status.CLOSED;
            this.driver = Driver.NONE;
            this.url = url;
            this.login = login;
            this.password = password;
            this.connection = null;
        }
    }

    /**
     * Close.
     */
    public void close()
    {
        if (this.connection != null)
        {
            try
            {
                this.connection.close();
            }
            catch (SQLException exception)
            {
                logger.error("Error closing database.", exception);
            }
            finally
            {
                this.connection = null;
            }
        }

        this.status = Status.CLOSED;
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     * @throws SQLException
     *             the SQL exception
     */
    public Connection getConnection() throws SQLException
    {
        Connection result;

        if (isClosed())
        {
            throw new SQLException("Database not opened.");
        }
        else
        {
            if (this.connection != null)
            {
                result = this.connection;
            }
            else
            {
                throw new IllegalArgumentException("Connection not initialized.");
            }
        }

        //
        return result;

    }

    /**
     * Gets the database size.
     *
     * @return the database size
     */
    public long getDatabaseSize()
    {
        long result;

        try
        {
            if (isOpened())
            {
                switch (this.driver)
                {
                    case HSQL:
                    {
                        result = 0;
                    }
                    break;

                    case MARIADB:
                    case MYSQL:
                    {
                        String sql = "SELECT SUM(data_length + index_length)  FROM information_schema.tables where table_schema = (select database());";
                        result = queryNumber(sql);
                    }
                    break;

                    case POSTGRES:
                    {
                        String sql = "SELECT pg_database_size(pg_database.datname) FROM pg_database WHERE pg_database.datname = (select current_catalog);";
                        result = queryNumber(sql);
                    }
                    break;

                    case SQLITE:
                    {
                        File file = new File(this.url.replace("jdbc:sqlite:", ""));
                        result = file.length();
                    }
                    break;

                    case NONE:
                    default:
                        result = 0;
                }
            }
            else
            {
                result = 0;
            }
        }
        catch (SQLException exception)
        {
            logger.error("ERROR querying database size.", exception);
            result = 0;
        }

        //
        return result;
    }

    /**
     * Gets the driver.
     *
     * @return the driver
     */
    public Driver getDriver()
    {
        Driver result;

        result = this.driver;

        //
        return result;
    }

    /**
     * Gets the login.
     * 
     * @return the login
     */
    public String getLogin()
    {
        String result;

        result = this.login;

        //
        return result;
    }

    /**
     * Gets the password.
     * 
     * @return the password
     */
    public String getPassword()
    {
        String result;

        result = this.password;

        //
        return result;
    }

    /**
     * Gets the url.
     * 
     * @return the url
     */
    public String getUrl()
    {
        String result;

        result = this.url;

        //
        return result;
    }

    /**
     * Checks if is closed.
     *
     * @return true, if is closed
     */
    public boolean isClosed()
    {
        boolean result;

        if (this.status == Status.CLOSED)
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Checks if is opened.
     *
     * @return true, if is opened
     */
    public boolean isOpened()
    {
        boolean result;

        result = !isClosed();

        //
        return result;
    }

    /**
     * Open.
     *
     * @throws SQLException
     *             the SQL exception
     */
    public void open() throws SQLException
    {
        try
        {
            //
            close();

            //
            if (this.url == null)
            {
                throw new IllegalArgumentException("Undefined source.");
            }
            else
            {
                if (this.url.startsWith("jdbc:sqlite:"))
                {
                    this.driver = Driver.SQLITE;
                    Class.forName("org.sqlite.JDBC");
                }
                else if (this.url.startsWith("jdbc:postgresql:"))
                {
                    Class.forName("org.postgresql.Driver");
                    this.driver = Driver.POSTGRES;
                }
                else if (this.url.startsWith("jdbc:mysql:"))
                {
                    Class.forName("com.mysql.jdbc.Driver");
                    this.driver = Driver.MYSQL;
                }
                else if (this.url.startsWith("jdbc:mariadb:"))
                {
                    Class.forName("org.mariadb.jdbc.Driver");
                    this.driver = Driver.MARIADB;
                }
                else
                {
                    throw new SQLException("Unknown database type.");
                }

                this.connection = DriverManager.getConnection(this.url, this.login, this.password);
                this.status = Status.OPENED;
                logger.info("Single connection opened with [{}].", this.url);
            }
        }
        catch (ClassNotFoundException exception)
        {
            throw new SQLException("ERROR with driver name.", exception);
        }
    }

    /**
     * Query number.
     *
     * @param sql
     *            the sql
     * @return the long
     * @throws SQLException
     *             the SQL exception
     */
    public long queryNumber(final String sql) throws SQLException
    {
        long result;

        if (isClosed())
        {
            throw new ClosedDatabaseException();
        }
        else
        {
            Connection connection = null;
            Statement statement = null;
            ResultSet resultSet = null;
            try
            {
                connection = getConnection();
                connection.setAutoCommit(true);
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);

                resultSet.next();
                result = resultSet.getLong(1);
            }
            finally
            {
                SQLUtils.closeQuietly(statement, resultSet);
            }
        }

        //
        return result;
    }

    /**
     * Sets the login.
     * 
     * @param login
     *            the new login
     */
    public void setLogin(final String login)
    {
        this.login = login;
    }

    /**
     * Sets the password.
     * 
     * @param password
     *            the new password
     */
    public void setPassword(final String password)
    {
        this.password = password;
    }

    /**
     * Sets the url.
     * 
     * @param url
     *            the new url
     */
    public void setUrl(final String url)
    {
        this.url = url;
    }
}
