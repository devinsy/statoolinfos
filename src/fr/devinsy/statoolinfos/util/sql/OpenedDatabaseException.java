/*
 * Copyright (C) 2018-2022 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.util.sql;

import java.sql.SQLException;

/**
 * The Class OpenedDatabaseException.
 */
public class OpenedDatabaseException extends SQLException
{
    private static final long serialVersionUID = -8218498455798186368L;

    /**
     * Instantiates a new opened database exception.
     */
    public OpenedDatabaseException()
    {
        super("Invalid database status for this operation: opened.");
    }

    /**
     * Instantiates a new opened database exception.
     *
     * @param message
     *            the message
     */
    public OpenedDatabaseException(final String message)
    {
        super(message);
    }

    /**
     * Instantiates a new opened database exception.
     *
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public OpenedDatabaseException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Instantiates a new opened database exception.
     *
     * @param cause
     *            the cause
     */
    public OpenedDatabaseException(final Throwable cause)
    {
        super(cause);
    }
}
