/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.util.sql;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.statoolinfos.core.StatoolInfosException;

/**
 * The Class SQLUtils.
 */
public final class SQLUtils
{
    /**
     * Instantiates a new SQL utils.
     */
    private SQLUtils()
    {
    }

    /**
     * Close quietly.
     *
     * @param connection
     *            the connection
     */
    public static void closeQuietly(final Connection connection)
    {
        if (connection != null)
        {
            try
            {
                connection.close();
            }
            catch (SQLException exception)
            {
                exception.printStackTrace();
            }
        }
    }

    /**
     * Close quietly.
     *
     * @param connection
     *            the connection
     * @param statement
     *            the statement
     * @param resultSet
     *            the result set
     */
    public static void closeQuietly(final Connection connection, final Statement statement, final ResultSet resultSet)
    {
        //
        if (connection != null)
        {
            try
            {
                connection.close();
            }
            catch (SQLException exception)
            {
                exception.printStackTrace();
            }
        }

        //
        if (statement != null)
        {
            try
            {
                statement.close();
            }
            catch (SQLException exception)
            {
                exception.printStackTrace();
            }
        }

        //
        if (resultSet != null)
        {
            try
            {
                resultSet.close();
            }
            catch (SQLException exception)
            {
                exception.printStackTrace();
            }
        }
    }

    /**
     * Close quietly.
     *
     * @param statement
     *            the statement
     * @param resultSet
     *            the result set
     */
    public static void closeQuietly(final Statement statement, final ResultSet resultSet)
    {
        //
        if (statement != null)
        {
            try
            {
                statement.close();
            }
            catch (SQLException exception)
            {
                exception.printStackTrace();
            }
        }

        //
        if (resultSet != null)
        {
            try
            {
                resultSet.close();
            }
            catch (SQLException exception)
            {
                exception.printStackTrace();
            }
        }
    }

    /**
     * Gets the connexion.
     *
     * @param serverUrl
     *            the server url
     * @param databaseName
     *            the database name
     * @param login
     *            the login
     * @param password
     *            the password
     * @return the connexion
     * @throws StatoolInfosException
     *             the agir statool exception
     */
    public static Connection getConnexion(final String serverUrl, final String databaseName, final String login, final String password) throws StatoolInfosException
    {
        Connection result;

        try
        {
            if (StringUtils.isBlank(serverUrl))
            {
                throw new IllegalArgumentException("Undefined server URL.");
            }
            else if (StringUtils.isBlank(databaseName))
            {
                throw new IllegalArgumentException("Undefined database name.");
            }
            else if (StringUtils.isBlank(login))
            {
                throw new IllegalArgumentException("Undefined database login.");
            }
            else
            {
                Class.forName("com.mysql.jdbc.Driver").getConstructor().newInstance();
                result = DriverManager.getConnection(serverUrl + databaseName + "?useUnicode=true&amp;amp;characterEncoding=utf8", login, password);
            }
        }
        catch (SQLException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException
                | ClassNotFoundException exception)
        {
            throw new StatoolInfosException("Database connection failed: " + exception.getMessage(), exception);
        }

        //
        return result;
    }

    /**
     * Gets the nullable long.
     *
     * @param resultSet
     *            the result set
     * @param index
     *            the index
     * @return the nullable long
     * @throws SQLException
     *             the SQL exception
     */
    public static Long getNullableLong(final ResultSet resultSet, final int index) throws SQLException
    {
        Long result;

        if (resultSet.getObject(index) == null)
        {
            result = null;
        }
        else
        {
            result = resultSet.getLong(index);
        }

        //
        return result;
    }

    /**
     * To date time.
     * 
     * @param source
     *            the source
     * @return the date time
     */
    public static LocalDateTime toDateTime(final java.sql.Timestamp source)
    {
        LocalDateTime result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            long seconds = source.getTime() / 1000;
            long nanos = (source.getTime() - seconds * 1000) * 1000000;
            result = LocalDateTime.ofEpochSecond(seconds, (int) nanos, ZoneOffset.UTC);
        }

        //
        return result;
    }

    /**
     * To date time.
     *
     * @param source
     *            the source
     * @return the local date time
     */
    public static LocalDateTime toLocalDateTime(final java.sql.Timestamp source)
    {
        LocalDateTime result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            long seconds = source.getTime() / 1000;
            long nanos = (source.getTime() - seconds * 1000) * 1000000;
            result = LocalDateTime.ofEpochSecond(seconds, (int) nanos, ZoneOffset.UTC);
        }

        //
        return result;
    }

    /**
     * To timestamp.
     * 
     * @param source
     *            the source
     * @return the java.sql. timestamp
     */
    public static java.sql.Timestamp toTimestamp(final Date source)
    {
        java.sql.Timestamp result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = new java.sql.Timestamp(source.getTime() * 1000);
        }

        //
        return result;
    }

    /**
     * To timestamp.
     * 
     * @param source
     *            the source
     * @return the java.sql. timestamp
     */
    public static java.sql.Timestamp toTimestamp(final LocalDateTime source)
    {
        java.sql.Timestamp result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            long seconds = source.toEpochSecond(ZoneOffset.UTC);
            long milliseconds = source.getNano() / 1000000;
            result = new java.sql.Timestamp(seconds * 1000 + milliseconds);
        }

        //
        return result;
    }
}
