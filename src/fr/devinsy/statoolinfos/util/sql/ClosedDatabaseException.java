/*
 * Copyright (C) 2018-2022 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.util.sql;

import java.sql.SQLException;

/**
 * The Class ClosedDatabaseException.
 */
public class ClosedDatabaseException extends SQLException
{
    private static final long serialVersionUID = 9220220563426218282L;

    /**
     * Instantiates a new closed database exception.
     */
    public ClosedDatabaseException()
    {
        super("Invalid database status for this operation: closed.");
    }

    /**
     * Instantiates a new closed database exception.
     *
     * @param message
     *            the message
     */
    public ClosedDatabaseException(final String message)
    {
        super(message);
    }

    /**
     * Instantiates a new closed database exception.
     *
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public ClosedDatabaseException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Instantiates a new closed database exception.
     *
     * @param cause
     *            the cause
     */
    public ClosedDatabaseException(final Throwable cause)
    {
        super(cause);
    }
}
