/*
 * Copyright (C) 2020 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.util;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class BuildInformation.
 */
public class BuildInformation
{
    private static final Logger logger = LoggerFactory.getLogger(BuildInformation.class);

    private static String BUILD_INFORMATION_FILE = "/fr/devinsy/statoolinfos/build_information.properties";

    private static class SingletonHolder
    {
        private static final BuildInformation instance = new BuildInformation();
    }

    private String productName;
    private String majorRevision;
    private String minorRevision;
    private String buildNumber;
    private String snapshotRevision;
    private String buildDate;
    private String generator;
    private String buildAuthor;

    /**
     * Instantiates a new builds the information.
     */
    private BuildInformation()
    {
        Properties build = new Properties();

        try
        {
            //
            URL buildInformationFile = BuildInformation.class.getResource(BUILD_INFORMATION_FILE);

            if (buildInformationFile != null)
            {
                build.load(BuildInformation.class.getResource(BUILD_INFORMATION_FILE).openStream());
            }

            //
            this.productName = build.getProperty("product.name", "DevInProgress");
            this.majorRevision = build.getProperty("product.revision.major", "d");
            this.minorRevision = build.getProperty("product.revision.minor", "e");
            this.buildNumber = build.getProperty("product.revision.build", "v");
            this.snapshotRevision = build.getProperty("product.revision.snapshot", "").replace("SNAPSHOT_", "");
            this.buildDate = build.getProperty("product.revision.date", "today");
            this.generator = build.getProperty("product.revision.generator", "n/a");
            this.buildAuthor = build.getProperty("product.revision.author", "n/a");

        }
        catch (IOException exception)
        {
            logger.error("Error loading the build.properties file: " + exception.getMessage());
            logger.error(ExceptionUtils.getStackTrace(exception));

            this.productName = "n/a";
            this.majorRevision = "n/a";
            this.minorRevision = "n/a";
            this.buildNumber = "n/a";
            this.buildDate = "n/a";
            this.generator = "n/a";
            this.buildAuthor = "n/a";
        }
    }

    /**
     * Builds the author.
     * 
     * @return the string
     */
    public String buildAuthor()
    {
        return this.buildAuthor;
    }

    /**
     * Builds the date.
     * 
     * @return the string
     */
    public String buildDate()
    {
        return this.buildDate;
    }

    /**
     * Builds the number.
     * 
     * @return the string
     */
    public String buildNumber()
    {
        return this.buildNumber;
    }

    /**
     * Generator.
     * 
     * @return the string
     */
    public String generator()
    {
        return this.generator;
    }

    /**
     * Major revision.
     * 
     * @return the string
     */
    public String majorRevision()
    {
        return this.majorRevision;
    }

    /**
     * Minor revision.
     * 
     * @return the string
     */
    public String minorRevision()
    {
        return this.minorRevision;
    }

    /**
     * Product name.
     * 
     * @return the string
     */
    public String productName()
    {
        return this.productName;
    }

    /**
     * Version name.
     *
     * @return the string
     */
    public String protocol()
    {
        String result;

        result = String.format("%s %s.%s.%s", this.productName, this.majorRevision, this.minorRevision, this.buildNumber);

        //
        return result;
    }

    /**
     * Snapshot revision.
     *
     * @return the string
     */
    public String snapshotRevision()
    {
        return this.snapshotRevision;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String result;

        result = String.format("%s %s.%s.%s built on %s by %s", this.productName, this.majorRevision, this.minorRevision, this.buildNumber, this.buildDate, this.buildAuthor);

        //
        return result;
    }

    /**
     * Version.
     * 
     * @return the string
     */
    public String version()
    {
        String result;

        result = String.format("%s.%s.%s%s", this.majorRevision, this.minorRevision, this.buildNumber, this.snapshotRevision);

        //
        return result;
    }

    /**
     * Instance.
     * 
     * @return the builds the information
     */
    public static BuildInformation instance()
    {
        return SingletonHolder.instance;
    }

    /**
     * Checks if is defined.
     * 
     * @return true, if is defined
     */
    public static boolean isDefined()
    {
        boolean result;

        if (BuildInformation.class.getResource(BUILD_INFORMATION_FILE) == null)
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }
}
