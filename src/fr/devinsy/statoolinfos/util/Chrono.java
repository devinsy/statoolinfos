/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.util;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * The Class Chrono.
 */
public class Chrono
{
    public static final long DEFAULT_MESSAGE_INTERVAL = 60;

    private LocalDateTime start;
    private long previousDurationMessage;
    private long messageInterval;

    /**
     * Instantiates a new time keeper.
     */
    public Chrono()
    {
        reset();
        this.messageInterval = DEFAULT_MESSAGE_INTERVAL;
    }

    /**
     * Check message.
     *
     * @param current
     *            the current
     * @param max
     *            the max
     */
    public void checkMessage(final long current, final long max)
    {
        long currentDuration = duration();

        if ((currentDuration % this.messageInterval == 0) && (currentDuration != this.previousDurationMessage))
        {
            this.previousDurationMessage = currentDuration;
            System.out.println(format());
            System.out.println(String.format("%s %d/%d", format(), current, max));
        }
    }

    /**
     * Duration.
     *
     * @return the long
     */
    public long duration()
    {
        long result;

        result = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) - this.start.toEpochSecond(ZoneOffset.UTC);

        //
        return result;
    }

    /**
     * Format.
     *
     * @return the string
     */
    public String format()
    {
        String result;

        if (this.start == null)
        {
            result = "n/a";
        }
        else
        {
            LocalDateTime end = LocalDateTime.now();
            Duration duration = Duration.between(this.start, end);
            result = format(duration);
        }

        //
        return result;
    }

    /**
     * Reset.
     */
    public void reset()
    {
        this.start = null;
        this.previousDurationMessage = 0;
    }

    /**
     * Sets the check intervale.
     *
     * @param seconds
     *            the new check intervale
     */
    public void setMessageInterval(final long seconds)
    {
        this.messageInterval = seconds;
    }

    /**
     * Start.
     *
     * @return the chrono
     */
    public Chrono start()
    {
        Chrono result;

        this.start = LocalDateTime.now();
        this.previousDurationMessage = 0L;

        result = this;

        //
        return result;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString()
    {
        String result;

        result = format();

        //
        return result;
    }

    /**
     * Format.
     *
     * @param duration
     *            the duration
     * @return the string
     */
    public static String format(final Duration duration)
    {
        String result;

        long seconds = duration.toSecondsPart();
        long minutes = duration.toMinutesPart();
        long hours = duration.toHoursPart();

        result = String.format("%02d:%02d:%02d", hours, minutes, seconds);

        //
        return result;
    }
}
