/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.regex.Pattern;

/**
 * The Class Files.
 */
public class Files extends ArrayList<File>
{
    private static final long serialVersionUID = -8116253751377991944L;

    /**
     * Instantiates a new files.
     */
    public Files()
    {
        super();
    }

    /**
     * Instantiates a new files.
     *
     * @param source
     *            the source
     */
    public Files(final Collection<? extends File> source)
    {
        super();
        if (source != null)
        {
            addAll(source);
        }
    }

    /**
     * Instantiates a new files.
     *
     * @param source
     *            the source
     */
    public Files(final File source)
    {
        super();
        if (source != null)
        {
            add(source);
        }
    }

    /**
     * Instantiates a new files.
     *
     * @param initialCapacity
     *            the initial capacity
     */
    public Files(final int initialCapacity)
    {
        super(initialCapacity);
    }

    /**
     * Keep directories.
     *
     * @return the files
     */
    public Files keepDirectoryType()
    {
        Files result;

        Iterator<File> iterator = iterator();
        while (iterator.hasNext())
        {
            File file = iterator.next();
            if (!file.isDirectory())
            {
                iterator.remove();
            }
        }

        result = this;

        //
        return result;
    }

    /**
     * Keep file type.
     *
     * @return the files
     */
    public Files keepFileType()
    {
        Files result;

        Iterator<File> iterator = iterator();
        while (iterator.hasNext())
        {
            File file = iterator.next();
            if (!file.isFile())
            {
                iterator.remove();
            }
        }

        result = this;

        //
        return result;
    }

    /**
     * Keep.
     *
     * @param regex
     *            the regex
     * @return the files
     */
    public Files keepName(final String regex)
    {
        Files result;

        Pattern pattern = Pattern.compile(regex);

        Iterator<File> iterator = iterator();
        while (iterator.hasNext())
        {
            File file = iterator.next();

            if (!pattern.matcher(file.getName()).matches())
            {
                iterator.remove();
            }
        }

        result = this;

        //
        return result;
    }

    /**
     * Keep by path.
     *
     * @param regex
     *            the regex
     * @return the files
     */
    public Files keepPath(final String regex)
    {
        Files result;

        Pattern pattern = Pattern.compile(regex);

        Iterator<File> iterator = iterator();
        while (iterator.hasNext())
        {
            File file = iterator.next();
            if (!pattern.matcher(file.getAbsolutePath()).matches())
            {
                iterator.remove();
            }
        }

        result = this;

        //
        return result;
    }

    /**
     * Removes the containing.
     *
     * @param token
     *            the token
     * @return the files
     */
    public Files removeContaining(final String token)
    {
        Files result;

        Iterator<File> iterator = iterator();
        while (iterator.hasNext())
        {
            File file = iterator.next();
            if (file.getName().contains(token))
            {
                iterator.remove();
            }
        }

        result = this;

        //
        return result;
    }

    /**
     * Removes the file type.
     *
     * @return the files
     */
    public Files removeFileType()
    {
        Files result;

        Iterator<File> iterator = iterator();
        while (iterator.hasNext())
        {
            File file = iterator.next();
            if (file.isFile())
            {
                iterator.remove();
            }
        }

        result = this;

        //
        return result;
    }

    /**
     * Removes the hidden.
     *
     * @return the files
     */
    public Files removeHidden()
    {
        Files result;

        Iterator<File> iterator = iterator();
        while (iterator.hasNext())
        {
            File file = iterator.next();
            if (file.getName().startsWith("."))
            {
                iterator.remove();
            }
        }

        result = this;

        //
        return result;
    }

    /**
     * Sort.
     *
     * @param sorting
     *            the sorting
     * @return the files
     */
    public Files sort(final FileComparator.Sorting sorting)
    {
        Files result;

        sort(new FileComparator(sorting));

        result = this;

        //
        return result;
    }

    /**
     * Sort by name.
     *
     * @return the files
     */
    public Files sortByName()
    {
        Files result;

        result = sort(FileComparator.Sorting.NAME);

        //
        return result;
    }

    /**
     * Sort by pathname.
     *
     * @return the files
     */
    public Files sortByPathname()
    {
        Files result;

        result = sort(FileComparator.Sorting.NAME);

        //
        return result;
    }

    /**
     * Of.
     *
     * @param source
     *            the source
     * @return the files
     */
    public static Files of(final File source)
    {
        Files result;

        result = new Files();
        if (source != null)
        {
            for (File file : source.listFiles())
            {
                result.add(file);
            }
        }

        //
        return result;
    }
}
