/*
 * Copyright (C) 2022 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.util;

import java.io.File;
import java.util.Iterator;

/**
 * The Class FileLines.
 */
public class FileLines implements Iterable<String>
{
    private File source;

    /**
     * Instantiates a new file lines.
     *
     * @param source
     *            the source
     */
    public FileLines(final File source)
    {
        this.source = source;
    }

    /* (non-Javadoc)
     * @see java.lang.Iterable#iterator()
     */
    @Override
    public Iterator<String> iterator()
    {
        Iterator<String> result;

        result = new FilesLineIterator(this.source);

        //
        return result;
    }
}
