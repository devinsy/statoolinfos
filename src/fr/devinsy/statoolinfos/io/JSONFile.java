/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import fr.devinsy.statoolinfos.core.Categories;
import fr.devinsy.statoolinfos.core.Federation;
import fr.devinsy.statoolinfos.core.Organization;
import fr.devinsy.statoolinfos.core.Organizations;
import fr.devinsy.statoolinfos.core.Service;
import fr.devinsy.statoolinfos.core.Services;
import fr.devinsy.statoolinfos.properties.PathProperty;
import fr.devinsy.statoolinfos.properties.PathPropertyList;
import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringsUtils;

/**
 * The Class JSONFile.
 */
public class JSONFile
{
    /**
     * Escape.
     *
     * @param source
     *            the source
     * @return the string
     */
    public static String escapeJSON(final String source)
    {
        String result;

        result = StringEscapeUtils.escapeJson(source);

        //
        return result;
    }

    /**
     * Save.
     *
     * @param file
     *            the file
     * @param federation
     *            the federation
     * @throws UnsupportedEncodingException
     *             the unsupported encoding exception
     * @throws FileNotFoundException
     *             the file not found exception
     */
    public static void save(final File file, final Federation federation) throws UnsupportedEncodingException, FileNotFoundException
    {
        StringList lines = new StringList();

        lines.appendln("{ \"federation\" : ");

        lines.addAll(toJSON(federation));

        lines.removeLast();
        lines.appendln(",");
        lines.appendln("\"organizations\" : [");

        for (Organization organization : federation.getOrganizations())
        {
            lines.addAll(toJSON(organization));
            lines.removeLast();

            lines.appendln(",");
            lines.append("\"services\" : [");

            for (Service service : organization.getServices())
            {
                lines.addAll(toJSON(service));
                lines.appendln(",");
            }
            if (!organization.getServices().isEmpty())
            {
                lines.removeLast(2);
            }
            lines.appendln();
            lines.appendln("]");
            lines.append("}");
            lines.appendln(",");
        }
        if (!federation.getOrganizations().isEmpty())
        {
            lines.removeLast(2);
            lines.appendln();
        }

        lines.appendln("]");

        lines.appendln("}");
        lines.appendln("}");

        StringsUtils.writeToFile(file, lines);
    }

    /**
     * Save.
     *
     * @param file
     *            the file
     * @param organizations
     *            the organizations
     * @throws UnsupportedEncodingException
     *             the unsupported encoding exception
     * @throws FileNotFoundException
     *             the file not found exception
     */
    public static void save(final File file, final Organizations organizations) throws UnsupportedEncodingException, FileNotFoundException
    {
        StringList lines = new StringList();

        lines.appendln("{ \"organizations\" : [");

        for (Organization organization : organizations)
        {
            lines.addAll(toJSON(organization));
            lines.append(",");
        }
        if (!organizations.isEmpty())
        {
            lines.removeLast();
        }

        lines.appendln("] }");

        StringsUtils.writeToFile(file, lines);
    }

    /**
     * Save.
     *
     * @param file
     *            the file
     * @param services
     *            the services
     * @throws UnsupportedEncodingException
     *             the unsupported encoding exception
     * @throws FileNotFoundException
     *             the file not found exception
     */
    public static void save(final File file, final Services services) throws UnsupportedEncodingException, FileNotFoundException
    {
        StringList lines = new StringList();

        lines.appendln("{ \"services\" : [");

        for (Service service : services)
        {
            lines.addAll(toJSON(service));
            lines.append(",");
        }
        if (!services.isEmpty())
        {
            lines.removeLast();
        }

        lines.appendln("] }");

        StringsUtils.writeToFile(file, lines);
    }

    /**
     * To JSON.
     *
     * @param properties
     *            the properties
     * @return the string list
     */
    public static StringList toJSON(final PathPropertyList properties)
    {
        StringList result;

        result = new StringList();

        result.append("{");

        if (properties != null)
        {
            boolean firstDone = false;
            for (PathProperty property : properties)
            {
                if (StringUtils.isNotBlank(property.getValue()))
                {
                    firstDone = true;
                    result.append("\"");
                    result.append(StringEscapeUtils.escapeJson(property.getPath()));
                    result.append("\" : \"");
                    result.append(StringEscapeUtils.escapeJson(property.getValue()));
                    result.append("\"");
                    result.append(",");
                }
            }
            if (firstDone)
            {
                result.removeLast();
            }
        }
        result.append("}");

        //
        return result;
    }

    /**
     * To JSON.
     *
     * @param service
     *            the service
     * @param categories
     *            the categories
     * @return the string list
     */
    public static StringList toJSON(final Service service, final Categories categories)
    {
        StringList result;

        result = toJSON(service);

        //
        result.removeLast();
        result.append(",");

        //
        String organizationName;
        if (service.getOrganization() == null)
        {
            organizationName = "";
        }
        else
        {
            organizationName = service.getOrganization().getName();
        }
        result.append("\"");
        result.append("organization.name");
        result.append("\" : \"");
        result.append(organizationName);
        result.append("\"");
        result.append(",");

        //
        result.append("\"");
        result.append("software.categories");
        result.append("\" : \"");
        result.append(StringEscapeUtils.escapeJson(categories.findBySoftware(service.getSoftwareName()).toString()));
        result.append("\"");

        result.append("}");

        //
        return result;
    }
}
