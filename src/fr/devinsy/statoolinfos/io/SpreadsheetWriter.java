/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.io;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * The Interface SpreadsheetWriter.
 */
public interface SpreadsheetWriter
{
    /**
     * Close.
     *
     * @throws FileNotFoundException
     *             the file not found exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    void close() throws FileNotFoundException, IOException;

    /**
     * Write cell.
     *
     * @param content
     *            the content
     */
    void writeCell(String content);

    /**
     * Write endpage.
     */
    void writeEndpage();

    /**
     * Write end row.
     */
    void writeEndRow();
}
