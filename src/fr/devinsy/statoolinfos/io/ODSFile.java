/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.io;

import java.io.File;
import java.io.IOException;

import fr.devinsy.statoolinfos.core.Categories;
import fr.devinsy.statoolinfos.core.Organizations;
import fr.devinsy.statoolinfos.core.Services;

/**
 * The Class ODSFile.
 */
public class ODSFile
{
    public static final int MAX_LINE_SIZE = 1024;

    protected enum Status
    {
        MANDATORY,
        OPTIONAL
    }

    /**
     * Save.
     *
     * @param file
     *            the file
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void save(final File file, final Organizations source) throws IOException
    {
        ODSWriter out = null;
        try
        {
            out = new ODSWriter(file);
            CSVFile.write(out, source);
        }
        finally
        {
            if (out != null)
            {
                out.close();
            }
        }
    }

    /**
     * Save.
     *
     * @param file
     *            the file
     * @param source
     *            the source
     * @param categories
     *            the categories
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void save(final File file, final Services source, final Categories categories) throws IOException
    {
        ODSWriter out = null;
        try
        {
            out = new ODSWriter(file);
            CSVFile.write(out, source, categories);
        }
        finally
        {
            if (out != null)
            {
                out.close();
            }
        }
    }
}
