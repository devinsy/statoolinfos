/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.io;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.statoolinfos.core.Categories;
import fr.devinsy.statoolinfos.core.Organization;
import fr.devinsy.statoolinfos.core.Organizations;
import fr.devinsy.statoolinfos.core.Service;
import fr.devinsy.statoolinfos.core.Services;
import fr.devinsy.statoolinfos.properties.PathProperty;
import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringSet;

/**
 * The Class CSVFile.
 */
public class CSVFile
{
    public static final int MAX_LINE_SIZE = 4096;
    public static final String SEPARATOR = ";";

    /**
     * Save.
     *
     * @param file
     *            the file
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void save(final File file, final Organizations source) throws IOException
    {
        CSVWriter out = null;
        try
        {
            out = new CSVWriter(file, ';');

            write(out, source);
        }
        finally
        {
            out.close();
        }
    }

    /**
     * Save.
     *
     * @param file
     *            the file
     * @param source
     *            the source
     * @param categories
     *            the categories
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void save(final File file, final Services source, final Categories categories) throws IOException
    {
        CSVWriter out = null;
        try
        {
            out = new CSVWriter(file, ';');
            write(out, source, categories);
        }
        finally
        {
            out.close();
        }
    }

    /**
     * Write.
     *
     * @param out
     *            the out
     * @param organizations
     *            the organizations
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void write(final SpreadsheetWriter out, final Organizations organizations) throws IOException
    {
        // Build label list.
        StringList labels = new StringList(100);
        StringSet dejavu = new StringSet(100);
        StringList prefixes = new StringList("file", "organization", "crawl");
        for (String prefix : prefixes)
        {
            for (Organization organization : organizations)
            {
                for (PathProperty property : organization.getByPrefix(prefix))
                {
                    if (!dejavu.contains(property.getPath()))
                    {
                        labels.add(property.getPath());
                        dejavu.add(property.getPath());
                    }
                }
            }
        }
        dejavu.clear();

        // Write label line.
        for (String label : labels)
        {
            out.writeCell(label);
        }
        out.writeEndRow();

        // Write organization lines.
        for (Organization organization : organizations)
        {
            for (String label : labels)
            {
                String value = organization.get(label);
                out.writeCell(value);
            }
            out.writeEndRow();
        }
    }

    /**
     * Write.
     *
     * @param out
     *            the out
     * @param services
     *            the services
     * @param categories
     *            the categories
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void write(final SpreadsheetWriter out, final Services services, final Categories categories) throws IOException
    {
        // Build label list.
        StringList labels = new StringList(100);
        StringSet dejavu = new StringSet(100);
        StringList prefixes = new StringList("file", "service", "host", "software", "crawl");
        for (String prefix : prefixes)
        {
            for (Service service : services)
            {
                for (PathProperty property : service.getByPrefix(prefix))
                {
                    if (!dejavu.contains(property.getPath()))
                    {
                        labels.add(property.getPath());
                        dejavu.add(property.getPath());
                    }
                }
            }
        }
        dejavu.clear();
        int index = labels.indexOf("service.name");
        labels.add(index, "organization.name");
        index = labels.indexOf("software.name");
        labels.add(index, "software.categories");

        // Write label line.
        for (String label : labels)
        {
            out.writeCell(label);
        }
        out.writeEndRow();

        // Write service lines.
        for (Service service : services)
        {
            for (String label : labels)
            {
                String value;
                if (StringUtils.equals(label, "organization.name"))
                {
                    value = service.getOrganization().getName();
                }
                else if (StringUtils.equals(label, "software.categories"))
                {
                    value = categories.findBySoftware(service.getSoftwareName()).toString();
                }
                else
                {
                    value = service.get(label);
                }

                out.writeCell(value);
            }
            out.writeEndRow();
        }
    }
}
