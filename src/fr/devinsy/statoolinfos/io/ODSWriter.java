/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

/**
 * The Class ODSWriter.
 */
public class ODSWriter implements SpreadsheetWriter
{
    private File file;
    private SpreadSheet workbook;
    private Sheet currentSheet;
    private int currentSheetIndex;
    private int currentRow;
    private int currentColumn;

    /**
     * Instantiates a new ODS writer.
     *
     * @param file
     *            the file
     */
    public ODSWriter(final File file)
    {
        this.file = file;
        this.workbook = SpreadSheet.createEmpty(new DefaultTableModel());

        this.workbook.getSheet(0).setName(file.getName());

        this.currentSheetIndex = 0;
        this.currentSheet = this.workbook.getSheet(this.currentSheetIndex);
        this.currentRow = 0;
        this.currentColumn = 0;
    }

    /**
     * Close.
     *
     * @throws FileNotFoundException
     *             the file not found exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    public void close() throws FileNotFoundException, IOException
    {
        this.workbook.saveAs(this.file);
    }

    /**
     * Write cell.
     *
     * @param content
     *            the content
     */
    @Override
    public void writeCell(final String content)
    {
        this.currentSheet.ensureColumnCount(this.currentColumn + 1);
        this.currentSheet.ensureRowCount(this.currentRow + 1);

        if (NumberUtils.isCreatable(content))
        {
            this.currentSheet.getCellAt(this.currentColumn, this.currentRow).setValue(Double.valueOf(content));
        }
        else
        {
            this.currentSheet.getCellAt(this.currentColumn, this.currentRow).setValue(StringUtils.defaultIfBlank(content, ""));
        }

        this.currentColumn += 1;
    }

    /**
     * Write endpage.
     */
    @Override
    public void writeEndpage()
    {
    }

    /**
     * Write end row.
     */
    @Override
    public void writeEndRow()
    {
        this.currentRow += 1;
        this.currentColumn = 0;
    }
}
