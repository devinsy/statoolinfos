/*
 * Copyright (C) 2020-2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.checker;

import java.util.ArrayList;
import java.util.regex.Pattern;

import fr.devinsy.statoolinfos.core.Service.Status;

/**
 * The Class PropertyChecks.
 */
public class PropertyChecks extends ArrayList<PropertyCheck>
{
    private static final long serialVersionUID = 64500205250126257L;

    /**
     * Instantiates a new property checks.
     */
    public PropertyChecks()
    {
        super();
    }

    /**
     * Extract active lines.
     *
     * @return the property checks
     */
    public PropertyChecks getActiveLines()
    {
        PropertyChecks result;

        result = new PropertyChecks();

        Pattern pattern = Pattern.compile("^(\\s*[^#\\s]|#\\s*\\[).*$");

        for (PropertyCheck check : this)
        {
            if (pattern.matcher(check.getLine()).matches())
            {
                result.add(check);
            }
        }

        //
        return result;
    }

    /**
     * Gets the alert count.
     *
     * @return the alert count
     */
    public int getAlertCount()
    {
        int result;

        result = 0;
        for (PropertyCheck check : this)
        {
            if (check.getStatus() == Status.ALERT)
            {
                result += 1;
            }
        }

        //
        return result;
    }

    /**
     * Extract alert lines.
     *
     * @return the property checks
     */
    public PropertyChecks getAlertLines()
    {
        PropertyChecks result;

        result = new PropertyChecks();

        for (PropertyCheck check : this)
        {
            if (check.getStatus() != Status.OK)
            {
                result.add(check);
            }
        }

        //
        return result;
    }

    /**
     * Gets the error count.
     *
     * @return the error count
     */
    public int getErrorCount()
    {
        int result;

        result = 0;
        for (PropertyCheck check : this)
        {
            if (check.getStatus() == Status.ERROR)
            {
                result += 1;
            }
        }

        //
        return result;
    }

    /**
     * Gets the void count.
     *
     * @return the void count
     */
    public int getVoidCount()
    {
        int result;

        result = 0;
        for (PropertyCheck check : this)
        {
            if (check.getStatus() == Status.VOID)
            {
                result += 1;
            }
        }

        //
        return result;
    }

    /**
     * Gets the warning count.
     *
     * @return the warning count
     */
    public int getWarningCount()
    {
        int result;

        result = 0;
        for (PropertyCheck check : this)
        {
            if (check.getStatus() == Status.WARNING)
            {
                result += 1;
            }
        }

        //
        return result;
    }

    /**
     * Sets the file name.
     *
     * @param fileName
     *            the new file name
     */
    public PropertyChecks setFileName(final String fileName)
    {
        PropertyChecks result;

        for (PropertyCheck check : this)
        {
            check.setFileName(fileName);
        }

        result = this;

        //
        return result;
    }
}