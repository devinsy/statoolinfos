/*
 * Copyright (C) 2020 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.checker;

import java.util.ArrayList;
import java.util.Iterator;

import fr.devinsy.statoolinfos.checker.PropertyRule.PropertyMode;

/**
 * The Class PropertyRuleList.
 */
public class PropertyRules extends ArrayList<PropertyRule>
{
    private static final long serialVersionUID = 5808894132916693496L;

    /**
     * Instantiates a new property rules.
     */
    public PropertyRules()
    {
        super();
    }

    /**
     * Adds the.
     *
     * @param path
     *            the path
     * @param pattern
     *            the pattern
     * @param mode
     *            the mode
     */
    public void add(final String path, final String pattern, final PropertyMode mode)
    {
        String targetPath;
        if (path.startsWith("^"))
        {
            targetPath = path;
        }
        else
        {
            targetPath = path.replace(".", "\\.");
        }

        PropertyRule rule = new PropertyRule(path, targetPath, pattern, mode);

        this.add(rule);
    }

    /**
     * Adds the.
     *
     * @param label
     *            the label
     * @param path
     *            the path
     * @param pattern
     *            the pattern
     * @param mode
     *            the mode
     */
    public void add(final String label, final String path, final String pattern, final PropertyMode mode)
    {
        PropertyRule rule = new PropertyRule(label, path, pattern, mode);

        this.add(rule);
    }

    /**
     * Find.
     *
     * @param path
     *            the path
     * @return the property rule
     */
    public PropertyRule find(final String path)
    {
        PropertyRule result;

        boolean ended = false;
        Iterator<PropertyRule> iterator = iterator();
        result = null;
        while (!ended)
        {
            if (iterator.hasNext())
            {
                PropertyRule rule = iterator.next();

                if (rule.checkPath(path))
                {
                    ended = true;
                    result = rule;
                }
            }
            else
            {
                ended = true;
                result = null;
            }
        }

        //
        return result;
    }

    /**
     * Gets the mandatories.
     *
     * @return the mandatories
     */
    public PropertyRules getMandatories()
    {
        PropertyRules result;

        result = new PropertyRules();

        for (PropertyRule rule : this)
        {
            if (rule.isMandatory())
            {
                result.add(rule);
            }
        }

        //
        return result;
    }

    /**
     * Gets the wished.
     *
     * @return the wished
     */
    public PropertyRules getWished()
    {
        PropertyRules result;

        result = new PropertyRules();

        for (PropertyRule rule : this)
        {
            if (rule.isWished())
            {
                result.add(rule);
            }
        }

        //
        return result;
    }
}