/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.checker;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class PropertyRule.
 */
public class PropertyRule
{
    public enum PropertyMode
    {
        MANDATORY,
        WISHED,
        OPTIONAL
    }

    private String label;
    private Pattern pathPattern;
    private Pattern valuePattern;
    private PropertyMode mode;

    /**
     * Instantiates a new property rule.
     *
     * @param pathRegex
     *            the path
     * @param valueRegex
     *            the pattern
     * @param mode
     *            the mode
     */
    public PropertyRule(final String pathRegex, final String valueRegex, final PropertyMode mode)
    {
        this(pathRegex, pathRegex, valueRegex, mode);
    }

    /**
     * Instantiates a new property rule.
     *
     * @param label
     *            the label
     * @param pathRegex
     *            the path regex
     * @param valueRegex
     *            the value regex
     * @param mode
     *            the mode
     */
    public PropertyRule(final String label, final String pathRegex, final String valueRegex, final PropertyMode mode)
    {
        this.pathPattern = Pattern.compile(pathRegex, Pattern.CASE_INSENSITIVE);
        this.valuePattern = Pattern.compile(valueRegex, Pattern.CASE_INSENSITIVE);
        this.mode = mode;
        this.label = label;
    }

    /**
     * Check path.
     *
     * @param value
     *            the value
     * @return true, if successful
     */
    public boolean checkPath(final String value)
    {
        boolean result;

        Matcher matcher = this.pathPattern.matcher(StringUtils.toRootLowerCase(value));
        result = matcher.matches();

        //
        return result;
    }

    /**
     * Check.
     *
     * @param value
     *            the value
     * @return true, if successful
     */
    public boolean checkValue(final String value)
    {
        boolean result;

        Matcher matcher = this.valuePattern.matcher(value);
        result = matcher.matches();

        //
        return result;
    }

    /**
     * Gets the label.
     *
     * @return the label
     */
    public String getLabel()
    {
        return this.label;
    }

    /**
     * Gets the mode.
     *
     * @return the mode
     */
    public PropertyMode getMode()
    {
        return this.mode;
    }

    /**
     * Gets the path pattern.
     *
     * @return the path pattern
     */
    public String getPathPattern()
    {
        String result;

        result = this.pathPattern.pattern();

        //
        return result;
    }

    /**
     * Gets the pattern string.
     *
     * @return the pattern string
     */
    public String getValuePattern()
    {
        String result;

        result = this.valuePattern.pattern();

        //
        return result;
    }

    /**
     * Checks if is mandatory.
     *
     * @return true, if is mandatory
     */
    public boolean isMandatory()
    {
        boolean result;

        if (this.mode == PropertyMode.MANDATORY)
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Checks if is optional.
     *
     * @return true, if is optional
     */
    public boolean isOptional()
    {
        boolean result;

        if (this.mode == PropertyMode.OPTIONAL)
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Checks if is wished.
     *
     * @return true, if is wished
     */
    public boolean isWished()
    {
        boolean result;

        if (this.mode == PropertyMode.WISHED)
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Sets the mode.
     *
     * @param mode
     *            the new mode
     */
    public void setMode(final PropertyMode mode)
    {
        this.mode = mode;
    }

    /**
     * Sets the pattern.
     *
     * @param regex
     *            the new pattern
     */
    public void setPattern(final String regex)
    {
        this.valuePattern = Pattern.compile(regex);
    }
}