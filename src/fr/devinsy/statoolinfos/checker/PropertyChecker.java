/*
 * Copyright (C) 2020-2022 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.checker;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.statoolinfos.checker.PropertyRule.PropertyMode;
import fr.devinsy.statoolinfos.core.Service.Status;
import fr.devinsy.statoolinfos.properties.PathProperty;
import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringsUtils;

/**
 * The Class PropertyChecker.
 */
public class PropertyChecker
{
    public static final String ALL = "^.*$";
    public static final String BOM = "\ufeff";
    public static final String COMMENT = "^#.*$";
    public static final String STRING = "^.+$";
    public static final String DATETIME = "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}(:\\d{2}([\\.,]\\d+)?)?([+-]\\d\\d:\\d\\d|Z)?";
    public static final String DATE = "^(\\d{4}-\\d{2}-\\d{2}|\\d{2}/\\d{2}/\\d{4}|\\d{2}/\\d{4})";
    public static final String URL = "^https?://[\\w-_\\.]+(\\.\\w+)+(:\\d+)?(/.*)?$";
    public static final String EMAIL = "^.*@.*$";
    public static final String NUMERIC = "^\\d+$";
    public static final String NUMERICS = "^\\d*(,\\d*)*$";
    public static final String MONTHS = "^\\d*(,\\d*){0,11}$";
    public static final String WEEKS = "^\\d*(,\\d*){0,52}$";
    public static final String DAYS = "^\\d*(,\\d*){0,365}$";
    public static final String COUNTRY_CODE = "^[a-zA-Z]{2}$";
    public static final String DECIMAL_DEGREE = "^[+-]?\\d+[,\\.]\\d{1,8}$";

    public static final String SUBS = "^subs\\.\\S+$";
    public static final String METRICS_NAME = "^metrics\\.\\S+\\.name$";
    public static final String METRICS_DESCRIPTION = "^metrics\\.\\S+\\.description$";
    public static final String METRICS_YEAR = "^metrics\\.\\S+\\.\\d{4}$";
    public static final String METRICS_MONTHS = "^metrics\\.\\S+\\.\\d{4}\\.months$";
    public static final String METRICS_WEEKS = "^metrics\\.\\S+\\.\\d{4}\\.weeks$";
    public static final String METRICS_DAYS = "^metrics\\.\\S+\\.\\d{4}\\.days$";
    public static final String CRAWL = "^crawl\\.\\S+$";

    private PropertyRules federationRules;
    private PropertyRules organizationRules;
    private PropertyRules serviceRules;
    private PropertyRules metricsRules;

    /**
     * Instantiates a new property checker.
     */
    public PropertyChecker()
    {
        //
        this.federationRules = new PropertyRules();

        this.federationRules.add("file.class", "^[Ff]ederation$", PropertyMode.MANDATORY);
        this.federationRules.add("file.generator", STRING, PropertyMode.WISHED);
        this.federationRules.add("file.datetime", DATETIME, PropertyMode.WISHED);
        this.federationRules.add("file.protocol", STRING, PropertyMode.WISHED);

        this.federationRules.add("federation.name", STRING, PropertyMode.MANDATORY);
        this.federationRules.add("federation.description", STRING, PropertyMode.WISHED);
        this.federationRules.add("federation.website", URL, PropertyMode.WISHED);
        this.federationRules.add("federation.logo", URL, PropertyMode.WISHED);
        this.federationRules.add("federation.contact.url", URL, PropertyMode.WISHED);
        this.federationRules.add("federation.contact.email", EMAIL, PropertyMode.WISHED);
        this.federationRules.add("federation.socialnetworks.*", "^federation\\.socialnetworks\\.\\S+$", URL, PropertyMode.OPTIONAL);
        this.federationRules.add("federation.legal.url", URL, PropertyMode.WISHED);
        this.federationRules.add("federation.guide.user", URL, PropertyMode.WISHED);
        this.federationRules.add("federation.guide.technical", URL, PropertyMode.WISHED);
        this.federationRules.add("federation.startdate", DATE, PropertyMode.WISHED);
        this.federationRules.add("federation.enddate", DATE, PropertyMode.OPTIONAL);

        this.federationRules.add(SUBS, URL, PropertyMode.OPTIONAL);
        this.federationRules.add(METRICS_NAME, STRING, PropertyMode.OPTIONAL);
        this.federationRules.add(METRICS_DESCRIPTION, STRING, PropertyMode.OPTIONAL);
        this.federationRules.add(METRICS_YEAR, NUMERIC, PropertyMode.OPTIONAL);
        this.federationRules.add(METRICS_MONTHS, MONTHS, PropertyMode.OPTIONAL);
        this.federationRules.add(METRICS_WEEKS, WEEKS, PropertyMode.OPTIONAL);
        this.federationRules.add(METRICS_DAYS, DAYS, PropertyMode.OPTIONAL);

        this.federationRules.add(CRAWL, ALL, PropertyMode.MANDATORY);

        //
        this.organizationRules = new PropertyRules();

        this.organizationRules.add("file.class", "^[Oo]rganization$", PropertyMode.MANDATORY);
        this.organizationRules.add("file.generator", STRING, PropertyMode.WISHED);
        this.organizationRules.add("file.datetime", DATETIME, PropertyMode.WISHED);
        this.organizationRules.add("file.protocol", STRING, PropertyMode.WISHED);

        this.organizationRules.add("organization.name", STRING, PropertyMode.MANDATORY);
        this.organizationRules.add("organization.description", STRING, PropertyMode.WISHED);
        this.organizationRules.add("organization.status.level", "^(ACTIVE|IDLE|AWAY)$", PropertyMode.MANDATORY);
        this.organizationRules.add("organization.status.description", STRING, PropertyMode.OPTIONAL);
        this.organizationRules.add("organization.website", URL, PropertyMode.WISHED);
        this.organizationRules.add("organization.logo", URL, PropertyMode.WISHED);
        this.organizationRules.add("organization.socialnetworks.*", "^organization\\.socialnetworks\\.\\S+$", URL, PropertyMode.OPTIONAL);
        this.organizationRules.add("organization.chatrooms.*", "^organization\\.chatrooms\\.\\S+$", STRING, PropertyMode.OPTIONAL);
        this.organizationRules.add("organization.owner.name", STRING, PropertyMode.OPTIONAL);
        this.organizationRules.add("organization.owner.website", URL, PropertyMode.OPTIONAL);
        this.organizationRules.add("organization.owner.logo", URL, PropertyMode.OPTIONAL);
        this.organizationRules.add("organization.contact.url", URL, PropertyMode.WISHED);
        this.organizationRules.add("organization.contact.email", EMAIL, PropertyMode.WISHED);
        this.organizationRules.add("organization.legal.url", "^organization\\.legal(\\.url)?$", URL, PropertyMode.WISHED);
        this.organizationRules.add("organization.guide.user", URL, PropertyMode.WISHED);
        this.organizationRules.add("organization.guide.technical", URL, PropertyMode.WISHED);
        this.organizationRules.add("organization.startdate", DATE, PropertyMode.WISHED);
        this.organizationRules.add("organization.enddate", DATE, PropertyMode.OPTIONAL);
        this.organizationRules.add("organization.memberof.*.startdate", "^organization\\.memberof\\.\\S+\\.startdate$", DATE, PropertyMode.MANDATORY);
        this.organizationRules.add("organization.memberof.*.enddate", "^organization\\.memberof\\.\\S+\\.enddate$", DATE, PropertyMode.OPTIONAL);
        this.organizationRules.add("organization.memberof.*.status.level", "^organization\\.memberof\\.\\S+\\.status.level$", "^(ACTIVE|IDLE|AWAY)$", PropertyMode.MANDATORY);
        this.organizationRules.add("organization.memberof.*.status.description", "^organization\\.memberof\\.\\S+\\.status.description$", STRING, PropertyMode.OPTIONAL);
        this.organizationRules.add("organization.country.name", STRING, PropertyMode.WISHED);
        this.organizationRules.add("organization.country.code", COUNTRY_CODE, PropertyMode.MANDATORY);
        this.organizationRules.add("organization.funding.*", "^organization\\.funding\\.\\S+$", STRING, PropertyMode.OPTIONAL);
        this.organizationRules.add("organization.geolocation.latitude", DECIMAL_DEGREE, PropertyMode.OPTIONAL);
        this.organizationRules.add("organization.geolocation.longitude", DECIMAL_DEGREE, PropertyMode.OPTIONAL);
        this.organizationRules.add("organization.geolocation.address", STRING, PropertyMode.OPTIONAL);
        this.organizationRules.add("organization.type", "^(ASSOCIATION|INFORMAL|COOPERATIVE|MICROCOMPANY|COMPANY|INDIVIDUAL|OTHER)$", PropertyMode.MANDATORY);

        this.organizationRules.add(SUBS, URL, PropertyMode.OPTIONAL);
        this.organizationRules.add(METRICS_NAME, STRING, PropertyMode.OPTIONAL);
        this.organizationRules.add(METRICS_DESCRIPTION, STRING, PropertyMode.OPTIONAL);
        this.organizationRules.add(METRICS_YEAR, NUMERIC, PropertyMode.OPTIONAL);
        this.organizationRules.add(METRICS_MONTHS, MONTHS, PropertyMode.OPTIONAL);
        this.organizationRules.add(METRICS_WEEKS, WEEKS, PropertyMode.OPTIONAL);
        this.organizationRules.add(METRICS_DAYS, DAYS, PropertyMode.OPTIONAL);

        this.organizationRules.add(CRAWL, ALL, PropertyMode.MANDATORY);

        //
        this.serviceRules = new PropertyRules();

        this.serviceRules.add("file.class", "^[Ss]ervice$", PropertyMode.MANDATORY);
        this.serviceRules.add("file.generator", STRING, PropertyMode.WISHED);
        this.serviceRules.add("file.datetime", DATETIME, PropertyMode.WISHED);
        this.serviceRules.add("file.protocol", STRING, PropertyMode.WISHED);

        this.serviceRules.add("service.name", STRING, PropertyMode.MANDATORY);
        this.serviceRules.add("service.description", STRING, PropertyMode.WISHED);
        this.serviceRules.add("service.website", URL, PropertyMode.MANDATORY);
        this.serviceRules.add("service.logo", URL, PropertyMode.WISHED);
        this.serviceRules.add("service.contact.url", URL, PropertyMode.WISHED);
        this.serviceRules.add("service.contact.email", EMAIL, PropertyMode.WISHED);
        this.serviceRules.add("service.legal.url", URL, PropertyMode.WISHED);
        this.serviceRules.add("service.guide.user", URL, PropertyMode.WISHED);
        this.serviceRules.add("service.guide.technical", URL, PropertyMode.WISHED);
        this.serviceRules.add("service.startdate", DATE, PropertyMode.MANDATORY);
        this.serviceRules.add("service.enddate", DATE, PropertyMode.OPTIONAL);
        this.serviceRules.add("service.status.level", "^(OK|WARNING|ALERT|ERROR|OVER|VOID)$", PropertyMode.MANDATORY);
        this.serviceRules.add("service.status.description", STRING, PropertyMode.OPTIONAL);
        this.serviceRules.add("service.registration", "^(None|Free|Member|Client)(\\s*[,;+]\\s*(None|Free|Member|Client))*$", PropertyMode.MANDATORY);
        this.serviceRules.add("service.registration.load", "^(open|full)$", PropertyMode.MANDATORY);
        this.serviceRules.add("service.install.type", "^(DISTRIBUTION|PROVIDER|PACKAGE|TOOLING|CLONEREPO|ARCHIVE|SOURCES|CONTAINER)$", PropertyMode.MANDATORY);

        this.serviceRules.add("software.name", STRING, PropertyMode.MANDATORY);
        this.serviceRules.add("software.website", URL, PropertyMode.WISHED);
        this.serviceRules.add("software.license.url", URL, PropertyMode.MANDATORY);
        this.serviceRules.add("software.license.name", STRING, PropertyMode.MANDATORY);
        this.serviceRules.add("software.version", STRING, PropertyMode.WISHED);
        this.serviceRules.add("software.source.url", URL, PropertyMode.WISHED);
        this.serviceRules.add("software.modules", STRING, PropertyMode.OPTIONAL);

        this.serviceRules.add("host.name", STRING, PropertyMode.MANDATORY);
        this.serviceRules.add("host.description", STRING, PropertyMode.OPTIONAL);
        this.serviceRules.add("host.server.distribution", STRING, PropertyMode.MANDATORY);
        this.serviceRules.add("host.server.type", "^(NANO|PHYSICAL|VIRTUAL|SHARED|CLOUD)$", PropertyMode.MANDATORY);
        this.serviceRules.add("host.provider.type", "^(HOME|HOSTEDBAY|HOSTEDSERVER|OUTSOURCED)$", PropertyMode.MANDATORY);
        this.serviceRules.add("host.provider.hypervisor", STRING, PropertyMode.OPTIONAL);
        this.serviceRules.add("host.country.name", STRING, PropertyMode.WISHED);
        this.serviceRules.add("host.country.code", COUNTRY_CODE, PropertyMode.MANDATORY);

        this.serviceRules.add(SUBS, URL, PropertyMode.OPTIONAL);
        this.serviceRules.add(METRICS_NAME, STRING, PropertyMode.OPTIONAL);
        this.serviceRules.add(METRICS_DESCRIPTION, STRING, PropertyMode.OPTIONAL);
        this.serviceRules.add(METRICS_YEAR, NUMERIC, PropertyMode.OPTIONAL);
        this.serviceRules.add(METRICS_MONTHS, MONTHS, PropertyMode.OPTIONAL);
        this.serviceRules.add(METRICS_WEEKS, WEEKS, PropertyMode.OPTIONAL);
        this.serviceRules.add(METRICS_DAYS, DAYS, PropertyMode.OPTIONAL);

        this.serviceRules.add(CRAWL, ALL, PropertyMode.MANDATORY);

        //
        this.metricsRules = new PropertyRules();

        this.metricsRules.add("file.class", "^[Mm]etrics$", PropertyMode.MANDATORY);
        this.metricsRules.add("file.generator", STRING, PropertyMode.WISHED);
        this.metricsRules.add("file.datetime", DATETIME, PropertyMode.WISHED);
        this.metricsRules.add("file.protocol", STRING, PropertyMode.WISHED);

        this.metricsRules.add(METRICS_NAME, STRING, PropertyMode.OPTIONAL);
        this.metricsRules.add(METRICS_DESCRIPTION, STRING, PropertyMode.OPTIONAL);
        this.metricsRules.add(METRICS_YEAR, NUMERIC, PropertyMode.OPTIONAL);
        this.metricsRules.add(METRICS_MONTHS, MONTHS, PropertyMode.OPTIONAL);
        this.metricsRules.add(METRICS_WEEKS, WEEKS, PropertyMode.OPTIONAL);
        this.metricsRules.add(METRICS_DAYS, DAYS, PropertyMode.OPTIONAL);

        this.metricsRules.add(CRAWL, ALL, PropertyMode.MANDATORY);
    }

    /**
     * Check.
     *
     * @param lines
     *            the lines
     * @return the property checks
     */
    public PropertyChecks check(final StringList lines, final PropertyRules rules)
    {
        PropertyChecks result;

        result = new PropertyChecks();

        //
        if ((lines != null) && (!lines.isEmpty()) && (lines.get(0).startsWith(BOM)))
        {
            String line = lines.get(0).substring(1);
            lines.set(0, line);
        }

        //
        PropertyRules requiredRules = rules.getMandatories();
        requiredRules.addAll(rules.getWished());
        for (PropertyRule rule : requiredRules)
        {
            boolean ended = false;
            Iterator<String> iterator = lines.iterator();
            while (!ended)
            {
                if (iterator.hasNext())
                {
                    String line = iterator.next();

                    if ((!StringUtils.isBlank(line)) && (!line.startsWith("#")) && (line.contains("=")))
                    {
                        String[] tokens = line.split("=", 2);
                        PathProperty property = new PathProperty(tokens[0].trim(), tokens[1].trim());

                        if (rule.checkPath(property.getPath()))
                        {
                            ended = true;
                        }
                    }
                }
                else
                {
                    ended = true;
                    Status status;
                    switch (rule.getMode())
                    {
                        default:
                        case MANDATORY:
                            status = Status.ERROR;
                        break;
                        case WISHED:
                            status = Status.WARNING;
                        break;
                    }
                    PropertyCheck check = new PropertyCheck(0, rule.getLabel(), status, "Propriété manquante");
                    result.add(check);
                }
            }
        }

        //
        int lineIndex = 1;
        for (String line : lines)
        {
            PropertyCheck check;
            if (line == null)
            {
                check = new PropertyCheck(lineIndex, "", Status.OK);
                check.setComment("OK");
            }
            else if ((StringUtils.isEmpty(line)) || (isComment(line)))
            {
                check = new PropertyCheck(lineIndex, line, Status.OK);
                check.setComment("OK");
            }
            else if (!isProperty(line))
            {
                check = new PropertyCheck(lineIndex, line, Status.ERROR);
                check.setComment("Ligne malformée");
            }
            else
            {
                String[] tokens = line.split("=", 2);
                PathProperty property = new PathProperty(tokens[0].trim(), tokens[1].trim());
                PropertyRule rule = rules.find(property.getPath());

                check = new PropertyCheck(lineIndex, line, Status.VOID);

                if (rule == null)
                {
                    check.setStatus(Status.VOID);
                    check.setComment("Propriété inconnue");
                }
                else if (rule.isOptional())
                {
                    if (StringUtils.isBlank(property.getValue()))
                    {
                        check.setStatus(Status.OK);
                        check.setComment("OK");
                    }
                    else if (rule.checkValue(property.getValue()))
                    {
                        check.setStatus(Status.OK);
                        check.setComment("OK");
                    }
                    else
                    {
                        check.setStatus(Status.ERROR);
                        check.setComment("Valeur incorrecte");
                    }
                }
                else if (rule.isWished())
                {
                    if (StringUtils.isBlank(property.getValue()))
                    {
                        check.setStatus(Status.WARNING);
                        check.setComment("Valeur recommandée");
                    }
                    else if (rule.checkValue(property.getValue()))
                    {
                        check.setStatus(Status.OK);
                        check.setComment("OK");
                    }
                    else
                    {
                        check.setStatus(Status.ERROR);
                        check.setComment("Valeur incorrecte");
                    }
                }
                else if (rule.isMandatory())
                {
                    if (StringUtils.isBlank(property.getValue()))
                    {
                        check.setStatus(Status.ERROR);
                        check.setComment("Valeur obligatoire");
                    }
                    else if (rule.checkValue(property.getValue()))
                    {
                        check.setStatus(Status.OK);
                        check.setComment("OK");
                    }
                    else
                    {
                        check.setStatus(Status.ERROR);
                        check.setComment("Valeur incorrecte");
                    }
                }
            }

            result.add(check);
            lineIndex += 1;
        }

        //
        return result;
    }

    /**
     * Check federation.
     *
     * @param file
     *            the file
     * @return the property checks
     * @throws IOException
     */
    public PropertyChecks checkFederation(final File file) throws IOException
    {
        PropertyChecks result;

        StringList lines = StringsUtils.load(file);
        result = check(lines, this.federationRules);

        //
        return result;
    }

    /**
     * Check metrics.
     *
     * @param file
     *            the file
     * @return the property checks
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public PropertyChecks checkMetrics(final File file) throws IOException
    {
        PropertyChecks result;

        StringList lines = StringsUtils.load(file);
        result = check(lines, this.metricsRules);

        //
        return result;
    }

    /**
     * Check organization.
     *
     * @param file
     *            the file
     * @return the property checks
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public PropertyChecks checkOrganization(final File file) throws IOException
    {
        PropertyChecks result;

        StringList lines = StringsUtils.load(file);
        result = check(lines, this.organizationRules);

        //
        return result;
    }

    /**
     * Check service.
     *
     * @param file
     *            the file
     * @return the property checks
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public PropertyChecks checkService(final File file) throws IOException
    {
        PropertyChecks result;

        StringList lines = StringsUtils.load(file);
        result = check(lines, this.serviceRules);

        //
        return result;
    }

    /**
     * Checks if is comment.
     *
     * @param line
     *            the line
     * @return true, if is comment
     */
    public static boolean isComment(final String line)
    {
        boolean result;

        // Method 1
        // public static final Pattern COMMENT_PATTERN =
        // Pattern.compile(COMMENT);
        // result = COMMENT_PATTERN.matcher(line).matches();

        // Method 2
        // result = StringUtils.startsWith(line, "#");

        // Method 3
        if ((line != null) && (line.length() > 0) && (line.charAt(0) == '#'))
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Checks if is property.
     *
     * @param line
     *            the line
     * @return true, if is property
     */
    public static boolean isProperty(final String line)
    {
        boolean result;

        // Method 1, take so much time, > 1000 ms for each property file.
        // public static final Pattern PROPERTY_PATTERN =
        // Pattern.compile("^[^#].*[^\\s].*=.*$");
        // result = PROPERTY_PATTERN.matcher(line).matches();

        // Method 2, well optimized, < 1 ms for each property file.
        if ((line == null) || (line.length() == 0))
        {
            result = false;
        }
        else
        {
            int splitter = line.indexOf('=');
            if (splitter == -1)
            {
                result = false;
            }
            else
            {
                String path = line.substring(0, splitter);
                if ((path.length() == 0) || (path.charAt(0) == '#') || (StringUtils.isBlank(path)))
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
        }

        //
        return result;
    }
}