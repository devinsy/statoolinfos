/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.checker;

import fr.devinsy.statoolinfos.core.Service.Status;

/**
 * The Class PropertyCheck.
 */
public class PropertyCheck
{
    private String fileName;
    private long index;
    private String line;
    private Status status;
    private String comment;

    /**
     * Instantiates a new property check.
     *
     * @param index
     *            the index
     * @param line
     *            the line
     */
    public PropertyCheck(final long index, final String line)
    {
        this(index, line, Status.VOID);
    }

    /**
     * Instantiates a new property check.
     *
     * @param index
     *            the index
     * @param line
     *            the line
     * @param status
     *            the status
     */
    public PropertyCheck(final long index, final String line, final Status status)
    {
        this(index, line, status, "");
    }

    /**
     * Instantiates a new property check.
     *
     * @param index
     *            the index
     * @param line
     *            the line
     * @param status
     *            the status
     * @param comment
     *            the comment
     */
    public PropertyCheck(final long index, final String line, final Status status, final String comment)
    {
        this.fileName = null;
        this.index = index;
        this.line = line;
        this.status = status;
        this.comment = comment;
    }

    /**
     * Gets the comment.
     *
     * @return the comment
     */
    public String getComment()
    {
        return this.comment;
    }

    /**
     * Gets the file name.
     *
     * @return the file name
     */
    public String getFileName()
    {
        return this.fileName;
    }

    /**
     * Gets the index.
     *
     * @return the index
     */
    public long getIndex()
    {
        return this.index;
    }

    /**
     * Gets the line.
     *
     * @return the line
     */
    public String getLine()
    {
        return this.line;
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    public Status getStatus()
    {
        return this.status;
    }

    /**
     * Sets the comment.
     *
     * @param comment
     *            the new comment
     */
    public void setComment(final String comment)
    {
        this.comment = comment;
    }

    /**
     * Sets the file name.
     *
     * @param fileName
     *            the new file name
     */
    public void setFileName(final String fileName)
    {
        this.fileName = fileName;
    }

    /**
     * Sets the index.
     *
     * @param index
     *            the new index
     */
    public void setIndex(final long index)
    {
        this.index = index;
    }

    /**
     * Sets the line.
     *
     * @param line
     *            the new line
     */
    public void setLine(final String line)
    {
        this.line = line;
    }

    /**
     * Sets the status.
     *
     * @param status
     *            the new status
     */
    public void setStatus(final Status status)
    {
        this.status = status;
    }
}