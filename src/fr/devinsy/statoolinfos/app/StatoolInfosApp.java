/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.app;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.statoolinfos.build.Builder;
import fr.devinsy.statoolinfos.core.BotFilter;
import fr.devinsy.statoolinfos.core.Configuration;
import fr.devinsy.statoolinfos.core.Factory;
import fr.devinsy.statoolinfos.core.Federation;
import fr.devinsy.statoolinfos.core.StatoolInfosContext;
import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.crawl.Crawler;
import fr.devinsy.statoolinfos.metrics.Prober;
import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLog;
import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLogs;
import fr.devinsy.statoolinfos.metrics.httperrorlog.HttpErrorLog;
import fr.devinsy.statoolinfos.metrics.httperrorlog.HttpErrorLogs;
import fr.devinsy.statoolinfos.properties.PathProperties;
import fr.devinsy.statoolinfos.properties.PathPropertyUtils;
import fr.devinsy.statoolinfos.stats.ip.IpStat;
import fr.devinsy.statoolinfos.stats.ip.IpStator;
import fr.devinsy.statoolinfos.stats.useragent.UserAgentStat;
import fr.devinsy.statoolinfos.stats.useragent.UserAgentStator;
import fr.devinsy.statoolinfos.stats.visitor.VisitorStat;
import fr.devinsy.statoolinfos.stats.visitor.VisitorStator;
import fr.devinsy.statoolinfos.uptime.UptimeJournal;
import fr.devinsy.statoolinfos.uptime.UptimeSurveyor;
import fr.devinsy.statoolinfos.util.Chrono;
import fr.devinsy.statoolinfos.util.Files;
import fr.devinsy.statoolinfos.util.FilesUtils;
import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringsUtils;

/**
 * The Class StatoolInfosApp.
 */
public class StatoolInfosApp
{
    private static Logger logger = LoggerFactory.getLogger(StatoolInfosApp.class);

    /**
     * Builds the.
     *
     * @param configurationFile
     *            the input
     * @throws StatoolInfosException
     *             the statool infos exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void build(final File configurationFile) throws StatoolInfosException, IOException
    {
        Builder.build(configurationFile);
    }

    /**
     * Clear.
     *
     * @param configurationFile
     *            the input
     * @throws StatoolInfosException
     *             the statool infos exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void clear(final File configurationFile) throws StatoolInfosException, IOException
    {
        logger.info("Clear {}", configurationFile.getAbsolutePath());
        Configuration configuration = Factory.loadConfiguration(configurationFile);

        Builder.clear(configuration);
        new Crawler(configuration.getCrawlCacheDirectory()).clear();
    }

    /**
     * Crawl.
     *
     * @param configurationFile
     *            the input
     * @throws StatoolInfosException
     *             the statool infos exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void crawl(final File configurationFile) throws StatoolInfosException, IOException
    {
        Configuration configuration = Factory.loadConfiguration(configurationFile);
        Crawler crawler = new Crawler(configuration.getCrawlCacheDirectory());
        crawler.crawl(configuration.getCrawlInputURL());
        crawler.storeJournal();
    }

    /**
     * Format.
     *
     * @param inputFile
     *            the input file
     * @throws StatoolInfosException
     *             the statool infos exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void format(final File inputFile) throws StatoolInfosException, IOException
    {
        logger.info("Format {}", inputFile);

        if (inputFile == null)
        {
            throw new StatoolInfosException("Input is undefined.");
        }
        else if (!inputFile.exists())
        {
            throw new StatoolInfosException("Input does not exist.");
        }
        else if (!inputFile.isFile())
        {
            throw new StatoolInfosException("Input is not a file.");
        }
        else
        {
            // Load input properties.
            PathProperties inputs = PathPropertyUtils.load(inputFile);

            // Save input properties.
            inputFile.renameTo(new File(inputFile.getParentFile(), inputFile.getName() + ".bak"));
            PathPropertyUtils.save(inputFile, inputs);
        }
    }

    /**
     * List files.
     *
     * @param configurationFile
     *            the configuration file
     */
    public static void listAccessFiles(final File configurationFile)
    {
        try
        {
            if ((configurationFile == null) || (!configurationFile.exists()))
            {
                System.out.println("No configuration file found.");
            }
            else
            {
                System.out.println("Listing HttpAccesLog files from [" + configurationFile.toString() + "]");
                Configuration configuration = Factory.loadConfiguration(configurationFile);

                logger.info("== List HttpAccessLog files.");
                String source = configuration.getProbeHttpAccessLogSource();
                logger.info("source=[{}]", source);

                Files files = FilesUtils.searchByWildcard(source);
                for (File file : files)
                {
                    System.out.println(file.getAbsolutePath());
                }
            }
        }
        catch (StatoolInfosException exception)
        {
            exception.printStackTrace();
        }
    }

    /**
     * List files.
     *
     * @param files
     *            the files
     */
    public static void listAccessFiles(final Files files)
    {
        if (files != null)
        {
            System.out.println("Listing HttpAccesLog files from [" + files.toString() + "]");

            logger.info("== List HttpAccessLog files");
            for (File file : files)
            {
                System.out.println(file.getAbsolutePath());
            }
        }
    }

    /**
     * List error files.
     *
     * @param configurationFile
     *            the configuration file
     */
    public static void listErrorFiles(final File configurationFile)
    {
        try
        {
            if ((configurationFile == null) || (!configurationFile.exists()))
            {
                System.out.println("No configuration file found.");
            }
            else
            {
                System.out.println("Listing HttpErrorLog files from [" + configurationFile.toString() + "]");
                Configuration configuration = Factory.loadConfiguration(configurationFile);

                logger.info("== List HttpErrorLog files.");
                String source = configuration.getProbeHttpErrorLogSource();
                logger.info("source=[{}]", source);

                Files files = FilesUtils.searchByWildcard(source);
                for (File file : files)
                {
                    System.out.println(file.getAbsolutePath());
                }
            }
        }
        catch (StatoolInfosException exception)
        {
            exception.printStackTrace();
        }
    }

    /**
     * List error files.
     *
     * @param files
     *            the files
     */
    public static void listErrorFiles(final Files files)
    {
        if (files != null)
        {
            System.out.println("Listing HttpErrorLog files from [" + files.toString() + "]");

            logger.info("== List HttpErrorLog files");
            for (File file : files)
            {
                System.out.println(file.getAbsolutePath());
            }
        }
    }

    /**
     * List error logs.
     *
     * @param configurationFile
     *            the configuration file
     */
    public static void listErrorLogs(final File configurationFile)
    {
        try
        {
            if ((configurationFile == null) || (!configurationFile.exists()))
            {
                System.out.println("No configuration file found.");
            }
            else
            {
                System.out.println("Testing HttpErrorLog lines from [" + configurationFile.toString() + "]");
                Configuration configuration = Factory.loadConfiguration(configurationFile);

                logger.info("== Testing HttpErrorLog lines.");
                String source = configuration.getProbeHttpErrorLogSource();
                String dateTimePattern = configuration.getProbeHttpErrorLogDateTimePattern();
                String pattern = configuration.getProbeHttpErrorLogPattern();
                logger.info("source=[{}]", source);
                logger.info("pattern=[{}]", pattern);
                logger.info("dateTimePattern=[{}]", dateTimePattern);

                HttpErrorLogs logs = new HttpErrorLogs(FilesUtils.searchByWildcard(source), pattern, dateTimePattern);

                for (HttpErrorLog log : logs)
                {
                    System.out.println(log.toStringLog());
                }
            }
        }
        catch (Exception exception)
        {
            logger.error("Error: {}", exception.getMessage());
            exception.printStackTrace();
        }
    }

    /**
     * List error logs.
     *
     * @param source
     *            the source
     */
    public static void listErrorLogs(final Files source)
    {
        try
        {
            HttpErrorLogs logs = new HttpErrorLogs(source);
            for (HttpErrorLog log : logs)
            {
                System.out.println(log.toStringLog());
            }
        }
        catch (IllegalArgumentException exception)
        {
            System.out.println("Bad format line detected. Aborting…");
            exception.printStackTrace();
        }
        catch (DateTimeParseException exception)
        {
            System.out.println("Bad datetime format detected. Aborting…");
        }
        catch (IOException exception)
        {
            System.out.println("File error detected. Aborting…");
            exception.printStackTrace();
        }
    }

    /**
     * List ips.
     *
     * @param configurationFile
     *            the configuration file
     * @param filter
     *            the filter
     */
    public static void listIps(final File configurationFile, final BotFilter filter)
    {
        try
        {
            if ((configurationFile == null) || (!configurationFile.exists()))
            {
                System.out.println("No configuration file found.");
            }
            else
            {
                System.out.println("Testing HttpAccesLog lines from [" + configurationFile.toString() + "]");
                Configuration configuration = Factory.loadConfiguration(configurationFile);

                logger.info("== Testing HttpAccessLog lines.");
                String source = configuration.getProbeHttpAccessLogSource();
                String dateTimePattern = configuration.getProbeHttpAccessLogDateTimePattern();
                String pattern = configuration.getProbeHttpAccessLogPattern();
                String pathFilter = configuration.getProbeHttpAccessLogPathFilter();
                logger.info("source=[{}]", source);
                logger.info("pattern=[{}]", pattern);
                logger.info("dateTimePattern=[{}]", dateTimePattern);
                logger.info("pathFilter=[{}]", pathFilter);

                Chrono chrono = new Chrono().start();
                IpStator stator = new IpStator();
                HttpAccessLogs logs = new HttpAccessLogs(FilesUtils.searchByWildcard(source), pattern, dateTimePattern, pathFilter);
                for (HttpAccessLog log : logs)
                {
                    if (filter.matches(log))
                    {
                        stator.putLog(log);
                    }
                }

                for (IpStat stat : stator.getIps())
                {
                    System.out.println(stat.getValue());
                }

                System.err.println(String.format("%s %10d", "Ip count:  ", stator.getIps().size()));
                System.err.println(String.format("%s %10d", "Log count: ", stator.getLogCount()));
                System.err.println(chrono.format());
            }
        }
        catch (Exception exception)
        {
            logger.error("Error: {}", exception.getMessage());
            exception.printStackTrace();
        }
    }

    /**
     * List ips.
     *
     * @param source
     *            the source
     * @param filter
     *            the filter
     */
    public static void listIps(final Files source, final BotFilter filter)
    {
        try
        {
            Chrono chrono = new Chrono().start();

            IpStator stator = new IpStator();
            HttpAccessLogs logs = new HttpAccessLogs(source);
            for (HttpAccessLog log : logs)
            {
                if (filter.matches(log))
                {
                    stator.putLog(log);
                }
            }

            for (IpStat stat : stator.getIps())
            {
                System.out.println(stat.getValue());
            }

            System.err.println(String.format("%s %10d", "Ip count:  ", stator.getIps().size()));
            System.err.println(String.format("%s %10d", "Log count: ", stator.getLogCount()));
            System.err.println(chrono.format());
        }
        catch (IllegalArgumentException exception)
        {
            System.out.println("Bad format line detected. Aborting…");
            exception.printStackTrace();
        }
        catch (DateTimeParseException exception)
        {
            System.out.println("Bad datetime format detected. Aborting…");
        }
        catch (IOException exception)
        {
            System.out.println("File error detected. Aborting…");
            exception.printStackTrace();
        }
    }

    /**
     * List logs.
     *
     * @param configurationFile
     *            the configuration file
     * @param filter
     *            the filter
     */
    public static void listLogs(final File configurationFile, final BotFilter filter)
    {
        try
        {
            if ((configurationFile == null) || (!configurationFile.exists()))
            {
                System.out.println("No configuration file found.");
            }
            else
            {
                System.out.println("Testing HttpAccesLog lines from [" + configurationFile.toString() + "]");
                Configuration configuration = Factory.loadConfiguration(configurationFile);

                logger.info("== Testing HttpAccessLog lines.");
                String source = configuration.getProbeHttpAccessLogSource();
                String dateTimePattern = configuration.getProbeHttpAccessLogDateTimePattern();
                String pattern = configuration.getProbeHttpAccessLogPattern();
                String pathFilter = configuration.getProbeHttpAccessLogPathFilter();
                logger.info("source=[{}]", source);
                logger.info("pattern=[{}]", pattern);
                logger.info("dateTimePattern=[{}]", dateTimePattern);
                logger.info("pathFilter=[{}]", pathFilter);

                HttpAccessLogs logs = new HttpAccessLogs(FilesUtils.searchByWildcard(source), pattern, dateTimePattern, pathFilter);

                for (HttpAccessLog log : logs)
                {
                    if (filter.matches(log))
                    {
                        System.out.println(log.toStringLog());
                    }
                }
            }
        }
        catch (Exception exception)
        {
            logger.error("Error: {}", exception.getMessage());
            exception.printStackTrace();
        }
    }

    /**
     * List logs.
     *
     * @param source
     *            the source
     * @param filter
     *            the filter
     */
    public static void listLogs(final Files source, final BotFilter filter)
    {
        try
        {
            HttpAccessLogs logs = new HttpAccessLogs(source);
            for (HttpAccessLog log : logs)
            {
                if (filter.matches(log))
                {
                    System.out.println(log.toStringLog());
                }
            }
        }
        catch (IllegalArgumentException exception)
        {
            System.out.println("Bad format line detected. Aborting…");
            exception.printStackTrace();
        }
        catch (DateTimeParseException exception)
        {
            System.out.println("Bad datetime format detected. Aborting…");
        }
        catch (IOException exception)
        {
            System.out.println("File error detected. Aborting…");
            exception.printStackTrace();
        }
    }

    /**
     * List user agents.
     *
     * @param configurationFile
     *            the configuration file
     * @param filter
     *            the filter
     */
    public static void listUserAgents(final File configurationFile, final BotFilter filter)
    {
        try
        {
            if ((configurationFile == null) || (!configurationFile.exists()))
            {
                System.out.println("No configuration file found.");
            }
            else
            {
                System.out.println("Testing HttpAccesLog lines from [" + configurationFile.toString() + "]");
                Configuration configuration = Factory.loadConfiguration(configurationFile);

                logger.info("== Testing HttpAccessLog lines.");
                String source = configuration.getProbeHttpAccessLogSource();
                String dateTimePattern = configuration.getProbeHttpAccessLogDateTimePattern();
                String pattern = configuration.getProbeHttpAccessLogPattern();
                String pathFilter = configuration.getProbeHttpAccessLogPathFilter();
                logger.info("source=[{}]", source);
                logger.info("pattern=[{}]", pattern);
                logger.info("dateTimePattern=[{}]", dateTimePattern);
                logger.info("pathFilter=[{}]", pathFilter);

                Chrono chrono = new Chrono().start();
                UserAgentStator stator = new UserAgentStator();
                HttpAccessLogs logs = new HttpAccessLogs(FilesUtils.searchByWildcard(source), pattern, dateTimePattern, pathFilter);

                for (HttpAccessLog log : logs)
                {
                    if (filter.matches(log))
                    {
                        stator.putLog(log);
                    }
                }

                for (UserAgentStat stat : stator.getUserAgentStats().sortByLabel())
                {
                    System.out.println(stat.getUserAgent());
                }

                System.err.println(String.format("%s %10d", "UserAgent count: ", stator.getUserAgentStats().size()));
                System.err.println(String.format("%s %10d", "Log count:       ", stator.getLogCount()));
                System.err.println(chrono.format());
            }
        }
        catch (Exception exception)
        {
            logger.error("Error: {}", exception.getMessage());
            exception.printStackTrace();
        }
    }

    /**
     * List user agents.
     *
     * @param source
     *            the source
     * @param filter
     *            the filter
     */
    public static void listUserAgents(final Files source, final BotFilter filter)
    {
        try
        {
            UserAgentStator stator = new UserAgentStator();

            Chrono chrono = new Chrono().start();
            HttpAccessLogs logs = new HttpAccessLogs(source);
            for (HttpAccessLog log : logs)
            {
                if (filter.matches(log))
                {
                    stator.putLog(log);
                }
            }

            for (UserAgentStat stat : stator.getUserAgentStats().sortByLabel())
            {
                System.out.println(stat.getUserAgent());
            }

            System.err.println(String.format("%s %10d", "UserAgent count: ", stator.getUserAgentStats().size()));
            System.err.println(String.format("%s %10d", "Log count:       ", stator.getLogCount()));
            System.err.println(chrono.format());
        }
        catch (IllegalArgumentException exception)
        {
            System.out.println("Bad format line detected. Aborting…");
            exception.printStackTrace();
        }
        catch (DateTimeParseException exception)
        {
            System.out.println("Bad datetime format detected. Aborting…");
            exception.printStackTrace();
        }
        catch (IOException exception)
        {
            System.out.println("File error detected. Aborting…");
            exception.printStackTrace();
        }
    }

    /**
     * List visitors.
     *
     * @param configurationFile
     *            the configuration file
     * @param filter
     *            the filter
     */
    public static void listVisitors(final File configurationFile, final BotFilter filter)
    {
        try
        {
            if ((configurationFile == null) || (!configurationFile.exists()))
            {
                System.out.println("No configuration file found.");
            }
            else
            {
                System.out.println("Testing HttpAccesLog lines from [" + configurationFile.toString() + "]");
                Configuration configuration = Factory.loadConfiguration(configurationFile);

                logger.info("== Testing HttpAccessLog lines.");
                String source = configuration.getProbeHttpAccessLogSource();
                String dateTimePattern = configuration.getProbeHttpAccessLogDateTimePattern();
                String pattern = configuration.getProbeHttpAccessLogPattern();
                String pathFilter = configuration.getProbeHttpAccessLogPathFilter();
                logger.info("source=[{}]", source);
                logger.info("pattern=[{}]", pattern);
                logger.info("dateTimePattern=[{}]", dateTimePattern);
                logger.info("pathFilter=[{}]", pathFilter);

                Chrono chrono = new Chrono().start();
                VisitorStator stator = new VisitorStator();
                HttpAccessLogs logs = new HttpAccessLogs(FilesUtils.searchByWildcard(source), pattern, dateTimePattern, pathFilter);
                for (HttpAccessLog log : logs)
                {
                    if (filter.matches(log))
                    {
                        stator.putLog(log);
                    }
                }

                for (VisitorStat stat : stator.getVisitorStats().sortByIp())
                {
                    System.out.println(stat.getIp() + " " + stat.getUserAgent());
                }

                System.err.println(String.format("%s %10d", "Visitor count: ", stator.getVisitorStats().size()));
                System.err.println(String.format("%s %10d", "Log count:     ", stator.getLogCount()));
                System.err.println(chrono.format());
            }
        }
        catch (Exception exception)
        {
            logger.error("Error: {}", exception.getMessage());
            exception.printStackTrace();
        }
    }

    /**
     * List visitors.
     *
     * @param source
     *            the source
     * @param filter
     *            the filter
     */
    public static void listVisitors(final Files source, final BotFilter filter)
    {
        try
        {
            VisitorStator stator = new VisitorStator();

            Chrono chrono = new Chrono().start();
            HttpAccessLogs logs = new HttpAccessLogs(source);
            for (HttpAccessLog log : logs)
            {
                if (filter.matches(log))
                {
                    stator.putLog(log);
                }
            }

            for (VisitorStat stat : stator.getVisitorStats().sortByIp())
            {
                System.out.println(stat.getIp() + " " + stat.getUserAgent());
            }

            System.err.println(String.format("%s %10d", "Visitor count: ", stator.getVisitorStats().size()));
            System.err.println(String.format("%s %10d", "Log count:     ", stator.getLogCount()));
            System.err.println(chrono.format());
        }
        catch (IllegalArgumentException exception)
        {
            System.out.println("Bad format line detected. Aborting…");
            exception.printStackTrace();
        }
        catch (DateTimeParseException exception)
        {
            System.out.println("Bad datetime format detected. Aborting…");
            exception.printStackTrace();
        }
        catch (IOException exception)
        {
            System.out.println("File error detected. Aborting…");
            exception.printStackTrace();
        }
    }

    /**
     * Probe.
     *
     * @param configurationFile
     *            the configuration file
     * @param dayCountFilter
     *            the day count filter
     * @throws StatoolInfosException
     *             the statool infos exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void probe(final File configurationFile, final int dayCountFilter) throws StatoolInfosException, IOException
    {
        Prober.probe(configurationFile, dayCountFilter);
    }

    /**
     * Stat ips.
     *
     * @param configurationFile
     *            the configuration file
     * @param filter
     *            the filter
     */
    public static void statIps(final File configurationFile, final BotFilter filter)
    {
        try
        {
            if ((configurationFile == null) || (!configurationFile.exists()))
            {
                System.out.println("No configuration file found.");
            }
            else
            {
                System.out.println("Testing HttpAccesLog lines from [" + configurationFile.toString() + "]");
                Configuration configuration = Factory.loadConfiguration(configurationFile);

                logger.info("== Testing HttpAccessLog lines.");
                String source = configuration.getProbeHttpAccessLogSource();
                String dateTimePattern = configuration.getProbeHttpAccessLogDateTimePattern();
                String pattern = configuration.getProbeHttpAccessLogPattern();
                String pathFilter = configuration.getProbeHttpAccessLogPathFilter();
                logger.info("source=[{}]", source);
                logger.info("pattern=[{}]", pattern);
                logger.info("dateTimePattern=[{}]", dateTimePattern);
                logger.info("pathFilter=[{}]", pathFilter);

                Chrono chrono = new Chrono().start();
                IpStator stator = new IpStator();
                HttpAccessLogs logs = new HttpAccessLogs(FilesUtils.searchByWildcard(source), pattern, dateTimePattern, pathFilter);
                for (HttpAccessLog log : logs)
                {
                    if (filter.matches(log))
                    {
                        stator.putLog(log);
                    }
                }

                System.err.println("IpCount Ip");
                for (IpStat stat : stator.getIps().sortByCount().reverse())
                {
                    System.out.println(stat.getCount() + " " + stat.getValue());
                }

                System.err.println(String.format("%s %10d", "Ip  count: ", stator.getIps().size()));
                System.err.println(String.format("%s %10d", "Log count: ", stator.getLogCount()));
                System.err.println(chrono.format());
            }
        }
        catch (Exception exception)
        {
            logger.error("Error: {}", exception.getMessage());
            exception.printStackTrace();
        }
    }

    /**
     * Stat ips.
     *
     * @param source
     *            the source
     * @param filter
     *            the filter
     */
    public static void statIps(final Files source, final BotFilter filter)
    {
        try
        {
            IpStator stator = new IpStator();

            Chrono chrono = new Chrono().start();
            HttpAccessLogs logs = new HttpAccessLogs(source);
            for (HttpAccessLog log : logs)
            {
                if (filter.matches(log))
                {
                    stator.putLog(log);
                }
            }

            System.err.println("IpCount Ip");
            for (IpStat stat : stator.getIps().sortByCount().reverse())
            {
                System.out.println(stat.getCount() + " " + stat.getValue());
            }

            System.err.println(String.format("%s %10d", "Ip  count: ", stator.getIps().size()));
            System.err.println(String.format("%s %10d", "Log count: ", stator.getLogCount()));
            System.err.println(chrono.format());
        }
        catch (IllegalArgumentException exception)
        {
            System.out.println("Bad format line detected. Aborting…");
            exception.printStackTrace();
        }
        catch (DateTimeParseException exception)
        {
            System.out.println("Bad datetime format detected. Aborting…");
            exception.printStackTrace();
        }
        catch (IOException exception)
        {
            System.out.println("File error detected. Aborting…");
            exception.printStackTrace();
        }
    }

    /**
     * Stat user agents.
     *
     * @param configurationFile
     *            the configuration file
     * @param filter
     *            the filter
     */
    public static void statUserAgents(final File configurationFile, final BotFilter filter)
    {
        try
        {
            if ((configurationFile == null) || (!configurationFile.exists()))
            {
                System.out.println("No configuration file found.");
            }
            else
            {
                System.out.println("Testing HttpAccesLog lines from [" + configurationFile.toString() + "]");
                Configuration configuration = Factory.loadConfiguration(configurationFile);

                logger.info("== Testing HttpAccessLog lines.");
                String source = configuration.getProbeHttpAccessLogSource();
                String dateTimePattern = configuration.getProbeHttpAccessLogDateTimePattern();
                String pattern = configuration.getProbeHttpAccessLogPattern();
                String pathFilter = configuration.getProbeHttpAccessLogPathFilter();
                logger.info("source=[{}]", source);
                logger.info("pattern=[{}]", pattern);
                logger.info("dateTimePattern=[{}]", dateTimePattern);
                logger.info("pathFilter=[{}]", pathFilter);

                Chrono chrono = new Chrono().start();
                UserAgentStator stator = new UserAgentStator();
                HttpAccessLogs logs = new HttpAccessLogs(FilesUtils.searchByWildcard(source), pattern, dateTimePattern, pathFilter);
                for (HttpAccessLog log : logs)
                {
                    if (filter.matches(log))
                    {
                        stator.putLog(log);
                    }
                }

                System.err.println("LogCount IpCount VisitCount UserAgent");
                for (UserAgentStat stat : stator.getUserAgentStats().sortByCount().reverse())
                {
                    System.out.println(String.format("%d %d %d %s", stat.getLogCount(), stat.getIpCount(), stat.getVisitCount(), stat.getUserAgent()));
                }

                System.err.println(String.format("%s %10d", "User Agent count: ", stator.getUserAgentStats().size()));
                System.err.println(String.format("%s %10d", "Log count:        ", stator.getLogCount()));
                System.err.println(chrono.format());
            }
        }
        catch (Exception exception)
        {
            logger.error("Error: {}", exception.getMessage());
            exception.printStackTrace();
        }
    }

    /**
     * Stat user agents.
     *
     * @param source
     *            the source
     * @param filter
     *            the filter
     */
    public static void statUserAgents(final Files source, final BotFilter filter)
    {
        try
        {
            UserAgentStator stator = new UserAgentStator();

            Chrono chrono = new Chrono().start();
            HttpAccessLogs logs = new HttpAccessLogs(source);
            for (HttpAccessLog log : logs)
            {
                if (filter.matches(log))
                {
                    stator.putLog(log);
                }
            }

            System.err.println("LogCount IpCount VisitCount UserAgent");
            for (UserAgentStat stat : stator.getUserAgentStats().sortByCount().reverse())
            {
                System.out.println(String.format("%d %d %d %s", stat.getLogCount(), stat.getIpCount(), stat.getVisitCount(), stat.getUserAgent()));
            }

            System.err.println(String.format("%s %10d", "User Agent count: ", stator.getUserAgentStats().size()));
            System.err.println(String.format("%s %10d", "Log count:        ", stator.getLogCount()));
            System.err.println(chrono.format());
        }
        catch (IllegalArgumentException exception)
        {
            System.out.println("Bad format line detected. Aborting…");
            exception.printStackTrace();
        }
        catch (DateTimeParseException exception)
        {
            System.out.println("Bad datetime format detected. Aborting…");
            exception.printStackTrace();
        }
        catch (IOException exception)
        {
            System.out.println("File error detected. Aborting…");
            exception.printStackTrace();
        }
    }

    /**
     * Stat visitors.
     *
     * @param configurationFile
     *            the configuration file
     * @param filter
     *            the filter
     */
    public static void statVisitors(final File configurationFile, final BotFilter filter)
    {
        try
        {
            if ((configurationFile == null) || (!configurationFile.exists()))
            {
                System.out.println("No configuration file found.");
            }
            else
            {
                System.out.println("Testing HttpAccesLog lines from [" + configurationFile.toString() + "]");
                Configuration configuration = Factory.loadConfiguration(configurationFile);

                logger.info("== Testing HttpAccessLog lines.");
                String source = configuration.getProbeHttpAccessLogSource();
                String dateTimePattern = configuration.getProbeHttpAccessLogDateTimePattern();
                String pattern = configuration.getProbeHttpAccessLogPattern();
                String pathFilter = configuration.getProbeHttpAccessLogPathFilter();
                logger.info("source=[{}]", source);
                logger.info("pattern=[{}]", pattern);
                logger.info("dateTimePattern=[{}]", dateTimePattern);
                logger.info("pathFilter=[{}]", pathFilter);

                Chrono chrono = new Chrono().start();
                VisitorStator stator = new VisitorStator();
                HttpAccessLogs logs = new HttpAccessLogs(FilesUtils.searchByWildcard(source), pattern, dateTimePattern, pathFilter);
                for (HttpAccessLog log : logs)
                {
                    if (filter.matches(log))
                    {
                        stator.putLog(log);
                    }
                }

                System.err.println("VisitCount LogCount VisitDuration VisitDuration Ip UserAgent");
                for (VisitorStat stat : stator.getVisitorStats().sortByVisitCount().reverse())
                {
                    System.out.println(
                            String.format("%d %d %d %s %s %s", stat.getVisits().size(), stat.getLogCount(), stat.getVisits().getDurationSum().toSeconds(),
                                    Chrono.format(stat.getVisits().getDurationSum()),
                                    stat.getIp(), stat.getUserAgent()));
                }

                System.err.println(String.format("%s %10d", "Visitor count: ", stator.getVisitorStats().size()));
                System.err.println(String.format("%s %10d", "Log count:     ", stator.getLogCount()));
                System.err.println(chrono.format());
            }
        }
        catch (Exception exception)
        {
            logger.error("Error: {}", exception.getMessage());
            exception.printStackTrace();
        }
    }

    /**
     * Stat visitors.
     *
     * @param source
     *            the source
     * @param filter
     *            the filter
     */
    public static void statVisitors(final Files source, final BotFilter filter)
    {
        try
        {
            VisitorStator stator = new VisitorStator();

            Chrono chrono = new Chrono().start();
            HttpAccessLogs logs = new HttpAccessLogs(source);
            for (HttpAccessLog log : logs)
            {
                if (filter.matches(log))
                {
                    stator.putLog(log);
                }
            }

            System.err.println("VisitCount LogCount VisitDuration VisitDuration Ip UserAgent");
            for (VisitorStat stat : stator.getVisitorStats().sortByVisitCount().reverse())
            {
                System.out.println(
                        String.format("%d %d %d %s %s %s", stat.getVisits().size(), stat.getLogCount(), stat.getVisits().getDurationSum().toSeconds(), Chrono.format(stat.getVisits().getDurationSum()),
                                stat.getIp(), stat.getUserAgent()));
            }

            System.err.println(String.format("%s %10d", "Visitor count: ", stator.getVisitorStats().size()));
            System.err.println(String.format("%s %10d", "Log count:     ", stator.getLogCount()));
            System.err.println(chrono.format());
        }
        catch (IllegalArgumentException exception)
        {
            System.out.println("Bad format line detected. Aborting…");
            exception.printStackTrace();
        }
        catch (DateTimeParseException exception)
        {
            System.out.println("Bad datetime format detected. Aborting…");
            exception.printStackTrace();
        }
        catch (IOException exception)
        {
            System.out.println("File error detected. Aborting…");
            exception.printStackTrace();
        }

    }

    /**
     * Tag date.
     *
     * @param inputFile
     *            the source
     * @throws StatoolInfosException
     *             the statool infos exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void tagDate(final File inputFile) throws StatoolInfosException, IOException
    {
        System.out.println("TagDate " + inputFile);

        if (inputFile == null)
        {
            throw new StatoolInfosException("Null parameter [source].");
        }
        else if (!inputFile.exists())
        {
            throw new StatoolInfosException("Input does not exist.");
        }
        else if (!inputFile.isFile())
        {
            throw new StatoolInfosException("Input is not a file.");
        }
        else
        {
            StringList lines = StringsUtils.load(inputFile);

            for (int lineIndex = 0; lineIndex < lines.size(); lineIndex++)
            {
                String line = lines.get(lineIndex);

                if (StringUtils.startsWith(line, "file.datetime="))
                {
                    lines.set(lineIndex, "file.datetime=" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));
                }
            }

            inputFile.renameTo(new File(inputFile.getParentFile(), inputFile.getName() + ".bak"));
            StringsUtils.save(inputFile, lines);
        }
    }

    /**
     * Uptime.
     *
     * @param configurationFile
     *            the configuration file
     * @throws StatoolInfosException
     *             the statool infos exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void uptime(final File configurationFile) throws StatoolInfosException, IOException
    {
        StatoolInfosContext context = new StatoolInfosContext(configurationFile);

        UptimeJournal journal = context.getUptimeJournal();
        Federation federation = context.getFederation();
        UptimeSurveyor.survey(journal, federation.getURLActiveAll());
        context.getCache().storeUptimeJournal(journal);
    }
}
