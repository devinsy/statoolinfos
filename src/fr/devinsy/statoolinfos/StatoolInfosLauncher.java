/*
 * Copyright (C) 2020-2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.builder.api.AppenderComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory;
import org.apache.logging.log4j.core.config.builder.api.RootLoggerComponentBuilder;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.statoolinfos.cli.StatoolInfosCLI;

/**
 * The Class StatoolInfosLauncher.
 */
public final class StatoolInfosLauncher
{
    /**
     * Instantiates a new statool infos launcher.
     */
    private StatoolInfosLauncher()
    {
    }

    /**
     * The main method.
     * 
     * @param args
     *            the arguments
     */
    public static void main(final String[] args)
    {
        String logFileEnv = System.getenv().get("LOG4J_CONFIGURATION_FILE");
        String logFileProperty = System.getProperty("log4j2.configurationFile");

        // Configure log.
        if ((StringUtils.isBlank(logFileEnv) && (StringUtils.isBlank(logFileProperty))))
        {
            File loggerConfig = new File("log4j2.properties");
            if (loggerConfig.exists())
            {
                // Try to initialize logs with a log file.
                Configurator.initialize(null, loggerConfig.getAbsolutePath());
                Logger logger = LoggerFactory.getLogger(StatoolInfosLauncher.class);
                logger.info("Dedicated log configuration done.");
                logger.info("Configuration file was found in [{}].", loggerConfig.getAbsoluteFile());
            }
            else
            {
                //
                Level logEnvLevel = Level.getLevel(StringUtils.defaultString(System.getenv().get("LOG4J_LEVEL"), " "));
                Level logPropertyLevel = Level.getLevel(StringUtils.defaultString(System.getProperty("log4j2.level", " ")));

                Level level;
                if (logPropertyLevel != null)
                {
                    level = logPropertyLevel;
                }
                else if (logEnvLevel != null)
                {
                    level = logEnvLevel;
                }
                else
                {
                    level = Level.INFO;
                }

                // Build a custom default log configuration.
                ConfigurationBuilder<BuiltConfiguration> builder = ConfigurationBuilderFactory.newConfigurationBuilder();
                builder.setStatusLevel(Level.ERROR);
                builder.setConfigurationName("CustomDefaultLogger");
                AppenderComponentBuilder appenderBuilder = builder.newAppender("Console", "CONSOLE").addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT);
                appenderBuilder.add(builder.newLayout("PatternLayout").addAttribute("pattern", "%m%n"));
                RootLoggerComponentBuilder rootLogger = builder.newRootLogger(level);
                rootLogger.add(builder.newAppenderRef("Console"));
                builder.add(appenderBuilder);
                builder.add(rootLogger);
                Configurator.reconfigure(builder.build());

                Logger logger = LoggerFactory.getLogger(StatoolInfosLauncher.class);
                logger.debug("Custom default log configuration done.");

                /*
                Configurator.initialize(new DefaultConfiguration());
                Configurator.setRootLevel(Level.INFO);
                Logger logger = LoggerFactory.getLogger(StatoolInfosLauncher.class);
                logger.debug("Default log configuration done.");
                */
            }
        }

        // Run.
        StatoolInfosCLI.run(args);
    }
}
