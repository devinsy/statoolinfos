/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics;

import java.util.ArrayList;
import java.util.Collections;

/**
 * The Class StringCounterList.
 */
public class StringCounterList extends ArrayList<StringCounter>
{
    private static final long serialVersionUID = 6392647621400816570L;

    /**
     * Instantiates a new string counter list.
     */
    public StringCounterList()
    {
        super();
    }

    /**
     * Reverse.
     *
     * @return the categories
     */
    public StringCounterList reverse()
    {
        StringCounterList result;

        Collections.reverse(this);

        result = this;

        //
        return result;
    }

    /**
     * Sort.
     *
     * @param sorting
     *            the sorting
     * @return the string counter list
     */
    public StringCounterList sort(final StringCounterComparator.Sorting sorting)
    {
        StringCounterList result;

        sort(new StringCounterComparator(sorting));

        result = this;

        //
        return result;
    }

    /**
     * Sort by counter.
     *
     * @return the string counter list
     */
    public StringCounterList sortByCounter()
    {
        StringCounterList result;

        result = sort(StringCounterComparator.Sorting.COUNTER);

        //
        return result;
    }

    /**
     * Sort by name.
     *
     * @return the services
     */
    public StringCounterList sortByString()
    {
        StringCounterList result;

        result = sort(StringCounterComparator.Sorting.STRING);

        //
        return result;
    }
}
