/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics;

import java.util.HashMap;

/**
 * The Class TimeMarkCounters.
 */
public class TimeMarkCounters extends HashMap<String, Long>
{
    private static final long serialVersionUID = -3437543089769867597L;

    /**
     * Instantiates a new time mark counters.
     */
    public TimeMarkCounters()
    {
        super();
    }

    /**
     * Gets the.
     *
     * @param timeMark
     *            the time mark
     * @return the long
     */
    public Long get(final String timeMark)
    {
        Long result;

        result = super.get(timeMark);

        //
        return result;
    }

    /**
     * Inc.
     *
     * @param timemark
     *            the timemark
     */
    public void inc(final String timemark)
    {
        Long value = super.get(timemark);
        if (value == null)
        {
            value = 0L;
        }

        put(timemark, value + 1);
    }
}
