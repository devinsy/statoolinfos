/*
 * Copyright (C) 2022-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.gitea;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * The Class GiteaAPI.
 */
public class GiteaAPI
{
    private String url;
    private String token;
    private JSONArray users;
    private JSONArray organizations;
    private JSONArray repositories;

    /**
     * Instantiates a new gitea API.
     *
     * @param url
     *            the url
     * @param token
     *            the token
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws ParseException
     *             the parse exception
     */
    public GiteaAPI(final String url, final String token) throws IOException, ParseException
    {
        try
        {
            this.url = StringUtils.removeEnd(url, "/");
            this.token = token;

            //
            String json = IOUtils.toString(new URL(this.url + "/api/v1/admin/users?limit=100000&token=" + this.token), Charset.defaultCharset());
            this.users = (JSONArray) (new JSONParser().parse(json));

            //
            json = IOUtils.toString(new URL(this.url + "/api/v1/admin/orgs?limit=100000&token=" + this.token), Charset.defaultCharset());
            this.organizations = (JSONArray) (new JSONParser().parse(json));

            //
            json = IOUtils.toString(new URL(this.url + "/api/v1/repos/search?limit=100000&token=" + this.token), Charset.defaultCharset());
            this.repositories = (JSONArray) ((JSONObject) (new JSONParser().parse(json))).get("data");
        }
        catch (IOException exception)
        {
            String message = exception.getMessage();
            if (message.matches(".*token=\\w+.*"))
            {
                message = message.replaceAll("token=\\w+", "token=*******");
                throw new IOException(message);
            }
            else
            {
                throw exception;
            }
        }
    }

    /**
     * Gets the active user count.
     *
     * @return the active user count
     */
    public long getActiveUserCount()
    {
        long result;

        result = 0;
        LocalDateTime limit = LocalDateTime.now().minusDays(31);
        for (Object user : this.users)
        {
            String lastLogin = getJSONString(user, "last_login");

            if (!StringUtils.isBlank(lastLogin))
            {
                try
                {
                    LocalDateTime date = LocalDateTime.parse(lastLogin, DateTimeFormatter.ISO_DATE_TIME);
                    if (date.isAfter(limit))
                    {
                        result += 1;
                    }
                }
                catch (DateTimeParseException exception)
                {
                    System.out.println("OUPS");
                }
            }
        }

        //
        return result;
    }

    /**
     * Gets the fork repository count.
     *
     * @return the fork repository count
     */
    public long getForkRepositoryCount()
    {
        long result;

        result = 0;
        for (Object repository : this.repositories)
        {
            if (getJSONBoolean(repository, "fork"))
            {
                result += 1;
            }
        }

        //
        return result;
    }

    /**
     * Gets the mirror repository count.
     *
     * @return the mirror repository count
     */
    public long getMirrorRepositoryCount()
    {
        long result;

        result = 0;
        for (Object repository : this.repositories)
        {
            if (getJSONBoolean(repository, "mirror"))
            {
                result += 1;
            }
        }

        //
        return result;
    }

    /**
     * Gets the organization count.
     *
     * @return the organization count
     */
    public long getOrganizationCount()
    {
        long result;

        result = this.organizations.size();

        //
        return result;
    }

    /**
     * Gets the private organization count.
     *
     * @return the private organization count
     */
    public long getPrivateOrganizationCount()
    {
        long result;

        result = 0;
        for (Object organization : this.organizations)
        {
            String visibility = getJSONString(organization, "visibility");

            if (StringUtils.equals(visibility, "private"))
            {
                result += 1;
            }
        }

        //
        return result;
    }

    /**
     * Gets the private repository count.
     *
     * @return the private repository count
     */
    public long getPrivateRepositoryCount()
    {
        long result;

        result = 0;
        for (Object repository : this.repositories)
        {
            if (getJSONBoolean(repository, "private"))
            {
                result += 1;
            }
        }

        //
        return result;
    }

    /**
     * Gets the public organization count.
     *
     * @return the public organization count
     */
    public long getPublicOrganizationCount()
    {
        long result;

        result = 0;
        for (Object organization : this.organizations)
        {
            String visibility = getJSONString(organization, "visibility");

            if (StringUtils.equals(visibility, "public"))
            {
                result += 1;
            }
        }

        //
        return result;
    }

    /**
     * Gets the public repository count.
     *
     * @return the public repository count
     */
    public long getPublicRepositoryCount()
    {
        long result;

        result = 0;
        for (Object repository : this.repositories)
        {
            if (!getJSONBoolean(repository, "private"))
            {
                result += 1;
            }
        }

        //
        return result;
    }

    /**
     * Gets the repository count.
     *
     * @return the repository count
     */
    public long getRepositoryCount()
    {
        long result;

        result = this.repositories.size();

        //
        return result;
    }

    /**
     * Gets the user count.
     *
     * @return the user count
     */
    public long getUserCount()
    {
        long result;

        result = this.users.size();

        //
        return result;
    }

    /**
     * Gets the boolean.
     *
     * @param object
     *            the object
     * @param key
     *            the key
     * @return the boolean
     */
    private static boolean getJSONBoolean(final Object object, final String key)
    {
        Boolean result;

        Object value = ((JSONObject) object).get(key);
        if (value == null)
        {
            result = false;
        }
        else
        {
            result = (Boolean) value;
        }

        //
        return result;
    }

    /**
     * Gets the long.
     *
     * @param object
     *            the object
     * @param key
     *            the key
     * @return the long
     */
    public static long getJSONLong(final Object object, final String key)
    {
        long result;

        Object value = ((JSONObject) object).get(key);
        if (value == null)
        {
            result = 0;
        }
        else
        {
            result = (Long) value;
        }

        //
        return result;
    }

    /**
     * Gets the as string.
     *
     * @param object
     *            the object
     * @param key
     *            the key
     * @return the as string
     */
    public static String getJSONString(final Object object, final String key)
    {
        String result;

        Object value = ((JSONObject) object).get(key);
        if (value == null)
        {
            result = "";
        }
        else
        {
            result = (String) value;
        }

        //
        return result;
    }
}
