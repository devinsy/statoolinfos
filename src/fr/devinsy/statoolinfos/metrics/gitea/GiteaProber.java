/*
 * Copyright (C) 2022-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.gitea;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.statoolinfos.core.DatabaseConfig;
import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.statoolinfos.metrics.UserCounters;
import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLog;
import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLogs;
import fr.devinsy.statoolinfos.metrics.httpaccess.HttpStatusCategory;
import fr.devinsy.statoolinfos.metrics.util.DatabaseProber;
import fr.devinsy.statoolinfos.metrics.util.DatafilesProber;
import fr.devinsy.statoolinfos.util.sql.SQLDatabase;
import fr.devinsy.strings.StringList;

/**
 * The Class GiteaProber.
 */
public class GiteaProber
{
    private static Logger logger = LoggerFactory.getLogger(GiteaProber.class);

    /**
     * Instantiates a new nextcloud prober.
     */
    public GiteaProber()
    {
    }

    /**
     * Probe.
     *
     * @param httpAccessLogs
     *            the http access logs
     * @param apiURL
     *            the api URL
     * @param apiToken
     *            the api token
     * @param dataPath
     *            the data path
     * @param databaseConfig
     *            the database config
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probe(final HttpAccessLogs httpAccessLogs, final String apiURL, final String apiToken, final File dataPath, final DatabaseConfig databaseConfig)
            throws IOException, StatoolInfosException
    {
        PathCounters result;

        // metrics.service.users
        // metrics.service.users.ipv4
        // metrics.service.users.ipv6
        result = probeHttpAccessLog(httpAccessLogs);

        // metrics.service.database.bytes
        try
        {
            if (databaseConfig.isSet())
            {
                SQLDatabase database = new SQLDatabase(databaseConfig.getUrl(), databaseConfig.getUser(), databaseConfig.getPassword());
                database.open();
                result.putAll(DatabaseProber.probe(database));
                database.close();
            }
            else
            {
                System.out.println("GITEA Database undefined.");
            }
        }
        catch (SQLException exception)
        {
            logger.error("ERROR with database.", exception);
        }

        // metrics.service.files.bytes
        // metrics.service.files.count
        result.putAll(DatafilesProber.probe(dataPath));

        //
        System.out.println("Probing GiteaAPI [" + apiURL + "]");
        if (!StringUtils.isBlank(apiURL) && (!StringUtils.isBlank(apiToken)))
        {
            try
            {
                GiteaAPI gitea = new GiteaAPI(apiURL, apiToken);

                // metrics.service.accounts
                // metrics.service.accounts.active
                result.set(gitea.getUserCount(), "metrics.service.accounts");
                result.set(gitea.getActiveUserCount(), "metrics.service.accounts.active");

                // metrics.forge.groups.* =
                // metrics.forge.groups.private.* =
                // metrics.forge.groups.public.* =
                result.set(gitea.getOrganizationCount(), "metrics.forge.groups");
                result.set(gitea.getPublicOrganizationCount(), "metrics.forge.groups.public");
                result.set(gitea.getPrivateOrganizationCount(), "metrics.forge.groups.private");

                // metrics.forge.projects.* =
                // metrics.forge.repositories.* =
                // metrics.forge.repositories.private.* =
                // metrics.forge.repositories.public.* =
                result.set(gitea.getRepositoryCount(), "metrics.forge.projects");
                result.set(gitea.getRepositoryCount(), "metrics.forge.repositories");
                result.set(gitea.getPublicRepositoryCount(), "metrics.forge.repositories.public");
                result.set(gitea.getPrivateRepositoryCount(), "metrics.forge.repositories.private");
                result.set(gitea.getForkRepositoryCount(), "metrics.forge.repositories.forks");
                result.set(gitea.getMirrorRepositoryCount(), "metrics.forge.repositories.mirrors");

                // metrics.issues.count.* =
                // metrics.issues.issuers.*=
                // metrics.issues.created.* =
                // metrics.issues.closed.* =

            }
            catch (IOException | ParseException exception)
            {
                System.out.println("ERROR using GiteaAPI: " + exception.getMessage());
            }
        }

        // ///////////////////////////////////////

        // # [Metrics spécifiques aux services de forges logicielles].
        //
        //
        // metrics.forge.commits.* =
        // metrics.forge.mergerequests.* =
        // metrics.forge.committers.* =
        // metrics.forge.mergerequesters.* =
        // metrics.forge.files.* =

        //
        return result;
    }

    /**
     * Probe http access log.
     *
     * @param logs
     *            the logs
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    private static PathCounters probeHttpAccessLog(final HttpAccessLogs logs) throws IOException, StatoolInfosException
    {
        PathCounters result;

        result = new PathCounters();

        // metrics.service.users
        // metrics.service.users.ipv4
        // metrics.service.users.ipv6
        UserCounters users = new UserCounters();
        UserCounters ipv4Users = new UserCounters();
        UserCounters ipv6Users = new UserCounters();

        StringList regexList = new StringList();

        /*
        GET /admin
        GET /admin/auths
        GET /admin/config
        GET /admin/emails
        GET /admin/hooks
        GET /admin/monitor
        GET /admin/notices
        GET /admin/orgs
        GET /admin/repos
        GET /admin/users
        */
        regexList.add("GET /admin(/auths|/config|/emails|/hooks|/monitor|/notices|/orgs|/repos|/users)? .*");

        /*
        GET /cpm
        GET /devinsy
        */
        // regexList.add("GET /\\w+ .*");

        /*
        GET /devinsy/bacasable
        */
        // regexList.add("GET /\\w+/\\w+ .*");

        /*
        GET /devinsy/bacasable/activity
        GET /devinsy/bacasable/commits/branch/master
        GET /devinsy/bacasable/compare/master...master
        GET /devinsy/bacasable/graph
        GET /devinsy/bacasable/issues
        GET /devinsy/bacasable/issues/new?project=1
        GET /devinsy/bacasable/projects
        GET /devinsy/bacasable/projects/1
        GET /devinsy/bacasable/projects/1/edit
        GET /devinsy/bacasable/pulls
        GET /devinsy/bacasable/pulls/1
        GET /devinsy/bacasable/releases
        GET /devinsy/bacasable/releases/tag/0.5.0
        GET /devinsy/bacasable/tags
        GET /devinsy/bacasable/wiki
        GET /devinsy/bacasable/wiki/Home
        */
        regexList.add("GET /\\S+/\\S+(/activity|/commits/branch/master|/compare/\\S+|/graph|/issues|/projects(/\\d+)|/pulls(/\\d+)|/releases(/tag/\\S+|/tags|/wiki|/wiki/\\S+)) .*");

        /*
        GET /devinsy/bacasable/settings
        GET /devinsy/bacasable/settings/branches
        GET /devinsy/bacasable/settings/collaboration
        GET /devinsy/bacasable/settings/hooks
        GET /devinsy/bacasable/settings/keys
        GET /devinsy/bacasable/settings/lfs
        GET /devinsy/bacasable/settings/lfs/locks
        GET /devinsy/bacasable/settings/lfs/pointers
        GET /devinsy/bacasable/settings/tags
         */
        regexList.add("GET /\\S+/\\S+/settings(/branches|/collaboration|/hooks|/keys|/lfs|/lfs/locks|/lfs/pointers|/tags)? .*");

        /*
        GET /org/create
        GET /org/devinsy/members
        GET /org/devinsy/settings
        GET /org/devinsy/teams
        GET /org/devinsy/teams/new
         */
        regexList.add("GET /org/(create|/\\S+/members|/\\S+/settings|/\\S+/teams|/\\S+/teams/new) .*");

        /*
        GET /explore/organizations
        GET /explore/repos
        GET /explore/users
        */
        regexList.add("GET (/explore/organizations|/explore/repos|/explore/users) .*");

        /*
        GET /issues
        GET /milestones
        GET /pulls
         */
        regexList.add("GET /(issues|milestones|pulls) .*");

        /*
        GET /repo/create
        GET /repo/fork/7
        GET /repo/migrate
        */
        regexList.add("GET /repo/(create|fork/\\d+|migrate) .*");

        /*
        GET /user/settings
        GET /user/settings/account
        GET /user/settings/appearance
        GET /user/settings/applications
        GET /user/settings/keys
        GET /user/settings/organization
        GET /user/settings/repos
        GET /user/settings/security
         */
        regexList.add("GET /user/settings(/account|/appearance|/applications|/keys|/organizations|/repos|/security)? .*");

        String regex = regexList.toString("(", "|", ")");

        Pattern USE_PATTERN = Pattern.compile(regex);

        //
        for (HttpAccessLog log : logs)
        {
            // General HTTP access logs.
            String year = log.getYear();
            String yearMonth = log.getYearMonth();
            String yearWeek = log.getYearWeek();
            String date = log.getDate();

            // metrics.service.users
            // metrics.service.users.ipv4
            // metrics.service.users.ipv6
            if ((log.getStatus().getCategory() == HttpStatusCategory.SUCCESS) && (!log.isBot()) && (USE_PATTERN.matcher(log.getRequest()).matches()))
            {
                String key = String.format("%s---%s", log.getIp(), log.getUserAgent());

                users.put(key, year, yearMonth, yearWeek, date);

                if (log.isIPv4())
                {
                    ipv4Users.put(key, year, yearMonth, yearWeek, date);
                }
                else
                {
                    ipv6Users.put(key, year, yearMonth, yearWeek, date);
                }
            }
        }

        //
        result.putAll(users.getCounters("metrics.service.users"));
        result.putAll(ipv4Users.getCounters("metrics.service.users.ipv4"));
        result.putAll(ipv6Users.getCounters("metrics.service.users.ipv6"));

        //
        return result;
    }
}
