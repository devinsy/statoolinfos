/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Matcher;

/**
 * The Class TimeMark.
 */
public class TimeMark
{
    private String value;

    /**
     * Instantiates a new time mark.
     *
     * @param value
     *            the value
     */
    public TimeMark(final String value)
    {
        this.value = value;
    }

    /**
     * Gets the year.
     *
     * @return the year
     */
    public Integer getYear()
    {
        Integer result;

        if (isYearMark())
        {
            result = Integer.parseInt(this.value);
        }
        else if (isYearMonth())
        {
            Matcher matcher = TimeMarkUtils.YEAR_MONTH_PATTERN.matcher(this.value);
            if (matcher.find())
            {
                result = Integer.parseInt(matcher.group("year"));
            }
            else
            {
                result = null;
            }
        }
        else if (isYearWeek())
        {
            Matcher matcher = TimeMarkUtils.YEAR_WEEK_PATTERN.matcher(this.value);
            if (matcher.find())
            {
                result = Integer.parseInt(matcher.group("year"));
            }
            else
            {
                result = null;
            }
        }
        else if (isDate())
        {
            Matcher matcher = TimeMarkUtils.DATE_PATTERN.matcher(this.value);
            if (matcher.find())
            {
                result = Integer.parseInt(matcher.group("year"));
            }
            else
            {
                result = null;
            }
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Checks if is date.
     *
     * @return true, if is date
     */
    public boolean isDate()
    {
        boolean result;

        result = TimeMarkUtils.isDate(this.value);

        //
        return result;
    }

    /**
     * Checks if is year mark.
     *
     * @return true, if is year mark
     */
    public boolean isYearMark()
    {
        boolean result;

        result = TimeMarkUtils.isYear(this.value);

        //
        return result;
    }

    /**
     * Checks if is year month.
     *
     * @return true, if is year month
     */
    public boolean isYearMonth()
    {
        boolean result;

        result = TimeMarkUtils.isYearMonth(this.value);

        //
        return result;
    }

    /**
     * Checks if is year week.
     *
     * @return true, if is year week
     */
    public boolean isYearWeek()
    {
        boolean result;

        result = TimeMarkUtils.isYearWeek(this.value);

        //
        return result;
    }

    /**
     * Sets the value.
     *
     * @param value
     *            the new value
     */
    public void setValue(final String value)
    {
        this.value = value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString()
    {
        String result;

        result = this.value;

        //
        return result;
    }

    /**
     * Day of.
     *
     * @param date
     *            the date
     * @return the time mark
     */
    public static TimeMark dayOf(final LocalDate date)
    {
        TimeMark result;

        String day = date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.FRANCE));
        result = new TimeMark(day);

        //
        return result;
    }

    /**
     * Day of.
     *
     * @param date
     *            the date
     * @return the time mark
     */
    public static TimeMark dayOf(final LocalDateTime date)
    {
        TimeMark result;

        String day = date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.FRANCE));
        result = new TimeMark(day);

        //
        return result;
    }

    /**
     * Month of.
     *
     * @param date
     *            the date
     * @return the time mark
     */
    public static TimeMark monthOf(final YearMonth date)
    {
        TimeMark result;

        String month = date.format(DateTimeFormatter.ofPattern("yyyy-MM", Locale.FRANCE));
        result = new TimeMark(month);

        //
        return result;
    }

    /**
     * Month of.
     *
     * @param date
     *            the date
     * @return the time mark
     */
    public static TimeMark yearMonthOf(final LocalDate date)
    {
        TimeMark result;

        String month = date.format(DateTimeFormatter.ofPattern("yyyy-MM", Locale.FRANCE));
        result = new TimeMark(month);

        //
        return result;
    }

    /**
     * Month of.
     *
     * @param date
     *            the date
     * @return the time mark
     */
    public static TimeMark yearMonthOf(final LocalDateTime date)
    {
        TimeMark result;

        String month = date.format(DateTimeFormatter.ofPattern("yyyy-MM", Locale.FRANCE));
        result = new TimeMark(month);

        //
        return result;
    }

    /**
     * Year of.
     *
     * @param date
     *            the date
     * @return the time mark
     */
    public static TimeMark yearOf(final LocalDate date)
    {
        TimeMark result;

        String year = date.format(DateTimeFormatter.ofPattern("yyyy", Locale.FRANCE));
        result = new TimeMark(year);

        //
        return result;
    }

    /**
     * Year of.
     *
     * @param date
     *            the date
     * @return the time mark
     */
    public static TimeMark yearOf(final LocalDateTime date)
    {
        TimeMark result;

        String year = date.format(DateTimeFormatter.ofPattern("yyyy", Locale.FRANCE));
        result = new TimeMark(year);

        //
        return result;
    }

    /**
     * Year week of.
     *
     * @param date
     *            the date
     * @return the time mark
     */
    public static TimeMark yearWeekOf(final LocalDate date)
    {
        TimeMark result;

        String yearWeek = date.format(DateTimeFormatter.ofPattern("yyyy-'W'ww", Locale.FRANCE));
        result = new TimeMark(yearWeek);

        //
        return result;
    }

    /**
     * Year week of.
     *
     * @param date
     *            the date
     * @return the time mark
     */
    public static TimeMark yearWeekOf(final LocalDateTime date)
    {
        TimeMark result;

        String yearWeek = date.format(DateTimeFormatter.ofPattern("yyyyww", Locale.FRANCE));
        result = new TimeMark(yearWeek);

        //
        return result;
    }
}
