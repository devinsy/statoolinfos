/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics;

import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.statoolinfos.util.IpUtils;
import fr.devinsy.strings.StringSet;

/**
 * The Class IpCounters.
 */
public class IpCounters extends HashMap<String, StringSet>
{
    private static final long serialVersionUID = -1597516678500117985L;

    /**
     * Instantiates a new path counters.
     */
    public IpCounters()
    {
        super();
    }

    /**
     * Gets the.
     *
     * @param timeMark
     *            the time mark
     * @return the string set
     */
    public StringSet get(final String timeMark)
    {
        StringSet result;

        result = super.get(timeMark);

        //
        return result;
    }

    /**
     * Gets the counters.
     *
     * @param prefix
     *            the prefix
     * @return the counters
     */
    public PathCounters getCounters(final String prefix)
    {
        PathCounters result;

        result = new PathCounters();

        for (String timeMark : keySet())
        {
            StringSet set = get(timeMark);
            result.inc(set.size(), prefix, timeMark);
        }

        //
        return result;
    }

    /**
     * Put.
     *
     * @param timemark
     *            the timemark
     * @param ip
     *            the ip
     */
    public void put(final String ip, final String timemark)
    {
        if (!StringUtils.isBlank(ip))
        {
            StringSet set = super.get(timemark);
            if (set == null)
            {
                set = new StringSet();
                put(timemark, set);
            }

            set.put(ip);
        }
    }

    /**
     * Put.
     *
     * @param ip
     *            the ip
     * @param timeMarks
     *            the time marks
     */
    public void put(final String ip, final String... timeMarks)
    {
        if (!StringUtils.isBlank(ip))
        {
            for (String timeMark : timeMarks)
            {
                put(ip, timeMark);
            }
        }
    }

    /**
     * Put ipv 4.
     *
     * @param ip
     *            the ip
     * @param timeMarks
     *            the time marks
     */
    public void putIpv4(final String ip, final String... timeMarks)
    {
        if (IpUtils.isIpv4(ip))
        {
            put(ip, timeMarks);
        }
    }

    /**
     * Put ipv 6.
     *
     * @param ip
     *            the ip
     * @param timeMarks
     *            the time marks
     */
    public void putIpv6(final String ip, final String... timeMarks)
    {
        if (IpUtils.isIpv6(ip))
        {
            put(ip, timeMarks);
        }
    }
}
