/*
 * Copyright (C) 2022-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.privatebin;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.statoolinfos.util.LineIterator;

/**
 * The Class PrivatebinPatchLogAnalyzer.
 */
public class PrivatebinPatchLogProber
{
    private static Logger logger = LoggerFactory.getLogger(PrivatebinPatchLogProber.class);

    // 2021-11-17 01:51:59 DELETE 7922dd508e0cf9b2
    public static final Pattern PATCH_LOG_PATTERN = Pattern.compile(
            "^(?<datetime>\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2})\\s+(?<action>\\S+)\\s+(?<id>\\S+)$");

    /**
     * Instantiates a new privatebin patch log analyzer.
     */
    private PrivatebinPatchLogProber()
    {
    }

    /**
     * Parses the log.
     *
     * @param line
     *            the line
     * @return the http log
     */
    public static PrivatebinPatchLog parseLog(final String line)
    {
        PrivatebinPatchLog result;

        Matcher matcher = PATCH_LOG_PATTERN.matcher(line);
        if (matcher.matches())
        {
            result = new PrivatebinPatchLog();
            result.setTime(LocalDateTime.parse(matcher.group("datetime"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withLocale(Locale.ENGLISH)));
            result.setAction(PrivatebinLogAction.of(matcher.group("action")));
            result.setId(matcher.group("id"));
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Probe.
     *
     * @param file
     *            the file
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static PathCounters probe(final File file) throws IOException
    {
        PathCounters result;

        System.out.println("Probing file [" + file.getAbsolutePath() + "]");

        result = new PathCounters();

        //
        int lineCount = 0;
        LineIterator iterator = new LineIterator(file);
        while (iterator.hasNext())
        {
            lineCount += 1;
            String line = iterator.next();

            try
            {
                PrivatebinPatchLog log = parseLog(line);
                // System.out.println("====> " + log);
                if (log == null)
                {
                    logger.warn("LINE IS NOT MATCHING [{}]", line);
                }
                else
                {
                    probeLog(log);
                }
            }
            catch (Exception exception)
            {
                logger.warn("Error parsing line [{}][{}]", line, exception.getMessage());
                exception.printStackTrace();
            }
        }
        System.out.println("PrivatebinPatchLog line count=" + lineCount);

        //
        return result;
    }

    /**
     * Probe log.
     *
     * @param log
     *            the log
     */
    public static PathCounters probeLog(final PrivatebinPatchLog log)
    {
        PathCounters result;

        result = new PathCounters();

        // logger.info("==================");
        if (log != null)
        {
            // logger.info("LINE IS MATCHING [{}]", log);

            // Timemarks.
            String year = log.getYear();
            String yearMonth = log.getYearMonth();
            String yearWeek = log.getYearWeek();
            String date = log.getDate();

            // metrics.pastebins.created
            // metrics.pastebins.reads
            // metrics.pastebins.deleted
            // metrics.pastebins.comment
            switch (log.getAction())
            {
                case COMMENT:
                    result.inc("metrics.pastebins.comment", year, yearMonth, yearWeek, date);
                break;

                case CREATE:
                    result.inc("metrics.pastebins.created", year, yearMonth, yearWeek, date);
                break;

                case DELETE:
                    result.inc("metrics.pastebins.deleted", year, yearMonth, yearWeek, date);
                break;

                case READ:
                    result.inc("metrics.pastebins.reads", year, yearMonth, yearWeek, date);
                break;

                default:
                    System.out.println("UNKNOWN Privatebin patch log ation!");
            }
        }

        //
        return result;
    }
}
