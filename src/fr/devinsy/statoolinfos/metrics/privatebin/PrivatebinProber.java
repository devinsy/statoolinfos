/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.privatebin;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.metrics.PathCounter;
import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.statoolinfos.metrics.UserCounters;
import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLog;
import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLogs;
import fr.devinsy.statoolinfos.metrics.util.DatafilesProber;
import fr.devinsy.statoolinfos.util.FilesUtils;

/**
 * The Class PrivatebinProber.
 */
public class PrivatebinProber
{
    /**
     * Instantiates a new privatebin prober.
     */
    private PrivatebinProber()
    {
    }

    /**
     * Probe.
     *
     * @param logs
     *            the logs
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    private static PathCounters probe(final HttpAccessLogs logs) throws IOException, StatoolInfosException
    {
        PathCounters result;

        result = new PathCounters();

        //
        UserCounters users = new UserCounters();
        UserCounters ipv4Users = new UserCounters();
        UserCounters ipv6Users = new UserCounters();

        //
        for (HttpAccessLog log : logs)
        {
            // logger.info("LINE IS MATCHING [{}]", log);
            // logger.info(log.getHttpUserAgent().toString());

            // General HTTP access logs.
            String year = log.getYear();
            String yearMonth = log.getYearMonth();
            String yearWeek = log.getYearWeek();
            String date = log.getDate();

            // metrics.service.users
            // metrics.service.users.ipv4
            // metrics.service.users.ipv6
            if (StringUtils.startsWithAny(log.getRequest(), "POST ", "GET /?pasteid="))
            {
                String key = String.format("%s---%s", log.getIp(), log.getUserAgent());
                users.put(key, year, yearMonth, yearWeek, date);

                if (log.isIPv4())
                {
                    ipv4Users.put(key, year, yearMonth, yearWeek, date);
                }
                else
                {
                    ipv6Users.put(key, year, yearMonth, yearWeek, date);
                }
            }

            // metrics.pastebins.created
            // metrics.pastebins.reads
            // metrics.pastebins.deleted
            if (StringUtils.startsWith(log.getRequest(), "POST "))
            {
                result.inc("metrics.pastebins.created", year, yearMonth, yearWeek, date);
            }
            else if (StringUtils.startsWith(log.getRequest(), "GET /?pasteid="))
            {
                if (StringUtils.contains(log.getRequest(), "&deletetoken="))
                {
                    result.inc("metrics.pastebins.deleted", year, yearMonth, yearWeek, date);
                }
                else
                {
                    result.inc("metrics.pastebins.reads", year, yearMonth, yearWeek, date);
                }
            }
        }

        //
        result.putAll(users.getCounters("metrics.service.users"));
        result.putAll(ipv4Users.getCounters("metrics.service.users.ipv4"));
        result.putAll(ipv6Users.getCounters("metrics.service.users.ipv6"));

        //
        return result;
    }

    /**
     * Probe.
     *
     * @param httpAccessLogs
     *            the http access logs
     * @param datafileDirectory
     *            the datafile directory
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probe(final HttpAccessLogs httpAccessLogs, final File datafileDirectory) throws IOException, StatoolInfosException
    {
        PathCounters result;

        // metrics.service.users
        // metrics.service.users.ipv4
        // metrics.service.users.ipv6
        // metrics.pastebins.created
        // metrics.pastebins.reads
        // metrics.pastebins.deleted
        result = probe(httpAccessLogs);

        // metrics.service.files.bytes
        // metrics.service.files.count
        result.putAll(DatafilesProber.probe(datafileDirectory));

        // metrics.pastebins.count
        if ((datafileDirectory != null) && (datafileDirectory.exists()) && (datafileDirectory.isDirectory()))
        {
            long count = FilesUtils.searchByWildcard(datafileDirectory.getAbsolutePath() + "/??/*").size();
            result.set(count, "metrics.pastebins.count");

            //
            File patchLogs = new File(datafileDirectory, "metrics.log");
            if (patchLogs.exists())
            {
                PathCounters data = PrivatebinPatchLogProber.probe(patchLogs);

                for (PathCounter counter : data.values())
                {
                    result.put(counter);
                }
            }
        }

        // metrics.pastebins.purged
        // purged = count(n-1) - count(n) + created - delete

        //
        return result;
    }
}
