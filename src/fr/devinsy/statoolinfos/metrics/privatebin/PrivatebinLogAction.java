/*
 * Copyright (C) 2022 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.privatebin;

import org.apache.commons.lang3.StringUtils;

/**
 * The Enum PrivatebinLogAction.
 */
public enum PrivatebinLogAction
{
    COMMENT,
    CREATE,
    DELETE,
    READ;

    /**
     * Of.
     *
     * @param value
     *            the value
     * @return the privatebin log action
     */
    public static PrivatebinLogAction of(final String value)
    {
        PrivatebinLogAction result;

        if (StringUtils.equals(value, "COMMENT"))
        {
            result = COMMENT;
        }
        else if (StringUtils.equals(value, "CREATE"))
        {
            result = CREATE;
        }
        else if (StringUtils.equals(value, "DELETE"))
        {
            result = DELETE;
        }
        else if (StringUtils.equals(value, "READ"))
        {
            result = READ;
        }
        else
        {
            result = null;
        }

        //
        return result;
    }
}
