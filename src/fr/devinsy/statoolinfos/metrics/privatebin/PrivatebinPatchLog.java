/*
 * Copyright (C) 2022-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.privatebin;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import fr.devinsy.statoolinfos.metrics.TimeMarkUtils;

/**
 * The Class PrivatebinPatchLog.
 */
public class PrivatebinPatchLog
{
    private LocalDateTime time;
    private PrivatebinLogAction action;
    private String id;

    /**
     * Instantiates a new privatebin patch log.
     */
    public PrivatebinPatchLog()
    {
        this.time = null;
        this.action = null;
        this.id = null;
    }

    /**
     * Gets the action.
     *
     * @return the action
     */
    public PrivatebinLogAction getAction()
    {
        return this.action;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public String getDate()
    {
        String result;

        result = this.time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.FRANCE));

        //
        return result;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId()
    {
        return this.id;
    }

    /**
     * Gets the time.
     *
     * @return the time
     */
    public LocalDateTime getTime()
    {
        return this.time;
    }

    /**
     * Gets the year.
     *
     * @return the year
     */
    public String getYear()
    {
        String result;

        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearOf(this.time);
        }

        //
        return result;
    }

    /**
     * Gets the year month.
     *
     * @return the year month
     */
    public String getYearMonth()
    {
        String result;

        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearMonthOf(this.time);
        }
        //
        return result;
    }

    /**
     * Gets the year week.
     *
     * @return the year week
     */
    public String getYearWeek()
    {
        String result;
        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearWeekOf(this.time);
        }
        //
        return result;
    }

    /**
     * Sets the action.
     *
     * @param action
     *            the new action
     */
    public void setAction(final PrivatebinLogAction action)
    {
        this.action = action;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(final String id)
    {
        this.id = id;
    }

    /**
     * Sets the time.
     *
     * @param time
     *            the new time
     */
    public void setTime(final LocalDateTime time)
    {
        this.time = time;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String result;

        result = String.format("[time=%s, action=%s, id=%s]", this.time, this.action, this.id);

        //
        return result;
    }
}
