/*
 * Copyright (C) 2021-2022 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.mumble;

import java.io.IOException;

import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.metrics.PathCounters;

/**
 * The Class MinetestProber.
 */
public class MumbleProber
{
    /**
     * Instantiates a new minetest prober.
     */
    public MumbleProber()
    {
    }

    /**
     * Probe.
     *
     * @param logs
     *            the logs
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probe(final String logs) throws IOException, StatoolInfosException
    {
        PathCounters result;

        // metrics.service.users
        // metrics.service.ip
        // metrics.service.ip.ipv4
        // metrics.service.ip.ipv6
        // metrics.audioconferencing.joiners
        // metrics.audioconferencing.rooms.active
        // metrics.mumble.logs.debug
        // metrics.mumble.logs.warning
        // metrics.mumble.logs.critical
        // metrics.mumble.logs.fatal
        // metrics.mumble.logs.default
        // metrics.mumble.globalbans
        // metrics.mumble.globalbans.ip
        result = MumbleLogAnalyzer.probe(logs);

        // result.putAll(MumbleDatabaseAnalyzer.probe());
        // metrics.service.accounts
        // metrics.service.database.bytes

        // metrics.service.files.bytes

        //
        return result;
    }
}
