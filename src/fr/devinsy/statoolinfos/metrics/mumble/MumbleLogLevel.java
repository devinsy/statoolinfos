/*
 * Copyright (C) 2021-2022 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.mumble;

import org.apache.commons.lang3.StringUtils;

/**
 * The Enum MinetestLogLevel.
 * 
 * List comes from Mumble-server source:
 * https://salsa.debian.org/pkg-voip-team/mumble/-/blob/debian/src/murmur/main.cpp#L74
 */
public enum MumbleLogLevel
{
    DEBUG,
    WARNING,
    CRITICAL,
    FATAL,
    DEFAULT;

    /**
     * Of.
     *
     * @param value
     *            the value
     * @return the mumble log level
     */
    public static MumbleLogLevel of(final String value)
    {
        MumbleLogLevel result;

        if (StringUtils.equals(value, "D"))
        {
            result = DEBUG;
        }
        else if (StringUtils.equals(value, "W"))
        {
            result = WARNING;
        }
        else if (StringUtils.equals(value, "C"))
        {
            result = CRITICAL;
        }
        else if (StringUtils.equals(value, "F"))
        {
            result = FATAL;
        }
        else
        {
            result = DEFAULT;
        }

        //
        return result;
    }
}
