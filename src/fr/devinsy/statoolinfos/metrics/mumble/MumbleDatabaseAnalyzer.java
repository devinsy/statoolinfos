/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.mumble;

import java.io.IOException;

import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.metrics.PathCounters;

/**
 * The Class HttpAccessLogProber.
 */
public class MumbleDatabaseAnalyzer
{
    /**
     * Instantiates a new http access log prober.
     */
    private MumbleDatabaseAnalyzer()
    {
    }

    /**
     * Probe.
     *
     * @param databasePath
     *            the database path
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probe(final String databasePath) throws IOException, StatoolInfosException
    {
        PathCounters result;

        result = new PathCounters();

        //
        return result;
    }
}
