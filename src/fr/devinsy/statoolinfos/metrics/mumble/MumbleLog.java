/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.mumble;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.devinsy.statoolinfos.metrics.TimeMarkUtils;
import fr.devinsy.strings.StringList;

/**
 * The Class MinetestLog.
 */
public class MumbleLog
{
    // 1 => <8:neox(-1)> Authenticated
    public static final Pattern NICK_PATTERN = Pattern.compile("^\\d+ => <\\d+:(?<nick>\\S+)\\(-?\\d+\\)> Authenticated$");

    // 1 => <6:Cpm(-1)> Moved Cpm:6(-1) to Salon blabla 1[3:0]
    public static final Pattern ROOM_PATTERN = Pattern.compile("^\\d+ => <\\S+> Moved \\S+ to (?<room>.+)\\[.+\\]$");

    // 1 => <6:(-1)> New connection: 123.456.78.90:54958
    // 1 => <2:(-1)> New connection: [1234:5678:90ab:cdef::aaaa:eeee]:57588
    public static final Pattern IP_PATTERN = Pattern.compile("^\\d+ => \\S+ New connection: \\[?(?<ip>.+)\\]?:\\d+$");

    // 1 => Ignoring connection: 123.45.555.555:44124 (Global ban)
    public static final Pattern GLOBALBAN_PATTERN = Pattern.compile("^\\d+ => Ignoring connection: \\[?(?<ip>.+)\\]?:\\d+ \\(Global ban\\)$");

    private MumbleLogLevel level;
    private LocalDateTime time;
    private String message;

    /**
     * Instantiates a new mumble log.
     */
    public MumbleLog()
    {
        this.time = null;
        this.level = null;
        this.message = null;
    }

    public String getDate()
    {
        String result;

        result = this.time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.FRANCE));

        //
        return result;
    }

    /**
     * Gets the global ban ip.
     *
     * @return the global ban ip
     */
    public String getGlobalBanIp()
    {
        String result;

        Matcher matcher = GLOBALBAN_PATTERN.matcher(this.message);
        if (matcher.matches())
        {
            result = matcher.group("ip");
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Gets the ip.
     *
     * @return the ip
     */
    public String getIp()
    {
        String result;

        Matcher matcher = IP_PATTERN.matcher(this.message);
        if (matcher.matches())
        {
            result = matcher.group("ip");
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    public MumbleLogLevel getLevel()
    {
        return this.level;
    }

    public String getMessage()
    {
        return this.message;
    }

    /**
     * Gets the nickname.
     *
     * @return the nickname
     */
    public String getNickname()
    {
        String result;

        Matcher matcher = NICK_PATTERN.matcher(this.message);
        if (matcher.matches())
        {
            result = matcher.group("nick");
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Gets the room name.
     *
     * @return the room name
     */
    public String getRoomName()
    {
        String result;

        Matcher matcher = ROOM_PATTERN.matcher(this.message);
        if (matcher.matches())
        {
            result = matcher.group("room");
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Gets the time.
     *
     * @return the time
     */
    public LocalDateTime getTime()
    {
        return this.time;
    }

    /**
     * Gets the year.
     *
     * @return the year
     */
    public String getYear()
    {
        String result;

        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearOf(this.time);
        }

        //
        return result;
    }

    /**
     * Gets the year month.
     *
     * @return the year month
     */
    public String getYearMonth()
    {
        String result;

        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearMonthOf(this.time);
        }
        //
        return result;
    }

    /**
     * Gets the year week.
     *
     * @return the year week
     */
    public String getYearWeek()
    {
        String result;
        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearWeekOf(this.time);
        }
        //
        return result;
    }

    /**
     * Sets the level.
     *
     * @param level
     *            the new level
     */
    public void setLevel(final MumbleLogLevel level)
    {
        this.level = level;
    }

    /**
     * Sets the message.
     *
     * @param message
     *            the new message
     */
    public void setMessage(final String message)
    {
        this.message = message;
    }

    /**
     * Sets the time.
     *
     * @param time
     *            the new time
     */
    public void setTime(final LocalDateTime time)
    {
        this.time = time;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        String result;

        StringList buffer = new StringList();

        buffer.append("[time=").append(this.time).append("]");

        result = buffer.toString();

        //
        return result;
    }
}
