/*
 * Copyright (C) 2021-2023 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.mumble;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.metrics.IpCounters;
import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.statoolinfos.metrics.UserCounters;
import fr.devinsy.statoolinfos.util.FilesUtils;
import fr.devinsy.statoolinfos.util.IpUtils;
import fr.devinsy.statoolinfos.util.LineIterator;

/**
 * The Class HttpAccessLogProber.
 */
public class MumbleLogAnalyzer
{
    private static Logger logger = LoggerFactory.getLogger(MumbleLogAnalyzer.class);

    // https://salsa.debian.org/pkg-voip-team/mumble/-/blob/debian/src/murmur/main.cpp#L92
    // QString m= QString::fromLatin1("<%1>%2 %3")
    // .arg(QChar::fromLatin1(c))
    // .arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz"))
    // .arg(msg);
    public static final Pattern MUMBLE_LOG_PATTERN = Pattern.compile("^<(?<level>[DWCFX])>(?<datetime>\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}.\\d{3}) (?<message>.*)$");

    private int errorCount;
    private PathCounters counters;
    private UserCounters users;
    private IpCounters ips;
    private IpCounters ipv4;
    private IpCounters ipv6;
    private UserCounters rooms;
    private IpCounters globalBanIps;

    /**
     * Instantiates a new http access log prober.
     */
    public MumbleLogAnalyzer()
    {
        this.counters = new PathCounters();
        this.users = new UserCounters();
        this.ips = new IpCounters();
        this.ipv4 = new IpCounters();
        this.ipv6 = new IpCounters();
        this.rooms = new UserCounters();
        this.globalBanIps = new IpCounters();
    }

    /**
     * Gets the counters.
     *
     * @return the counters
     */
    public PathCounters getCounters()
    {
        PathCounters result;

        result = new PathCounters();
        result.putAll(this.counters);

        result.putAll(this.users.getCounters("metrics.service.users"));
        result.putAll(this.ips.getCounters("metrics.service.ip"));
        result.putAll(this.ipv4.getCounters("metrics.service.ip.ipv4"));
        result.putAll(this.ipv6.getCounters("metrics.service.ip.ipv6"));
        result.putAll(this.rooms.getCounters("metrics.audioconferencing.rooms.active"));
        result.putAll(this.globalBanIps.getCounters("metrics.mumble.globalbans.ip"));

        //
        return result;
    }

    /**
     * Gets the error count.
     *
     * @return the error count
     */
    public int getErrorCount()
    {
        return this.errorCount;
    }

    /**
     * Probe.
     *
     * @param file
     *            the file
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void probe(final File file) throws IOException
    {
        System.out.println("Probing file [" + file.getAbsolutePath() + "]");

        //
        LineIterator iterator = new LineIterator(file);
        while (iterator.hasNext())
        {
            String line = iterator.next();

            try
            {
                if ((!StringUtils.isBlank(line)) && (!StringUtils.startsWith(line, " ")) && (!StringUtils.startsWith(line, "-")))
                {
                    MumbleLog log = parseLog(line);
                    if (log == null)
                    {
                        logger.warn("LINE IS NOT MATCHING [{}]", line);
                    }
                    else
                    {
                        probeLog(log);
                    }
                }
            }
            catch (Exception exception)
            {
                logger.warn("Error parsing line [{}][{}]", line, exception.getMessage());
                exception.printStackTrace();
                this.errorCount += 1;
            }
        }
    }

    /**
     * Probe log.
     *
     * @param log
     *            the log
     */
    public void probeLog(final MumbleLog log)
    {
        // logger.info("==================");
        if (log != null)
        {
            // logger.info("LINE IS MATCHING [{}]", log);

            // Timemarks.
            String year = log.getYear();
            String yearMonth = log.getYearMonth();
            String yearWeek = log.getYearWeek();
            String date = log.getDate();

            // metrics.mumble.logs
            this.counters.inc("metrics.mumble.logs", year, yearMonth, yearWeek, date);

            // metrics.mumble.logs.debug
            // metrics.mumble.logs.warning
            // metrics.mumble.logs.critical
            // metrics.mumble.logs.fatal
            // metrics.mumble.logs.default
            if (log.getLevel() == MumbleLogLevel.DEBUG)
            {
                this.counters.inc("metrics.mumble.logs.debug", year, yearMonth, yearWeek, date);
            }
            else if (log.getLevel() == MumbleLogLevel.WARNING)
            {
                this.counters.inc("metrics.mumble.logs.warning", year, yearMonth, yearWeek, date);
            }
            else if (log.getLevel() == MumbleLogLevel.CRITICAL)
            {
                this.counters.inc("metrics.mumble.logs.critical", year, yearMonth, yearWeek, date);
            }
            else if (log.getLevel() == MumbleLogLevel.FATAL)
            {
                this.counters.inc("metrics.mumble.logs.fatal", year, yearMonth, yearWeek, date);
            }
            else if (log.getLevel() == MumbleLogLevel.DEFAULT)
            {
                this.counters.inc("metrics.mumble.logs.default", year, yearMonth, yearWeek, date);
            }
            else
            {
                this.counters.inc("metrics.mumble.logs.unknown", year, yearMonth, yearWeek, date);
            }

            // metrics.service.users
            this.users.put(log.getNickname(), year, yearMonth, yearWeek, date);

            // metrics.service.users.max
            // TODO

            // metrics.audioconferencing.joiners
            if (log.getMessage().contains(" Authenticated"))
            {
                this.counters.inc("metrics.audioconferencing.joiners", year, yearMonth, yearWeek, date);
            }

            // metrics.sevice.ip
            // metrics.service.ip.ipv4
            // metrics.service.ip.ipv6
            String ip = log.getIp();
            if (ip != null)
            {
                this.ips.put(ip, year, yearMonth, yearWeek, date);
                if (IpUtils.isIpv4(ip))
                {
                    this.ipv4.put(ip, year, yearMonth, yearWeek, date);
                }
                else
                {
                    this.ipv6.put(ip, year, yearMonth, yearWeek, date);
                }
            }

            // metrics.audioconferencing.rooms.active
            this.rooms.put(log.getRoomName(), year, yearMonth, yearWeek, date);

            // metrics.mumble.globalbans
            String globalBanIp = log.getGlobalBanIp();
            if (globalBanIp != null)
            {
                this.counters.inc("metrics.mumble.globalbans", year, yearMonth, yearWeek, date);
                this.globalBanIps.put(globalBanIp, year, yearMonth, yearWeek, date);
            }

            // metrics.audioconferencing.conferences
            // metrics.audioconferencing.hours
            // metrics.audioconferencing.traffic.received.bytes
            // metrics.audioconferencing.traffic.sent.bytes
            // TODO
        }
    }

    /**
     * Parses the log.
     *
     * @param line
     *            the line
     * @return the http log
     */
    public static MumbleLog parseLog(final String line)
    {
        MumbleLog result;

        Matcher matcher = MUMBLE_LOG_PATTERN.matcher(line);
        if (matcher.matches())
        {
            result = new MumbleLog();
            result.setTime(LocalDateTime.parse(matcher.group("datetime"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS").withLocale(Locale.ENGLISH)));
            result.setLevel(MumbleLogLevel.of(matcher.group("level")));
            result.setMessage(matcher.group("message"));
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Probe.
     *
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probe(final String source) throws IOException, StatoolInfosException
    {
        PathCounters result;

        MumbleLogAnalyzer analyzer = new MumbleLogAnalyzer();

        for (File file : FilesUtils.searchByWildcard(source))
        {
            analyzer.probe(file);
        }

        result = analyzer.getCounters();

        //
        return result;
    }
}
