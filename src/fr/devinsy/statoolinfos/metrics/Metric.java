/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics;

import java.time.YearMonth;

import fr.devinsy.statoolinfos.properties.MonthValues;
import fr.devinsy.strings.StringList;

/**
 * The Class Metric.
 */
public class Metric
{
    public enum Type
    {
        MONTHS,
        WEEKS,
        DAYS
    }

    private String path;
    private String name;
    private String description;
    private String startYear;
    private StringList yearValues;
    private StringList monthValues;
    private StringList weekValues;
    private StringList dayValues;

    /**
     * Instantiates a new metric.
     *
     * @param path
     *            the path
     * @param name
     *            the name
     * @param description
     *            the description
     * @param startYear
     *            the start year
     */
    public Metric(final String path, final String name, final String description, final String startYear)
    {
        this.path = path;
        this.name = name;
        this.description = description;
        this.startYear = startYear;
        this.yearValues = new StringList();
        this.monthValues = new StringList();
        this.weekValues = new StringList();
        this.dayValues = new StringList();
    }

    /**
     * Gets the day values.
     *
     * @return the day values
     */
    public StringList getDayValues()
    {
        return this.dayValues;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        return this.description;
    }

    /**
     * Gets the month values.
     *
     * @return the month values
     */
    public StringList getMonthValues()
    {
        return this.monthValues;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * Gets the path.
     *
     * @return the path
     */
    public String getPath()
    {
        return this.path;
    }

    /**
     * Gets the start year.
     *
     * @return the start year
     */
    public String getStartYear()
    {
        return this.startYear;
    }

    /**
     * Gets the week values.
     *
     * @return the week values
     */
    public StringList getWeekValues()
    {
        return this.weekValues;
    }

    /**
     * Gets the year values.
     *
     * @return the year values
     */
    public StringList getYearValues()
    {
        return this.yearValues;
    }

    /**
     * Checks if is empty.
     *
     * @return true, if is empty
     */
    public boolean isEmpty()
    {
        boolean result;

        if ((this.monthValues.isEmpty()) && (this.weekValues.isEmpty()) && (this.dayValues.isEmpty()))
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the new description
     */
    public void setDescription(final String description)
    {
        this.description = description;
    }

    /**
     * Sets the name.
     *
     * @param name
     *            the new name
     */
    public void setName(final String name)
    {
        this.name = name;
    }

    /**
     * Sets the start year.
     *
     * @param startYear
     *            the new start year
     */
    public void setStartYear(final String startYear)
    {
        this.startYear = startYear;
    }

    /**
     * To month values.
     *
     * @return the month values
     */
    public MonthValues toMonthValues()
    {
        MonthValues result;

        result = new MonthValues();
        result.setLabel(this.name);

        YearMonth timestamp = YearMonth.of(Integer.valueOf(this.startYear), 01);
        for (String value : this.monthValues)
        {
            result.put(timestamp, Double.valueOf(value));
            timestamp = timestamp.plusMonths(1);
        }

        //
        return result;
    }
}
