/*
 * Copyright (C) 2022-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Pattern;

import org.threeten.extra.YearWeek;

/**
 * The Class TimeMarkUtils.
 */
public class TimeMarkUtils
{
    public static final Pattern YEAR_PATTERN = Pattern.compile("^(?<year>\\d{4})$");
    public static final Pattern YEAR_MONTH_PATTERN = Pattern.compile("^(?<year>\\d{4})-(?<month>\\d{2})$");
    public static final Pattern YEAR_WEEK_PATTERN = Pattern.compile("^(?<year>\\d{4})-W(?<week>\\d{2})$");
    public static final Pattern DATE_PATTERN = Pattern.compile("^(?<year>\\d{4})-(?<month>\\d{2})-(?<day>\\d{2})$");

    /**
     * Instantiates a new time mark utils.
     */
    private TimeMarkUtils()
    {
    }

    /**
     * Day of.
     *
     * @param date
     *            the date
     * @return the string
     */
    public static String dateOf(final LocalDateTime date)
    {
        String result;

        result = date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.FRANCE));

        //
        return result;
    }

    /**
     * Checks if is date.
     *
     * @param value
     *            the value
     * @return true, if is date
     */
    public static boolean isDate(final String value)
    {
        boolean result;

        result = (DATE_PATTERN.matcher(value).matches());

        //
        return result;
    }

    /**
     * Checks if is year mark.
     *
     * @param value
     *            the value
     * @return true, if is year mark
     */
    public static boolean isYear(final String value)
    {
        boolean result;

        result = (YEAR_PATTERN.matcher(value).matches());

        //
        return result;
    }

    /**
     * Checks if is year month.
     *
     * @param value
     *            the value
     * @return true, if is year month
     */
    public static boolean isYearMonth(final String value)
    {
        boolean result;

        result = (YEAR_MONTH_PATTERN.matcher(value).matches());

        //
        return result;
    }

    /**
     * Checks if is year week.
     *
     * @param value
     *            the value
     * @return true, if is year week
     */
    public static boolean isYearWeek(final String value)
    {
        boolean result;

        result = (YEAR_WEEK_PATTERN.matcher(value).matches());

        //
        return result;
    }

    /**
     * Parses the date.
     *
     * @param value
     *            the value
     * @return the local date
     */
    public static LocalDate parseDate(final String value)
    {
        LocalDate result;

        result = LocalDate.parse(value);

        //
        return result;
    }

    /**
     * Parses the year.
     *
     * @param value
     *            the value
     * @return the int
     */
    public static int parseYear(final String value)
    {
        int result;

        result = Integer.parseInt(value);

        //
        return result;
    }

    /**
     * Parses the year month.
     *
     * @param value
     *            the value
     * @return the year month
     */
    public static YearMonth parseYearMonth(final String value)
    {
        YearMonth result;

        result = YearMonth.parse(value);

        //
        return result;
    }

    /**
     * Parses the year week.
     *
     * @param value
     *            the value
     * @return the year week
     */
    public static YearWeek parseYearWeek(final String value)
    {
        YearWeek result;

        result = YearWeek.parse(value);

        //
        return result;
    }

    /**
     * Month of.
     *
     * @param date
     *            the date
     * @return the string
     */
    public static String yearMonthOf(final LocalDateTime date)
    {
        String result;

        result = date.format(DateTimeFormatter.ofPattern("yyyy-MM", Locale.FRANCE));

        //
        return result;
    }

    /**
     * Year month of.
     *
     * @param date
     *            the date
     * @return the string
     */
    public static String yearMonthOf(final ZonedDateTime date)
    {
        String result;

        result = date.format(DateTimeFormatter.ofPattern("yyyy-MM", Locale.FRANCE));

        //
        return result;
    }

    /**
     * Year of.
     *
     * @param date
     *            the date
     * @return the string
     */
    public static String yearOf(final LocalDateTime date)
    {
        String result;

        result = date.format(DateTimeFormatter.ofPattern("yyyy", Locale.FRANCE));

        //
        return result;
    }

    /**
     * Year of.
     *
     * @param date
     *            the date
     * @return the string
     */
    public static String yearOf(final ZonedDateTime date)
    {
        String result;

        result = date.format(DateTimeFormatter.ofPattern("yyyy", Locale.FRANCE));

        //
        return result;
    }

    /**
     * Year week of.
     *
     * @param date
     *            the date
     * @return the string
     */
    public static String yearWeekOf(final LocalDateTime date)
    {
        String result;

        result = YearWeek.from(date).toString();

        //
        return result;
    }

    /**
     * Year week of.
     *
     * @param date
     *            the date
     * @return the string
     */
    public static String yearWeekOf(final ZonedDateTime date)
    {
        String result;

        result = YearWeek.from(date).toString();

        //
        return result;
    }
}
