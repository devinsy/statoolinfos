/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.minetest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.devinsy.statoolinfos.metrics.TimeMarkUtils;
import fr.devinsy.strings.StringList;

/**
 * The Class MinetestLog.
 */
public class MinetestLog
{
    // Cpm [fc00:e0a:22:ce10::adc5:a] joins game. List of players: Cpm
    public static final Pattern JOIN_PATTERN = Pattern.compile("^(?<nickname>\\S+) \\[(?<ip>.+)\\] joins game\\..*$");
    public static final Pattern NICK_PATTERN = Pattern
            .compile(
                    "^(player )?(?<nickname>\\S+) (activates |crafts |damaged by |digs |has gottern award |leaves game |moves stuff |places node |respawns |right-clicks object |takes stuff |times out\\.).*$");

    private LocalDateTime time;
    private MinetestLogLevel level;
    private String module;
    private String message;

    /**
     * Instantiates a new http access log.
     */
    public MinetestLog()
    {
        this.time = null;
        this.level = null;
        this.module = null;
        this.message = null;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public String getDate()
    {
        String result;

        result = this.time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.FRANCE));

        //
        return result;
    }

    /**
     * Gets the ip.
     *
     * @return the ip
     */
    public String getIp()
    {
        String result;

        Matcher matcher = JOIN_PATTERN.matcher(this.message);
        if (matcher.matches())
        {
            result = matcher.group("ip");
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Gets the level.
     *
     * @return the level
     */
    public MinetestLogLevel getLevel()
    {
        return this.level;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage()
    {
        return this.message;
    }

    /**
     * Gets the module.
     *
     * @return the module
     */
    public String getModule()
    {
        return this.module;
    }

    /**
     * Gets the nickname.
     *
     * @return the nickname
     */
    public String getNickname()
    {
        String result;

        Matcher matcher = NICK_PATTERN.matcher(this.message);
        if (matcher.matches())
        {
            result = matcher.group("nickname");
        }
        else
        {
            matcher = JOIN_PATTERN.matcher(this.message);
            if (matcher.matches())
            {
                result = matcher.group("nickname");
            }
            else
            {
                result = null;
            }
        }

        //
        return result;
    }

    /**
     * Gets the time.
     *
     * @return the time
     */
    public LocalDateTime getTime()
    {
        return this.time;
    }

    /**
     * Gets the year.
     *
     * @return the year
     */
    public String getYear()
    {
        String result;

        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearOf(this.time);
        }

        //
        return result;
    }

    /**
     * Gets the year month.
     *
     * @return the year month
     */
    public String getYearMonth()
    {
        String result;

        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearMonthOf(this.time);
        }
        //
        return result;
    }

    /**
     * Gets the year week.
     *
     * @return the year week
     */
    public String getYearWeek()
    {
        String result;
        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearWeekOf(this.time);
        }
        //
        return result;
    }

    /**
     * Sets the level.
     *
     * @param level
     *            the new level
     */
    public void setLevel(final MinetestLogLevel level)
    {
        this.level = level;
    }

    /**
     * Sets the message.
     *
     * @param message
     *            the new message
     */
    public void setMessage(final String message)
    {
        this.message = message;
    }

    /**
     * Sets the module.
     *
     * @param module
     *            the new module
     */
    public void setModule(final String module)
    {
        this.module = module;
    }

    /**
     * Sets the time.
     *
     * @param time
     *            the new time
     */
    public void setTime(final LocalDateTime time)
    {
        this.time = time;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString()
    {
        String result;

        StringList buffer = new StringList();

        buffer.append("[time=").append(this.time).append("]");

        result = buffer.toString();

        //
        return result;
    }
}
