/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.minetest;

import org.apache.commons.lang3.StringUtils;

/**
 * The Enum MinetestLogLevel.
 */
public enum MinetestLogLevel
{
    ACTION,
    WARNING,
    ERROR,
    NONE,
    UNKNOWN;

    /**
     * Of.
     *
     * @param value
     *            the value
     * @return the minetest log level
     */
    public static MinetestLogLevel of(final String value)
    {
        MinetestLogLevel result;

        if (StringUtils.equals(value, "ACTION"))
        {
            result = ACTION;
        }
        else if (StringUtils.equals(value, "WARNING"))
        {
            result = WARNING;
        }
        else if (StringUtils.equals(value, "ERROR"))
        {
            result = ERROR;
        }
        else if (StringUtils.equals(value, "NONE"))
        {
            result = NONE;
        }
        else
        {
            result = UNKNOWN;
        }

        //
        return result;
    }
}
