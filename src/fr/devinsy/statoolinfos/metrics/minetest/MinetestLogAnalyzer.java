/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.minetest;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.metrics.IpCounters;
import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.statoolinfos.metrics.UserCounters;
import fr.devinsy.statoolinfos.util.FilesUtils;
import fr.devinsy.statoolinfos.util.IpUtils;
import fr.devinsy.statoolinfos.util.LineIterator;

/**
 * The Class HttpAccessLogProber.
 */
public class MinetestLogAnalyzer
{
    private static Logger logger = LoggerFactory.getLogger(MinetestLogAnalyzer.class);

    // log_format combined '$remote_addr - $remote_user [$time_local] '
    // '"$request" $status $body_bytes_sent '
    // '"$http_referer" "$http_user_agent"';
    public static final Pattern MINETEST_LOG_PATTERN = Pattern.compile(
            "^(?<datetime>\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}): (?<level>[^\\[]*)\\[(?<module>[^\\]]+)\\]: (?<message>.*)$");

    private int errorCount;
    private PathCounters counters;
    private UserCounters activePlayers;
    private IpCounters ips;
    private IpCounters ipv4;
    private IpCounters ipv6;

    /**
     * Instantiates a new http access log prober.
     */
    public MinetestLogAnalyzer()
    {
        this.counters = new PathCounters();
        this.activePlayers = new UserCounters();
        this.ips = new IpCounters();
        this.ipv4 = new IpCounters();
        this.ipv6 = new IpCounters();
    }

    /**
     * Gets the counters.
     *
     * @return the counters
     */
    public PathCounters getCounters()
    {
        PathCounters result;

        result = new PathCounters();
        result.putAll(this.counters);

        result.putAll(this.activePlayers.getCounters("metrics.service.users"));
        result.putAll(this.activePlayers.getCounters("metrics.service.accounts.active"));
        result.putAll(this.ips.getCounters("metrics.service.ip"));
        result.putAll(this.ipv4.getCounters("metrics.service.ip.ipv4"));
        result.putAll(this.ipv6.getCounters("metrics.service.ip.ipv6"));

        //
        return result;
    }

    /**
     * Gets the error count.
     *
     * @return the error count
     */
    public int getErrorCount()
    {
        return this.errorCount;
    }

    /**
     * Probe.
     *
     * @param file
     *            the file
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void probe(final File file) throws IOException
    {
        System.out.println("Probing file [" + file.getAbsolutePath() + "]");

        //
        LineIterator iterator = new LineIterator(file);
        while (iterator.hasNext())
        {
            String line = iterator.next();

            try
            {
                if ((!StringUtils.isBlank(line)) && (!StringUtils.startsWith(line, " ")) && (!StringUtils.startsWith(line, "-")))
                {
                    MinetestLog log = parseLog(line);
                    if (log == null)
                    {
                        logger.warn("LINE IS NOT MATCHING [{}]", line);
                    }
                    else
                    {
                        probeLog(log);
                    }
                }
            }
            catch (Exception exception)
            {
                logger.warn("Error parsing line [{}][{}]", line, exception.getMessage());
                exception.printStackTrace();
                this.errorCount += 1;
            }
        }
    }

    /**
     * Probe log.
     *
     * @param log
     *            the log
     */
    public void probeLog(final MinetestLog log)
    {
        // logger.info("==================");
        if (log != null)
        {
            // logger.info("LINE IS MATCHING [{}]", log);

            // Timemarks.
            String year = log.getYear();
            String yearMonth = log.getYearMonth();
            String yearWeek = log.getYearWeek();
            String date = log.getDate();

            // metrics.metaverse.logs
            this.counters.inc("metrics.metaverse.logs", year, yearMonth, yearWeek, date);

            // metrics.metaverse.logs.action
            // metrics.metaverse.logs.warning
            // metrics.metaverse.logs.error
            // metrics.metaverse.logs.none
            // metrics.metaverse.logs.unknown
            // metrics.metaverse.wrongpassword
            if (log.getLevel() == MinetestLogLevel.ACTION)
            {
                this.counters.inc("metrics.metaverse.logs.action", year, yearMonth, yearWeek, date);
                if (log.getMessage().endsWith(" supplied wrong password (auth mechanism: SRP)."))
                {
                    this.counters.inc("metrics.metaverse.wrongpassword", year, yearMonth, yearWeek, date);
                }
            }
            else if (log.getLevel() == MinetestLogLevel.WARNING)
            {
                this.counters.inc("metrics.metaverse.logs.warning", year, yearMonth, yearWeek, date);
            }
            else if (log.getLevel() == MinetestLogLevel.ERROR)
            {
                this.counters.inc("metrics.metaverse.logs.error", year, yearMonth, yearWeek, date);
            }
            else if (log.getLevel() == MinetestLogLevel.NONE)
            {
                this.counters.inc("metrics.metaverse.logs.none", year, yearMonth, yearWeek, date);
            }
            else if (log.getLevel() == MinetestLogLevel.UNKNOWN)
            {
                this.counters.inc("metrics.metaverse.logs.unknown", year, yearMonth, yearWeek, date);
            }

            // metrics.service.users (= metrics.metaverse.players.active)
            this.activePlayers.put(log.getNickname(), year, yearMonth, yearWeek, date);

            // metrics.metaverse.players.max
            // TODO

            // metrics.metaverse.joiners
            if (log.getMessage().contains(" joins game."))
            {
                this.counters.inc("metrics.metaverse.joiners", year, yearMonth, yearWeek, date);
            }
            else if (log.getMessage().contains("CHAT: <"))
            {
                this.counters.inc("metrics.metaverse.chat", year, yearMonth, yearWeek, date);
            }

            // metrics.service.ip
            // metrics.service.ip.ipv4
            // metrics.service.ip.ipv6
            String ip = log.getIp();
            if (ip != null)
            {
                this.ips.put(ip, year, yearMonth, yearWeek, date);

                if (IpUtils.isIpv4(ip))
                {
                    this.ipv4.put(ip, year, yearMonth, yearWeek, date);
                }
                else
                {
                    this.ipv6.put(ip, year, yearMonth, yearWeek, date);
                }
            }

            // metrics.metaverse.games (visits)
            // metrics.metaverse.games.duration (visits)
            // TODO
        }
    }

    /**
     * Parses the log.
     *
     * @param line
     *            the line
     * @return the http log
     */
    public static MinetestLog parseLog(final String line)
    {
        MinetestLog result;

        Matcher matcher = MINETEST_LOG_PATTERN.matcher(line);
        if (matcher.matches())
        {
            result = new MinetestLog();
            result.setTime(LocalDateTime.parse(matcher.group("datetime"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withLocale(Locale.ENGLISH)));
            result.setLevel(MinetestLogLevel.of(matcher.group("level")));
            result.setModule(matcher.group("module"));
            result.setMessage(matcher.group("message"));
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Probe.
     *
     * @param source
     *            the source
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probe(final String source) throws IOException, StatoolInfosException
    {
        PathCounters result;

        MinetestLogAnalyzer analyzer = new MinetestLogAnalyzer();

        for (File file : FilesUtils.searchByWildcard(source))
        {
            analyzer.probe(file);
        }

        result = analyzer.getCounters();

        //
        return result;
    }
}
