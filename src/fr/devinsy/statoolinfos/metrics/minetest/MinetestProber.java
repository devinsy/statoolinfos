/*
 * Copyright (C) 2021-2023 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.minetest;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.statoolinfos.core.DatabaseConfig;
import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.statoolinfos.util.sql.SQLDatabase;
import fr.devinsy.strings.StringList;

/**
 * The Class MinetestProber.
 */
public class MinetestProber
{
    private static Logger logger = LoggerFactory.getLogger(MinetestProber.class);

    /**
     * Instantiates a new minetest prober.
     */
    public MinetestProber()
    {
    }

    /**
     * Probe.
     *
     * @param logs
     *            the logs
     * @param playerDatabaseConfig
     *            the player database config
     * @param worldDatabaseConfig
     *            the world database config
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probe(final String logs, final DatabaseConfig playerDatabaseConfig, final DatabaseConfig worldDatabaseConfig) throws IOException, StatoolInfosException
    {
        PathCounters result;

        // metrics.service.users, metrics.service.accounts.active
        // metrics.metaverse.joiners
        // metrics.service.ip
        // metrics.service.ip.ipv4
        // metrics.service.ip.ipv6
        // metrics.metaverse.logs
        // metrics.metaverse.logs.action
        // metrics.metaverse.logs.warning
        // metrics.metaverse.logs.error
        // metrics.metaverse.logs.none
        // metrics.metaverse.logs.unknown
        // metrics.metaverse.wrongpassword
        result = MinetestLogAnalyzer.probe(logs);

        // metrics.service.database.bytes
        // metrics.service.accounts
        {
            long databaseSize = 0;
            StringList timemarks = result.getNowTimeMarks();
            try
            {
                System.out.println("Probing database [" + playerDatabaseConfig.getUrl() + "]");
                if (playerDatabaseConfig.isSet())
                {
                    SQLDatabase database = new SQLDatabase(playerDatabaseConfig.getUrl(), playerDatabaseConfig.getUser(), playerDatabaseConfig.getPassword());
                    database.open();
                    databaseSize += database.getDatabaseSize();

                    // metrics.service.accounts
                    long accountCount = database.queryNumber("select count(*) from player;");
                    result.set(accountCount, "metrics.service.accounts", timemarks);

                    //
                    database.close();
                }
                else
                {
                    System.out.println("Minetest Players Database undefined.");
                }
            }
            catch (SQLException exception)
            {
                logger.error("ERROR with database.", exception);
            }

            if (!StringUtils.equals(playerDatabaseConfig.getUrl(), worldDatabaseConfig.getUrl()))
            {
                try
                {
                    System.out.println("Probing database [" + worldDatabaseConfig.getUrl() + "]");
                    if (worldDatabaseConfig.isSet())
                    {
                        SQLDatabase database = new SQLDatabase(worldDatabaseConfig.getUrl(), worldDatabaseConfig.getUser(), worldDatabaseConfig.getPassword());
                        database.open();
                        databaseSize += database.getDatabaseSize();
                        database.close();
                    }
                    else
                    {
                        System.out.println("Minetest Worlds Database undefined.");
                    }
                }
                catch (SQLException exception)
                {
                    logger.error("ERROR with database.", exception);
                }
            }

            // metrics.service.database.bytes
            result.set(databaseSize, "metrics.service.database.bytes", timemarks);
        }

        // metrics.service.files.bytes

        //
        return result;
    }
}
