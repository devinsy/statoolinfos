/*
 * Copyright (C) 2022-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.libreqr;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.statoolinfos.metrics.UserCounters;
import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLog;
import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLogs;
import fr.devinsy.statoolinfos.metrics.util.DatafilesProber;

/**
 * The Class LibreQRProber.
 */
public class LibreQRProber
{
    /**
     * Instantiates a new privatebin prober.
     */
    private LibreQRProber()
    {
    }

    /**
     * Probe.
     *
     * @param httpAccessLogs
     *            the http access logs
     * @param dataPath
     *            the data path
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probe(final HttpAccessLogs httpAccessLogs, final File dataPath) throws IOException, StatoolInfosException
    {
        PathCounters result;

        // metrics.service.users
        // metrics.service.users.ipv4
        // metrics.service.users.ipv6
        // metrics.barcodes.created
        // metrics.libreqr.barcodes.downloads
        result = probeHttpAccessLog(httpAccessLogs);

        // metrics.service.files.bytes, metrics.libreqr.cache.bytes
        // metrics.service.files.count, metrics.libreqr.cache.count
        result.putAll(DatafilesProber.probe(dataPath));

        //
        return result;
    }

    /**
     * Probe http access log.
     *
     * @param logs
     *            the logs
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    private static PathCounters probeHttpAccessLog(final HttpAccessLogs logs) throws IOException, StatoolInfosException
    {
        PathCounters result;

        result = new PathCounters();

        //
        UserCounters users = new UserCounters();
        UserCounters ipv4Users = new UserCounters();
        UserCounters ipv6Users = new UserCounters();

        Pattern USE_PATTERN = Pattern.compile("GET /temp/\\w+\\.png.*");
        Pattern CREATE_PATTERN = Pattern.compile("POST / .*");

        //
        for (HttpAccessLog log : logs)
        {
            // General HTTP access logs.
            String year = log.getYear();
            String yearMonth = log.getYearMonth();
            String yearWeek = log.getYearWeek();
            String date = log.getDate();

            // metrics.service.users
            // metrics.service.users.ipv4
            // metrics.service.users.ipv6
            if ((!log.isBot()) && (USE_PATTERN.matcher(log.getRequest()).matches()))
            {
                String key = String.format("%s---%s", log.getIp(), log.getUserAgent());

                users.put(key, year, yearMonth, yearWeek, date);

                if (log.isIPv4())
                {
                    ipv4Users.put(key, year, yearMonth, yearWeek, date);
                }
                else
                {
                    ipv6Users.put(key, year, yearMonth, yearWeek, date);
                }
            }

            // metrics.barcodes.created
            if ((log.getStatus().getCode() == 200) && (CREATE_PATTERN.matcher(log.getRequest()).matches()))
            {
                result.inc("metrics.barcodes.created", year, yearMonth, yearWeek, date);
            }

            // metrics.libreqr.barcodes.downloads
            if (USE_PATTERN.matcher(log.getRequest()).matches())
            {
                result.inc("metrics.libreqr.barcodes.downloads", year, yearMonth, yearWeek, date);
            }
        }

        //
        result.putAll(users.getCounters("metrics.service.users"));
        result.putAll(ipv4Users.getCounters("metrics.service.users.ipv4"));
        result.putAll(ipv6Users.getCounters("metrics.service.users.ipv6"));

        //
        return result;
    }
}
