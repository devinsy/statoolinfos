/*
 * Copyright (C) 2022 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.xmpp;

import java.io.File;
import java.io.IOException;

import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.statoolinfos.util.FilesUtils;

/**
 * The Class XmppProber.
 */
public class XmppProber
{
    /**
     * Instantiates a new xmpp prober.
     */
    public XmppProber()
    {
    }

    /**
     * Probe.
     *
     * @param httpLogs
     *            the http logs
     * @param httpLogRegex
     *            the http log regex
     * @param dataPath
     *            the data path
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probe(final String httpLogs, final String httpLogRegex, final File dataPath) throws IOException, StatoolInfosException
    {
        PathCounters result;

        // metrics.service.users
        // metrics.service.users.ipv4
        // metrics.service.users.ipv6
        result = XmppHttpLogAnalyzer.probe(FilesUtils.searchByWildcard(httpLogs), httpLogRegex);

        // metrics.service.files.bytes
        result.putAll(XmppDataAnalyzer.probe(dataPath));

        // metrics.xmpp.domains
        // ejabberdctl registered_vhosts | wc -l

        // metrics.xmpp.accounts
        // for domain in $(ejabberdctl registered_vhosts); do ejabberdctl
        // registered_users $domain ; done | wc -l

        // metrics.xmpp.accounts.active

        // metrics.xmpp.biboumi.users
        // metrics.xmpp.biboumi.servers
        // metrics.xmpp.biboumi.channels

        // echo " - actifs: $(count_active_accounts)"
        // echo " - inactifs: $(count_inactive_accounts '1 MONTH')"
        // echo " - suppressibles: $(count_removable_accounts '6 MONTHS')"
        // echo ""
        // # echo "Nombre d'utilisateurs du pont IRC: $(count_biboumi_users)
        // dont $(count_biboumi_external_users) provenant de la fédération"
        // # echo " - actifs: $(count_biboumi_active_users '1 MONTH') (provenant
        // de la fédération: $(count_biboumi_active_external_users '1 MONTH'))"
        // # echo " - inactifs: $(count_biboumi_inactive_users '1 MONTH')
        // (provenant de la fédération: $(count_biboumi_inactive_external_users
        // '1 MONTH'))"
        // echo ""
        // echo "Un compte inactif est un compte ne s'étant pas connecté durant
        // le mois écoulé."
        // echo "Un compte suppressible n'a jamais lu le message de bienvenue et
        // ne s'est pas connecté durant les six derniers mois."
        // echo ""
        // echo "Occupation d'espace disque des archives de messages (MAM):
        // $(count_archived_messages) o"
        // echo " - Messages stockés pour le pont IRC (MAM):
        // $(count_biboumi_archived_messages)"
        // echo ""
        // echo "Salons XMPP actifs: $(ejabberdctl muc_online_rooms global | wc
        // -l)"
        // # echo "Salons IRC actifs : $(count_biboumi_active_chan)"
        // # echo " - $(count_biboumi_active_servers) serveur(s) connectés"
        // echo ""
        // echo "Occupation d'espace disque téléversement de fichiers: $(du -sh
        // ${STORAGE_DIR} | awk '{print $1}')io"
        // per_account_storage_usage
        // echo "Quota de stockage par utilisateur: $(get_hard_quota) Mio"
        // echo ""
        // echo "Nombre d'appels audio/vidéo transmis: $(zgrep -h "$DATE"
        // /var/log/ejabberd/ejabberd* | grep -i -e 'Offering stun' | wc -l)"
        // echo "Quantité de données relayée (UDP): moyenne
        // $(get_avg_UDP_turn_relayed_data) MiB, max
        // $(get_max_UDP_turn_relayed_data) MiB, total
        // $(get_total_UDP_turn_relayed_data) MiB"
        // echo "Quantité de données relayée (TCP): moyenne
        // $(get_avg_TCP_turn_relayed_data) MiB, max
        // $(get_max_TCP_turn_relayed_data) MiB, total
        // $(get_total_TCP_turn_relayed_data) MiB"

        //
        return result;
    }
}
