/*
 * Copyright (C) 2022-2023 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.xmpp;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;

import org.apache.commons.io.FileUtils;

import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.statoolinfos.metrics.TimeMark;

// TODO: Auto-generated Javadoc
/**
 * The Class LibreQRDataAnalyzer.
 */
public class XmppDataAnalyzer
{
    /**
     * Instantiates a new http access log prober.
     */
    private XmppDataAnalyzer()
    {
    }

    /**
     * Probe.
     *
     * @param dataDirectory
     *            the data directory
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probe(final File dataDirectory) throws IOException, StatoolInfosException
    {
        PathCounters result;

        System.out.println("Probing directory [" + dataDirectory + "]");

        result = new PathCounters();

        if (dataDirectory != null)
        {
            if ((dataDirectory.exists()) && (dataDirectory.isDirectory()))
            {
                LocalDate now = LocalDate.now();
                String year = TimeMark.yearOf(now).toString();
                String yearMonth = TimeMark.yearMonthOf(now).toString();
                String yearWeek = TimeMark.yearWeekOf(now).toString();
                String date = TimeMark.dayOf(now).toString();

                // metrics.service.files.bytes
                long size = FileUtils.sizeOfDirectory(dataDirectory);
                result.set(size, "metrics.service.files.bytes", year, yearMonth, yearWeek, date);
            }
            else
            {
                System.out.println("WARNING: LibreQR data path is not valid.");
            }
        }

        //
        return result;
    }
}
