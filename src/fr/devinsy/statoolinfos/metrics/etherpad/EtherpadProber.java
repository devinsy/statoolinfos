/*
 * Copyright (C) 2022-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.etherpad;

import java.io.IOException;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.statoolinfos.core.DatabaseConfig;
import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLogs;
import fr.devinsy.statoolinfos.metrics.util.DatabaseProber;
import fr.devinsy.statoolinfos.util.Files;
import fr.devinsy.statoolinfos.util.sql.SQLDatabase;
import fr.devinsy.strings.StringList;

/**
 * The Class EtherpadProber.
 */
public class EtherpadProber
{
    private static Logger logger = LoggerFactory.getLogger(EtherpadProber.class);

    /**
     * Instantiates a new etherpad prober.
     */
    public EtherpadProber()
    {
    }

    /**
     * Probe.
     *
     * @param httpAccessLogs
     *            the http access logs
     * @param logs
     *            the logs
     * @param databaseConfig
     *            the database config
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probe(final HttpAccessLogs httpAccessLogs, final Files logs, final DatabaseConfig databaseConfig) throws IOException, StatoolInfosException
    {
        PathCounters result;

        // metrics.service.users
        // metrics.service.users.ipv4
        // metrics.service.users.ipv6
        if (logs.isEmpty())
        {
            result = EtherpadHttpLogAnalyzer.probe(httpAccessLogs);
        }
        else
        {
            result = EtherpadLogProber.probe(logs);
        }

        try
        {
            if (databaseConfig.isSet())
            {
                SQLDatabase database = new SQLDatabase(databaseConfig.getUrl(), databaseConfig.getUser(), databaseConfig.getPassword());
                database.open();

                // metrics.service.database.bytes
                result.putAll(DatabaseProber.probe(database));

                // metrics.textprocessors.characters
                // metrics.textprocessors.words

                // metrics.etherpad.database.pads
                // metrics.textprocessors.files
                StringList timemarks = result.getNowTimeMarks();
                String sql = "select count(*)  from store where key like 'pad:%' and key not like 'pad:%:%';";
                long count = database.queryNumber(sql);
                result.set(count, "metrics.etherpad.pads", timemarks);
                result.set(count, "metrics.textprocessors.files", timemarks);

                // metrics.etherpad.database.revs.
                sql = "SELECT count(*) from store where key like 'pad:%:revs:%';";
                count = database.queryNumber(sql);
                result.set(count, "metrics.etherpad.revs", timemarks);

                // metrics.etherpad.database.lines
                sql = "SELECT count(*) from store;";
                count = database.queryNumber(sql);
                result.set(count, "metrics.etherpad.database.lines", timemarks);

                // metrics.etherpad.database.lines.globalAuthor
                sql = "SELECT count(*) from store where key like 'globalAuthor:%';";
                count = database.queryNumber(sql);
                result.set(count, "metrics.etherpad.database.lines.globalAuthor", timemarks);

                // metrics.etherpad.database.lines.pad
                sql = "SELECT count(*) from store where key like 'pad:%';";
                count = database.queryNumber(sql);
                result.set(count, "metrics.etherpad.database.lines.pad", timemarks);

                // metrics.etherpad.database.lines.pad2readonly
                sql = "SELECT count(*) from store where key like 'pad2readonly:%';";
                count = database.queryNumber(sql);
                result.set(count, "metrics.etherpad.database.lines.pad2readonly", timemarks);

                // metrics.etherpad.database.lines.readonly2pad
                sql = "SELECT count(*) from store where key like 'readonly2pad:%';";
                count = database.queryNumber(sql);
                result.set(count, "metrics.etherpad.database.lines.readonly2pad", timemarks);

                // metrics.etherpad.database.lines.sessionstorage
                sql = "SELECT count(*) from store where key like 'sessionstorage:%';";
                count = database.queryNumber(sql);
                result.set(count, "metrics.etherpad.database.lines.sessionstorage", timemarks);

                // metrics.etherpad.database.lines.token2author
                sql = "SELECT count(*) from store where key like 'token2author:%';";
                count = database.queryNumber(sql);
                result.set(count, "metrics.etherpad.database.lines.token2author", timemarks);

                //
                database.close();
            }
            else
            {
                System.out.println("Etherpad Database undefined.");
            }

            // https://etherpad.org/doc/v1.8.18/#index_statistics
            // Etherpad keeps track of the goings-on inside the edit machinery.
            // If you'd like to have a look at this, just point your browser to
            // /stats.
            // We currently measure:
            //
            // totalUsers (counter)
            // connects (meter)
            // disconnects (meter)
            // pendingEdits (counter)
            // edits (timer)
            // failedChangesets (meter)
            // httpRequests (timer)
            // http500 (meter)
            // memoryUsage (gauge)
            //
            // https://pad.libre-service.eu/stats/

        }
        catch (SQLException exception)
        {
            logger.error("ERROR with database.", exception);
        }

        //
        return result;
    }
}
