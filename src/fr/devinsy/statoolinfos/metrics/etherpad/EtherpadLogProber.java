/*
 * Copyright (C) 2022 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.etherpad;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.statoolinfos.metrics.UserCounters;
import fr.devinsy.statoolinfos.util.Files;
import fr.devinsy.statoolinfos.util.LineIterator;

/**
 * The Class EtherpadLogProber.
 */
public class EtherpadLogProber
{
    private static Logger logger = LoggerFactory.getLogger(EtherpadLogProber.class);

    // 2021-11-17 01:51:59 DELETE 7922dd508e0cf9b2
    public static final Pattern LOG_PATTERN = Pattern.compile(
            "^\\[(?<datetime>\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}(\\.\\d{3})?)]\\s\\[(?<level>[^]]+)]\\s+(?<type>\\S+) - \\[(?<event>[^]]+)]\\spad:(?<padname>\\S+)\\ssocket:\\S+\\sIP:(?<ip>\\S+) authorID:(?<author>\\S+)( username:.+)?$");
    /*
    [2022-03-28 02:19:35.842] [INFO] access - [ENTER] pad:test socket:Qr8BEX3mP6EmheyLAAAA IP:82.65.227.202 authorID:a.vZ7pRZcyfzUxO26i username:admin
    [2022-03-28 18:50:10.963] [INFO] access - [CREATE] pad:test7 socket:WddndPw_4vt01-0YAAAC IP:82.65.227.202 authorID:a.vZ7pRZcyfzUxO26i username:admin
    [2022-03-28 18:52:00.668] [INFO] access - [ENTER] pad:test7 socket:gA4Ba57988zrGJNmAAAD IP:82.65.227.202 authorID:a.vZ7pRZcyfzUxO26i username:admin
    [2022-03-28 18:52:27.947] [INFO] access - [ENTER] pad:test7 socket:9ZrFheETmDUpUcS1AAAE IP:82.65.227.202 authorID:a.AMpqExl3JHbPL52c
    [2022-03-28 18:59:58.501] [INFO] access - [ENTER] pad:test socket:mCgPMxg2wb7upHP8AAAG IP:82.65.227.202 authorID:a.vZ7pRZcyfzUxO26i username:admin
    [2022-03-28 19:01:24.884] [INFO] access - [LEAVE] pad:test socket:mCgPMxg2wb7upHP8AAAG IP:82.65.227.202 authorID:a.vZ7pRZcyfzUxO26i username:admin
     */

    public static final Pattern DELETE_EMPTY_LOG_PATTERN = Pattern.compile(
            "^\\[(?<datetime>\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}(\\.\\d{3})?)]\\s\\[(?<level>[^]]+)]\\s+(?<type>\\S+) - Deleting (?<padname>\\S+) when user leaved since empty$");
    /*
     [2022-03-29 00:43:57.085] [INFO] ep_delete_empty_pads - Deleting test8 when user leaved since empty
     */

    /**
     * Instantiates a new etherpad log prober.
     */
    private EtherpadLogProber()
    {
    }

    /**
     * Parses the log.
     *
     * @param line
     *            the line
     * @return the http log
     */
    public static EtherpadLog parseLog(final String line)
    {
        EtherpadLog result;

        Matcher matcher = LOG_PATTERN.matcher(line);
        if (matcher.matches())
        {
            result = new EtherpadLog();
            result.setTime(LocalDateTime.parse(matcher.group("datetime"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS").withLocale(Locale.ENGLISH)));
            result.setLevel(matcher.group("level"));
            result.setType(matcher.group("type"));
            result.setEvent(matcher.group("event"));
            result.setPadname(matcher.group("padname"));
            result.setIp(matcher.group("ip"));
            result.setAuthor(matcher.group("author"));
        }
        else
        {
            matcher = DELETE_EMPTY_LOG_PATTERN.matcher(line);
            if (matcher.matches())
            {
                result = new EtherpadLog();
                result.setTime(LocalDateTime.parse(matcher.group("datetime"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS").withLocale(Locale.ENGLISH)));
                result.setLevel(matcher.group("level"));
                result.setType(matcher.group("type"));
                result.setEvent("Deleting");
                result.setAuthor(matcher.group("padname"));
            }
            else
            {
                result = null;
            }
        }

        //
        return result;
    }

    /**
     * Probe.
     *
     * @param files
     *            the files
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static PathCounters probe(final Files files) throws IOException
    {
        PathCounters result;

        result = new PathCounters();

        UserCounters users = new UserCounters();
        UserCounters ipv4Users = new UserCounters();
        UserCounters ipv6Users = new UserCounters();
        UserCounters actives = new UserCounters();

        for (File file : files)
        {
            System.out.println("Probing file [" + file.getAbsolutePath() + "]");

            //
            int lineCount = 0;
            LineIterator iterator = new LineIterator(file);
            while (iterator.hasNext())
            {
                lineCount += 1;
                String line = iterator.next();

                // Remove ANSI Escape Codes.
                line = line.replaceAll("\u001B\\[[\\d;]*[^\\d;]", "");

                try
                {
                    EtherpadLog log = parseLog(line);
                    // System.out.println("====> " + log);
                    if (log == null)
                    {
                        if (line.contains("access -"))
                        {
                            logger.warn("LINE IS NOT MATCHING [{}]", line);
                        }
                    }
                    else
                    {
                        // Timemarks.
                        String year = log.getYear();
                        String yearMonth = log.getYearMonth();
                        String yearWeek = log.getYearWeek();
                        String date = log.getDate();

                        if (StringUtils.equals(log.getType(), "access"))
                        {
                            // metrics.service.users
                            // metrics.service.users.ipv4
                            // metrics.service.users.ipv6
                            users.put(log.getAuthor(), year, yearMonth, yearWeek, date);
                            if (log.isIPv4())
                            {
                                ipv4Users.put(log.getAuthor(), year, yearMonth, yearWeek, date);
                            }
                            else
                            {
                                ipv6Users.put(log.getAuthor(), year, yearMonth, yearWeek, date);
                            }

                            // metrics.etherpad.pads.actives
                            actives.put(log.getPadname(), year, yearMonth, yearWeek, date);

                            // metrics.etherpad.pads.created
                            // alias metrics.textprocessors.created
                            if (StringUtils.equals(log.getEvent(), "CREATE"))
                            {
                                result.inc("metrics.etherpad.pads.created", year, yearMonth, yearWeek, date);
                                result.inc("metrics.textprocessors.created", year, yearMonth, yearWeek, date);
                            }

                            if (StringUtils.equalsAny(log.getEvent(), "CREATE", "ENTER"))
                            {
                                result.inc("metrics.etherpad.pads.reads", year, yearMonth, yearWeek, date);
                                result.inc("metrics.textprocessors.reads", year, yearMonth, yearWeek, date);
                            }
                        }
                        else if (StringUtils.equals(log.getType(), "ep_delete_empty_pads"))
                        {
                            result.inc("metrics.etherpad.pads.deleted", year, yearMonth, yearWeek, date);
                            result.inc("metrics.textprocessors.deleted", year, yearMonth, yearWeek, date);
                        }
                    }
                }
                catch (Exception exception)
                {
                    logger.warn("Error parsing line [{}][{}]", line, exception.getMessage());
                    exception.printStackTrace();
                }
            }
            System.out.println("PrivatebinPatchLog line count=" + lineCount);
        }

        //
        result.putAll(users.getCounters("metrics.service.users"));
        result.putAll(ipv4Users.getCounters("metrics.service.users.ipv4"));
        result.putAll(ipv6Users.getCounters("metrics.service.users.ipv6"));
        result.putAll(actives.getCounters("metrics.etherpad.pads.actives"));

        //
        return result;
    }
}
