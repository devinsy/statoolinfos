/*
 * Copyright (C) 2022-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.etherpad;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import fr.devinsy.statoolinfos.metrics.TimeMark;
import fr.devinsy.statoolinfos.metrics.TimeMarkUtils;
import fr.devinsy.strings.StringList;

/**
 * The Class EtherpadLog.
 */
public class EtherpadLog
{
    private LocalDateTime time;
    private String level;
    private String type;
    private String event;
    private String padname;
    private String ip;
    private String author;

    /**
     * Instantiates a new etherpad log.
     */
    public EtherpadLog()
    {
        this.time = null;
        this.level = null;
        this.type = null;
        this.event = null;
        this.padname = null;
        this.ip = null;
        this.author = null;
    }

    /**
     * Gets the author.
     *
     * @return the author
     */
    public String getAuthor()
    {
        return this.author;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public String getDate()
    {
        String result;

        result = this.time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.FRANCE));

        //
        return result;
    }

    /**
     * Gets the event.
     *
     * @return the event
     */
    public String getEvent()
    {
        return this.event;
    }

    /**
     * Gets the ip.
     *
     * @return the ip
     */
    public String getIp()
    {
        return this.ip;
    }

    /**
     * Gets the level.
     *
     * @return the level
     */
    public String getLevel()
    {
        return this.level;
    }

    /**
     * Gets the padname.
     *
     * @return the padname
     */
    public String getPadname()
    {
        return this.padname;
    }

    /**
     * Gets the time.
     *
     * @return the time
     */
    public LocalDateTime getTime()
    {
        return this.time;
    }

    /**
     * Gets the time marks.
     *
     * @return the time marks
     */
    public StringList getTimeMarks()
    {
        StringList result;

        result = new StringList();

        String year = TimeMark.yearOf(this.time).toString();
        String yearMonth = TimeMark.yearMonthOf(this.time).toString();
        String yearWeek = TimeMark.yearWeekOf(this.time).toString();
        String date = TimeMark.dayOf(this.time).toString();

        result.add(year);
        result.add(yearMonth);
        result.add(yearWeek);
        result.add(date);

        //
        return result;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType()
    {
        return this.type;
    }

    /**
     * Gets the year.
     *
     * @return the year
     */
    public String getYear()
    {
        String result;

        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearOf(this.time);
        }

        //
        return result;
    }

    /**
     * Gets the year month.
     *
     * @return the year month
     */
    public String getYearMonth()
    {
        String result;

        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearMonthOf(this.time);
        }
        //
        return result;
    }

    /**
     * Gets the year week.
     *
     * @return the year week
     */
    public String getYearWeek()
    {
        String result;
        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearWeekOf(this.time);
        }
        //
        return result;
    }

    /**
     * Checks if is ipv4.
     *
     * @return true, if is ipv4
     */
    public boolean isIPv4()
    {
        boolean result;

        if (this.ip == null)
        {
            result = false;
        }
        else
        {
            result = this.ip.contains(".");
        }

        //
        return result;
    }

    /**
     * Checks if is ipv6.
     *
     * @return true, if is ipv6
     */
    public boolean isIPv6()
    {
        boolean result;

        if (this.ip == null)
        {
            result = false;
        }
        else
        {
            result = this.ip.contains(":");
        }

        //
        return result;
    }

    /**
     * Sets the author.
     *
     * @param author
     *            the new author
     */
    public void setAuthor(final String author)
    {
        this.author = author;
    }

    /**
     * Sets the event.
     *
     * @param event
     *            the new event
     */
    public void setEvent(final String event)
    {
        this.event = event;
    }

    /**
     * Sets the ip.
     *
     * @param ip
     *            the new ip
     */
    public void setIp(final String ip)
    {
        this.ip = ip;
    }

    /**
     * Sets the level.
     *
     * @param level
     *            the new level
     */
    public void setLevel(final String level)
    {
        this.level = level;
    }

    /**
     * Sets the padname.
     *
     * @param padname
     *            the new padname
     */
    public void setPadname(final String padname)
    {
        this.padname = padname;
    }

    /**
     * Sets the time.
     *
     * @param time
     *            the new time
     */
    public void setTime(final LocalDateTime time)
    {
        this.time = time;
    }

    /**
     * Sets the type.
     *
     * @param type
     *            the new type
     */
    public void setType(final String type)
    {
        this.type = type;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String result;

        result = String.format("[time=%s, level=%s, type=%s, event=%s, %padname=%s, ip=%s, auhtor=%s]", this.time, this.level, this.type, this.event, this.padname, this.ip, this.author);

        //
        return result;
    }
}
