/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

/**
 * The Class HttpStatus.
 */
public class HttpStatus
{
    private int code;
    private String message;
    private String description;
    private HttpStatusCategory category;

    /**
     * Instantiates a new http status.
     *
     * @param code
     *            the code
     * @param message
     *            the message
     * @param description
     *            the description
     */
    public HttpStatus(final int code, final String message, final String description)
    {
        this.code = code;
        this.message = message;
        this.description = description;
        this.category = HttpStatusCategory.of(code);
    }

    /**
     * Gets the category.
     *
     * @return the category
     */
    public HttpStatusCategory getCategory()
    {
        return this.category;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public int getCode()
    {
        return this.code;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        return this.description;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage()
    {
        return this.message;
    }

    /**
     * Sets the category.
     *
     * @param category
     *            the new category
     */
    public void setCategory(final HttpStatusCategory category)
    {
        this.category = category;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the new code
     */
    public void setCode(final int code)
    {
        this.code = code;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the new description
     */
    public void setDescription(final String description)
    {
        this.description = description;
    }

    /**
     * Sets the message.
     *
     * @param message
     *            the new message
     */
    public void setMessage(final String message)
    {
        this.message = message;
    }
}
