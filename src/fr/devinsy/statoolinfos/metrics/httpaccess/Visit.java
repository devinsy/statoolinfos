/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;

import org.threeten.extra.YearWeek;

/**
 * The Class Visit.
 */
public class Visit
{
    private LocalDateTime start;
    private LocalDateTime end;

    /**
     * Instantiates a new visit.
     *
     * @param start
     *            the start
     */
    public Visit(final LocalDateTime start)
    {
        this(start, start);
    }

    /**
     * Instantiates a new visit.
     *
     * @param start
     *            the start
     * @param end
     *            the end
     */
    public Visit(final LocalDateTime start, final LocalDateTime end)
    {
        if (start == null)
        {
            throw new IllegalArgumentException("Null parameter [start].");
        }
        else if (end == null)
        {
            throw new IllegalArgumentException("Null parameter [end].");
        }
        else
        {
            this.start = start;
            this.end = end;
        }
    }

    /**
     * Gets the duration.
     *
     * @return the duration
     */
    public Duration getDuration()
    {
        Duration result;

        result = Duration.between(this.start, this.end);

        //
        return result;
    }

    public LocalDateTime getEnd()
    {
        return this.end;
    }

    public LocalDateTime getStart()
    {
        return this.start;
    }

    /**
     * Checks if is matching.
     *
     * @param year
     *            the year
     * @return true, if is matching
     */
    public boolean isMatching(final int year)
    {
        boolean result;

        if ((year >= this.start.getYear()) && (year <= this.end.getYear()))
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Checks if is matching.
     *
     * @param date
     *            the date
     * @return true, if is matching
     */
    public boolean isMatching(final LocalDate date)
    {
        boolean result;

        if (date == null)
        {
            result = false;
        }
        else
        {
            LocalDate startDate = this.start.toLocalDate();
            LocalDate endDate = this.end.toLocalDate();

            if ((date.isBefore(startDate)) || (date.isAfter(endDate)))
            {
                result = false;
            }
            else
            {
                result = true;
            }
        }

        //
        return result;
    }

    /**
     * Checks if is matching.
     *
     * @param date
     *            the date
     * @return true, if is matching
     */
    public boolean isMatching(final LocalDateTime date)
    {
        boolean result;

        if (date == null)
        {
            result = false;
        }
        else
        {
            if ((date.isBefore(this.start)) || (date.isAfter(this.end)))
            {
                result = false;
            }
            else
            {
                result = true;
            }
        }

        //
        return result;
    }

    /**
     * Checks if is matching.
     *
     * @param yearMonth
     *            the year month
     * @return true, if is matching
     */
    public boolean isMatching(final YearMonth yearMonth)
    {
        boolean result;

        if (yearMonth == null)
        {
            result = false;
        }
        else
        {
            YearMonth startYearMonth = YearMonth.from(this.start);
            YearMonth endYearMonth = YearMonth.from(this.end);

            if ((yearMonth.isBefore(startYearMonth)) || (yearMonth.isAfter(endYearMonth)))
            {
                result = false;
            }
            else
            {
                result = true;
            }
        }

        //
        return result;
    }

    /**
     * Checks if is matching.
     *
     * @param yearWeek
     *            the year week
     * @return true, if is matching
     */
    public boolean isMatching(final YearWeek yearWeek)
    {
        boolean result;

        YearWeek startYearWeek = YearWeek.from(this.start);
        YearWeek endYearWeek = YearWeek.from(this.end);

        if ((yearWeek.isBefore(startYearWeek)) || (yearWeek.isAfter(endYearWeek)))
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    public void setEnd(final LocalDateTime end)
    {
        this.end = end;
    }

    public void setStart(final LocalDateTime start)
    {
        this.start = start;
    }
}
