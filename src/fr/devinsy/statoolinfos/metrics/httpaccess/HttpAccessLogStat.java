/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

/**
 * The Class HttpAccesLogStat.
 */
public class HttpAccessLogStat
{
    private int hitCount;
    private int hitIpv4Count;
    private int hitIpv6Count;

    private int pageCount;
    private int pageIpv4Count;
    private int pageIpv6Count;

    private int byteCount;
    private int byteIpv4Count;
    private int byteIpv6Count;

    private int fileCount;
    private int fileIpv4Count;
    private int fileIpv6Count;

    private int status1xxCount;
    private int status2xxCount;
    private int status3xxCount;
    private int status4xxCount;
    private int status5xxCount;

    private int ipCount;
    private int ipIpv4Count;
    private int ipIpv6Count;

    /**
     * Instantiates a new http acces log stat.
     */
    public HttpAccessLogStat()
    {
    }
}
