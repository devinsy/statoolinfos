/*
 * Copyright (C) 2022-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

/**
 * The Class HttpMethod.
 */
public enum HttpMethod
{
    GET,
    HEAD,
    POST,
    OPTIONS,
    CONNECT,
    TRACE,
    PUT,
    PATCH,
    DELETE,
    UNKNOWN;

    /**
     * Parses the.
     *
     * @param value
     *            the value
     * @return the http method
     */
    public static HttpMethod of(final String value)
    {
        HttpMethod result;

        if (value == null)
        {
            result = null;
        }
        else
        {
            try
            {
                result = valueOf(value);
            }
            catch (IllegalArgumentException exception)
            {
                result = UNKNOWN;
            }
        }

        //
        return result;
    }
}