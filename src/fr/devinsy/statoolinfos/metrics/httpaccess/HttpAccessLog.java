/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Pattern;

import fr.devinsy.statoolinfos.metrics.TimeMarkUtils;
import fr.devinsy.strings.StringList;

/**
 * The Class HttpAccessLog.
 * 
 */
public class HttpAccessLog
{
    public static final Pattern ASSET_PATTERN = Pattern.compile("^.*\\.(avi|css|gif|ico|jpeg|jpg|js|mp3|mp4|ogg|png|svg|wav) HTTP.*$");

    private String ip;
    private String remoteUser;
    private ZonedDateTime time;
    private String request;
    private HttpStatus status;
    private long bodyBytesSent;
    private String referer;
    private UserAgent userAgent;

    /**
     * Instantiates a new http access log.
     */
    public HttpAccessLog()
    {
        this.ip = null;
        this.remoteUser = null;
        this.time = null;
        this.request = null;
        this.status = null;
        this.bodyBytesSent = 0;
        this.referer = null;
        this.userAgent = null;
    }

    /**
     * Gets the body bytes sent.
     *
     * @return the body bytes sent
     */
    public long getBodyBytesSent()
    {
        return this.bodyBytesSent;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public String getDate()
    {
        String result;

        result = this.time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.FRANCE));

        //
        return result;
    }

    /**
     * Gets the ip.
     *
     * @return the ip
     */
    public String getIp()
    {
        return this.ip;
    }

    /**
     * Gets the method.
     *
     * @return the method
     */
    public HttpMethod getMethod()
    {
        HttpMethod result;

        if (this.request == null)
        {
            result = HttpMethod.UNKNOWN;
        }
        else
        {
            int space = this.request.indexOf(' ');
            if (space == -1)
            {
                result = HttpMethod.UNKNOWN;
            }
            else
            {
                result = HttpMethod.of(this.request.substring(0, space));
            }
        }

        //
        return result;
    }

    /**
     * Gets the referer.
     *
     * @return the referer
     */
    public String getReferer()
    {
        return this.referer;
    }

    /**
     * Gets the remote user.
     *
     * @return the remote user
     */
    public String getRemoteUser()
    {
        return this.remoteUser;
    }

    /**
     * Gets the request.
     *
     * @return the request
     */
    public String getRequest()
    {
        return this.request;
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    public HttpStatus getStatus()
    {
        return this.status;
    }

    /**
     * Gets the time.
     *
     * @return the time
     */
    public LocalDateTime getTime()
    {
        return this.time.toLocalDateTime();
    }

    /**
     * Gets the user agent.
     *
     * @return the user agent
     */
    public UserAgent getUserAgent()
    {
        return this.userAgent;
    }

    /**
     * Gets the year.
     *
     * @return the year
     */
    public String getYear()
    {
        String result;

        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearOf(this.time);
        }

        //
        return result;
    }

    /**
     * Gets the year month.
     *
     * @return the year month
     */
    public String getYearMonth()
    {
        String result;

        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearMonthOf(this.time);
        }
        //
        return result;
    }

    /**
     * Gets the year week.
     *
     * @return the year week
     */
    public String getYearWeek()
    {
        String result;
        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearWeekOf(this.time);
        }
        //
        return result;
    }

    /**
     * Checks if is bot.
     *
     * @return true, if is bot
     */
    public boolean isBot()
    {
        boolean result;

        result = UserAgentBotDetector.isBot(this.userAgent.toString());

        //
        return result;
    }

    /**
     * Checks if is ipv4.
     *
     * @return true, if is ipv4
     */
    public boolean isIPv4()
    {
        boolean result;

        if (this.ip == null)
        {
            result = false;
        }
        else
        {
            result = this.ip.contains(".");
        }

        //
        return result;
    }

    /**
     * Checks if is ipv6.
     *
     * @return true, if is ipv6
     */
    public boolean isIPv6()
    {
        boolean result;

        if (this.ip == null)
        {
            result = false;
        }
        else
        {
            result = this.ip.contains(":");
        }

        //
        return result;
    }

    /**
     * Checks if is visit.
     *
     * @return true, if is visit
     */
    public boolean isVisit()
    {
        boolean result;

        if ((this.status.getCategory() == HttpStatusCategory.SUCCESS) && (!isBot()) && (this.request.startsWith("GET ")) &&
                (!ASSET_PATTERN.matcher(this.request).matches()))
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Sets the body bytes sent.
     *
     * @param bodyBytesSent
     *            the new body bytes sent
     */
    public void setBodyBytesSent(final long bodyBytesSent)
    {
        this.bodyBytesSent = bodyBytesSent;
    }

    /**
     * Sets the ip.
     *
     * @param ip
     *            the new ip
     */
    public void setIp(final String ip)
    {
        this.ip = ip;
    }

    /**
     * Sets the referer.
     *
     * @param referer
     *            the new referer
     */
    public void setReferer(final String referer)
    {
        this.referer = referer;
    }

    /**
     * Sets the remote user.
     *
     * @param remoteUser
     *            the new remote user
     */
    public void setRemoteUser(final String remoteUser)
    {
        this.remoteUser = remoteUser;
    }

    /**
     * Sets the request.
     *
     * @param request
     *            the new request
     */
    public void setRequest(final String request)
    {
        this.request = request;
    }

    /**
     * Sets the status.
     *
     * @param status
     *            the new status
     */
    public void setStatus(final HttpStatus status)
    {
        this.status = status;
    }

    /**
     * Sets the time.
     *
     * @param time
     *            the new time
     */
    public void setTime(final LocalDateTime time)
    {
        this.time = ZonedDateTime.of(time, ZoneId.systemDefault());
    }

    /**
     * Sets the time.
     *
     * @param time
     *            the new time
     */
    public void setTime(final ZonedDateTime time)
    {
        this.time = time;
    }

    /**
     * Sets the user agent.
     *
     * @param userAgent
     *            the new user agent
     */
    public void setUserAgent(final UserAgent userAgent)
    {
        this.userAgent = userAgent;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString()
    {
        String result;

        StringList buffer = new StringList();

        buffer.append("[ip=").append(this.ip).append("]");
        buffer.append("[remoteUser=").append(this.remoteUser).append("]");
        buffer.append("[time=").append(this.time).append("]");
        buffer.append("[request=").append(this.request).append("]");
        buffer.append("[status=").append(this.status.getCode()).append("]");
        buffer.append("[bodyBytesSent=").append(this.bodyBytesSent).append("]");
        buffer.append("[referer=").append(this.referer).append("]");
        buffer.append("[userAgent=").append(this.userAgent).append("]");

        result = buffer.toString();

        //
        return result;
    }

    /**
     * To string log.
     *
     * @return the string
     */
    public String toStringLog()
    {
        String result;

        // "^(?<remoteAddress>[a-zA-F0-9\\\\:\\\\.]+) - (?<remoteUser>[^\\[]+)
        // \\[(?<time>[^\\]]+)\\] \"(?<request>.*)\" (?<status>\\d+)
        // (?<bodyBytesSent>\\d+) \"(?<referer>.*)\"
        // \"(?<userAgent>[^\"]*)\".*$");

        // result = String.format("%s %s %s \\[%s\\] \"%s\" %d %d \"%s\"
        // \"%s\"",
        result = String.format("%s - %s [%s] \"%s\" %d %d \"%s\" \"%s\"",
                this.ip,
                this.remoteUser,
                this.time.format(DateTimeFormatter.ofPattern("dd/MMM/yyyy:HH:mm:ss Z", Locale.ENGLISH)),
                this.request,
                this.status.getCode(),
                this.bodyBytesSent,
                this.referer,
                this.userAgent);

        //
        return result;
    }
}
