/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

/**
 * The Enum HttpStatusCategory.
 */
public enum HttpStatusCategory
{
    INFORMATIONAL,
    SUCCESS,
    REDIRECTION,
    CLIENT_ERROR,
    SERVER_ERROR,
    INVALID;

    /**
     * Checks if is client error.
     *
     * @param httpCode
     *            the http code
     * @return true, if is client error
     */
    public static boolean isClientError(final int httpCode)
    {
        boolean result;

        result = ((httpCode / 100) == 4);

        //
        return result;
    }

    /**
     * Checks if is informational.
     *
     * @param httpCode
     *            the http code
     * @return true, if is informational
     */
    public static boolean isInformational(final int httpCode)
    {
        boolean result;

        result = ((httpCode / 100) == 1);

        //
        return result;
    }

    /**
     * Checks if is redirection.
     *
     * @param httpCode
     *            the http code
     * @return true, if is redirection
     */
    public static boolean isRedirection(final int httpCode)
    {
        boolean result;

        result = ((httpCode / 100) == 3);

        //
        return result;
    }

    /**
     * Checks if is server error.
     *
     * @param httpCode
     *            the http code
     * @return true, if is server error
     */
    public static boolean isServerError(final int httpCode)
    {
        boolean result;

        result = ((httpCode / 100) == 5);

        //
        return result;
    }

    /**
     * Checks if is success.
     *
     * @param httpCode
     *            the http code
     * @return true, if is success
     */
    public static boolean isSuccess(final int httpCode)
    {
        boolean result;

        result = ((httpCode / 100) == 2);

        //
        return result;
    }

    /**
     * Checks if is unauthorized.
     *
     * @param httpCode
     *            the http code
     * @return true, if is unauthorized
     */
    public static boolean isUnauthorized(final int httpCode)
    {
        boolean result;

        result = (httpCode == 401);

        //
        return result;
    }

    /**
     * Value of.
     *
     * @param httpCode
     *            the code
     * @return the http status category
     */
    public static HttpStatusCategory of(final int httpCode)
    {
        HttpStatusCategory result;

        if (isInformational(httpCode))
        {
            result = HttpStatusCategory.INFORMATIONAL;
        }
        else if (isSuccess(httpCode))
        {
            result = HttpStatusCategory.SUCCESS;
        }
        else if (isRedirection(httpCode))
        {
            result = HttpStatusCategory.REDIRECTION;
        }
        else if (isClientError(httpCode))
        {
            result = HttpStatusCategory.CLIENT_ERROR;
        }
        else if (isServerError(httpCode))
        {
            result = HttpStatusCategory.SERVER_ERROR;
        }
        else
        {
            result = HttpStatusCategory.INVALID;
        }

        //
        return result;
    }
}
