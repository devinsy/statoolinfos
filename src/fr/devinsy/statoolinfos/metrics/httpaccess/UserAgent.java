/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringsUtils;

/**
 * The Class UserAgent.
 */
public class UserAgent
{
    private String source;

    /**
     * Instantiates a new user agent.
     *
     * @param source
     *            the source
     */
    public UserAgent(final String source)
    {
        this.source = source;
    }

    /**
     * Gets the browser.
     * 
     * WARNING: MUST BE GREATLY IMPROVED
     *
     * @return the browser
     */
    public UserAgentBrowser getBrowser()
    {
        UserAgentBrowser result;

        if (StringUtils.containsIgnoreCase(this.source, "Chrome/"))
        {
            result = UserAgentBrowser.CHROME;
        }
        else if (StringUtils.containsIgnoreCase(this.source, "Chromium/"))
        {
            result = UserAgentBrowser.CHROMIUM;
        }
        else if (StringUtils.containsIgnoreCase(this.source, "Firefox/"))
        {
            result = UserAgentBrowser.FIREFOX;
        }
        else if (StringUtils.containsIgnoreCase(this.source, "Lynx"))
        {
            result = UserAgentBrowser.LYNX;
        }
        else if (StringUtils.containsIgnoreCase(this.source, "MSIE  "))
        {
            result = UserAgentBrowser.MSIE;
        }
        else if (StringsUtils.containsAnyIgnoreCase(this.source, "Opera", "OPR/"))
        {
            result = UserAgentBrowser.OPERA;
        }
        else if (StringUtils.containsIgnoreCase(this.source, "Safari/"))
        {
            result = UserAgentBrowser.SAFARI;
        }
        else
        {
            result = UserAgentBrowser.OTHER;
        }

        //
        return result;
    }

    /**
     * Gets the device.
     * 
     * WARNING: MUST BE GREATLY IMPROVED
     *
     * @return the device
     */
    public UserAgentDevice getDevice()
    {
        UserAgentDevice result;

        if (StringsUtils.containsAnyIgnoreCase(this.source, "Playstation"))
        {
            result = UserAgentDevice.CONSOLE;
        }
        else if (StringsUtils.containsAnyIgnoreCase(this.source, "Desktop"))
        {
            result = UserAgentDevice.PC;
        }
        else if (StringsUtils.containsAnyIgnoreCase(this.source, "Android", "iPhone"))
        {
            result = UserAgentDevice.PHONE;
        }
        else if (StringsUtils.containsAnyIgnoreCase(this.source, "iPad"))
        {
            result = UserAgentDevice.TABLET;
        }
        else
        {
            result = UserAgentDevice.OTHER;
        }

        //
        return result;
    }

    /**
     * Gets the os.
     *
     * @return the os
     */
    public UserAgentOS getOS()
    {
        UserAgentOS result;

        if (StringUtils.containsIgnoreCase(this.source, "Android"))
        {
            result = UserAgentOS.ANDROID;
        }
        else if (StringUtils.containsIgnoreCase(this.source, "FreeBSD"))
        {
            result = UserAgentOS.FREEBSD;
        }
        else if (StringsUtils.containsAnyIgnoreCase(this.source, "Debian", "Gentoo", "Linux aarch", "Linux arm", "Linux i586", "Linux i686", "Linux x64", "Linux x86_64"))
        {
            result = UserAgentOS.GNULINUX;
        }
        else if (StringsUtils.containsAnyIgnoreCase(this.source, "Debian", "Gentoo", "Linux aarch", "Linux arm", "Linux i586", "Linux i686", "Linux x64", "Linux x86_64"))
        {
            result = UserAgentOS.GNULINUX;
        }
        else if (StringsUtils.containsAnyIgnoreCase(this.source, "iPhone"))
        {
            result = UserAgentOS.IOS;
        }
        else if (StringsUtils.containsAnyIgnoreCase(this.source, "Macintosh", "Mac OS X", "Mac OS Desktop"))
        {
            result = UserAgentOS.MACOS;
        }
        else if (StringUtils.containsIgnoreCase(this.source, "windows"))
        {
            result = UserAgentOS.MSWINDOWS;
        }
        else if (StringUtils.containsIgnoreCase(this.source, "NetBSD"))
        {
            result = UserAgentOS.NETBSD;
        }
        else if (StringUtils.containsIgnoreCase(this.source, "OpenBSD"))
        {
            result = UserAgentOS.OPENBSD;
        }
        else
        {
            result = UserAgentOS.OTHER;
        }

        //
        return result;
    }

    /**
     * Gets the type.
     *
     * WARNING: MUST BE GREATLY IMPROVED
     *
     * @return the type
     */
    public UserAgentType getType()
    {
        UserAgentType result;

        // Source: http://www.useragentstring.com/pages/useragentstring.php.
        StringList crawlerKeys = new StringList();
        crawlerKeys.add("Accoona-AI-Agent");
        crawlerKeys.add("AddSugarSpiderBot ");
        crawlerKeys.add("AnyApexBot");
        crawlerKeys.add("Arachmo");
        crawlerKeys.add("B-l-i-t-z-B-O-T");
        crawlerKeys.add("Baiduspider");
        crawlerKeys.add("BecomeBot");
        crawlerKeys.add("BeslistBot");
        crawlerKeys.add("BillyBobBot");
        crawlerKeys.add("Bimbot");
        crawlerKeys.add("Bingbot");
        crawlerKeys.add("BlitzBOT");
        crawlerKeys.add("Chrome");
        crawlerKeys.add("Firefox");
        crawlerKeys.add("Lynx");

        if (StringsUtils.containsAnyIgnoreCase(this.source, "Firefox", "Chrome", "Lynx", "MSIE", "Safari"))
        {
            result = UserAgentType.BROWSER;
        }
        else if (StringUtils.containsIgnoreCase(this.source, "Crawler"))
        {
            result = UserAgentType.CRAWLER;
        }
        else if (StringUtils.containsIgnoreCase(this.source, "Validator"))
        {
            result = UserAgentType.VALIDATOR;
        }
        else
        {
            result = UserAgentType.OTHER;
        }

        //
        return result;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString()
    {
        String result;

        result = this.source;

        //
        return result;
    }
}
