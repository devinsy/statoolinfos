/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringsUtils;

/**
 * The Class UserAgentBotDetector.
 */
public class UserAgentBotDetector
{
    private static final StringList startList = new StringList();
    private static final StringList containList = new StringList();

    static
    {
        StringList lines;
        try
        {
            lines = StringsUtils.load(UserAgentBotDetector.class.getResource("/fr/devinsy/statoolinfos/metrics/httpaccess/userAgentBotDetectorData.txt"));
        }
        catch (IOException exception)
        {
            exception.printStackTrace();
            lines = new StringList();
        }

        for (String line : lines)
        {
            if (line.startsWith("^"))
            {
                startList.add(line.substring(1));
            }
            else
            {
                containList.add(line);
            }
        }
    }

    /**
     * Instantiates a new user agent bot detector.
     */
    private UserAgentBotDetector()
    {
    }

    /**
     * Checks if is bot.
     *
     * @param userAgent
     *            the user agent
     * @return true, if is bot
     */
    public static boolean isBot(final String userAgent)
    {
        boolean result;

        if (StringUtils.isBlank(userAgent))
        {
            result = true;
        }
        else if (StringUtils.equalsAny(userAgent.trim(), "-"))
        {
            result = true;
        }
        else if (StringsUtils.containsAnyIgnoreCase(userAgent, containList))
        {
            result = true;
        }
        else if (StringsUtils.startsWithAny(userAgent, startList))
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }
}
