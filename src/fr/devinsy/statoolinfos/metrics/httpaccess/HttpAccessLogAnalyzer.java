/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.metrics.IpCounters;
import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.strings.StringList;

/**
 * The Class HttpAccessLogProber.
 */
public class HttpAccessLogAnalyzer
{
    private int ignoredLineCount;
    private int errorCount;
    private PathCounters counters;
    private IpCounters ips;
    private IpCounters ipv4;
    private IpCounters ipv6;
    private IpCounters botIps;
    private IpCounters humanIps;
    private VisitorCounters requesters;
    private VisitorCounters ipv4Requesters;
    private VisitorCounters ipv6Requesters;
    private VisitorCounters botRequesters;
    private VisitorCounters humanRequesters;
    private VisitCounters visits;
    private VisitCounters ipv4Visits;
    private VisitCounters ipv6Visits;
    private VisitorCounters visitors;
    private VisitorCounters ipv4Visitors;
    private VisitorCounters ipv6Visitors;

    /**
     * Instantiates a new http access log prober.
     */
    public HttpAccessLogAnalyzer()
    {
        this.errorCount = 0;
        this.ignoredLineCount = 0;

        this.counters = new PathCounters();
        this.ips = new IpCounters();
        this.ipv4 = new IpCounters();
        this.ipv6 = new IpCounters();
        this.botIps = new IpCounters();
        this.humanIps = new IpCounters();

        this.requesters = new VisitorCounters();
        this.ipv4Requesters = new VisitorCounters();
        this.ipv6Requesters = new VisitorCounters();
        this.humanRequesters = new VisitorCounters();
        this.botRequesters = new VisitorCounters();

        this.visits = new VisitCounters();
        this.ipv4Visits = new VisitCounters();
        this.ipv6Visits = new VisitCounters();

        this.visitors = new VisitorCounters();
        this.ipv4Visitors = new VisitorCounters();
        this.ipv6Visitors = new VisitorCounters();
    }

    /**
     * Gets the counters.
     *
     * @return the counters
     */
    public PathCounters getCounters()
    {
        PathCounters result;

        result = new PathCounters();
        result.putAll(this.counters);

        result.putAll(this.ips.getCounters("metrics.http.ip"));
        result.putAll(this.ipv4.getCounters("metrics.http.ip.ipv4"));
        result.putAll(this.ipv6.getCounters("metrics.http.ip.ipv6"));
        result.putAll(this.botIps.getCounters("metrics.http.ip.bots"));
        result.putAll(this.humanIps.getCounters("metrics.http.ip.humans"));

        result.putAll(this.requesters.getCounters("metrics.http.requesters"));
        result.putAll(this.ipv4Requesters.getCounters("metrics.http.requesters.ipv4"));
        result.putAll(this.ipv6Requesters.getCounters("metrics.http.requesters.ipv6"));
        result.putAll(this.botRequesters.getCounters("metrics.http.requesters.bots"));
        result.putAll(this.humanRequesters.getCounters("metrics.http.requesters.humans"));

        result.putAll(this.visits.getCounters("metrics.http.visits"));
        result.putAll(this.ipv4Visits.getCounters("metrics.http.visits.ipv4"));
        result.putAll(this.ipv6Visits.getCounters("metrics.http.visits.ipv6"));

        result.putAll(this.visitors.getCounters("metrics.http.visitors"));
        result.putAll(this.ipv4Visitors.getCounters("metrics.http.visitors.ipv4"));
        result.putAll(this.ipv6Visitors.getCounters("metrics.http.visitors.ipv6"));

        //
        return result;
    }

    /**
     * Probe log.
     *
     * @param log
     *            the log
     */
    public void probeLog(final HttpAccessLog log)
    {
        // logger.info("==================");
        if (log != null)
        {
            // logger.info("LINE IS MATCHING [{}]", log);
            // logger.info(log.getHttpUserAgent().toString());

            // General HTTP access logs.
            String year = log.getYear();
            String yearMonth = log.getYearMonth();
            String yearWeek = log.getYearWeek();
            String date = log.getDate();

            // metrics.http.hits
            // metrics.http.hits.ipv4
            // metrics.http.hits.ipv6
            this.counters.inc("metrics.http.hits", year, yearMonth, yearWeek, date);

            if (log.isIPv4())
            {
                this.counters.inc("metrics.http.hits.ipv4", year, yearMonth, yearWeek, date);
            }
            else
            {
                this.counters.inc("metrics.http.hits.ipv6", year, yearMonth, yearWeek, date);
            }

            // metrics.http.files
            if (log.getBodyBytesSent() != 0)
            {
                this.counters.inc("metrics.http.files", year, yearMonth, yearWeek, date);
            }

            // metrics.http.bytes
            this.counters.inc(log.getBodyBytesSent(), "metrics.http.bytes", year, yearMonth, yearWeek, date);

            // metrics.http.hits.bots.* =
            if (log.isBot())
            {
                this.counters.inc("metrics.http.hits.bots", year, yearMonth, yearWeek, date);
            }
            else
            {
                // metrics.http.hits.humans.*
                this.counters.inc("metrics.http.hits.humans", year, yearMonth, yearWeek, date);

                if (log.isIPv4())
                {
                    // metrics.http.hits.humans.ipv4.*
                    this.counters.inc("metrics.http.hits.humans.ipv4", year, yearMonth, yearWeek, date);
                }
                else
                {
                    // metrics.http.hits.humans.ipv6.*
                    this.counters.inc("metrics.http.hits.humans.ipv6", year, yearMonth, yearWeek, date);
                }
            }

            // metrics.http.pages.* =
            if ((log.getStatus().getCategory() == HttpStatusCategory.SUCCESS) && (isPage(log.getRequest())))
            {
                this.counters.inc("metrics.http.pages", year, yearMonth, yearWeek, date);
            }

            // metrics.http.ip.* =
            this.ips.put(log.getIp(), year, yearMonth, yearWeek, date);

            // metrics.http.ip.ipv4.* =
            // metrics.http.ip.ipv6.* =
            if (log.isIPv4())
            {
                this.ipv4.put(log.getIp(), year, yearMonth, yearWeek, date);
            }
            else
            {
                this.ipv6.put(log.getIp(), year, yearMonth, yearWeek, date);
            }

            // metrics.http.ip.bots.*
            // metrics.http.ip.humans.*
            if (log.isBot())
            {
                this.botIps.put(log.getIp(), year, yearMonth, yearWeek, date);
            }
            else
            {
                this.humanIps.put(log.getIp(), year, yearMonth, yearWeek, date);
            }

            // metrics.http.requesters.* =
            this.requesters.put(log.getIp(), log.getUserAgent(), year, yearMonth, yearWeek, date);

            // metrics.http.requesters.ipv4.* =
            // metrics.http.requesters.ipv6.* =
            if (log.isIPv4())
            {
                this.ipv4Requesters.put(log.getIp(), log.getUserAgent(), year, yearMonth, yearWeek, date);
            }
            else
            {
                this.ipv6Requesters.put(log.getIp(), log.getUserAgent(), year, yearMonth, yearWeek, date);
            }

            // metrics.http.requesters.bots.*
            // metrics.http.requesters.humans.*
            if (log.isBot())
            {
                this.botRequesters.put(log.getIp(), log.getUserAgent(), year, yearMonth, yearWeek, date);
            }
            else
            {
                this.humanRequesters.put(log.getIp(), log.getUserAgent(), year, yearMonth, yearWeek, date);
            }

            // metrics.http.visits.* =
            // metrics.http.visits.ipv4.* =
            // metrics.http.visits.ipv6.* =
            // metrics.http.visitors.* =
            // metrics.http.visitors.ipv4.* =
            // metrics.http.visitors.ipv6.* =
            if (log.isVisit())
            {
                // metrics.http.visits.* =
                // metrics.http.visits.ipv4.* =
                // metrics.http.visits.ipv6.* =
                this.visits.putVisit(log);
                this.visits.storeTimeMarks(year, yearMonth, yearWeek, date);

                if (log.isIPv4())
                {
                    this.ipv4Visits.putVisit(log);
                    this.ipv4Visits.storeTimeMarks(year, yearMonth, yearWeek, date);
                }
                else
                {
                    this.ipv6Visits.putVisit(log);
                    this.ipv6Visits.storeTimeMarks(year, yearMonth, yearWeek, date);
                }

                // metrics.http.visitors.* =
                // metrics.http.visitors.ipv4.* =
                // metrics.http.visitors.ipv6.* =
                this.visitors.put(log.getIp(), log.getUserAgent(), year, yearMonth, yearWeek, date);

                if (log.isIPv4())
                {
                    this.ipv4Visitors.put(log.getIp(), log.getUserAgent(), year, yearMonth, yearWeek, date);
                }
                else
                {
                    this.ipv6Visitors.put(log.getIp(), log.getUserAgent(), year, yearMonth, yearWeek, date);
                }
            }

            // metrics.http.methods.XXXX
            String method = log.getMethod().toString();
            this.counters.inc("metrics.http.methods." + method, year, yearMonth, yearWeek, date);

            // metrics.http.status.XXXX
            String status = String.valueOf(log.getStatus().getCode());
            this.counters.inc("metrics.http.status." + status, year, yearMonth, yearWeek, date);

            // metrics.http.os.XXXXX
            UserAgentOS os = log.getUserAgent().getOS();
            this.counters.inc("metrics.http.os." + os.toString().toLowerCase(), year, yearMonth, yearWeek, date);

            // metrics.http.browsers.XXXXX
            UserAgentBrowser browser = log.getUserAgent().getBrowser();
            this.counters.inc("metrics.http.browsers." + browser.toString().toLowerCase(), year, yearMonth, yearWeek, date);

            // metrics.http.devices.XXXXX =
            UserAgentDevice device = log.getUserAgent().getDevice();
            this.counters.inc("metrics.http.devices." + device.toString().toLowerCase(), year, yearMonth, yearWeek, date);

            // metrics.http.countries.XX =
        }
    }

    /**
     * Checks if is page.
     * 
     * <pre>
     * null             => false
     * ""               => false
     * "     "          => false
     * "GET /foo"       => true
     * "GET /foo42"     => true
     * "GET /foo.cgi"   => true
     * "GET /foo.htm"  => true
     * "GET /foo.html"  => true
     * "GET /foo.php"   => true
     * "GET /foo.xhtml"  => true
     * "GET /foo.jpg"   => false
     * </pre>
     * 
     * @param request
     *            the request
     * @return true, if is page
     */
    public static boolean isPage(final String request)
    {
        boolean result;

        if (StringUtils.isBlank(request))
        {
            result = false;
        }
        else
        {
            Pattern PAGE_PATTERN = Pattern.compile("^.* (?<path>/[^ \\?]*)(\\?.*| .*)?$");

            Matcher matcher = PAGE_PATTERN.matcher(request);
            if (matcher.find())
            {
                String path = matcher.group("path").toLowerCase();

                result = path.matches("^.*(/|/[^/]*\\.html|/[^/]*\\.htm|/[^/]*\\.xhtml|/[^/]*\\.cgi|/[^/]*\\.php|/[^/\\.]*[a-z0-9])$");

                // System.out.println(result + " [" + path + "] [" + request +
                // "]");
            }
            else
            {
                result = false;
            }
        }

        //
        return result;
    }

    /**
     * Probe.
     *
     * @param logs
     *            the logs
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probe(final HttpAccessLogs logs) throws IOException, StatoolInfosException
    {
        PathCounters result;

        HttpAccessLogAnalyzer analyzer = new HttpAccessLogAnalyzer();

        HttpAccessLogIterator logIterator = (HttpAccessLogIterator) logs.iterator();
        while (logIterator.hasNext())
        {
            analyzer.probeLog(logIterator.next());
        }
        result = analyzer.getCounters();

        StringList timemarks = result.getNowTimeMarks();
        analyzer.getCounters().set(logIterator.getLogCount(), "metrics.http.logs.count", timemarks);
        analyzer.getCounters().set(logIterator.getIgnoredLogCount(), "metrics.http.logs.ignored", timemarks);
        analyzer.getCounters().set(logIterator.getFailedLogCount(), "metrics.http.logs.failed", timemarks);

        //
        return result;
    }
}
