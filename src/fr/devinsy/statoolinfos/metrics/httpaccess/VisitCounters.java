/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.threeten.extra.YearWeek;

import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.statoolinfos.metrics.TimeMarkUtils;
import fr.devinsy.strings.StringSet;

/**
 * The Class VisitCounters.
 */
public class VisitCounters extends HashMap<String, Visits>
{
    private static final long serialVersionUID = 7081954620691577515L;

    private static Logger logger = LoggerFactory.getLogger(VisitCounters.class);

    private StringSet timeMarks;

    /**
     * Instantiates a new path counters.
     */
    public VisitCounters()
    {
        super();
        this.timeMarks = new StringSet();
    }

    /**
     * Compute key.
     *
     * @param log
     *            the log
     * @return the string
     */
    private String computeKey(final HttpAccessLog log)
    {
        String result;

        result = computeKey(log.getIp(), log.getUserAgent().toString());

        //
        return result;
    }

    /**
     * Compute key.
     *
     * @param ip
     *            the ip
     * @param userAgent
     *            the user agent
     * @return the string
     */
    private String computeKey(final String ip, final String userAgent)
    {
        String result;

        result = String.format("%s--%s", ip, userAgent);

        //
        return result;
    }

    /**
     * Gets the.
     *
     * @param ip
     *            the ip
     * @param userAgent
     *            the user agent
     * @return the visits
     */
    public Visits get(final String ip, final String userAgent)
    {
        Visits result;

        String key = computeKey(ip, userAgent);

        result = get(key);

        //
        return result;
    }

    /**
     * Gets the counters.
     *
     * @return the counters
     */
    public PathCounters getCounters(final String prefix)
    {
        PathCounters result;

        result = new PathCounters();

        logger.info("timemark count={}", this.timeMarks.size());
        for (String timemark : this.timeMarks)
        {
            long count = 0;

            if (TimeMarkUtils.isDate(timemark))
            {
                LocalDate date = TimeMarkUtils.parseDate(timemark);

                for (Visits visits : this.values())
                {
                    count += visits.count(date);
                }
            }
            else if (TimeMarkUtils.isYearMonth(timemark))
            {
                YearMonth yearMonth = TimeMarkUtils.parseYearMonth(timemark);
                for (Visits visits : this.values())
                {
                    count += visits.count(yearMonth);
                }
            }
            else if (TimeMarkUtils.isYearWeek(timemark))
            {
                YearWeek yearWeek = TimeMarkUtils.parseYearWeek(timemark);
                for (Visits visits : this.values())
                {
                    count += visits.count(yearWeek);
                }
            }
            else if (TimeMarkUtils.isYear(timemark))
            {
                int year = TimeMarkUtils.parseYear(timemark);
                for (Visits visits : this.values())
                {
                    count += visits.count(year);
                }
            }

            // logger.info("timemark/count = {}/{}", timemark, count);

            result.inc(count, prefix, timemark);
        }

        logger.info("visits counters [{}]", result.size());

        //
        return result;
    }

    /**
     * Put visit.
     *
     * @param log
     *            the log
     */
    public void putVisit(final HttpAccessLog log)
    {
        if (log != null)
        {
            String key = computeKey(log);

            Visits visits = get(key);
            if (visits == null)
            {
                visits = new Visits();
                put(key, visits);
            }

            visits.add(log.getTime());
        }
    }

    /**
     * Store time marks.
     *
     * @param timemarks
     *            the timemarks
     */
    public void storeTimeMarks(final String... timemarks)
    {
        for (String timemark : timemarks)
        {
            this.timeMarks.put(timemark);
        }
    }
}
