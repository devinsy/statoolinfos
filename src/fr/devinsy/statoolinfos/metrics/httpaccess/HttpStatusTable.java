/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * The Class HttpStatusTable.
 */
public class HttpStatusTable
{
    public static final Pattern HTTP_CODE_PATTERN = Pattern.compile("\\d{3}");

    private static class SingletonHolder
    {
        private static final HttpStatusTable instance = new HttpStatusTable();
    }

    private HashMap<Integer, HttpStatus> codes;

    /**
     * Instantiates a new http status table.
     */
    private HttpStatusTable()
    {
        this.codes = new HashMap<Integer, HttpStatus>();

        put(100, "Continue", "Attente de la suite de la requête.");
        put(101, "Switching Protocols", "Acceptation du changement de protocole.");
        put(102, "Processing", "WebDAV RFC 25183,4: Traitement en cours (évite que le client dépasse le temps d’attente limite).");
        put(103, "Early Hints", "RFC 82975 : (Expérimental) Dans l'attente de la réponse définitive, le serveur retourne des liens que le client peut commencer à télécharger.");

        put(200, "OK", "Requête traitée avec succès. La réponse dépendra de la méthode de requête utilisée.");
        put(201, "Created", "Requête traitée avec succès et création d’un document.");
        put(202, "Accepted", "Requête traitée, mais sans garantie de résultat.");
        put(203, "Non-Authoritative Information", "Information retournée, mais générée par une source non certifiée.");
        put(204, "No Content", "Requête traitée avec succès mais pas d’information à renvoyer.");
        put(205, "Reset Content", "Requête traitée avec succès, la page courante peut être effacée.");
        put(206, "Partial Content", "Une partie seulement de la ressource a été transmise.");
        put(207, "Multi-Status", "WebDAV : Réponse multiple.");
        put(208, "Already Reported", "WebDAV : Le document a été envoyé précédemment dans cette collection.");
        put(210, "Content Different", "WebDAV : La copie de la ressource côté client diffère de celle du serveur (contenu ou propriétés).");
        put(226, "IM Used",
                "RFC 32296 : Le serveur a accompli la requête pour la ressource, et la réponse est une représentation du résultat d'une ou plusieurs manipulations d'instances appliquées à l'instance actuelle.");

        // put();
    }

    /**
     * Gets the.
     *
     * @param code
     *            the code
     * @return the http status
     */
    public HttpStatus get(final int code)
    {
        HttpStatus result;

        result = this.codes.get(code);

        if (result == null)
        {
            result = put(code, "UNOFFICIAL", "n/a");
        }

        //
        return result;
    }

    /**
     * Gets the.
     *
     * @param code
     *            the code
     * @return the http status
     */
    public HttpStatus get(final String code)
    {
        HttpStatus result;

        if (HTTP_CODE_PATTERN.matcher(code).matches())
        {
            int value = Integer.valueOf(code);
            result = get(value);
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Put.
     *
     * @param code
     *            the code
     * @param message
     *            the message
     * @param description
     *            the description
     * @return the http status
     */
    private HttpStatus put(final int code, final String message, final String description)
    {
        HttpStatus result;

        result = new HttpStatus(code, message, description);
        this.codes.put(code, result);

        //
        return result;
    }

    /**
     * Instance.
     *
     * @return the http status table
     */
    public static HttpStatusTable instance()
    {
        return SingletonHolder.instance;
    }
}
