/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

import java.util.HashMap;

import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.strings.StringSet;

/**
 * The Class VisitorCounters.
 */
public class VisitorCounters extends HashMap<String, StringSet>
{
    private static final long serialVersionUID = -6892042933297886639L;

    /**
     * Instantiates a new visitor counters.
     */
    public VisitorCounters()
    {
        super();
    }

    /**
     * Gets the.
     *
     * @param timeMark
     *            the time mark
     * @return the string set
     */
    private StringSet get(final String timeMark)
    {
        StringSet result;

        result = super.get(timeMark);

        //
        return result;
    }

    /**
     * Gets the counters.
     *
     * @return the counters
     */
    public PathCounters getCounters(final String prefix)
    {
        PathCounters result;

        result = new PathCounters();

        for (String timeMark : keySet())
        {
            StringSet set = get(timeMark);
            result.inc(set.size(), prefix, timeMark);
        }

        //
        return result;
    }

    /**
     * Gets the counts of a timeMark.
     *
     * @param timeMark
     *            the time mark
     * @return the counts
     */
    public int getCounts(final String timeMark)
    {
        int result;

        StringSet set = get(timeMark);
        if (set == null)
        {
            result = 0;
        }
        else
        {
            result = set.size();
        }

        //
        return result;
    }

    /**
     * Put.
     *
     * @param ip
     *            the ip
     * @param timeMarks
     *            the time marks
     */
    public void put(final String ip, final UserAgent userAgent, final String... timeMarks)
    {
        for (String timeMark : timeMarks)
        {
            put(ip, userAgent, timeMark);
        }
    }

    /**
     * Put.
     *
     * @param timemark
     *            the timemark
     * @param ip
     *            the ip
     */
    public void put(final String ip, final UserAgent userAgent, final String timemark)
    {
        StringSet set = super.get(timemark);
        if (set == null)
        {
            set = new StringSet();
            put(timemark, set);
        }

        set.put(ip + userAgent);
    }
}
