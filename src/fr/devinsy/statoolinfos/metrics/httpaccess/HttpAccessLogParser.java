/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class HttpAccessLogParser.
 */
public class HttpAccessLogParser
{
    // log_format combined '$remote_addr - $remote_user [$time_local] '
    // '"$request" $status $body_bytes_sent '
    // '"$http_referer" "$http_user_agent"';
    public static final Pattern COMBINED_PATTERN = Pattern.compile(
            "^(?<remoteAddress>[a-fA-F0-9\\:\\.]+) - (?<remoteUser>[^\\[]+) \\[(?<time>[^\\]]+)\\] \"(?<request>(?>[^\"]|\\\")*)\" (?<status>\\d+) (?<bodyBytesSent>\\d+) \"(?<referer>(?>[^\"]|\\\")*)\" \"(?<userAgent>(?>[^\"]|\\\")*)\".*$");

    public static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MMM/yyyy:HH:mm:ss Z").withLocale(Locale.ENGLISH);

    /**
     * Instantiates a new http access log parser.
     */
    private HttpAccessLogParser()
    {
    }

    /**
     * Parses the date time.
     *
     * @param dateTime
     *            the date time
     * @param formatter
     *            the formatter
     * @return the zoned date time
     */
    public static ZonedDateTime parseDateTime(final CharSequence dateTime, final DateTimeFormatter formatter)
    {
        ZonedDateTime result;

        if ((formatter == null) || (formatter == DATETIME_FORMATTER))
        {
            result = ZonedDateTime.parse(dateTime, DATETIME_FORMATTER);
        }
        else
        {
            try
            {
                result = ZonedDateTime.parse(dateTime, formatter);
            }
            catch (DateTimeParseException exception)
            {
                result = ZonedDateTime.parse(dateTime, DATETIME_FORMATTER);
            }
        }

        //
        return result;
    }

    /**
     * Parses the log.
     *
     * @param line
     *            the line
     * @return the http access log
     */
    public static HttpAccessLog parseLog(final String line)
    {
        HttpAccessLog result;

        result = parseLog(line, COMBINED_PATTERN, DATETIME_FORMATTER);

        //
        return result;
    }

    /**
     * Parses the log.
     *
     * @param line
     *            the line
     * @param pattern
     *            the pattern
     * @param dateTimeFormatter
     *            the date time formatter
     * @return the http access log
     */
    public static HttpAccessLog parseLog(final String line, final Pattern pattern, final DateTimeFormatter dateTimeFormatter)
    {
        HttpAccessLog result;

        Matcher matcher = pattern.matcher(line);
        if (matcher.matches())
        {
            result = new HttpAccessLog();
            result.setIp(matcher.group("remoteAddress"));
            result.setRemoteUser(matcher.group("remoteUser"));
            result.setTime(parseDateTime(matcher.group("time"), dateTimeFormatter));
            result.setRequest(matcher.group("request"));
            result.setStatus(HttpStatusTable.instance().get(matcher.group("status")));
            result.setBodyBytesSent(Long.valueOf(matcher.group("bodyBytesSent")));
            result.setReferer(matcher.group("referer"));
            result.setUserAgent(new UserAgent(matcher.group("userAgent")));
        }
        else
        {
            result = null;
        }

        //
        return result;
    }
}
