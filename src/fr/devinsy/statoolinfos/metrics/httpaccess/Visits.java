/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.ArrayList;

import org.threeten.extra.YearWeek;

/**
 * The Class Visits.
 */
public class Visits extends ArrayList<Visit>
{
    private static final long serialVersionUID = 4172108458534266440L;

    private static final int BORDER_MINUTES = 30;

    /**
     * Instantiates a new visits.
     */
    public Visits()
    {
        super();
    }

    /**
     * Adds the.
     *
     * @param date
     *            the date
     */
    public void add(final LocalDateTime date)
    {
        boolean ended = false;
        int index = 0;
        while (!ended)
        {
            if (index < size())
            {
                Visit visit = get(index);

                if (date.isBefore(visit.getStart().minusMinutes(BORDER_MINUTES)))
                {
                    this.add(index, new Visit(date));
                    ended = true;
                }
                else if (date.isBefore(visit.getStart()))
                {
                    visit.setStart(date);
                    if (index > 0)
                    {
                        Visit previous = get(index - 1);
                        if (previous.getEnd().isAfter(visit.getStart().minusMinutes(BORDER_MINUTES)))
                        {
                            visit.setStart(previous.getStart());
                            remove(index - 1);
                        }
                    }
                    ended = true;
                }
                else if (date.isAfter(visit.getEnd().plusMinutes(BORDER_MINUTES)))
                {
                    index += 1;
                }
                else if (date.isAfter(visit.getEnd()))
                {
                    visit.setEnd(date);
                    if (index + 1 < size())
                    {
                        Visit next = get(index + 1);
                        if (next.getStart().isBefore(visit.getEnd().plusMinutes(BORDER_MINUTES)))
                        {
                            visit.setEnd(next.getEnd());
                            remove(index + 1);
                        }
                    }
                    ended = true;
                }
                else
                {
                    // Matching visit case.
                    ended = true;
                }
            }
            else
            {
                add(new Visit(date));
                ended = true;
            }
        }
    }

    /**
     * Count.
     *
     * @param year
     *            the year
     * @return the long
     */
    public long count(final int year)
    {
        long result;

        result = 0;
        for (Visit visit : this)
        {
            if (visit.isMatching(year))
            {
                result += 1;
            }
        }

        //
        return result;
    }

    /**
     * Count.
     *
     * @param date
     *            the date
     * @return the long
     */
    public long count(final LocalDate date)
    {
        long result;

        result = 0;
        for (Visit visit : this)
        {
            if (visit.isMatching(date))
            {
                result += 1;
            }
        }

        //
        return result;
    }

    /**
     * Count.
     *
     * @param yearMonth
     *            the year month
     * @return the long
     */
    public long count(final YearMonth yearMonth)
    {
        long result;

        result = 0;
        for (Visit visit : this)
        {
            if (visit.isMatching(yearMonth))
            {
                result += 1;
            }
        }

        //
        return result;
    }

    /**
     * Count.
     *
     * @param yearWeek
     *            the year week
     * @return the long
     */
    public long count(final YearWeek yearWeek)
    {
        long result;

        result = 0;
        for (Visit visit : this)
        {
            if (visit.isMatching(yearWeek))
            {
                result += 1;
            }
        }

        //
        return result;
    }

    /**
     * Gets the duration sum.
     *
     * @return the duration sum
     */
    public Duration getDurationSum()
    {
        Duration result;

        result = Duration.ofSeconds(0);
        for (Visit visit : this)
        {
            result = result.plus(visit.getDuration());
        }

        //
        return result;
    }
}
