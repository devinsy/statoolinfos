/*
 * Copyright (C) 2021-2023 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.statoolinfos.util.FilesUtils;
import fr.devinsy.strings.StringList;

/**
 * The Class DatafilesProber.
 */
public class DatafilesProber
{
    /**
     * Instantiates a new datafiles prober.
     */
    private DatafilesProber()
    {
    }

    /**
     * Probe.
     *
     * @param directory
     *            the directory
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static PathCounters probe(final File directory) throws IOException
    {
        PathCounters result;

        result = probe(directory, "metrics.service");

        //
        return result;
    }

    /**
     * Probe.
     *
     * @param directory
     *            the directory
     * @param prefix
     *            the prefix
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static PathCounters probe(final File directory, final String prefix) throws IOException
    {
        PathCounters result;

        System.out.println("Probing directory [" + directory + "]");

        result = new PathCounters();

        if ((directory != null) && (directory.exists()) && (directory.isDirectory()))
        {
            StringList timemarks = result.getNowTimeMarks();

            // metrics.service.datafiles.bytes
            long size = FileUtils.sizeOfDirectory(directory);
            result.set(size, prefix + ".datafiles.bytes", timemarks);

            // metrics.service.datafiles.count
            long count = FilesUtils.listRecursively(directory.getAbsoluteFile()).size();
            result.set(count, prefix + ".datafiles.count", timemarks);
        }
        else
        {
            System.out.println("WARNING: datafile path not valid.");
        }

        //
        return result;
    }
}
