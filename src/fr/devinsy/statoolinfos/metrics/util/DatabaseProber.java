/*
 * Copyright (C) 2022 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.util;

import java.io.IOException;

import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.statoolinfos.util.sql.SQLDatabase;
import fr.devinsy.strings.StringList;

/**
 * The Class DatabaseProber.
 */
public class DatabaseProber
{
    /**
     * Instantiates a new database prober.
     */
    private DatabaseProber()
    {
    }

    /**
     * Probe.
     *
     * @param database
     *            the database
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static PathCounters probe(final SQLDatabase database) throws IOException
    {
        PathCounters result;

        result = new PathCounters();

        if ((database != null) && (database.isOpened()))
        {
            System.out.println("Probing database [" + database.getUrl() + "]");

            // metrics.service.database.bytes
            long bytes = database.getDatabaseSize();
            StringList timemarks = result.getNowTimeMarks();
            result.set(bytes, "metrics.service.database.bytes", timemarks);
        }

        //
        return result;
    }
}
