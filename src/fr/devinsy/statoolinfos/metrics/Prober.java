/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.statoolinfos.core.Configuration;
import fr.devinsy.statoolinfos.core.DatabaseConfig;
import fr.devinsy.statoolinfos.core.Factory;
import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.core.StatoolInfosUtils;
import fr.devinsy.statoolinfos.metrics.etherpad.EtherpadProber;
import fr.devinsy.statoolinfos.metrics.gitea.GiteaProber;
import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLogAnalyzer;
import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLogs;
import fr.devinsy.statoolinfos.metrics.httperrorlog.HttpErrorLogAnalyzer;
import fr.devinsy.statoolinfos.metrics.httperrorlog.HttpErrorLogs;
import fr.devinsy.statoolinfos.metrics.libreqr.LibreQRProber;
import fr.devinsy.statoolinfos.metrics.minetest.MinetestProber;
import fr.devinsy.statoolinfos.metrics.mumble.MumbleProber;
import fr.devinsy.statoolinfos.metrics.privatebin.PrivatebinProber;
import fr.devinsy.statoolinfos.properties.PathProperties;
import fr.devinsy.statoolinfos.properties.PathProperty;
import fr.devinsy.statoolinfos.properties.PathPropertyUtils;
import fr.devinsy.statoolinfos.util.BuildInformation;
import fr.devinsy.statoolinfos.util.FilesUtils;
import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringsUtils;

/**
 * The Class Prober.
 */
public class Prober
{
    private static Logger logger = LoggerFactory.getLogger(Prober.class);

    public static final Pattern YEAR_PATTERN = Pattern.compile("^\\d{4}$");

    /**
     * Instantiates a new prober.
     */
    private Prober()
    {
    }

    /**
     * Extract year.
     *
     * @param line
     *            the line
     * @return the string
     */
    private static boolean isYear(final String value)
    {
        boolean result;

        result = YEAR_PATTERN.matcher(value).matches();

        //
        return result;
    }

    /**
     * Probe.
     *
     * @param configuration
     *            the configuration
     * @param dayCountFilter
     *            the day count filter
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static void probe(final Configuration configuration, final int dayCountFilter) throws IOException, StatoolInfosException
    {
        StringList types = configuration.getProbeTypes();

        System.out.println("Targets=" + types.toStringWithBrackets());

        if (!types.isEmpty())
        {
            PathCounters counters = new PathCounters();
            for (String type : types)
            {
                try
                {
                    Method method = Prober.class.getMethod("probe" + type, Configuration.class);
                    PathCounters subCounters = (PathCounters) method.invoke(null, configuration);
                    counters.putAll(subCounters);
                }
                catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException exception)
                {
                    logger.error("Prober not found: [{}]", type);
                }
            }

            // Filter.
            logger.info("== Filtering with {}", dayCountFilter);
            if (dayCountFilter >= 0)
            {
                counters = counters.searchByPeriod(LocalDate.now().minusDays(dayCountFilter), LocalDate.now());
            }

            //
            File target = configuration.getProbeTarget();
            logger.info("target=[{}]", target);

            if (target == null)
            {
                throw new IllegalArgumentException("Undefined target.");
            }
            else
            {
                if (target.exists())
                {
                    // Load.
                    logger.info("== Reading previous target file.");
                    PathCounters previousCounters = readMetrics(target);
                    logger.info("previous size={}", previousCounters.size());

                    // Temporary fixed 2022-02.
                    previousCounters.renamePath("metrics.service.files", "metrics.service.datafiles");
                    previousCounters.renamePath("metrics.barcodes.count", "metrics.barcodes.created");

                    // Merge.
                    logger.info("== Merging");
                    for (PathCounter counter : counters.values())
                    {
                        PathCounter previousCounter = previousCounters.get(counter.getPath(), counter.getTimeMark());
                        if (previousCounter == null)
                        {
                            previousCounters.put(counter);
                        }
                        else
                        {
                            previousCounter.setCounter(counter.getCounter());
                        }
                    }

                    //
                    counters = previousCounters;

                    //
                    logger.info("== Backing previous target file.");
                    target.renameTo(new File(target.getParentFile(), target.getName() + ".bak"));
                }

                writeMetrics(target, counters);
            }

            //
            logger.info("== Writing.");
            logger.info("size={}", counters.size());
        }

    }

    /**
     * Probe.
     *
     * @param configurationFile
     *            the configuration file
     * @param dayCountFilter
     *            the day count filter
     * @throws StatoolInfosException
     *             the statool infos exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void probe(final File configurationFile, final int dayCountFilter) throws StatoolInfosException, IOException
    {
        logger.info("Probe {}", configurationFile.getAbsolutePath());

        Configuration configuration = Factory.loadConfiguration(configurationFile);

        probe(configuration, dayCountFilter);
    }

    /**
     * Probe etherpad.
     *
     * @param configuration
     *            the configuration
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probeEtherpad(final Configuration configuration) throws IOException, StatoolInfosException
    {
        PathCounters result;

        logger.info("== Probing Etherpad.");
        String logs = configuration.get("conf.probe.etherpad.logs");
        DatabaseConfig database = configuration.getDatabaseConfig("conf.probe.etherpad");
        logger.info("logs=[{}]", logs);
        logger.info("database={}", database.toString());

        String httpLogs = configuration.getProbeHttpAccessLogSource();
        String httpAccessLogPattern = configuration.getProbeHttpAccessLogPattern();
        String httpAccessLogDateTimePattern = configuration.getProbeHttpAccessLogDateTimePattern();
        String httpAccessPathFilter = configuration.getProbeHttpAccessLogPathFilter();
        logger.info("httpLogs=[{}]", httpLogs);
        logger.info("httpAccessPattern=[{}]", httpAccessLogPattern);
        logger.info("httpAccessDateTimePattern=[{}]", httpAccessLogDateTimePattern);
        logger.info("httpAccessPathFilter=[{}]", httpAccessPathFilter);
        HttpAccessLogs httpAccessLogs = new HttpAccessLogs(FilesUtils.searchByWildcard(httpLogs), httpAccessLogPattern, httpAccessLogDateTimePattern, httpAccessPathFilter);

        result = EtherpadProber.probe(httpAccessLogs, FilesUtils.searchByWildcard(logs), database);

        //
        return result;
    }

    /**
     * Probe framadate.
     *
     * @param configuration
     *            the configuration
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probeFramadate(final Configuration configuration) throws IOException, StatoolInfosException
    {
        PathCounters result;

        logger.info("== Probing Framadate.");
        String source = configuration.getProbeHttpAccessLogSource();
        logger.info("source=[{}]", source);

        // TODO
        // PathCounters datafilesPath =
        // HttpErrorLogAnalyzer.probe(source);
        // counters.putAll(data);

        result = new PathCounters();

        //
        return result;
    }

    /**
     * Probe gitea.
     *
     * @param configuration
     *            the configuration
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probeGitea(final Configuration configuration) throws IOException, StatoolInfosException
    {
        PathCounters result;

        logger.info("== Probing Gitea.");
        File dataDirectory = configuration.getAsFile("conf.probe.gitea.data");
        String apiURL = configuration.get("conf.probe.gitea.api.url");
        String apiToken = configuration.get("conf.probe.gitea.api.token");
        DatabaseConfig database = configuration.getDatabaseConfig("conf.probe.gitea");
        logger.info("dataPath=[{}]", dataDirectory);
        logger.info("apiURL=[{}]", apiURL);
        logger.info("apiToken=[{}]", apiToken == null ? "null" : "************************");
        logger.info("database={}", database.toString());

        String httpLogs = configuration.getProbeHttpAccessLogSource();
        String httpAccessLogPattern = configuration.getProbeHttpAccessLogPattern();
        String httpAccessLogDateTimePattern = configuration.getProbeHttpAccessLogDateTimePattern();
        String httpAccessLogPathFilter = configuration.getProbeHttpAccessLogPathFilter();
        logger.info("httpLogs=[{}]", httpLogs);
        logger.info("httpAccessPattern=[{}]", httpAccessLogPattern);
        logger.info("httpAccessDateTimePattern=[{}]", httpAccessLogDateTimePattern);
        logger.info("httpAccessPath=[{}]", httpAccessLogPathFilter);
        HttpAccessLogs httpAccessLogs = new HttpAccessLogs(FilesUtils.searchByWildcard(httpLogs), httpAccessLogPattern, httpAccessLogDateTimePattern, httpAccessLogPathFilter);

        result = GiteaProber.probe(httpAccessLogs, apiURL, apiToken, dataDirectory, database);

        //
        return result;
    }

    /**
     * Probe htt access log.
     *
     * @param configuration
     *            the configuration
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probeHttpAccessLog(final Configuration configuration) throws IOException, StatoolInfosException
    {
        PathCounters result;

        logger.info("== Probing HttpAccessLog.");
        String source = configuration.getProbeHttpAccessLogSource();
        String pattern = configuration.getProbeHttpAccessLogPattern();
        String dateTimePattern = configuration.getProbeHttpAccessLogDateTimePattern();
        String pathFilter = configuration.getProbeHttpAccessLogPathFilter();
        logger.info("source=[{}]", source);
        logger.info("pattern=[{}]", pattern);
        logger.info("dateTimePattern=[{}]", dateTimePattern);
        logger.info("path=[{}]", pathFilter);

        HttpAccessLogs httpAccessLogs = new HttpAccessLogs(FilesUtils.searchByWildcard(source), pattern, dateTimePattern, pathFilter);

        result = HttpAccessLogAnalyzer.probe(httpAccessLogs);

        //
        return result;
    }

    /**
     * Probe http error log.
     *
     * @param configuration
     *            the configuration
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probeHttpErrorLog(final Configuration configuration) throws IOException, StatoolInfosException
    {
        PathCounters result;

        logger.info("== Probing HttpErrorLog.");
        String source = configuration.getProbeHttpErrorLogSource();
        String pattern = configuration.getProbeHttpErrorLogPattern();
        String dateTimePattern = configuration.getProbeHttpErrorLogDateTimePattern();
        logger.info("source=[{}]", source);
        logger.info("pattern=[{}]", pattern);
        logger.info("dateTimePattern=[{}]", dateTimePattern);

        HttpErrorLogs httpErrorLogs = new HttpErrorLogs(FilesUtils.searchByWildcard(source), pattern, dateTimePattern);

        result = HttpErrorLogAnalyzer.probe(httpErrorLogs);

        //
        return result;
    }

    /**
     * Probe libre QR.
     *
     * @param configuration
     *            the configuration
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probeLibreQR(final Configuration configuration) throws IOException, StatoolInfosException
    {
        PathCounters result;

        logger.info("== Probing LibreQR.");

        String httpAccessSource = configuration.getProbeHttpAccessLogSource();
        String httpAccessLogPattern = configuration.getProbeHttpAccessLogPattern();
        String httpAccessLogDateTimePattern = configuration.getProbeHttpAccessLogDateTimePattern();
        String httpAccessLogPathFilter = configuration.getProbeHttpAccessLogPathFilter();
        logger.info("httpAccessSource=[{}]", httpAccessSource);
        logger.info("httpAccessPattern=[{}]", httpAccessLogPattern);
        logger.info("httpAccessDateTimePattern=[{}]", httpAccessLogDateTimePattern);
        logger.info("httpAccessPath=[{}]", httpAccessLogPathFilter);
        HttpAccessLogs httpAccessLogs = new HttpAccessLogs(FilesUtils.searchByWildcard(httpAccessSource), httpAccessLogPattern, httpAccessLogDateTimePattern, httpAccessLogPathFilter);

        File dataDirectory = configuration.getAsFile("conf.probe.libreqr.datafiles");
        logger.info("dataDirectory=[{}]", dataDirectory);

        result = LibreQRProber.probe(httpAccessLogs, dataDirectory);

        //
        return result;
    }

    /**
     * Probe minetest.
     *
     * @param configuration
     *            the configuration
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probeMinetest(final Configuration configuration) throws IOException, StatoolInfosException
    {
        PathCounters result;

        logger.info("== Probing Minetest.");
        String source = configuration.get("conf.probe.minetest.logs");
        DatabaseConfig playerDatabase = configuration.getDatabaseConfig("conf.probe.minetest.players");
        DatabaseConfig worldDatabase = configuration.getDatabaseConfig("conf.probe.minetest.worlds");
        logger.info("source=[{}]", source);
        logger.info("players database={}", playerDatabase.toString());
        logger.info("wordls database={}", worldDatabase.toString());

        result = MinetestProber.probe(source, playerDatabase, worldDatabase);

        //
        return result;
    }

    /**
     * Probe mumble.
     *
     * @param configuration
     *            the configuration
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probeMumble(final Configuration configuration) throws IOException, StatoolInfosException
    {
        PathCounters result;

        logger.info("== Probing Mumble.");
        String source = configuration.get("conf.probe.mumble.logs");
        logger.info("source=[{}]", source);

        result = MumbleProber.probe(source);

        //
        return result;
    }

    /**
     * Probe private bin.
     *
     * @param configuration
     *            the configuration
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probePrivateBin(final Configuration configuration) throws IOException, StatoolInfosException
    {
        PathCounters result;

        logger.info("== Probing Privatebin.");
        String httpAccessLogSource = configuration.getProbeHttpAccessLogSource();
        String httpAccessLogPattern = configuration.getProbeHttpAccessLogPattern();
        String httpAccessLogDateTimePattern = configuration.getProbeHttpAccessLogDateTimePattern();
        String httpAccessLogPathFilter = configuration.getProbeHttpAccessLogPathFilter();
        logger.info("source=[{}]", httpAccessLogSource);
        logger.info("pattern=[{}]", httpAccessLogPattern);
        logger.info("httpAccessDateTimePattern=[{}]", httpAccessLogDateTimePattern);
        logger.info("httpAccessPath=[{}]", httpAccessLogPathFilter);
        HttpAccessLogs httpAccessLogs = new HttpAccessLogs(FilesUtils.searchByWildcard(httpAccessLogSource), httpAccessLogPattern, httpAccessLogDateTimePattern, httpAccessLogPathFilter);

        File dataDirectory = configuration.getAsFile("conf.probe.privatebin.data");
        logger.info("dataDirectory=[{}]", dataDirectory);

        result = PrivatebinProber.probe(httpAccessLogs, dataDirectory);

        //
        return result;
    }

    /**
     * Read metrics.
     *
     * @param inputFile
     *            the input file
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static PathCounters readMetrics(final File inputFile) throws IOException
    {
        PathCounters result;

        result = new PathCounters();

        PathProperties lines = PathPropertyUtils.load(inputFile);

        for (PathProperty property : lines)
        {
            String metricName = property.getMetricName();
            if (metricName != null)
            {
                String leaf = property.getLeaf();
                if (isYear(leaf))
                {
                    PathCounter counter = new PathCounter(metricName, leaf);
                    counter.setCounter(NumberUtils.createLong(property.getValue()));
                    result.put(counter);
                }
                else if (StringUtils.equals(leaf, "months"))
                {
                    String year = property.getMetricYear();

                    StringList values = StatoolInfosUtils.splitMonthValues(property.getValue());
                    int monthIndex = 1;
                    for (String value : values)
                    {
                        if (!StringUtils.isBlank(value))
                        {
                            PathCounter counter = new PathCounter(metricName, String.format("%s-%02d", year, monthIndex));
                            counter.setCounter(NumberUtils.createLong(value));
                            result.put(counter);
                        }

                        monthIndex += 1;
                    }
                }
                else if (StringUtils.equals(leaf, "weeks"))
                {
                    String year = property.getMetricYear();

                    StringList values = StatoolInfosUtils.splitWeekValues(property.getValue());
                    int weekIndex = 1;
                    for (String value : values)
                    {
                        if (!StringUtils.isBlank(value))
                        {
                            PathCounter counter = new PathCounter(metricName, String.format("%s-W%02d", year, weekIndex));
                            counter.setCounter(NumberUtils.createLong(value));
                            result.put(counter);
                        }

                        weekIndex += 1;
                    }
                }
                else if (StringUtils.equals(leaf, "days"))
                {
                    String year = property.getMetricYear();

                    StringList values = StatoolInfosUtils.splitDayValues(property.getValue());
                    LocalDate day = LocalDate.of(Integer.parseInt(year), 1, 1);
                    for (String value : values)
                    {
                        if (!StringUtils.isBlank(value))
                        {
                            String date = day.format(DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.FRANCE));
                            PathCounter counter = new PathCounter(metricName, date);
                            counter.setCounter(NumberUtils.createLong(value));
                            result.put(counter);
                        }

                        day = day.plusDays(1);
                    }
                }
            }
        }

        //
        return result;
    }

    /**
     * Write metrics.
     *
     * @param target
     *            the target
     * @param counters
     *            the counters
     * @throws UnsupportedEncodingException
     *             the unsupported encoding exception
     * @throws FileNotFoundException
     *             the file not found exception
     */
    public static void writeMetrics(final File target, final PathCounters counters) throws UnsupportedEncodingException, FileNotFoundException
    {
        StringList prefixes = counters.getPrefixes();

        StringList metrics = new StringList();

        //
        metrics.appendln("# [File]");
        metrics.appendln("file.class=metrics");
        metrics.appendln("file.generator=StatoolInfos");
        metrics.appendln("file.datetime=" + LocalDateTime.now().toString());
        metrics.appendln("file.protocol=" + BuildInformation.instance().protocol());

        metrics.appendln();
        metrics.appendln("# [Metrics]");

        //
        for (String prefix : prefixes)
        {
            logger.info("====== {}", prefix);

            PathCounters prefixCounters = counters.getByPrefix(prefix);

            StringList years = prefixCounters.getYears().sort();

            for (String year : years)
            {
                // Year stat is complicated to build because needs all the
                // log of one year.
                // {
                // // Year.
                // PathCounter yearCounter = prefixCounters.get(prefix, year);
                // if (yearCounter != null)
                // {
                // String line = String.format("%s.%s=%s",
                // yearCounter.getPath(), yearCounter.getTimeMark(),
                // yearCounter.getCounter());
                // metrics.appendln(line);
                // }
                // }

                {
                    // Months.
                    StringList line = new StringList();
                    String values = prefixCounters.getMonthsValuesLine(prefix, year);
                    if (!StringUtils.isBlank(values))
                    {
                        line.append(prefix).append('.').append(year).append(".months=").append(values);
                        metrics.appendln(line);
                    }
                }

                {
                    // Weeks.
                    StringList line = new StringList();
                    String values = prefixCounters.getWeeksValuesLine(prefix, year);
                    if (!StringUtils.isBlank(values))
                    {
                        line.append(prefix).append('.').append(year).append(".weeks=").append(values);
                        metrics.appendln(line);
                    }
                }

                {
                    // Days.
                    StringList line = new StringList();
                    String values = prefixCounters.getDaysValuesLine(prefix, year);
                    if (!StringUtils.isBlank(values))
                    {
                        line.append(prefix).append('.').append(year).append(".days=").append(values);
                        metrics.appendln(line);
                    }
                }
            }

            metrics.appendln();
        }

        StringsUtils.writeToFile(target, metrics);
    }
}
