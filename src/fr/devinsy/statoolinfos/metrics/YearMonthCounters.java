/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics;

import java.time.YearMonth;
import java.util.HashMap;
import java.util.Iterator;

/**
 * The Class YearMonthCounters.
 */
public class YearMonthCounters extends HashMap<YearMonth, Long>
{
    private static final long serialVersionUID = 2707786945149971666L;

    /**
     * Instantiates a new year month counters.
     */
    public YearMonthCounters()
    {
        super();
    }

    /**
     * Gets the.
     *
     * @param timemark
     *            the timemark
     * @return the long
     */
    public long get(final YearMonth timemark)
    {
        long result;

        Long value = super.get(timemark);

        if (value == null)
        {
            result = 0;
        }
        else
        {
            result = value;
        }

        //
        return result;
    }

    /**
     * Gets the first.
     *
     * @return the first
     */
    public YearMonth getFirstTimeMark()
    {
        YearMonth result;

        result = null;
        Iterator<YearMonth> iterator = keySet().iterator();
        while (iterator.hasNext())
        {
            YearMonth current = iterator.next();

            if ((result == null) || (current.isBefore(result)))
            {
                result = current;
            }
        }

        //
        return result;
    }

    /**
     * Inc.
     *
     * @param timemark
     *            the timemark
     */
    public void inc(final YearMonth timemark)
    {
        Long value = super.get(timemark);
        if (value == null)
        {
            value = 0L;
        }

        put(timemark, value + 1);
    }
}
