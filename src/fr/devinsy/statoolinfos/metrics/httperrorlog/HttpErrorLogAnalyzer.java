/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httperrorlog;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.strings.StringList;

/**
 * The Class HttpErrorLogAnalyzer.
 */
public class HttpErrorLogAnalyzer
{
    private PathCounters counters;

    /**
     * Instantiates a new http error log analyzer.
     */
    public HttpErrorLogAnalyzer()
    {
        this.counters = new PathCounters();
    }

    /**
     * Gets the counters.
     *
     * @return the counters
     */
    public PathCounters getCounters()
    {
        PathCounters result;

        result = new PathCounters();
        result.putAll(this.counters);

        //
        return result;
    }

    /**
     * Probe log.
     *
     * @param log
     *            the log
     */
    public void probeLog(final HttpErrorLog log)
    {
        if (log != null)
        {
            // General HTTP access logs.
            String year = log.getYear();
            String yearMonth = log.getYearMonth();
            String yearWeek = log.getYearWeek();
            String date = log.getDate();

            // metrics.http.hits
            this.counters.inc("metrics.http.errors", year, yearMonth, yearWeek, date);

            // TODO metrics.http.errors.php
            if (StringUtils.containsIgnoreCase(log.getLevel(), "php"))
            {
                this.counters.inc("metrics.http.errors.php", year, yearMonth, yearWeek, date);
            }
        }
    }

    /**
     * Probe.
     *
     * @param logs
     *            the logs
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probe(final HttpErrorLogs logs) throws IOException, StatoolInfosException
    {
        PathCounters result;

        HttpErrorLogAnalyzer analyzer = new HttpErrorLogAnalyzer();

        HttpErrorLogIterator logIterator = (HttpErrorLogIterator) logs.iterator();
        while (logIterator.hasNext())
        {
            analyzer.probeLog(logIterator.next());
        }
        result = analyzer.getCounters();

        StringList timemarks = result.getNowTimeMarks();
        analyzer.getCounters().set(logIterator.getLogCount(), "metrics.http.logs.count", timemarks);
        analyzer.getCounters().set(logIterator.getFailedLogCount(), "metrics.http.logs.failed", timemarks);

        //
        return result;
    }
}
