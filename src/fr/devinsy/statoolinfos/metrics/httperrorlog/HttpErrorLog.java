/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httperrorlog;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import fr.devinsy.statoolinfos.metrics.TimeMarkUtils;
import fr.devinsy.strings.StringList;

/**
 * The Class HttpErrorLog.
 * 
 * Default Nginx error log format: timestamp + [level] + message.
 * 
 * 2021/01/06 15:27:17 [error] 12930#12930: *13091990 connect() failed (111:
 * Connection refused) while
 * 
 */
public class HttpErrorLog
{
    private LocalDateTime time;
    private String level;
    private String message;

    /**
     * Instantiates a new http error log.
     */
    public HttpErrorLog()
    {
        this.time = null;
        this.level = null;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public String getDate()
    {
        String result;

        result = this.time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.FRANCE));

        //
        return result;
    }

    /**
     * Gets the level.
     *
     * @return the level
     */
    public String getLevel()
    {
        return this.level;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage()
    {
        return this.message;
    }

    /**
     * Gets the time.
     *
     * @return the time
     */
    public LocalDateTime getTime()
    {
        return this.time;
    }

    /**
     * Gets the year.
     *
     * @return the year
     */
    public String getYear()
    {
        String result;

        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearOf(this.time);
        }

        //
        return result;
    }

    /**
     * Gets the year month.
     *
     * @return the year month
     */
    public String getYearMonth()
    {
        String result;

        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearMonthOf(this.time);
        }
        //
        return result;
    }

    /**
     * Gets the year week.
     *
     * @return the year week
     */
    public String getYearWeek()
    {
        String result;
        if (this.time == null)
        {
            result = null;
        }
        else
        {
            result = TimeMarkUtils.yearWeekOf(this.time);
        }
        //
        return result;
    }

    /**
     * Sets the level.
     *
     * @param level
     *            the new level
     */
    public void setLevel(final String level)
    {
        this.level = level;
    }

    /**
     * Sets the message.
     *
     * @param message
     *            the new message
     */
    public void setMessage(final String message)
    {
        this.message = message;
    }

    /**
     * Sets the time.
     *
     * @param time
     *            the new time
     */
    public void setTime(final LocalDateTime time)
    {
        this.time = time;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString()
    {
        String result;

        StringList buffer = new StringList();

        buffer.append("[time=").append(this.time).append("]");
        buffer.append("[level=").append(this.level).append("]");

        result = buffer.toString();

        //
        return result;
    }

    /**
     * To string log.
     *
     * @return the string
     */
    public String toStringLog()
    {
        String result;

        // "^(?<remoteAddress>[a-zA-F0-9\\\\:\\\\.]+) - (?<remoteUser>[^\\[]+)
        // \\[(?<time>[^\\]]+)\\] \"(?<request>.*)\" (?<status>\\d+)
        // (?<bodyBytesSent>\\d+) \"(?<referer>.*)\"
        // \"(?<userAgent>[^\"]*)\".*$");

        // result = String.format("%s %s %s \\[%s\\] \"%s\" %d %d \"%s\"
        // \"%s\"",
        result = String.format("%s [%s] %s",
                this.time.format(DateTimeFormatter.ofPattern("dd/MMM/yyyy:HH:mm:ss.SSS", Locale.ENGLISH)),
                this.level,
                this.message);

        //
        return result;
    }
}
