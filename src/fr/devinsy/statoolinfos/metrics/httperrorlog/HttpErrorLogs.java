/*
 * Copyright (C) 2022-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httperrorlog;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import fr.devinsy.statoolinfos.util.Files;

/**
 * The Class HttpErrorLogs.
 */
public class HttpErrorLogs implements Iterable<HttpErrorLog>
{
    private Files source;
    private String pattern;
    private String datePattern;

    /**
     * Instantiates a new http error logs.
     *
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public HttpErrorLogs(final File source) throws IOException
    {
        this(new Files(source), null, null);
    }

    /**
     * Instantiates a new http error logs.
     *
     * @param source
     *            the source
     * @param pattern
     *            the pattern
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public HttpErrorLogs(final File source, final String pattern) throws IOException
    {
        this(new Files(source), pattern, null);
    }

    /**
     * Instantiates a new http error logs.
     *
     * @param source
     *            the source
     * @param pattern
     *            the pattern
     * @param datePattern
     *            the date pattern
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public HttpErrorLogs(final File source, final String pattern, final String datePattern) throws IOException
    {
        this(new Files(source), pattern, datePattern);
    }

    /**
     * Instantiates a new http error logs.
     *
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public HttpErrorLogs(final Files source) throws IOException
    {
        this(source, null, null);
    }

    /**
     * Instantiates a new http error logs.
     *
     * @param source
     *            the source
     * @param pattern
     *            the pattern
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public HttpErrorLogs(final Files source, final String pattern) throws IOException
    {
        this(source, pattern, null);
    }

    /**
     * Instantiates a new http error logs.
     *
     * @param source
     *            the source
     * @param pattern
     *            the pattern
     * @param datePattern
     *            the date pattern
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public HttpErrorLogs(final Files source, final String pattern, final String datePattern) throws IOException
    {
        this.source = source;
        this.pattern = pattern;
        this.datePattern = datePattern;
    }

    /* (non-Javadoc)
     * @see java.lang.Iterable#iterator()
     */
    @Override
    public Iterator<HttpErrorLog> iterator()
    {
        HttpErrorLogIterator result;

        try
        {
            result = new HttpErrorLogIterator(this.source, this.pattern, this.datePattern);
        }
        catch (IOException exception)
        {
            throw new IllegalArgumentException("Error with iterator.", exception);
        }

        //
        return result;
    }
}
