/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httperrorlog;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class HttpErrorLogParser.
 */
public class HttpErrorLogParser
{
    public static final Pattern APACHE_ERROR_PATTERN = Pattern.compile("^\\[(?<time>[^\\]]+)\\]\\s\\[(?<level>[^\\]]*)\\]\\s(?<message>.*)$");
    public static final Pattern NGINX_ERROR_PATTERN = Pattern.compile("^(?<time>\\S+\\s\\S+)\\s\\[(?<level>[^\\]]*)\\]\\s(?<message>.*)$");

    public static final DateTimeFormatter APACHE_ERROR_DATETIME_FORMATTER = DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss.SSSSSS yyyy").withLocale(Locale.ENGLISH);
    public static final DateTimeFormatter NGINX_ERROR_DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss").withLocale(Locale.ENGLISH);

    /**
     * Instantiates a new http access log parser.
     */
    private HttpErrorLogParser()
    {
    }

    /**
     * Parses the date time.
     *
     * @param dateTime
     *            the date time
     * @param formatter
     *            the formatter
     * @return the zoned date time
     */
    public static ZonedDateTime parseDateTime(final CharSequence dateTime, final DateTimeFormatter formatter)
    {
        ZonedDateTime result;

        if ((dateTime == null) || (formatter == null))
        {
            result = null;
        }
        else
        {
            result = ZonedDateTime.parse(dateTime, formatter);
        }

        //
        return result;
    }

    /**
     * Parses the log.
     *
     * @param line
     *            the line
     * @param pattern
     *            the pattern
     * @param dateTimeFormatter
     *            the date time formatter
     * @return the http error log
     */
    public static HttpErrorLog parseLog(final String line, final Pattern pattern, final DateTimeFormatter dateTimeFormatter)
    {
        HttpErrorLog result;

        // log_format combined '$remote_addr - $remote_user [$time_local] '
        // '"$request" $status $body_bytes_sent '
        // '"$http_referer" "$http_user_agent"';
        // String pattern = "^(?<time>\\d{4}/\\d{2}/\\d{2} \\d{2}:\\d{2}:\\d{2})
        // \\[(?<level>[^\\]])\\] .*$";
        // String patternString =
        // "^(?<time>\\S+\\s\\S+)\\s\\[(?<level>[^\\]]*)\\]\\s.*$";

        if ((line == null) || (pattern == null) || (dateTimeFormatter == null))
        {
            result = null;
        }
        else
        {
            Matcher matcher = pattern.matcher(line);
            if (matcher.matches())
            {
                result = new HttpErrorLog();

                result.setTime(LocalDateTime.parse(matcher.group("time"), dateTimeFormatter));
                result.setLevel(matcher.group("level"));
                result.setMessage(matcher.group("message"));
            }
            else
            {
                result = null;
            }
        }

        //
        return result;
    }
}
