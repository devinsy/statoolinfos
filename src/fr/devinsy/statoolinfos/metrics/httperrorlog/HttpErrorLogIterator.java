/*
 * Copyright (C) 2022-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httperrorlog;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Locale;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.statoolinfos.util.Files;
import fr.devinsy.statoolinfos.util.FilesLineIterator;
import fr.devinsy.statoolinfos.util.FilesUtils;

/**
 * The Class HttpErrorLogIterator.
 */
public class HttpErrorLogIterator implements Iterator<HttpErrorLog>
{
    private static Logger logger = LoggerFactory.getLogger(HttpErrorLogIterator.class);

    private FilesLineIterator lineIterator;
    private Pattern pattern;
    private DateTimeFormatter dateTimeFormatter;
    private HttpErrorLog nextLog;
    private int logCount;
    private int failedLogCount;

    /**
     * Instantiates a new http error log iterator.
     *
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public HttpErrorLogIterator(final Files source) throws IOException
    {
        this(source, null, null);
    }

    /**
     * Instantiates a new http error log iterator.
     *
     * @param source
     *            the source
     * @param regex
     *            the regex
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public HttpErrorLogIterator(final Files source, final String regex) throws IOException
    {
        this(source, regex, null);
    }

    /**
     * Instantiates a new http error log iterator.
     *
     * @param source
     *            the source
     * @param linePattern
     *            the line pattern
     * @param dateTimePattern
     *            the date time pattern
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public HttpErrorLogIterator(final Files source, final String linePattern, final String dateTimePattern) throws IOException
    {
        this.lineIterator = new FilesLineIterator(source);
        this.nextLog = null;
        this.logCount = 0;
        this.failedLogCount = 0;

        Pattern defaultLinePattern;
        DateTimeFormatter defaultDateTimeFormatter;
        if ((linePattern == null) || (dateTimePattern == null))
        {
            if (isApacheHttpErrorLogFiles(source))
            {
                defaultLinePattern = HttpErrorLogParser.APACHE_ERROR_PATTERN;
                defaultDateTimeFormatter = HttpErrorLogParser.APACHE_ERROR_DATETIME_FORMATTER;
            }
            else
            {
                defaultLinePattern = HttpErrorLogParser.NGINX_ERROR_PATTERN;
                defaultDateTimeFormatter = HttpErrorLogParser.NGINX_ERROR_DATETIME_FORMATTER;
            }
        }
        else
        {
            // These values will never be used.
            defaultLinePattern = null;
            defaultDateTimeFormatter = null;
        }

        if (StringUtils.isBlank(linePattern))
        {
            this.pattern = defaultLinePattern;
        }
        else
        {
            this.pattern = Pattern.compile(linePattern);
        }

        if (StringUtils.isBlank(dateTimePattern))
        {
            this.dateTimeFormatter = defaultDateTimeFormatter;
        }
        else
        {
            String[] split = dateTimePattern.split("\\|");
            if (split.length != 2)
            {
                throw new IllegalArgumentException("Bad dateTimePattern format: [" + dateTimePattern + "].");
            }
            else
            {
                this.dateTimeFormatter = DateTimeFormatter.ofPattern(split[0]).withLocale(Locale.forLanguageTag(split[1]));
            }
        }
    }

    /**
     * Gets the failed log count.
     *
     * @return the failed log count
     */
    public int getFailedLogCount()
    {
        return this.failedLogCount;
    }

    /**
     * Gets the log count.
     *
     * @return the log count
     */
    public int getLogCount()
    {
        return this.logCount;
    }

    /* (non-Javadoc)
     * @see java.util.Iterator#hasNext()
     */
    @Override
    public boolean hasNext()
    {
        boolean result;

        preload();

        if (this.nextLog == null)
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /* (non-Javadoc)
     * @see java.util.Iterator#next()
     */
    @Override
    public HttpErrorLog next()
    {
        HttpErrorLog result;

        preload();

        result = this.nextLog;
        this.nextLog = null;

        //
        return result;
    }

    /**
     * Forward.
     */
    private void preload()
    {
        if (this.nextLog == null)
        {
            boolean ended = false;
            while (!ended)
            {
                if (this.lineIterator.hasNext())
                {
                    String line = this.lineIterator.next();
                    this.logCount += 1;

                    try
                    {
                        HttpErrorLog log = HttpErrorLogParser.parseLog(line, this.pattern, this.dateTimeFormatter);
                        if (log == null)
                        {
                            logger.warn("LINE IS NOT MATCHING [{}]", line);
                            this.failedLogCount += 1;
                        }
                        else
                        {
                            this.nextLog = log;
                            ended = true;
                        }
                    }
                    catch (Exception exception)
                    {
                        logger.warn("Error parsing line [{}][{}]", line, exception.getMessage());
                        this.failedLogCount += 1;
                        // exception.printStackTrace();
                    }
                }
                else
                {
                    this.nextLog = null;
                    ended = true;
                }
            }
        }
    }

    /**
     * Checks if is apache http error log files.
     *
     * @param files
     *            the files
     * @return true, if is apache http error log files
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static boolean isApacheHttpErrorLogFiles(final Files files) throws IOException
    {
        boolean result;

        String firstLine = FilesUtils.readFirstLineNotBlank(files);
        if (StringUtils.startsWith(firstLine, "["))
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }
}
