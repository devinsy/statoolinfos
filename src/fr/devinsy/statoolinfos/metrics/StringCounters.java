/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics;

import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class StringCounters.
 */
public class StringCounters extends HashMap<String, StringCounter>
{
    private static final long serialVersionUID = -4771969211702752306L;

    /**
     * Instantiates a new string counters.
     */
    public StringCounters()
    {
        super();
    }

    /**
     * Compute key.
     *
     * @param string
     *            the string
     * @return the string
     */
    public String computeKey(final String string)
    {
        String result;

        result = StringUtils.toRootLowerCase(string);

        //
        return result;
    }

    /**
     * Gets the.
     *
     * @param string
     *            the string
     * @return the long
     */
    public long get(final String string)
    {
        long result;

        String key = computeKey(string);

        StringCounter counter = super.get(key);

        if (counter == null)
        {
            result = 0L;
        }
        else
        {
            result = counter.getCounter();
        }

        //
        return result;
    }

    /**
     * Inc.
     *
     * @param string
     *            the key
     */
    public void inc(final String string)
    {
        String key = computeKey(string);

        StringCounter counter = super.get(key);

        if (counter == null)
        {
            counter = new StringCounter(string);
            put(key, counter);
        }

        counter.inc();
    }

    /**
     * To list.
     *
     * @return the string counter list
     */
    public StringCounterList toList()
    {
        StringCounterList result;

        result = new StringCounterList();

        result.addAll(values());

        //
        return result;
    }
}
