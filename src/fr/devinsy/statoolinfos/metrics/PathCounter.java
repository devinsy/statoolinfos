/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics;

/**
 * The Class PathCounter.
 */
public class PathCounter
{
    private String path;
    private String timeMark;
    private long counter;

    /**
     * Instantiates a new path counter.
     *
     * @param path
     *            the path
     * @param timeMark
     *            the time mark
     */
    public PathCounter(final String path, final String timeMark)
    {
        this.path = path;
        this.timeMark = timeMark;
        this.counter = 0;
    }

    /**
     * Gets the counter.
     *
     * @return the counter
     */
    public long getCounter()
    {
        return this.counter;
    }

    /**
     * Gets the path.
     *
     * @return the path
     */
    public String getPath()
    {
        return this.path;
    }

    /**
     * Gets the time mark.
     *
     * @return the time mark
     */
    public String getTimeMark()
    {
        return this.timeMark;
    }

    /**
     * Inc.
     */
    public void inc()
    {
        this.counter += 1;
    }

    /**
     * Inc.
     *
     * @param value
     *            the value
     */
    public void inc(final long value)
    {
        this.counter += value;
    }

    /**
     * Checks if is date.
     *
     * @return true, if is date
     */
    public boolean isDate()
    {
        boolean result;

        result = TimeMarkUtils.isDate(this.timeMark);

        //
        return result;
    }

    /**
     * Checks if is year.
     *
     * @return true, if is year
     */
    public boolean isYear()
    {
        boolean result;

        result = TimeMarkUtils.isYear(this.timeMark);

        //
        return result;
    }

    /**
     * Checks if is year mark.
     *
     * @return true, if is year mark
     */
    public boolean isYearMark()
    {
        boolean result;

        result = TimeMarkUtils.isYear(this.timeMark);

        //
        return result;
    }

    /**
     * Checks if is year week.
     *
     * @return true, if is year week
     */
    public boolean isYearWeek()
    {
        boolean result;

        result = TimeMarkUtils.isYearWeek(this.timeMark);

        //
        return result;
    }

    /**
     * Sets the counter.
     *
     * @param counter
     *            the new counter
     */
    public void setCounter(final long counter)
    {
        this.counter = counter;
    }

    /**
     * Sets the path.
     *
     * @param path
     *            the new path
     */
    public void setPath(final String path)
    {
        this.path = path;
    }

    /**
     * Sets the time mark.
     *
     * @param timeMark
     *            the new time mark
     */
    public void setTimeMark(final String timeMark)
    {
        this.timeMark = timeMark;
    }
}
