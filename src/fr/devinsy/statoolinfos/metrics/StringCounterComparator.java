/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics;

import java.util.Comparator;

import fr.devinsy.statoolinfos.util.CompareUtils;

/**
 * The Class StringCounterComparator.
 */
public class StringCounterComparator implements Comparator<StringCounter>
{
    public enum Sorting
    {
        STRING,
        COUNTER
    }

    private Sorting sorting;

    /**
     * Instantiates a new string counter comparator.
     *
     * @param sorting
     *            the sorting
     */
    public StringCounterComparator(final Sorting sorting)
    {
        this.sorting = sorting;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    @Override
    public int compare(final StringCounter alpha, final StringCounter bravo)
    {
        int result;

        result = compare(alpha, bravo, this.sorting);

        //
        return result;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @param sorting
     *            the sorting
     * @return the int
     */
    public static int compare(final StringCounter alpha, final StringCounter bravo, final Sorting sorting)
    {
        int result;

        if (sorting == null)
        {
            result = 0;
        }
        else
        {
            switch (sorting)
            {
                default:
                case STRING:
                    result = CompareUtils.compareIgnoreCase(getString(alpha), getString(bravo));
                break;

                case COUNTER:
                    result = CompareUtils.compare(getCounter(alpha), getCounter(bravo));
                    if (result == 0)
                    {
                        result = CompareUtils.compareIgnoreCase(getString(alpha), getString(bravo));
                    }
                break;
            }
        }

        //
        return result;
    }

    /**
     * Gets the counter.
     *
     * @param source
     *            the source
     * @return the counter
     */
    public static Long getCounter(final StringCounter source)
    {
        Long result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = (long) source.getCounter();
        }

        //
        return result;
    }

    /**
     * Gets the string.
     *
     * @param source
     *            the source
     * @return the string
     */
    public static String getString(final StringCounter source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.getString();
        }

        //
        return result;
    }
}
