/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.threeten.extra.YearWeek;

import fr.devinsy.statoolinfos.core.StatoolInfosUtils;
import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringSet;

/**
 * The Class PathCounters.
 */
public class PathCounters extends HashMap<String, PathCounter>
{
    private static final long serialVersionUID = 6881726853303684439L;

    /**
     * Instantiates a new path counters.
     */
    public PathCounters()
    {
        super();
    }

    /**
     * Compute key.
     *
     * @param path
     *            the path
     * @param timeMark
     *            the time mark
     * @return the string
     */
    public String computeKey(final String path, final String timeMark)
    {
        String result;

        result = path + "." + timeMark;

        //
        return result;
    }

    /**
     * Gets the.
     *
     * @param path
     *            the path
     * @param timeMark
     *            the time mark
     * @return the path counter
     */
    public PathCounter get(final String path, final String timeMark)
    {
        PathCounter result;

        String key = computeKey(path, timeMark);

        result = get(key);

        //
        return result;
    }

    /**
     * Gets the by prefix.
     *
     * @param prefix
     *            the prefix
     * @return the by prefix
     */
    public PathCounters getByPrefix(final String prefix)
    {
        PathCounters result;

        result = new PathCounters();

        for (PathCounter counter : this.values())
        {
            if (StringUtils.equals(counter.getPath(), prefix))
            {
                result.put(counter);
            }
        }

        //
        return result;
    }

    /**
     * Gets the count.
     *
     * @param path
     *            the path
     * @param timeMark
     *            the time mark
     * @return the count
     */
    public long getCount(final String path, final String timeMark)
    {
        long result;

        PathCounter counter = get(path, timeMark);
        if (counter == null)
        {
            result = 0;
        }
        else
        {
            result = counter.getCounter();
        }

        //
        return result;
    }

    /**
     * Gets the days values line.
     *
     * @param prefix
     *            the prefix
     * @param year
     *            the year
     * @return the days values line
     */
    public String getDaysValuesLine(final String prefix, final String year)
    {
        String result;

        StringList line = new StringList();

        LocalDate day = LocalDate.of(Integer.valueOf(year), 01, 01);
        boolean ended = false;
        while (!ended)
        {
            if (day.getYear() == Integer.valueOf(year))
            {
                String timeMark = day.format(DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.FRANCE));
                PathCounter dayCounter = get(prefix, timeMark);
                if (dayCounter != null)
                {
                    line.append(dayCounter.getCounter());
                }
                line.append(',');

                //
                day = day.plusDays(1);
            }
            else
            {
                ended = true;
            }
        }

        while ((!line.isEmpty()) && (line.getLast().equals(",")))
        {
            line.removeLast();
        }

        if (line.isEmpty())
        {
            result = "";
        }
        else
        {
            result = line.toString();
        }

        //
        return result;
    }

    /**
     * Gets the months values line.
     *
     * @param prefix
     *            the prefix
     * @param year
     *            the year
     * @return the months values line
     */
    public String getMonthsValuesLine(final String prefix, final String year)
    {
        String result;

        StringList line = new StringList();
        for (int month = 1; month <= 12; month++)
        {
            String timeMark = String.format("%s-%02d", year, month);
            PathCounter monthCounter = get(prefix, timeMark);
            if (monthCounter != null)
            {
                line.append(monthCounter.getCounter());
            }
            line.append(',');
        }

        while ((!line.isEmpty()) && (line.getLast().equals(",")))
        {
            line.removeLast();
        }

        if (line.isEmpty())
        {
            result = "";
        }
        else
        {
            result = line.toString();
        }

        //
        return result;
    }

    /**
     * Gets the now time marks.
     *
     * @return the now time marks
     */
    public StringList getNowTimeMarks()
    {
        StringList result;

        result = new StringList();

        LocalDate now = LocalDate.now();
        String year = TimeMark.yearOf(now).toString();
        String yearMonth = TimeMark.yearMonthOf(now).toString();
        String yearWeek = TimeMark.yearWeekOf(now).toString();
        String date = TimeMark.dayOf(now).toString();

        result.add(year);
        result.add(yearMonth);
        result.add(yearWeek);
        result.add(date);

        //
        return result;
    }

    /**
     * Gets the prefixes.
     *
     * @return the prefixes
     */
    public StringList getPrefixes()
    {
        StringList result;

        StringSet paths = new StringSet();
        for (PathCounter counter : this.values())
        {
            paths.put(counter.getPath());
        }

        result = new StringList(paths).sort();

        //
        return result;
    }

    /**
     * Gets the weeks values line.
     *
     * @param prefix
     *            the prefix
     * @param year
     *            the year
     * @return the weeks values line
     */
    public String getWeeksValuesLine(final String prefix, final String year)
    {
        String result;

        StringList line = new StringList();

        int weekCount = StatoolInfosUtils.getWeekCountOfYear(Integer.valueOf(year));

        for (int week = 1; week <= weekCount; week++)
        {
            String timeMark = String.format("%s-W%02d", year, week);
            PathCounter weekCounter = get(prefix, timeMark);
            if (weekCounter != null)
            {
                line.append(weekCounter.getCounter());
            }
            line.append(',');
        }

        while ((!line.isEmpty()) && (line.getLast().equals(",")))
        {
            line.removeLast();
        }

        if (line.isEmpty())
        {
            result = "";
        }
        else
        {
            result = line.toString();
        }
        //
        return result;
    }

    /**
     * Gets the years.
     *
     * @return the years
     */
    public StringList getYears()
    {
        StringList result;

        StringSet years = new StringSet();
        for (PathCounter counter : this.values())
        {
            TimeMark timemark = new TimeMark(counter.getTimeMark());

            Integer year = timemark.getYear();
            if (year != null)
            {
                years.put(year);
            }
        }

        result = new StringList(years);

        //
        return result;
    }

    /**
     * Inc.
     *
     * @param value
     *            the value
     * @param path
     *            the path
     * @param timeMark
     *            the time mark
     */
    public void inc(final long value, final String path, final String timeMark)
    {
        PathCounter counter = get(path, timeMark);
        if (counter == null)
        {
            counter = new PathCounter(path, timeMark);
            put(counter);
        }

        counter.inc(value);
    }

    /**
     * Inc.
     *
     * @param value
     *            the value
     * @param path
     *            the path
     * @param timeMarks
     *            the time marks
     */
    public void inc(final long value, final String path, final String... timeMarks)
    {
        for (String timeMark : timeMarks)
        {
            inc(value, path, timeMark);
        }
    }

    /**
     * Put.
     *
     * @param path
     *            the path
     * @param timeMark
     *            the time mark
     */
    public void inc(final String path, final String timeMark)
    {
        PathCounter counter = get(path, timeMark);
        if (counter == null)
        {
            counter = new PathCounter(path, timeMark);
            put(counter);
        }

        counter.inc();
    }

    /**
     * Inc.
     *
     * @param path
     *            the path
     * @param timeMarks
     *            the time marks
     */
    public void inc(final String path, final String... timeMarks)
    {
        for (String timeMark : timeMarks)
        {
            inc(path, timeMark);
        }
    }

    /**
     * Put.
     *
     * @param counter
     *            the counter
     */
    public void put(final PathCounter counter)
    {
        if (counter != null)
        {
            put(computeKey(counter.getPath(), counter.getTimeMark()), counter);
        }
    }

    /**
     * Rename path.
     *
     * @param oldPath
     *            the old path
     * @param newPath
     *            the new path
     * @return the long
     */
    public long renamePath(final String oldPath, final String newPath)
    {
        long result;

        result = 0;
        for (String key : new StringList(this.keySet()))
        {
            if (key.startsWith(oldPath))
            {
                result += 1;

                PathCounter counter = get(key);
                counter.setPath(counter.getPath().replace(oldPath, newPath));
                remove(key);
                put(counter);
                System.out.println("Renamed " + key + " to " + counter.getPath());
            }
        }

        //
        return result;
    }

    /**
     * Search by date.
     *
     * @param date
     *            the date
     * @return the path counters
     */
    public PathCounters searchByDate(final LocalDate date)
    {
        PathCounters result;

        result = new PathCounters();

        if (date != null)
        {
            String day = date.toString();
            String week = YearWeek.from(date).toString();
            String month = YearMonth.from(date).toString();

            for (PathCounter counter : this.values())
            {
                TimeMark mark = new TimeMark(counter.getTimeMark());

                if (mark.isDate() && (counter.getTimeMark().equals(day)))
                {
                    result.put(counter);
                }
                else if (mark.isYearWeek() && (counter.getTimeMark().equals(week)))
                {
                    result.put(counter);
                }
                else if (mark.isYearMonth() && (counter.getTimeMark().equals(month)))
                {
                    result.put(counter);
                }
            }
        }

        //
        return result;
    }

    public PathCounters searchByPeriod(final LocalDate startDate, final LocalDate endDate)
    {
        PathCounters result;

        result = new PathCounters();

        if ((startDate != null) && (endDate != null))
        {
            if (startDate.isAfter(endDate))
            {
                result = searchByPeriod(endDate, startDate);
            }
            else
            {
                StringSet days = new StringSet();
                StringSet weeks = new StringSet();
                StringSet months = new StringSet();
                for (LocalDate current = startDate; !current.isAfter(endDate); current = current.plusDays(1))
                {
                    days.add(current.toString());
                    weeks.add(YearWeek.from(current).toString());
                    months.add(YearMonth.from(current).toString());
                }

                for (PathCounter counter : this.values())
                {
                    TimeMark mark = new TimeMark(counter.getTimeMark());

                    if (mark.isDate() && (days.contains(counter.getTimeMark())))
                    {
                        result.put(counter);
                    }
                    else if (mark.isYearWeek() && (weeks.contains(counter.getTimeMark())))
                    {
                        result.put(counter);
                    }
                    else if (mark.isYearMonth() && (months.contains(counter.getTimeMark())))
                    {
                        result.put(counter);
                    }
                }
            }
        }

        //
        return result;
    }

    /**
     * Sets the.
     *
     * @param value
     *            the value
     * @param path
     *            the path
     */
    public void set(final long value, final String path)
    {
        set(value, path, getNowTimeMarks());
    }

    /**
     * Sets the.
     *
     * @param value
     *            the value
     * @param path
     *            the path
     * @param timeMarks
     *            the time marks
     */
    public void set(final long value, final String path, final String... timeMarks)
    {
        for (String timeMark : timeMarks)
        {
            set(value, path, timeMark);
        }
    }

    /**
     * Sets the.
     *
     * @param value
     *            the value
     * @param path
     *            the path
     * @param timeMark
     *            the time mark
     */
    public void set(final long value, final String path, final String timeMark)
    {
        PathCounter counter = get(path, timeMark);
        if (counter == null)
        {
            counter = new PathCounter(path, timeMark);
            put(counter);
        }

        counter.setCounter(value);
    }

    /**
     * Sets the.
     *
     * @param value
     *            the value
     * @param path
     *            the path
     * @param timeMarks
     *            the time marks
     */
    public void set(final long value, final String path, final StringList timeMarks)
    {
        for (String timeMark : timeMarks)
        {
            set(value, path, timeMark);
        }
    }
}
