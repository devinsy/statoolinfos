/*
 * Copyright (C) 2022-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.nextcloud;

import java.io.IOException;
import java.util.regex.Pattern;

import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.statoolinfos.metrics.UserCounters;
import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLog;
import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLogIterator;
import fr.devinsy.statoolinfos.util.Files;

/**
 * The Class NextcloudHttpLogAnalyzer.
 */
public class NextcloudHttpLogAnalyzer
{
    public static final Pattern USE_PATTERN = Pattern.compile("GET /temp/\\w+\\.png.*");
    public static final Pattern CREATE_PATTERN = Pattern.compile("POST / .*");

    private PathCounters counters;
    private UserCounters users;
    private UserCounters ipv4Users;
    private UserCounters ipv6Users;

    /**
     * Instantiates a new nextcloud http log analyzer.
     */
    public NextcloudHttpLogAnalyzer()
    {
        this.counters = new PathCounters();
        this.users = new UserCounters();
        this.ipv4Users = new UserCounters();
        this.ipv6Users = new UserCounters();
    }

    /**
     * Gets the counters.
     *
     * @return the counters
     */
    public PathCounters getCounters()
    {
        PathCounters result;

        result = new PathCounters();
        result.putAll(this.counters);

        result.putAll(this.users.getCounters("metrics.service.users"));
        result.putAll(this.ipv4Users.getCounters("metrics.service.users.ipv4"));
        result.putAll(this.ipv6Users.getCounters("metrics.service.users.ipv6"));

        //
        return result;
    }

    /**
     * Probe log.
     *
     * @param log
     *            the log
     */
    public void probeLog(final HttpAccessLog log)
    {
        if (log != null)
        {
            // General HTTP access logs.
            String year = log.getYear();
            String yearMonth = log.getYearMonth();
            String yearWeek = log.getYearWeek();
            String date = log.getDate();

            // metrics.service.users
            // metrics.service.users.ipv4
            // metrics.service.users.ipv6
            if ((!log.isBot()) && (USE_PATTERN.matcher(log.getRequest()).matches()))
            {
                String key = String.format("%s---%s", log.getIp(), log.getUserAgent());

                this.users.put(key, year, yearMonth, yearWeek, date);

                if (log.isIPv4())
                {
                    this.ipv4Users.put(key, year, yearMonth, yearWeek, date);
                }
                else
                {
                    this.ipv6Users.put(key, year, yearMonth, yearWeek, date);
                }
            }

            // metrics.barcodes.count
            if ((log.getStatus().getCode() == 200) && (CREATE_PATTERN.matcher(log.getRequest()).matches()))
            {
                this.counters.inc("metrics.barcodes.count", year, yearMonth, yearWeek, date);
            }
        }
    }

    /**
     * Probe.
     *
     * @param httpAccessLogFiles
     *            the http access log files
     * @param httpRegex
     *            the http regex
     * @return the path counters
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static PathCounters probe(final Files httpAccessLogFiles, final String httpRegex) throws IOException, StatoolInfosException
    {
        PathCounters result;

        NextcloudHttpLogAnalyzer analyzer = new NextcloudHttpLogAnalyzer();

        HttpAccessLogIterator logs = new HttpAccessLogIterator(httpAccessLogFiles, httpRegex);
        while (logs.hasNext())
        {
            analyzer.probeLog(logs.next());
        }

        result = analyzer.getCounters();

        //
        return result;
    }
}
