/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.uptime;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;

import fr.devinsy.strings.StringList;

/**
 * The Class Uptimes.
 */
public class Uptimes extends ArrayList<Uptime>
{
    private static final long serialVersionUID = 7892787666725111836L;

    /**
     * Instantiates a new uptimes.
     */
    public Uptimes()
    {
    }

    /* (non-Javadoc)
     * @see java.util.ArrayList#add(java.lang.Object)
     */
    @Override
    public boolean add(final Uptime uptime)
    {
        boolean result;

        if (uptime == null)
        {
            result = false;
        }
        else
        {
            result = super.add(uptime);
        }

        //
        return result;
    }

    /**
     * Gets the uptimes.
     *
     * @param date
     *            the date
     * @return the uptimes
     */
    public Uptimes getByDate(final LocalDate date)
    {
        Uptimes result;

        result = new Uptimes();

        for (Uptime uptime : this)
        {
            if (uptime.getDateTime().toLocalDate().isEqual(date))
            {
                result.add(uptime);
            }
        }

        //
        return result;
    }

    /**
     * Last.
     *
     * @return the uptime
     */
    public Uptime last()
    {
        Uptime result;

        if (isEmpty())
        {
            result = null;
        }
        else
        {
            result = get(size() - 1);
        }

        //
        return result;
    }

    /**
     * Purge.
     *
     * @param days
     *            the days
     */
    public void purge(final long days)
    {
        Iterator<Uptime> iterator = this.iterator();

        LocalDate limit = LocalDate.now().minusDays(days);

        while (iterator.hasNext())
        {
            Uptime uptime = iterator.next();

            if (uptime.getDateTime().toLocalDate().isBefore(limit))
            {
                iterator.remove();
            }
        }
    }

    /**
     * Sort.
     *
     * @param sorting
     *            the sorting
     * @return the files
     */
    public Uptimes sort(final UptimeComparator.Sorting sorting)
    {
        Uptimes result;

        sort(new UptimeComparator(sorting));

        result = this;

        //
        return result;
    }

    /**
     * Sort by date time.
     *
     * @return the uptimes
     */
    public Uptimes sortByDateTime()
    {
        Uptimes result;

        result = sort(UptimeComparator.Sorting.DATETIME);

        //
        return result;
    }

    /**
     * To time list.
     *
     * @return the string list
     */
    public StringList toTimeList()
    {
        StringList result;

        result = new StringList();
        for (Uptime uptime : this.sortByDateTime())
        {
            String line = String.format("%s %03d %s", uptime.getDateTime().format(DateTimeFormatter.ofPattern("HH:mm")), uptime.getCode(), uptime.getStatus().name());
            result.append(line);
        }

        //
        return result;
    }
}
