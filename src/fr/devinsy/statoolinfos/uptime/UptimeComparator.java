/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.uptime;

import java.time.LocalDateTime;
import java.util.Comparator;

import fr.devinsy.statoolinfos.util.CompareUtils;

/**
 * The Class UptimeComparator.
 */
public class UptimeComparator implements Comparator<Uptime>
{
    public enum Sorting
    {
        DATETIME
    }

    private Sorting sorting;

    /**
     * Instantiates a new uptime comparator.
     *
     * @param sorting
     *            the sorting
     */
    public UptimeComparator(final Sorting sorting)
    {
        this.sorting = sorting;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    @Override
    public int compare(final Uptime alpha, final Uptime bravo)
    {
        int result;

        result = compare(alpha, bravo, this.sorting);

        //
        return result;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @param sorting
     *            the sorting
     * @return the int
     */
    public static int compare(final Uptime alpha, final Uptime bravo, final Sorting sorting)
    {
        int result;

        if (sorting == null)
        {
            result = 0;
        }
        else
        {
            switch (sorting)
            {
                default:
                case DATETIME:
                    result = CompareUtils.compare(getDateTime(alpha), getDateTime(bravo));
                break;
            }
        }

        //
        return result;
    }

    /**
     * Gets the datetime.
     *
     * @param source
     *            the source
     * @return the datetime
     */
    public static LocalDateTime getDateTime(final Uptime source)
    {
        LocalDateTime result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.getDateTime();
        }

        //
        return result;
    }
}
