/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.uptime;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import fr.devinsy.statoolinfos.core.Service;
import fr.devinsy.statoolinfos.core.Services;
import fr.devinsy.strings.StringList;

/**
 * The Class ServiceUptimes.
 */
public class ServiceUptimes extends ArrayList<ServiceUptime>
{
    private static final long serialVersionUID = -4077776702697960767L;

    /**
     * Instantiates a new service uptimes.
     */
    public ServiceUptimes()
    {
    }

    /**
     * Adds the.
     *
     * @param service
     *            the service
     * @param uptime
     *            the uptime
     */
    public void add(final Service service, final Uptime uptime)
    {
        ServiceUptime target = new ServiceUptime(service, uptime);
        add(target);
    }

    /* (non-Javadoc)
     * @see java.util.ArrayList#add(java.lang.Object)
     */
    @Override
    public boolean add(final ServiceUptime uptime)
    {
        boolean result;

        if (uptime == null)
        {
            result = false;
        }
        else
        {
            result = super.add(uptime);
        }

        //
        return result;
    }

    /**
     * To time list.
     *
     * @return the string list
     */
    public StringList toTimeList()
    {
        StringList result;

        result = new StringList();
        for (Uptime uptime : this)
        {
            String line = String.format("%s %s %s %03d %s", uptime.getDateTime().format(DateTimeFormatter.ofPattern("HH:mm")), uptime.getCode(), uptime.getStatus().name());
            result.append(line);
        }

        //
        return result;
    }

    /**
     * Of.
     *
     * @param services
     *            the services
     * @param journal
     *            the journal
     * @return the service uptimes
     */
    public static ServiceUptimes of(final Services services, final UptimeJournal journal)
    {
        ServiceUptimes result;

        result = new ServiceUptimes();

        for (Service service : services)
        {
            if (service.getWebsiteURL() != null)
            {
                for (Uptime uptime : journal.getUptimes(service.getWebsiteURL()))
                {
                    result.add(service, uptime);
                }
            }
        }

        //
        return result;
    }
}
