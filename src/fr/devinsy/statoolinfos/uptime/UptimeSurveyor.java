/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.uptime;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import fr.devinsy.statoolinfos.util.Chrono;

/**
 * The Class UptimeChecker.
 */
public class UptimeSurveyor
{
    /**
     * Instantiates a new stat agent.
     */
    private UptimeSurveyor()
    {
    }

    /**
     * Survey.
     *
     * @param journal
     *            the journal
     * @param urls
     *            the urls
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void survey(final UptimeJournal journal, final URLSet urls) throws IOException
    {
        Uptimes uptimes = survey(urls);
        journal.addAll(uptimes);
        journal.purge(30);
    }

    /**
     * Survey.
     *
     * @param url
     *            the url
     * @return the int
     */
    public static int survey(final URL url)
    {
        int result;

        try
        {
            int boundMax = 7;
            boolean ended = false;
            URL currentURL = url;
            String currentUserAgent = "StatoolInfos Uptime Bot";
            result = 0;
            int currentBound = 0;
            while (!ended)
            {
                if (currentBound < boundMax)
                {
                    HttpURLConnection connection = (HttpURLConnection) currentURL.openConnection();
                    connection.setConnectTimeout(5 * 1000);
                    connection.setReadTimeout(5 * 1000);
                    connection.setRequestProperty("User-Agent", currentUserAgent);
                    result = connection.getResponseCode();
                    String location = connection.getHeaderField("Location");
                    connection.disconnect();

                    if ((result == 301) || (result == 302) || (result == 307) || (result == 308))
                    {
                        System.out.println(Thread.currentThread().getId() + " BOUND DETECTED " + currentURL.toString() + " -> " + location);
                        currentURL = new URL(location);

                        currentBound += 1;
                    }
                    else if (result == 403)
                    {
                        System.out.println(Thread.currentThread().getId() + " 403 FORBIDDEN DETECTED " + currentURL.toString() + " -> " + location);
                        currentUserAgent = "Mozilla from StatoolInfos Uptime B**";

                        currentBound += 1;
                    }
                    else
                    {
                        ended = true;
                    }
                }
                else
                {
                    ended = true;
                    result = 0;
                }
            }
        }
        catch (IOException exception)
        {
            result = 0;
        }

        //
        return result;
    }

    /**
     * Survey.
     *
     * @param urls
     *            the urls
     * @return the uptimes
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static Uptimes survey(final URLSet urls) throws IOException
    {
        Uptimes result;

        //
        result = new Uptimes();
        for (URL url : urls)
        {
            result.add(new Uptime(url));
        }

        Chrono chrono = new Chrono().start();

        //
        System.out.println("Survey of " + result.size());
        /* MonoThread version.
        for (Uptime uptime : result)
        {
            int code = survey(uptime.getURL());
            uptime.update(code);
            System.out.println("Uptime: " + uptime.getStatus().toString() + " " +
                    uptime.getURL().toString());
        }
        */

        //
        CookieManager cookieManager = new CookieManager();
        CookieHandler.setDefault(cookieManager);

        //
        ExecutorService pool = Executors.newFixedThreadPool(20);
        for (Uptime uptime : result)
        {
            pool.execute(new Runnable()
            {
                @Override
                public void run()
                {
                    int code = survey(uptime.getURL());
                    uptime.update(code);
                    System.out.println(Thread.currentThread().getId() + " Uptime: " + uptime.getStatus().toString() + " " + uptime.getURL().toString());
                }
            });
        }

        pool.shutdown();
        while (!pool.isTerminated())
        {
            try
            {
                System.out.println("Wait for URL survey.");
                pool.awaitTermination(5, TimeUnit.SECONDS);
            }
            catch (InterruptedException exception)
            {
                exception.printStackTrace();
            }
        }

        System.out.println("URL Survey time: " + chrono.toString());

        //
        return result;
    }
}
