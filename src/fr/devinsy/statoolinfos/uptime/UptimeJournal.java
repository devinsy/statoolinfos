/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.uptime;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;

import fr.devinsy.statoolinfos.core.Service;
import fr.devinsy.statoolinfos.core.Services;

/**
 * The Class UptimeJournal.
 */
public class UptimeJournal
{
    private HashMap<String, Uptimes> map;

    /**
     * Instantiates a new uptime journal.
     */
    public UptimeJournal()
    {
        super();
        this.map = new HashMap<String, Uptimes>();
    }

    /**
     * Adds the.
     *
     * @param uptime
     *            the uptime
     */
    public void add(final Uptime uptime)
    {
        if (uptime != null)
        {
            Uptimes uptimes = this.map.get(uptime.getURL().toString());

            if (uptimes == null)
            {
                uptimes = new Uptimes();
                this.map.put(uptime.getURL().toString(), uptimes);
            }

            uptimes.add(uptime);
        }
    }

    /**
     * Adds the all.
     *
     * @param uptimes
     *            the uptimes
     */
    public void addAll(final Uptimes uptimes)
    {
        if (uptimes != null)
        {
            for (Uptime uptime : uptimes)
            {
                add(uptime);
            }
        }
    }

    /**
     * Clear.
     */
    public void clear()
    {
        this.map.clear();
    }

    /**
     * Gets the uptime stats.
     *
     * @param url
     *            the url
     * @param date
     *            the date
     * @return the uptime stats
     */
    public UptimeStat getStat(final URL url, final LocalDate date)
    {
        UptimeStat result;

        result = new UptimeStat();

        if (url != null)
        {
            Uptime older = null;
            for (Uptime uptime : getUptimes(url, date))
            {
                if (uptime.getStatus() == UptimeStatus.OK)
                {
                    result.incOk();
                }
                else if (uptime.getStatus() == UptimeStatus.ERROR)
                {
                    result.incError();
                }

                if ((older == null) || (uptime.getDateTime().isAfter(older.getDateTime())))
                {
                    older = uptime;
                }
            }
            if (older != null)
            {
                result.setStatus(older.getStatus());
            }
        }

        //
        return result;
    }

    /**
     * Gets the status.
     *
     * @param url
     *            the url
     * @param date
     *            the date
     * @return the status
     */
    public UptimeStatus getStatus(final URL url, final LocalDate date)
    {
        UptimeStatus result;

        int count = 0;
        int okCount = 0;
        int errorCount = 0;
        for (Uptime uptime : getUptimes(url, date))
        {
            count += 1;
            if (uptime.getStatus() == UptimeStatus.OK)
            {
                okCount += 1;
            }
            else if (uptime.getStatus() == UptimeStatus.ERROR)
            {
                errorCount += 1;
            }
        }

        if (count == 0)
        {
            result = UptimeStatus.VOID;
        }
        else if (okCount == 0)
        {
            result = UptimeStatus.ERROR;
        }
        else if (errorCount == 1)
        {
            result = UptimeStatus.WARNING;
        }
        else if (errorCount > 1)
        {
            result = UptimeStatus.ERROR;
        }
        else
        {
            result = UptimeStatus.OK;
        }

        //
        return result;
    }

    /**
     * Gets the.
     *
     * @param url
     *            the url
     * @return the uptimes
     */
    public Uptimes getUptimes(final URL url)
    {
        Uptimes result;

        if (url == null)
        {
            result = new Uptimes();
        }
        else
        {
            result = this.map.get(url.toString());
            if (result == null)
            {
                result = new Uptimes();
            }
        }

        //
        return result;
    }

    /**
     * Gets the.
     *
     * @param url
     *            the url
     * @param date
     *            the date
     * @return the uptimes
     */
    public Uptimes getUptimes(final URL url, final LocalDate date)
    {
        Uptimes result;

        result = getUptimes(url);

        if (result == null)
        {
            result = new Uptimes();
        }
        else
        {
            result = result.getByDate(date);
        }

        //
        return result;
    }

    /**
     * Gets the uptimes all.
     *
     * @return the uptimes all
     */
    public Uptimes getUptimesAll()
    {
        Uptimes result;

        result = new Uptimes();

        for (String urlValue : this.map.keySet())
        {
            for (Uptime uptime : this.map.get(urlValue))
            {
                result.add(uptime);
            }
        }

        //
        return result;
    }

    /**
     * Gets the URL list.
     *
     * @return the URL list
     */
    public URLs getURLs()
    {
        URLs result;

        result = new URLs(this.map.keySet().size());
        for (String urlValue : this.map.keySet())
        {
            try
            {
                result.add(new URL(urlValue));
            }
            catch (MalformedURLException exception)
            {
                exception.printStackTrace();
            }
        }

        //
        return result;
    }

    /**
     * Checks for last in error.
     *
     * @param services
     *            the services
     * @return true, if successful
     */
    public boolean hasRecentError(final Services services)
    {
        boolean result;

        if (services == null)
        {
            result = false;
        }
        else if (isEmpty())
        {
            result = false;
        }
        else
        {
            boolean ended = false;
            Iterator<Service> iterator = services.iterator();
            result = false;
            while (!ended)
            {
                if (iterator.hasNext())
                {
                    Service current = iterator.next();
                    if (current.getWebsiteURL() != null)
                    {
                        Uptimes currentUptimes = getUptimes(current.getWebsiteURL());
                        if (!currentUptimes.isEmpty())
                        {
                            if (currentUptimes.last().getStatus() == UptimeStatus.ERROR)
                            {
                                ended = true;
                                result = true;
                            }
                        }
                    }
                }
                else
                {
                    ended = true;
                    result = false;
                }
            }
        }

        //
        return result;
    }

    /**
     * Checks if is empty.
     *
     * @return true, if is empty
     */
    public boolean isEmpty()
    {
        boolean result;

        result = this.map.isEmpty();

        //
        return result;
    }

    /**
     * Purge.
     *
     * @param days
     *            the days
     */
    public void purge(final long days)
    {
        for (Uptimes uptimes : this.map.values())
        {
            uptimes.purge(days);
        }
    }

    /**
     * Size.
     *
     * @return the int
     */
    public int size()
    {
        int result;

        result = this.map.size();

        //
        return result;
    }

    /**
     * Sort all.
     */
    public void sortAll()
    {
        for (Uptimes uptimes : this.map.values())
        {
            uptimes.sortByDateTime();
        }
    }
}
