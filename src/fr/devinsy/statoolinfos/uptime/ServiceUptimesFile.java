/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.uptime;

import java.io.File;
import java.io.IOException;

import fr.devinsy.statoolinfos.io.CSVWriter;
import fr.devinsy.statoolinfos.io.ODSWriter;
import fr.devinsy.statoolinfos.io.SpreadsheetWriter;

/**
 * The Class ServiceUptimesFile.
 */
public class ServiceUptimesFile
{
    /**
     * Save CSV.
     *
     * @param file
     *            the file
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void saveCSV(final File file, final ServiceUptimes source) throws IOException
    {
        CSVWriter out = null;
        try
        {
            out = new CSVWriter(file, ';');
            write(out, source);
        }
        finally
        {
            out.close();
        }
    }

    /**
     * Save ODS.
     *
     * @param file
     *            the file
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void saveODS(final File file, final ServiceUptimes source) throws IOException
    {
        ODSWriter out = null;
        try
        {
            out = new ODSWriter(file);
            write(out, source);
        }
        finally
        {
            out.close();
        }
    }

    /**
     * Write.
     *
     * @param out
     *            the out
     * @param uptimes
     *            the uptimes
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void write(final SpreadsheetWriter out, final ServiceUptimes uptimes) throws IOException
    {
        // Write label line.
        out.writeCell("serviceName");
        out.writeCell("organizationName");
        out.writeCell("DateTime");
        out.writeCell("Status");
        out.writeCell("Code");
        out.writeCell("URL");
        out.writeEndRow();

        // Write organization lines.
        for (ServiceUptime uptime : uptimes)
        {
            out.writeCell(uptime.getService().getName());
            out.writeCell(uptime.getService().getOrganization().getName());
            out.writeCell(uptime.getDateTime().toString());
            out.writeCell(uptime.getStatus().toString());
            out.writeCell(String.valueOf(uptime.getCode()));
            if (uptime.getURL() == null)
            {
                out.writeCell("");
            }
            else
            {
                out.writeCell(uptime.getURL().toString());
            }

            out.writeEndRow();
        }
    }
}
