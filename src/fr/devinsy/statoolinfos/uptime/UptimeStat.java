/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.uptime;

/**
 * The Class UptimeStat.
 */
public class UptimeStat
{
    private int count;
    private int okCount;
    private int warningCount;
    private int alertCount;
    private int errorCount;
    private int voidCount;
    private UptimeStatus last;

    /**
     * Instantiates a new uptime stat.
     */
    public UptimeStat()
    {
        this.count = 0;
        this.warningCount = 0;
        this.alertCount = 0;
        this.okCount = 0;
        this.errorCount = 0;
        this.voidCount = 0;
        this.last = null;
    }

    public int getAlertCount()
    {
        return this.alertCount;
    }

    /**
     * Gets the count.
     *
     * @return the count
     */
    public int getCount()
    {
        return this.count;
    }

    /**
     * Gets the error count.
     *
     * @return the error count
     */
    public int getErrorCount()
    {
        return this.errorCount;
    }

    /**
     * Gets the ok count.
     *
     * @return the ok count
     */
    public int getOkCount()
    {
        return this.okCount;
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    public UptimeStatus getStatus()
    {
        UptimeStatus result;

        if (this.count == 0)
        {
            result = UptimeStatus.VOID;
        }
        else if (this.errorCount > 0)
        {
            if (this.last == UptimeStatus.OK)
            {
                result = UptimeStatus.WARNING;
            }
            else if (this.okCount == 0)
            {
                result = UptimeStatus.ERROR;
            }
            else
            {
                result = UptimeStatus.ALERT;
            }
        }
        else
        {
            result = UptimeStatus.OK;
        }

        //
        return result;
    }

    /**
     * Gets the unavailable count.
     *
     * @return the unavailable count
     */
    public int getUnavailableCount()
    {
        int result;

        result = this.errorCount + this.alertCount + this.voidCount;

        //
        return result;
    }

    /**
     * Gets the void count.
     *
     * @return the void count
     */
    public int getVoidCount()
    {
        return this.voidCount;
    }

    /**
     * Gets the warning count.
     *
     * @return the warning count
     */
    public int getWarningCount()
    {
        return this.warningCount;
    }

    /**
     * Inc alert.
     */
    public void incAlert()
    {
        this.count += 1;
        this.alertCount += 1;
    }

    /**
     * Inc error.
     */
    public void incError()
    {
        this.count += 1;
        this.errorCount += 1;
    }

    /**
     * Inc ok.
     */
    public void incOk()
    {
        this.count += 1;
        this.okCount += 1;
    }

    /**
     * Inc void.
     */
    public void incVoid()
    {
        this.count += 1;
        this.voidCount += 1;
    }

    /**
     * Inc warning.
     */
    public void incWarning()
    {
        this.count += 1;
        this.warningCount += 1;
    }

    /**
     * Sets the status.
     *
     * @param status
     *            the new status
     */
    public void setStatus(final UptimeStatus status)
    {
        this.last = status;
    }
}
