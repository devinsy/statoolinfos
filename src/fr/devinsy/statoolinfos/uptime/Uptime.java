/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.uptime;

import java.net.URL;
import java.time.LocalDateTime;

import fr.devinsy.statoolinfos.metrics.httpaccess.HttpStatusCategory;

/**
 * The Class UptimeCheck.
 */
public class Uptime
{
    private URL url;
    private LocalDateTime datetime;
    private int code;
    private UptimeStatus status;

    /**
     * Instantiates a new uptime check.
     *
     * @param url
     *            the url
     */
    public Uptime(final URL url)
    {
        this.url = url;
        this.datetime = null;
        this.code = 0;
        this.status = UptimeStatus.VOID;
    }

    /**
     * Instantiates a new uptime.
     *
     * @param datetime
     *            the datetime
     * @param status
     *            the status
     * @param code
     *            the code
     * @param url
     *            the url
     */
    public Uptime(final URL url, final LocalDateTime datetime, final UptimeStatus status, final int code)
    {
        this.url = url;
        this.datetime = datetime;
        this.code = code;
        this.status = status;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public int getCode()
    {
        return this.code;
    }

    /**
     * Gets the date time.
     *
     * @return the date time
     */
    public LocalDateTime getDateTime()
    {
        return this.datetime;
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    public UptimeStatus getStatus()
    {
        return this.status;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public URL getURL()
    {
        return this.url;
    }

    /**
     * Update.
     *
     * @param code
     *            the code
     */
    public void update(final int code)
    {
        this.datetime = LocalDateTime.now();
        this.code = code;
        if ((HttpStatusCategory.isSuccess(code)) || (HttpStatusCategory.isUnauthorized(code)))
        {
            this.status = UptimeStatus.OK;
        }
        else
        {
            this.status = UptimeStatus.ERROR;
        }
    }
}
