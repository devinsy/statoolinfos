/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.uptime;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

import org.apache.commons.io.IOUtils;

/**
 * The Class UptimeJournalFile.
 */
public class UptimeJournalFile
{
    /**
     * Instantiates a new uptime journal file.
     */
    private UptimeJournalFile()
    {
    }

    /**
     * Load.
     *
     * @param file
     *            the file
     * @return the uptime journal
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static UptimeJournal load(final File file) throws IOException
    {
        UptimeJournal result;

        result = load(file, StandardCharsets.UTF_8);

        //
        return result;
    }

    /**
     * Load.
     *
     * @param file
     *            the file
     * @param charset
     *            the charset
     * @return the uptime journal
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static UptimeJournal load(final File file, final Charset charset) throws IOException
    {
        UptimeJournal result;

        if (file == null)
        {
            throw new IllegalArgumentException("File parameter is null.");
        }
        else
        {
            BufferedReader in = null;
            try
            {
                in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charset));
                result = read(in);
            }
            finally
            {
                IOUtils.closeQuietly(in);
            }
        }

        //
        return result;
    }

    /**
     * Read.
     *
     * @param in
     *            the in
     * @return the uptime journal
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static UptimeJournal read(final BufferedReader in) throws IOException
    {
        UptimeJournal result;

        result = new UptimeJournal();

        boolean ended = false;
        while (!ended)
        {
            String line = in.readLine();

            if (line == null)
            {
                ended = true;
            }
            else
            {
                Uptime uptime = valueOf(line);
                result.add(uptime);
            }
        }

        result.sortAll();

        //
        return result;
    }

    /**
     * Save.
     *
     * @param file
     *            the file
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void save(final File file, final UptimeJournal source) throws IOException
    {
        PrintWriter out = null;
        try
        {
            out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8));
            write(out, source);
        }
        finally
        {
            //
            IOUtils.closeQuietly(out);
        }
    }

    /**
     * Value of.
     *
     * @param line
     *            the line
     * @return the uptime
     */
    public static Uptime valueOf(final String line)
    {
        Uptime result;

        try
        {
            if (line == null)
            {
                result = null;
            }
            else
            {
                String[] tokens = line.split(" ", 4);

                LocalDateTime datetime = LocalDateTime.parse(tokens[0]);
                UptimeStatus status = UptimeStatus.valueOf(tokens[1]);
                int code = Integer.parseInt(tokens[2]);
                URL url = new URL(tokens[3]);

                result = new Uptime(url, datetime, status, code);
            }
        }
        catch (MalformedURLException exception)
        {
            result = null;
            exception.printStackTrace();
        }

        //
        return result;
    }

    /**
     * Write.
     *
     * @param out
     *            the out
     * @param journal
     *            the journal
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void write(final PrintWriter out, final UptimeJournal journal) throws IOException
    {
        if (journal != null)
        {
            for (Uptime uptime : journal.getUptimesAll())
            {
                String line = String.format("%s %s %d %s", uptime.getDateTime().toString(), uptime.getStatus(), uptime.getCode(), uptime.getURL());
                out.write(line);
                out.write("\n");
            }
        }
    }
}
