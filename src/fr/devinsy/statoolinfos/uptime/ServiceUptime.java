/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.uptime;

import fr.devinsy.statoolinfos.core.Service;

/**
 * The Class ServiceUptime.
 */
public class ServiceUptime extends Uptime
{
    private Service service;

    /**
     * Instantiates a new service uptime.
     *
     * @param service
     *            the service
     * @param uptime
     *            the uptime
     */
    public ServiceUptime(final Service service, final Uptime uptime)
    {
        // final URL url, final LocalDateTime datetime, final UptimeStatus
        // status, final int code
        super(uptime.getURL(), uptime.getDateTime(), uptime.getStatus(), uptime.getCode());
        this.service = service;
    }

    /**
     * Gets the service.
     *
     * @return the service
     */
    public Service getService()
    {
        return this.service;
    }
}
