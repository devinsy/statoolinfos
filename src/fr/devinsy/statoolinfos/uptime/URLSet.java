/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.uptime;

import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

/**
 * The Class URLSet.
 * 
 * WARNING: cannot extends {@code HashSet<URL>} because URL hashcode method
 * resolves value, so all URL with same IP have the same hashcode.
 */
public class URLSet implements Iterable<URL>
{
    private HashMap<String, URL> map;

    /**
     * Instantiates a new URL set.
     */
    public URLSet()
    {
        this.map = new HashMap<String, URL>();
    }

    /**
     * Adds the.
     *
     * @param url
     *            the url
     */
    public void add(final URL url)
    {
        if (url != null)
        {
            this.map.put(url.toString(), url);
        }
    }

    /**
     * Adds the all.
     *
     * @param urls
     *            the urls
     */
    public void addAll(final URLSet urls)
    {
        for (URL url : urls)
        {
            add(url);
        }
    }

    /**
     * Clear.
     */
    public void clear()
    {
        this.map.clear();
    }

    /* (non-Javadoc)
     * @see java.lang.Iterable#iterator()
     */
    @Override
    public Iterator<URL> iterator()
    {
        Iterator<URL> result;

        result = this.map.values().iterator();

        //
        return result;
    }

    /**
     * Size.
     *
     * @return the int
     */
    public int size()
    {
        int result;

        result = this.map.size();

        //
        return result;
    }
}
