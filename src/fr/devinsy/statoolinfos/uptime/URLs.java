/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.uptime;

import java.net.URL;
import java.util.ArrayList;

/**
 * The Class URLs.
 */
public class URLs extends ArrayList<URL>
{
    private static final long serialVersionUID = -2530910138006185902L;

    /**
     * Instantiates a new URLs.
     */
    public URLs()
    {
        super();
    }

    /**
     * Instantiates a new UR ls.
     *
     * @param capacity
     *            the capacity
     */
    public URLs(final int capacity)
    {
        super(capacity);
    }

}
