/*
 * Copyright (C) 2021-2023 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class DatabaseConfig.
 */
public class DatabaseConfig
{
    private String url;
    private String user;
    private String password;

    /**
     * Instantiates a new database config.
     */
    public DatabaseConfig()
    {
        this.url = null;
        this.user = null;
        this.password = null;
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    public String getPassword()
    {
        return this.password;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl()
    {
        return this.url;
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public String getUser()
    {
        return this.user;
    }

    /**
     * Checks if is sets the.
     *
     * @return true, if is sets the
     */
    public boolean isSet()
    {
        boolean result;

        if (StringUtils.isAnyBlank(this.url, this.user))
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /**
     * Sets the password.
     *
     * @param password
     *            the new password
     */
    public void setPassword(final String password)
    {
        this.password = password;
    }

    /**
     * Sets the url.
     *
     * @param url
     *            the new url
     */
    public void setUrl(final String url)
    {
        this.url = url;
    }

    /**
     * Sets the user.
     *
     * @param user
     *            the new user
     */
    public void setUser(final String user)
    {
        this.user = user;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String result;

        String password;
        if (StringUtils.isBlank(this.password))
        {
            password = "";
        }
        else
        {
            password = "**********";
        }

        result = String.format("[%s,%s,%s]", this.url, this.user, password);

        //
        return result;
    }
}
