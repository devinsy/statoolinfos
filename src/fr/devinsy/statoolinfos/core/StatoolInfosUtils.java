/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.Year;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.tika.Tika;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;

import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringsUtils;

/**
 * The Class StatoolInfosUtils.
 */
public class StatoolInfosUtils
{
    public static final DateTimeFormatter PATTERN_SHORTDATE = DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.FRANCE);
    public static final DateTimeFormatter PATTERN_LONGDATE = DateTimeFormatter.ofPattern("dd/MM/yyyy HH':'mm", Locale.FRANCE);

    /**
     * Default if zero.
     *
     * @param value
     *            the value
     * @param defaultValue
     *            the default value
     * @return the string
     */
    public static String defaultIfZero(final long value, final String defaultValue)
    {
        String result;

        if (value == 0)
        {
            result = defaultValue;
        }
        else
        {
            result = String.valueOf(value);
        }

        //
        return result;
    }

    /**
     * Epoch to local date time.
     *
     * @param epoch
     *            the epoch
     * @return the local date time
     */
    public static LocalDateTime epochToLocalDateTime(final long epoch)
    {
        LocalDateTime result;

        result = Instant.ofEpochMilli(epoch).atZone(ZoneId.systemDefault()).toLocalDateTime();

        //
        return result;
    }

    /**
     * File local date time.
     *
     * @param file
     *            the file
     * @return the local date time
     */
    public static LocalDateTime fileLocalDateTime(final File file)
    {
        LocalDateTime result;

        long epoch = file.lastModified();

        result = epochToLocalDateTime(epoch);

        //
        return result;
    }

    /**
     * Find extension.
     *
     * @param file
     *            the file
     * @return the string
     */
    public static String findExtension(final File file)
    {
        String result;

        try
        {
            Tika tika = new Tika();
            String mimeTypeLabel = tika.detect(file);
            MimeType mimeType = MimeTypes.getDefaultMimeTypes().forName(mimeTypeLabel);
            result = mimeType.getExtension();

            // Because Tika failed to recognize SVG file without xml header
            // line.
            if (result.equals(".txt") && (StringUtils.startsWithIgnoreCase(FileUtils.readFileToString(file, StandardCharsets.UTF_8), "<svg")))
            {
                result = ".svg";
            }
        }
        catch (IOException | MimeTypeException exception)
        {
            exception.printStackTrace();
            result = "";
        }

        //
        return result;
    }

    /**
     * Generate cat logo.
     *
     * @param seed
     *            the seed
     * @param target
     *            the target
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void generateCatLogo(final String seed, final File target) throws IOException
    {
        URL source = new URL("https://www.peppercarrot.com/extras/html/2016_cat-generator/avatar.php?seed=" + seed);

        FileUtils.copyURLToFile(source, target, 5000, 5000);
    }

    /**
     * Gets the week count of year.
     *
     * @param year
     *            the year
     * @return the week count of year
     */
    public static int getWeekCountOfYear(final int year)
    {
        int result;

        result = new GregorianCalendar(year, 11, 01).getActualMaximum(Calendar.WEEK_OF_YEAR);

        //
        return result;
    }

    /**
     * Gets the current time in long format.
     * 
     * @return the long
     */
    public static long now()
    {
        return new Date().getTime();
    }

    /**
     * Overlapp.
     *
     * @param start1
     *            the start 1
     * @param end1
     *            the end 1
     * @param start2
     *            the start 2
     * @param end2
     *            the end 2
     * @return true, if successful
     */
    public static boolean overlapp(final Year start1, final Year end1, final Year start2, final Year end2)
    {
        boolean result;

        if ((start1 == null) || (start2 == null))
        {
            result = false;
        }
        else
        {
            Year end11;
            if (end1 == null)
            {
                end11 = Year.now();
            }
            else
            {
                end11 = end1;
            }

            Year end22;
            if (end2 == null)
            {
                end22 = Year.now();
            }
            else
            {
                end22 = end2;
            }

            if ((end22.isBefore(start1)) || (start2.isAfter(end11)))
            {
                result = false;
            }
            else
            {
                result = true;
            }
        }

        //
        return result;
    }

    /**
     * Parses the date.
     *
     * @param date
     *            the date
     * @return the local date
     */
    public static LocalDate parseDate(final String date)
    {
        LocalDate result;

        try
        {
            if (date == null)
            {
                result = null;
            }
            else if (date.matches("^\\d{1,2}/\\d{1,2}/\\d{4}$"))
            {
                result = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            }
            else if (date.matches("^\\d{4}-\\d{1,2}-\\d{1,2}$"))
            {
                result = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            }
            else if (date.matches("^\\d{1,2}/\\d{4}$"))
            {
                result = LocalDate.parse("01/" + date, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            }
            else if (date.matches("\\d{4}-\\d{2}"))
            {
                result = LocalDate.parse(date + "-01", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            }
            else
            {
                result = null;
            }
        }
        catch (DateTimeParseException exception)
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Sha1sum a file.
     *
     * @param file
     *            the file
     * @return the string
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static String sha1sum(final File file) throws IOException
    {
        String result;

        result = DigestUtils.sha1Hex(FileUtils.readFileToByteArray(file));

        //
        return result;
    }

    /**
     * Split day values.
     *
     * @param values
     *            the values
     * @return the string list
     */
    public static StringList splitDayValues(final String values)
    {
        StringList result;

        result = new StringList();

        if (StringUtils.isNotBlank(values))
        {
            for (String value : values.split("[,]"))
            {
                if (StringUtils.isBlank(value))
                {
                    result.add("");
                }
                else
                {
                    result.add(value);
                }
            }

            for (int index = result.size(); index < 365; index++)
            {
                result.add("");
            }
        }

        //
        return result;
    }

    /**
     * Split month values.
     *
     * @param values
     *            the values
     * @return the string list
     */
    public static StringList splitMonthValues(final String values)
    {
        StringList result;

        result = new StringList();

        if (StringUtils.isNotBlank(values))
        {
            for (String value : values.split("[,]"))
            {
                if (StringUtils.isBlank(value))
                {
                    result.add("");
                }
                else
                {
                    result.add(value);
                }
            }

            for (int index = result.size(); index < 12; index++)
            {
                result.add("");
            }
        }

        //
        return result;
    }

    /**
     * Split week values.
     *
     * @param values
     *            the values
     * @return the string list
     */
    public static StringList splitWeekValues(final String values)
    {
        StringList result;

        result = new StringList();

        if (StringUtils.isNotBlank(values))
        {
            for (String value : values.split("[,]"))
            {
                if (StringUtils.isBlank(value))
                {
                    result.add("");
                }
                else
                {
                    result.add(value);
                }
            }

            for (int index = result.size(); index < 52; index++)
            {
                result.add("");
            }
        }

        //
        return result;
    }

    /**
     * To duration.
     *
     * @param startDate
     *            the start date
     * @param endDate
     *            the end date
     * @return the string
     */
    public static String toHumanDuration(final LocalDate startDate, final LocalDate endDate)
    {
        String result;

        if ((startDate == null) && (endDate == null))
        {
            result = null;
        }
        else if ((startDate == null) && (endDate != null))
        {
            result = toHumanDuration(LocalDate.now(), endDate);
        }
        else if ((startDate != null) && (endDate == null))
        {
            result = toHumanDuration(startDate, LocalDate.now());
        }
        else if ((startDate != null) && (endDate != null))
        {
            Period period = Period.between(startDate, LocalDate.now());

            if (period.getYears() == 0)
            {
                if (period.getMonths() == 0)
                {
                    if (period.getDays() <= 1)
                    {
                        result = String.format("%d jour", period.getDays());
                    }
                    else
                    {
                        result = String.format("%d jours", period.getDays());
                    }
                }
                else
                {
                    result = String.format("%d mois", period.getMonths());
                }
            }
            else if (period.getYears() == 1)
            {
                period.minusYears(1);

                if (period.getMonths() == 0)
                {
                    result = "1 an";
                }
                else
                {
                    result = String.format("1 an et %d mois", period.getMonths());
                }
            }
            else
            {
                long years = period.getYears();
                period.minusYears(period.getYears());

                if (period.getMonths() == 0)
                {
                    result = years + " ans";
                }
                else
                {
                    result = String.format("%d ans et %d mois", years, period.getMonths());
                }
            }
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * To human long.
     *
     * @param value
     *            the value
     * @return the string
     */
    public static String toHumanLong(final LocalDateTime value)
    {
        String result;

        result = toHumanLong(value, null);

        //
        return result;
    }

    /**
     * To human long.
     *
     * @param value
     *            the value
     * @param defaultValue
     *            the default value
     * @return the string
     */
    public static String toHumanLong(final LocalDateTime value, final String defaultValue)
    {
        String result;

        if (value == null)
        {
            result = null;
        }
        else
        {
            result = value.format(PATTERN_LONGDATE);
        }

        //
        return result;
    }

    /**
     * To human short.
     *
     * @param value
     *            the value
     * @return the string
     */
    public static String toHumanShort(final LocalDateTime value)
    {
        String result;

        result = toHumanShort(value, null);

        //
        return result;
    }

    /**
     * To human short.
     *
     * @param value
     *            the value
     * @param defaultValue
     *            the default value
     * @return the string
     */
    public static String toHumanShort(final LocalDateTime value, final String defaultValue)
    {
        String result;

        if (value == null)
        {
            result = null;
        }
        else
        {
            result = value.format(PATTERN_SHORTDATE);
        }

        //
        return result;
    }

    /**
     * To integer.
     *
     * @param value
     *            the value
     * @return the integer
     */
    public static Integer toInteger(final String value)
    {
        Integer result;

        if ((value == null) || (!NumberUtils.isDigits(value)))
        {
            result = null;
        }
        else
        {
            result = Integer.parseInt(value);
        }

        //
        return result;
    }

    /**
     * To Json numbers.
     *
     * @param source
     *            the source
     * @return the string
     */
    public static String toJSonNumbers(final StringList source)
    {
        String result;

        result = StringsUtils.toString(source, "[", ",", "]");

        //
        return result;
    }

    /**
     * To J son numbers.
     *
     * @param labels
     *            the labels
     * @param values
     *            the source
     * @return the string
     */
    public static String toJSonNumbers(final StringList labels, final StringList values)
    {
        String result;

        Iterator<String> labelIterator = labels.iterator();
        Iterator<String> valueIterator = values.iterator();

        StringList buffer = new StringList();
        while (labelIterator.hasNext())
        {
            String label = labelIterator.next();
            String value = valueIterator.next();

            // buffer.append("{t: new Date('" + label + "'), y: " + value +
            // "}");
            buffer.append("{t: '" + label + "', y: " + value + "}");
        }
        result = StringsUtils.toString(buffer, "[", ",", "]");

        //
        return result;
    }

    /**
     * To Json strings.
     *
     * @param source
     *            the source
     * @return the string
     */
    public static String toJSonStrings(final StringList source)
    {
        String result;

        StringList target = new StringList();

        target.append("[");
        for (String string : source)
        {
            target.append("'");
            target.append(string);
            target.append("'");
            target.append(",");
        }
        target.removeLast();
        target.append("]");

        result = target.toString();

        //
        return result;
    }

    /**
     * To percentage.
     *
     * @param value
     *            the value
     * @param max
     *            the max
     * @return the string
     */
    public static String toPercentage(final int value, final int max)
    {
        String result;

        result = (value * 100 / max) + "&#160;%";

        //
        return result;
    }

    /**
     * To technical name.
     *
     * @param source
     *            the source
     * @return the string
     */
    public static String toTechnicalName(final String source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = StringUtils.stripAccents(source.toLowerCase()).replaceAll("[^a-z0-9]+", "");
        }

        //
        return result;
    }

    /**
     * To year week.
     *
     * @param source
     *            the source
     * @return the string
     */
    public static String toYearWeek(final LocalDate source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.format(DateTimeFormatter.ofPattern("YYYYww", Locale.FRANCE));
        }

        //
        return result;
    }

    /**
     * Unactivate SSL check.
     */
    public static void unactivateSSLCheck()
    {
        // Create a trust manager that does not validate certificate chains.
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager()
                {
                    @Override
                    public void checkClientTrusted(final java.security.cert.X509Certificate[] certs, final String authType)
                    {
                    }

                    @Override
                    public void checkServerTrusted(final java.security.cert.X509Certificate[] certs, final String authType)
                    {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers()
                    {
                        return null;
                    }
                }
        };

        // Install the all-trusting trust manager.
        try
        {
            // SSLContext context = SSLContext.getInstance("SSL");
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
        }
        catch (Exception exception)
        {
        }
    }

    /**
     * Url last modified.
     *
     * @param url
     *            the url
     * @return the local date time
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static LocalDateTime urlLastModified(final URL url) throws IOException
    {
        LocalDateTime result;

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        Long epoch = connection.getLastModified();
        connection.disconnect();
        result = epochToLocalDateTime(epoch);
        // result =
        // Instant.ofEpochMilli(epoch).atZone(ZoneId.of("GMT")).toLocalDateTime();

        //
        return result;
    }
}
