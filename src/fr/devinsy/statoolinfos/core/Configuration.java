/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.statoolinfos.properties.PathProperties;
import fr.devinsy.statoolinfos.properties.PathProperty;
import fr.devinsy.statoolinfos.properties.PathPropertyList;
import fr.devinsy.strings.StringList;

/**
 * The Class PathProperty.
 */
public class Configuration extends PathPropertyList
{
    private static final long serialVersionUID = 2085950843956966741L;

    /**
     * Instantiates a new federation.
     */
    public Configuration()
    {
        super();
    }

    /**
     * Instantiates a new federation.
     *
     * @param properties
     *            the properties
     */
    public Configuration(final PathProperties properties)
    {
        super(properties);
    }

    /**
     * Adds the.
     *
     * @param path
     *            the path
     * @param value
     *            the value
     */
    public void add(final String path, final String value)
    {
        if (!StringUtils.isBlank(path))
        {
            PathProperty property = new PathProperty(path, value);
            this.add(property);
        }
    }

    /**
     * Gets the builds the directory.
     *
     * @return the builds the directory
     */
    public File getBuildDirectory()
    {
        File result;

        String path = getBuildDirectoryPath();
        if (StringUtils.isBlank(path))
        {
            result = null;
        }
        else
        {
            result = new File(path);
        }

        //
        return result;
    }

    /**
     * Gets the builds the directory path.
     *
     * @return the builds the directory path
     */
    public String getBuildDirectoryPath()
    {
        String result;

        result = get("conf.build.directory");

        //
        return result;
    }

    /**
     * Gets the builds the directory valid.
     *
     * @return the builds the directory valid
     */
    public File getBuildDirectoryValid()
    {
        File result;

        result = getBuildDirectory();
        if (result == null)
        {
            throw new IllegalArgumentException("Undefined build directory.");
        }
        else if (!result.exists())
        {
            throw new IllegalArgumentException("Build directory does not exist: " + result.getAbsolutePath());
        }

        //
        return result;
    }

    /**
     * Gets the builds the input.
     *
     * @return the builds the input
     */
    public File getBuildInput()
    {
        File result;

        String path = get("conf.build.input");
        if (StringUtils.isBlank(path))
        {
            result = null;
        }
        else
        {
            result = new File(path);
        }

        //
        return result;
    }

    /**
     * Gets the category file.
     *
     * @return the category file
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public File getCategoryFile() throws StatoolInfosException
    {
        File result;

        String path = get("conf.categories");
        if (StringUtils.isBlank(path))
        {
            result = null;
        }
        else
        {
            result = new File(path);
        }

        //
        return result;
    }

    /**
     * Gets the class name.
     *
     * @return the class name
     */
    public String getClassName()
    {
        String result;

        result = get("conf.class");

        //
        return result;
    }

    /**
     * Gets the crawl cache directory.
     *
     * @return the crawl cache directory
     */
    public File getCrawlCacheDirectory()
    {
        File result;

        result = new File(get("conf.crawl.cache"));

        //
        return result;
    }

    /**
     * Gets the crawl cache path.
     *
     * @return the crawl cache path
     */
    public String getCrawlCachePath()
    {
        String result;

        result = get("conf.crawl.cache");

        //
        return result;
    }

    /**
     * Gets the crawl input path.
     *
     * @return the crawl input path
     */
    public String getCrawlInputPath()
    {
        String result;

        result = get("conf.crawl.input");

        //
        return result;
    }

    /**
     * Gets the crawl input.
     *
     * @return the crawl input
     */
    public URL getCrawlInputURL()
    {
        URL result;

        try
        {
            String path = getCrawlInputPath();
            if (StringUtils.isBlank(path))
            {
                result = null;
            }
            else
            {
                result = new URL(path);
            }
        }
        catch (MalformedURLException exception)
        {
            exception.printStackTrace();
            result = null;
        }

        //
        return result;
    }

    /**
     * Gets the database config.
     *
     * @param prefix
     *            the prefix
     * @return the database config
     */
    public DatabaseConfig getDatabaseConfig(final String prefix)
    {
        DatabaseConfig result;

        result = new DatabaseConfig();

        result.setUrl(get(prefix + ".database.url"));
        result.setUser(get(prefix + ".database.user"));
        result.setPassword(get(prefix + ".database.password"));

        //
        return result;
    }

    /**
     * Gets the edito directory.
     *
     * @return the edito directory
     */
    public File getEditoDirectory()
    {
        File result;

        String directoryPath = get("conf.htmlize.edito.directory");

        if (StringUtils.isBlank(directoryPath))
        {
            result = null;
        }
        else
        {
            result = new File(directoryPath);
        }

        //
        return result;
    }

    /**
     * Gets the probe http access log date pattern.
     *
     * @return the probe http access log date pattern
     */
    public String getProbeHttpAccessLogDateTimePattern()
    {
        String result;

        result = get("conf.probe.httpaccesslog.datetimepattern");

        //
        return result;
    }

    /**
     * Gets the probe http access log path filter.
     *
     * @return the probe http access log path filter
     */
    public String getProbeHttpAccessLogPathFilter()
    {
        String result;

        result = get("conf.probe.httpaccesslog.pathfilter");

        //
        return result;
    }

    /**
     * Gets the probe http access log pattern.
     *
     * @return the probe http access log pattern
     */
    public String getProbeHttpAccessLogPattern()
    {
        String result;

        result = get("conf.probe.httpaccesslog.pattern");

        //
        return result;
    }

    /**
     * Gets the probe http access log file.
     *
     * @return the probe http access log file
     */
    public String getProbeHttpAccessLogSource()
    {
        String result;

        result = get("conf.probe.httpaccesslog.file");

        //
        return result;
    }

    /**
     * Gets the probe http access log sources.
     *
     * @return the probe http access log sources
     */
    public StringList getProbeHttpAccessLogSources()
    {
        StringList result;

        result = new StringList();
        String source = getProbeHttpAccessLogSource();

        if (StringUtils.isNotBlank(source))
        {
            String[] paths = source.split(",");

            for (String path : paths)
            {
                if (StringUtils.isNotBlank(path))
                {
                    result.add(StringUtils.trim(path));
                }
            }
        }

        //
        return result;
    }

    /**
     * Gets the probe http error log date time pattern.
     *
     * @return the probe http error log date time pattern
     */
    public String getProbeHttpErrorLogDateTimePattern()
    {
        String result;

        result = get("conf.probe.httperrorlog.datetimepattern");

        //
        return result;
    }

    /**
     * Gets the probe http error log pattern.
     *
     * @return the probe http error log pattern
     */
    public String getProbeHttpErrorLogPattern()
    {
        String result;

        result = get("conf.probe.httperrorlog.pattern");

        //
        return result;
    }

    /**
     * Gets the probe http error log source.
     *
     * @return the probe http error log source
     */
    public String getProbeHttpErrorLogSource()
    {
        String result;

        result = get("conf.probe.httperrorlog.file");

        //
        return result;
    }

    /**
     * Gets the probe target.
     *
     * @return the probe target
     */
    public File getProbeTarget()
    {
        File result;

        String filename = get("conf.probe.target");
        if (StringUtils.isBlank(filename))
        {
            result = null;
        }
        else
        {
            result = new File(filename);
        }

        //
        return result;
    }

    /**
     * Gets the probe types.
     *
     * @return the probe types
     */
    public StringList getProbeTypes()
    {
        StringList result;

        result = new StringList();

        String types = get("conf.probe.types");
        if (!StringUtils.isBlank(types))
        {
            for (String token : types.split("[, ;]"))
            {
                if (!StringUtils.isBlank(token))
                {
                    result.append(token);
                }
            }
        }

        //
        return result;
    }

    /**
     * Checks for valid cache.
     *
     * @return true, if successful
     */
    public boolean hasValidCache()
    {
        boolean result;

        String path = get("conf.crawl.cache");
        if (StringUtils.isBlank(path))
        {
            result = false;
        }
        else if (!new File(path).exists())
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /**
     * Checks if is category file defined.
     *
     * @return true, if is category file defined
     */
    public boolean isCategoryFileDefined()
    {
        boolean result;

        if (StringUtils.isBlank(get("conf.categories")))
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /**
     * Checks if is federation.
     *
     * @return true, if is federation
     */
    public boolean isFederation()
    {
        boolean result;

        result = StringUtils.equals(getClassName(), "federation");

        //
        return result;
    }

    /**
     * Checks if is organization.
     *
     * @return true, if is organization
     */
    public boolean isOrganization()
    {
        boolean result;

        result = StringUtils.equals(getClassName(), "organization");

        //
        return result;
    }

    /**
     * Checks if is service.
     *
     * @return true, if is service
     */
    public boolean isService()
    {
        boolean result;

        result = StringUtils.equals(getClassName(), "service");

        //
        return result;
    }
}
