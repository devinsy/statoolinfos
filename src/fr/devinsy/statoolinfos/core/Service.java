/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.YearMonth;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import fr.devinsy.statoolinfos.checker.PropertyChecks;
import fr.devinsy.statoolinfos.crawl.CrawlJournal;
import fr.devinsy.statoolinfos.properties.MonthValues;
import fr.devinsy.statoolinfos.properties.PathProperties;
import fr.devinsy.statoolinfos.properties.PathPropertyList;
import fr.devinsy.statoolinfos.uptime.URLSet;
import fr.devinsy.statoolinfos.util.URLUtils;

/**
 * The Class Service.
 */
public class Service extends PathPropertyList
{
    private static final long serialVersionUID = 3629841771102288863L;

    public enum HostProviderType
    {
        HOME,
        HOSTEDBAY,
        HOSTEDSERVER,
        OUTSOURCED,
        UNKNOWN
    }

    public enum HostServerType
    {
        NANO,
        PHYSICAL,
        VIRTUAL,
        SHARED,
        CLOUD,
        UNKNOWN
    }

    public enum RegistrationType
    {
        NONE,
        FREE,
        MEMBER,
        CLIENT,
        UNKNOWN
    }

    public enum ServiceInstallType
    {
        DISTRIBUTION,
        PROVIDER,
        TOOLING,
        PACKAGE,
        CLONEREPO,
        ARCHIVE,
        SOURCES,
        CONTAINER,
        UNKNOWN
    }

    public enum Status
    {
        OK,
        WARNING,
        ALERT,
        ERROR,
        OVER,
        VOID
    }

    private Organization organization;
    private File inputFile;
    private URL inputURL;
    private String logoFileName;
    private MetricsList metricsList;
    private PropertyChecks inputChecks;
    private CrawlJournal crawlJournal;

    /**
     * Instantiates a new service.
     */
    public Service()
    {
        this(null);
    }

    /**
     * Instantiates a new service.
     *
     * @param properties
     *            the properties
     */
    public Service(final PathProperties properties)
    {
        super(properties);
        this.inputChecks = new PropertyChecks();
        this.crawlJournal = new CrawlJournal();
        this.metricsList = new MetricsList();
    }

    /**
     * Gets the age.
     *
     * @return the age
     */
    public String getAge()
    {
        String result;

        result = StatoolInfosUtils.toHumanDuration(getStartDate(), getEndDate());

        //
        return result;
    }

    /**
     * Gets the contact email.
     *
     * @return the contact email
     */
    public String getContactEmail()
    {
        String result;

        result = get("service.contact.email");

        //
        return result;
    }

    /**
     * Gets the contact website.
     *
     * @return the contact website
     */
    public URL getContactURL()
    {
        URL result;

        result = getURL("service.contact.url");

        //
        return result;
    }

    /**
     * Gets the country code.
     *
     * @return the country code
     */
    public String getCountryCode()
    {
        String result;

        result = StringUtils.toRootUpperCase(get("host.country.code"));

        //
        return result;
    }

    /**
     * Gets the crawl date.
     *
     * @return the crawl date
     */
    public LocalDateTime getCrawlDate()
    {
        LocalDateTime result;

        result = LocalDateTime.parse(get("crawl.datetime"));

        //
        return result;
    }

    /**
     * Gets the crawled date.
     *
     * @return the crawled date
     */
    public LocalDateTime getCrawledDate()
    {
        LocalDateTime result;

        result = LocalDateTime.parse(get("crawl.file.datetime"));

        if (result.getYear() == 1970)
        {
            result = getCrawlDate();
        }

        //
        return result;
    }

    /**
     * Gets the crawl journal.
     *
     * @return the crawl journal
     */
    public CrawlJournal getCrawlJournal()
    {
        return this.crawlJournal;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        String result;

        result = get("service.description");

        //
        return result;
    }

    /**
     * Gets the end date.
     *
     * @return the end date
     */
    public LocalDate getEndDate()
    {
        LocalDate result;

        result = StatoolInfosUtils.parseDate(getEndDateValue());

        //
        return result;
    }

    /**
     * Gets the end date value.
     *
     * @return the end date value
     */
    public String getEndDateValue()
    {
        String result;

        result = get("service.enddate");

        //
        return result;
    }

    /**
     * Gets the end year.
     *
     * @return the end year
     */
    public Year getEndYear()
    {
        Year result;

        LocalDate date = getEndDate();

        if (date == null)
        {
            result = null;
        }
        else
        {
            result = Year.from(date);
        }

        //
        return result;
    }

    /**
     * Gets the host name.
     *
     * @return the host name
     */
    public String getHostName()
    {
        String result;

        result = get("host.name");

        //
        return result;
    }

    /**
     * Gets the host provider hypervisor.
     *
     * @return the host provider hypervisor
     */
    public String getHostProviderHypervisor()
    {
        String result;

        result = get("host.provider.hypervisor");

        //
        return result;
    }

    /**
     * Gets the host provider type.
     *
     * @return the host provider type
     */
    public HostProviderType getHostProviderType()
    {
        HostProviderType result;

        try
        {
            String value = StringUtils.toRootUpperCase(get("host.provider.type"));

            result = HostProviderType.valueOf(value);
        }
        catch (IllegalArgumentException | NullPointerException exception)
        {
            result = HostProviderType.UNKNOWN;
        }

        //
        return result;
    }

    /**
     * Gets the host server distribution.
     *
     * @return the host server distribution
     */
    public String getHostServerDistribution()
    {
        String result;

        result = get("host.server.distribution");

        //
        return result;
    }

    /**
     * Gets the host server type.
     *
     * @return the host server type
     */
    public HostServerType getHostServerType()
    {
        HostServerType result;

        try
        {
            String value = StringUtils.toRootUpperCase(get("host.server.type"));

            result = HostServerType.valueOf(value);
        }
        catch (IllegalArgumentException | NullPointerException exception)
        {
            result = HostServerType.UNKNOWN;
        }

        //
        return result;
    }

    /**
     * Gets the input checks.
     *
     * @return the input checks
     */
    public PropertyChecks getInputChecks()
    {
        return this.inputChecks;
    }

    /**
     * Gets the input checks all.
     *
     * @return the input checks all
     */
    public PropertyChecks getInputChecksAll()
    {
        PropertyChecks result;

        result = new PropertyChecks();

        result.addAll(this.inputChecks);

        for (Metrics metrics : getMetrics())
        {
            result.addAll(metrics.getInputChecks());
        }

        //
        return result;
    }

    /**
     * Gets the input file.
     *
     * @return the input file
     */
    public File getInputFile()
    {
        return this.inputFile;
    }

    /**
     * Gets the input URL.
     *
     * @return the input URL
     */
    public URL getInputURL()
    {
        return this.inputURL;
    }

    /**
     * Gets the legal website.
     *
     * @return the legal website
     */
    public URL getLegalURL()
    {
        URL result;

        String value = get("service.legal.url", "service.legal");

        result = URLUtils.of(value);

        //
        return result;
    }

    /**
     * Gets the license name.
     *
     * @return the license name
     */
    public String getLicenseName()
    {
        String result;

        result = get("software.license.name");

        //
        return result;
    }

    /**
     * Gets the local name.
     *
     * @return the local name
     */
    public String getLocalFileBaseName()
    {
        String result;

        result = this.organization.getTechnicalName() + "-" + getTechnicalName();

        //
        return result;
    }

    /**
     * Gets the local name.
     *
     * @return the local name
     */
    public String getLocalFileName()
    {
        String result;

        result = getLocalFileBaseName() + ".properties";

        //
        return result;
    }

    public String getLogoFileName()
    {
        return this.logoFileName;
    }

    /**
     * Gets the logo URL.
     *
     * @return the logo URL
     */
    public URL getLogoURL()
    {
        URL result;

        result = getURL("service.logo");

        //
        return result;
    }

    /**
     * Gets the metrics.
     *
     * @return the metrics
     */
    public MetricsList getMetrics()
    {
        return this.metricsList;
    }

    /**
     * Gets the month account count.
     *
     * @return the month account count
     */
    public long getMonthAccountCount(final YearMonth month)
    {
        long result;

        result = getMonthCount("metrics.service.accounts", month);

        //
        return result;
    }

    /**
     * Gets the month active account count.
     *
     * @return the month active account count
     */
    public long getMonthActiveAccountCount(final YearMonth month)
    {
        long result;

        result = getMonthCount("metrics.service.accounts.active", month);

        //
        return result;
    }

    /**
     * Gets the month count.
     *
     * @param path
     *            the path
     * @return the month count
     */
    public long getMonthCount(final String path, final YearMonth month)
    {
        long result;

        MonthValues values = getMetricMonthValues(path);
        values = values.extract(month, month);

        result = (long) values.sum();

        //
        return result;
    }

    /**
     * Gets the month database bytes.
     *
     * @param month
     *            the month
     * @return the month database bytes
     */
    public long getMonthDatabaseBytes(final YearMonth month)
    {
        long result;

        result = getMonthCount("metrics.service.database.bytes", month);

        //
        return result;
    }

    /**
     * Gets the month datafiles bytes.
     *
     * @param month
     *            the month
     * @return the month datafiles bytes
     */
    public long getMonthDatafilesBytes(final YearMonth month)
    {
        long result;

        result = getMonthCount("metrics.service.datafiles.bytes", month);

        //
        return result;
    }

    /**
     * Gets the month hit count.
     *
     * @return the month hit count
     */
    public long getMonthHitCount(final YearMonth month)
    {
        long result;

        result = getMonthCount("metrics.http.hits", month);

        //
        return result;
    }

    /**
     * Gets the month user count.
     *
     * @return the month user count
     */
    public long getMonthUserCount(final YearMonth month)
    {
        long result;

        result = getMonthCount("metrics.service.users", month);

        //
        return result;
    }

    /**
     * Gets the month visit count.
     *
     * @return the month visit count
     */
    public long getMonthVisitCount(final YearMonth month)
    {
        long result;

        result = getMonthCount("metrics.http.visits", month);

        //
        return result;
    }

    /**
     * Gets the month visitor count.
     *
     * @return the month visitor count
     */
    public long getMonthVisitorCount(final YearMonth month)
    {
        long result;

        result = getMonthCount("metrics.http.visitors", month);

        //
        return result;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        String result;

        result = get("service.name");

        if (StringUtils.isBlank(result))
        {
            String seed = get("crawl.url");
            result = DigestUtils.md5Hex(seed).substring(0, 8);
        }

        //
        return result;
    }

    /**
     * Gets the organization.
     *
     * @return the organization
     */
    public Organization getOrganization()
    {
        return this.organization;
    }

    /**
     * Gets the previous month user count.
     *
     * @return the previous month user count
     */
    public long getPreviousMonthUserCount()
    {
        long result;

        result = getMonthUserCount(YearMonth.now().minusMonths(1));

        //
        return result;
    }

    /**
     * Gets the previous month visit count.
     *
     * @return the previous month visit count
     */
    public long getPreviousMonthVisitCount()
    {
        long result;

        result = getMonthVisitCount(YearMonth.now().minusMonths(1));

        //
        return result;
    }

    /**
     * Gets the registration load.
     *
     * @return the registration load
     */
    public String getRegistrationLoad()
    {
        String result;

        result = get("service.registration.load");

        if (result == null)
        {
            result = "open";
        }

        //
        return result;
    }

    /**
     * Gets the service install type.
     *
     * @return the service install type
     */
    public ServiceInstallType getServiceInstallType()
    {
        ServiceInstallType result;

        try
        {
            String value = StringUtils.toRootUpperCase(get("service.install.type"));

            result = ServiceInstallType.valueOf(value);
        }
        catch (IllegalArgumentException | NullPointerException exception)
        {
            result = ServiceInstallType.UNKNOWN;
        }

        //
        return result;
    }

    /**
     * Gets the software description.
     *
     * @return the software description
     */
    public String getSoftwareDescription()
    {
        String result;

        result = get("software.description");

        //
        return result;
    }

    /**
     * Gets the software license name.
     *
     * @return the software license name
     */
    public String getSoftwareLicenseName()
    {
        String result;

        result = get("software.license.name");

        //
        return result;
    }

    /**
     * Gets the software license webpage.
     *
     * @return the software license webpage
     */
    public URL getSoftwareLicenseURL()
    {
        URL result;

        result = getURL("software.license.url");

        //
        return result;
    }

    /**
     * Gets the software name.
     *
     * @return the software name
     */
    public String getSoftwareName()
    {
        String result;

        result = get("software.name");

        //
        return result;
    }

    /**
     * Gets the software source URL.
     *
     * @return the software source URL
     */
    public URL getSoftwareSourceURL()
    {
        URL result;

        result = getURL("software.source.url");

        //
        return result;
    }

    /**
     * Gets the software technical name.
     *
     * @return the software technical name
     */
    public String getSoftwareTechnicalName()
    {
        String result;

        result = StatoolInfosUtils.toTechnicalName(getSoftwareName());

        //
        return result;
    }

    /**
     * Gets the software version.
     *
     * @return the software version
     */
    public String getSoftwareVersion()
    {
        String result;

        result = get("software.version");

        //
        return result;
    }

    /**
     * Gets the software website.
     *
     * @return the software website
     */
    public URL getSoftwareWebsite()
    {
        URL result;

        result = getURL("software.website");

        //
        return result;
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public LocalDate getStartDate()
    {
        LocalDate result;

        result = StatoolInfosUtils.parseDate(getStartDateValue());

        //
        return result;
    }

    /**
     * Gets the start date value.
     *
     * @return the start date value
     */
    public String getStartDateValue()
    {
        String result;

        result = get("service.startdate");

        //
        return result;
    }

    /**
     * Gets the start year.
     *
     * @return the start year
     */
    public Year getStartYear()
    {
        Year result;

        LocalDate date = getStartDate();

        if (date == null)
        {
            result = null;
        }
        else
        {
            result = Year.from(date);
        }

        //
        return result;
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    public Status getStatus()
    {
        Status result;

        String value = get("service.status", "service.status.level");

        if ((StringUtils.isBlank(value)) || (StringUtils.equalsAnyIgnoreCase(value, "unknown", "void")))
        {
            result = Status.VOID;
        }
        else if (StringUtils.equalsAnyIgnoreCase(value, "ON", "OK"))
        {
            result = Status.OK;
        }
        else if (StringUtils.equalsAnyIgnoreCase(value, "alert"))
        {
            result = Status.ALERT;
        }
        else if (StringUtils.equalsAnyIgnoreCase(value, "error", "ko", "broken", "off"))
        {
            result = Status.ERROR;
        }
        else if (StringUtils.equalsAnyIgnoreCase(value, "over", "terminated", "closed", "ended"))
        {
            result = Status.OVER;
        }
        else
        {
            result = Status.VOID;
        }

        //
        return result;
    }

    /**
     * Gets the status description.
     *
     * @return the status description
     */
    public String getStatusDescription()
    {
        String result;

        result = get("service.status.description");

        //
        return result;
    }

    /**
     * @return
     */
    public URL getTechnicalGuideURL()
    {
        URL result;

        String value = get("service.guide.technical", "service.guide.technical.url");

        result = URLUtils.of(value);

        //
        return result;
    }

    /**
     * Gets the technical name.
     *
     * @return the technical name
     */
    public String getTechnicalName()
    {
        String result;

        result = StatoolInfosUtils.toTechnicalName(getName());

        //
        return result;
    }

    /**
     * Gets the URL all.
     *
     * @return the URL all
     */
    public URLSet getURLAll()
    {
        URLSet result;

        result = new URLSet();

        result.add(getContactURL());
        result.add(getLegalURL());
        result.add(getLogoURL());
        result.add(getSoftwareWebsite());
        result.add(getSoftwareLicenseURL());
        result.add(getSoftwareSourceURL());
        result.add(getTechnicalGuideURL());
        result.add(getUserGuideURL());
        result.add(getWebsiteURL());

        //
        return result;
    }

    /**
     * User count.
     *
     * @return the int
     */
    public int getUserCount()
    {
        int result;

        result = 0;

        //
        return result;
    }

    /**
     * Gets the user doc.
     *
     * @return the user doc
     */
    public URL getUserGuideURL()
    {
        URL result;

        String value = get("service.documentation",
                "service.documentation.url",
                "service.documentation.user",
                "service.documentation.user.url",
                "service.documentation.tutorial",
                "service.documentation.tutorial.url",
                "service.guide.user",
                "service.guide.user.url");

        result = URLUtils.of(value);

        //
        return result;
    }

    /**
     * Gets the website.
     *
     * @return the website
     */
    public URL getWebsiteURL()
    {
        URL result;

        result = getURL("service.website");

        //
        return result;
    }

    /**
     * Checks if is active.
     *
     * @return true, if is active
     */
    public boolean isActive()
    {
        boolean result;

        if (((getStatus() == Service.Status.OK) || (getStatus() == Status.WARNING) || (getStatus() == Status.ERROR)) && (!isAway()))
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Checks if is away.
     *
     * @return true, if is away
     */
    public boolean isAway()
    {
        boolean result;

        if (getEndDateValue() == null)
        {
            result = false;
        }
        else
        {
            LocalDate endDate = getDate("service.enddate");

            if ((endDate == null) || (endDate.isAfter(LocalDate.now())))
            {
                result = false;
            }
            else
            {
                result = true;
            }
        }

        //
        return result;
    }

    /**
     * Checks if is registration client.
     *
     * @return true, if is registration client
     */
    public boolean isRegistrationClient()
    {
        boolean result;

        result = StringUtils.containsIgnoreCase(get("service.registration"), "Client");

        //
        return result;
    }

    /**
     * Checks if is registration free.
     *
     * @return true, if is registration free
     */
    public boolean isRegistrationFree()
    {
        boolean result;

        result = StringUtils.containsIgnoreCase(get("service.registration"), "Free");

        //
        return result;
    }

    /**
     * Checks if is registration load full.
     *
     * @return true, if is registration load full
     */
    public boolean isRegistrationLoadFull()
    {
        boolean result;

        result = StringUtils.containsIgnoreCase(get("service.registration.load"), "full");

        //
        return result;
    }

    /**
     * Checks if is registration load open.
     *
     * @return true, if is registration load open
     */
    public boolean isRegistrationLoadOpen()
    {
        boolean result;

        result = StringUtils.containsIgnoreCase(get("service.registration.load"), "open");

        //
        return result;
    }

    /**
     * Checks if is registration member.
     *
     * @return true, if is registration member
     */
    public boolean isRegistrationMember()
    {
        boolean result;

        result = StringUtils.containsIgnoreCase(get("service.registration"), "Member");

        //
        return result;
    }

    /**
     * Checks if is registration none.
     *
     * @return true, if is registration none
     */
    public boolean isRegistrationNone()
    {
        boolean result;

        result = StringUtils.containsIgnoreCase(get("service.registration"), "None");

        //
        return result;
    }

    /**
     * Checks if is registration unknown.
     *
     * @return true, if is registration unknown
     */
    public boolean isRegistrationUnknown()
    {
        boolean result;

        if (isRegistrationNone() || isRegistrationFree() || isRegistrationMember() || isRegistrationClient())
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /**
     * Sets the input file.
     *
     * @param inputFile
     *            the new input file
     */
    public void setInputFile(final File inputFile)
    {
        this.inputFile = inputFile;
    }

    /**
     * Sets the input URL.
     *
     * @param inputURL
     *            the new input URL
     */
    public void setInputURL(final URL inputURL)
    {
        this.inputURL = inputURL;
    }

    /**
     * Sets the logo file name.
     *
     * @param logoFileName
     *            the new logo file name
     */
    public void setLogoFileName(final String logoFileName)
    {
        this.logoFileName = logoFileName;
    }

    /**
     * Sets the organization.
     *
     * @param organization
     *            the new organization
     */
    public void setOrganization(final Organization organization)
    {
        this.organization = organization;
    }
}
