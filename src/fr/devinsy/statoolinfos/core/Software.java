/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import fr.devinsy.strings.StringList;

/**
 * The Class Software.
 */
public class Software
{
    private String name;
    private String description;
    private StringList aliases;

    /**
     * Instantiates a new software.
     *
     * @param name
     *            the name
     */
    public Software(final String name)
    {
        this(name, "");
    }

    /**
     * Instantiates a new software.
     *
     * @param name
     *            the name
     * @param description
     *            the description
     */
    public Software(final String name, final String description)
    {
        this.name = name;
        this.description = description;
        this.aliases = new StringList(name);
    }

    /**
     * Instantiates a new software.
     *
     * @param name
     *            the name
     * @param description
     *            the description
     * @param softwares
     *            the softwares
     */
    public Software(final String name, final String description, final StringList softwares)
    {
        this.name = name;
        this.description = description;
        this.aliases = new StringList(softwares);
    }

    /**
     * Gets the aliases.
     *
     * @return the aliases
     */
    public StringList getAliases()
    {
        return this.aliases;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        return this.description;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * Gets the technical name.
     *
     * @return the technical name
     */
    public String getTechnicalName()
    {
        String result;

        result = StatoolInfosUtils.toTechnicalName(getName());

        //
        return result;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the new description
     */
    public void setDescription(final String description)
    {
        this.description = description;
    }

    /**
     * Sets the name.
     *
     * @param name
     *            the new name
     */
    public void setName(final String name)
    {
        this.name = name;
    }

    /*
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        String result;

        result = String.format("[name=%s][description=%s][aliases=%s]", this.name, this.description, this.aliases.toStringSeparatedBy(','));

        //
        return result;
    }
}