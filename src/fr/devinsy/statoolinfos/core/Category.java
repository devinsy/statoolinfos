/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.strings.StringList;

/**
 * The Class Category.
 */
public class Category
{
    public static final String DEFAULT_LOGO_PATH = "default.svg";

    private String name;
    private String technicalName;
    private String description;
    private StringList softwares;
    private String logoPath;

    /**
     * Instantiates a new category.
     *
     * @param name
     *            the name
     * @param description
     *            the description
     */
    public Category(final String name, final String description)
    {
        this(name, description, null);
    }

    /**
     * Instantiates a new category.
     *
     * @param name
     *            the name
     * @param description
     *            the description
     * @param softwares
     *            the softwares
     */
    public Category(final String name, final String description, final StringList softwares)
    {
        this.name = name;
        this.technicalName = StatoolInfosUtils.toTechnicalName(getName());
        this.description = description;
        this.softwares = new StringList(softwares);
        this.logoPath = DEFAULT_LOGO_PATH;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        return this.description;
    }

    /**
     * Gets the logo path.
     *
     * @return the logo path
     */
    public String getLogoPath()
    {
        String result;

        if (StringUtils.isBlank(this.logoPath))
        {
            result = DEFAULT_LOGO_PATH;
        }
        else
        {
            result = this.logoPath;
        }

        //
        return result;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * Gets the softwares.
     *
     * @return the softwares
     */
    public StringList getSoftwares()
    {
        return this.softwares;
    }

    /**
     * Gets the technical name.
     *
     * @return the technical name
     */
    public String getTechnicalName()
    {
        String result;

        result = this.technicalName;

        //
        return result;
    }

    /**
     * Matches.
     *
     * @param service
     *            the service
     * @return true, if successful
     */
    public boolean matches(final Service service)
    {
        boolean result;

        result = matchesSoftware(service.getSoftwareName());

        //
        return result;
    }

    /**
     * Matches.
     *
     * @param softwareName
     *            the software name
     * @return true, if successful
     */
    public boolean matchesSoftware(final String softwareName)
    {
        boolean result;

        String target = StringUtils.stripAccents(softwareName).replaceAll("[\\W\\s]", "");

        boolean ended = false;
        Iterator<String> iterator = this.softwares.iterator();
        result = false;
        while (!ended)
        {
            if (iterator.hasNext())
            {
                String source = iterator.next();
                source = StringUtils.stripAccents(source).replaceAll("[\\W\\s]", "");

                if (StringUtils.equalsIgnoreCase(target, source))
                {
                    ended = true;
                    result = true;
                }
            }
            else
            {
                ended = true;
                result = false;
            }
        }

        //
        return result;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the new description
     */
    public void setDescription(final String description)
    {
        this.description = description;
    }

    /**
     * Sets the logo path.
     *
     * @param logoPath
     *            the new logo path
     */
    public void setLogoPath(final String logoPath)
    {
        if (StringUtils.isBlank(logoPath))
        {
            this.logoPath = DEFAULT_LOGO_PATH;
        }
        else
        {
            this.logoPath = logoPath;
        }
    }

    /**
     * Sets the name.
     *
     * @param name
     *            the new name
     */
    public void setName(final String name)
    {
        this.name = name;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString()
    {
        String result;

        result = String.format("[name=%s][description=%s][softwares=%s]", this.name, this.description, this.softwares.toStringSeparatedBy(','));

        //
        return result;
    }
}