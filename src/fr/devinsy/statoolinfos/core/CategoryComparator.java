/*
 * 
 */
package fr.devinsy.statoolinfos.core;

import java.util.Comparator;

import fr.devinsy.statoolinfos.util.CompareUtils;

/**
 * The Class OrganizationComparator.
 */
public class CategoryComparator implements Comparator<Category>
{
    public enum Sorting
    {
        NAME
    }

    private Sorting sorting;

    /**
     * Instantiates a new organization comparator.
     *
     * @param sorting
     *            the sorting
     */
    public CategoryComparator(final Sorting sorting)
    {
        this.sorting = sorting;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    @Override
    public int compare(final Category alpha, final Category bravo)
    {
        int result;

        result = compare(alpha, bravo, this.sorting);

        //
        return result;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @param sorting
     *            the sorting
     * @return the int
     */
    public static int compare(final Category alpha, final Category bravo, final Sorting sorting)
    {
        int result;

        if (sorting == null)
        {
            result = 0;
        }
        else
        {
            switch (sorting)
            {
                default:
                case NAME:
                    result = CompareUtils.compare(getName(alpha), getName(bravo));
                break;
            }
        }

        //
        return result;
    }

    /**
     * Gets the name.
     *
     * @param source
     *            the source
     * @return the name
     */
    public static String getName(final Category source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.getName();
        }

        //
        return result;
    }
}
