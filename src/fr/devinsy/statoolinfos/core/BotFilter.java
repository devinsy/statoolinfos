/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import fr.devinsy.statoolinfos.metrics.httpaccess.HttpAccessLog;

/**
 * The Enum LogFilter.
 */
public enum BotFilter
{
    ALL,
    BOT,
    NOBOT;

    /**
     * Matches.
     *
     * @param log
     *            the log
     * @return true, if successful
     */
    public boolean matches(final HttpAccessLog log)
    {
        boolean result;

        if (log == null)
        {
            result = false;
        }
        else if (this == ALL)
        {
            result = true;
        }
        else if ((this == BOT) && (log.isBot()))
        {
            result = true;
        }
        else if ((this == NOBOT) && (!log.isBot()))
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }
}