/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.statoolinfos.checker.PropertyChecks;
import fr.devinsy.statoolinfos.crawl.CrawlJournal;
import fr.devinsy.statoolinfos.properties.MonthValues;
import fr.devinsy.statoolinfos.properties.PathProperties;
import fr.devinsy.statoolinfos.properties.PathPropertyList;
import fr.devinsy.statoolinfos.properties.WeekValues;
import fr.devinsy.statoolinfos.properties.YearValues;
import fr.devinsy.statoolinfos.uptime.URLSet;
import fr.devinsy.statoolinfos.util.URLUtils;

/**
 * The Class PathProperty.
 */
public class Federation extends PathPropertyList
{
    private static final long serialVersionUID = -8970835291634661580L;
    private Organizations organizations;
    private URL inputURL;
    private File inputFile;
    private String logoFileName;
    private PropertyChecks inputChecks;
    private CrawlJournal crawlJournal;

    /**
     * Instantiates a new federation.
     */
    public Federation()
    {
        super();
        this.inputChecks = new PropertyChecks();
        this.organizations = new Organizations();
        this.crawlJournal = new CrawlJournal();
    }

    /**
     * Instantiates a new federation.
     *
     * @param properties
     *            the properties
     */
    public Federation(final PathProperties properties)
    {
        super(properties);
        this.inputChecks = new PropertyChecks();

        if ((properties == null) || (StringUtils.isBlank(properties.get("federation.name"))))
        {
            throw new IllegalArgumentException("Not a federation.");
        }
        else
        {
            this.organizations = new Organizations();
            this.crawlJournal = new CrawlJournal();
        }
    }

    /**
     * Gets the organizations active.
     *
     * @return the organizations active
     */
    public Organizations getActiveOrganizations()
    {
        Organizations result;

        result = this.organizations.filterActiveFor(getTechnicalName());

        //
        return result;
    }

    /**
     * Gets the active services.
     *
     * @return the active services
     */
    public long getActiveServiceCount()
    {
        long result;

        result = 0;
        for (Organization organization : getActiveOrganizations())
        {
            result += organization.getActiveServiceCount();
        }

        //
        return result;
    }

    /**
     * Gets the active services.
     *
     * @return the active services
     */
    public Services getActiveServices()
    {
        Services result;

        result = new Services();

        for (Organization organization : getActiveOrganizations())
        {
            result.addAll(organization.getActiveServices());
        }

        //
        return result;
    }

    /**
     * Gets the away organizations.
     *
     * @return the away organizations
     */
    public Organizations getAwayOrganizations()
    {
        Organizations result;

        result = this.organizations.filterAwayFor(getTechnicalName());

        //
        return result;
    }

    /**
     * Gets the contact email.
     *
     * @return the contact email
     */
    public String getContactEmail()
    {
        String result;

        result = get("federation.contact.email");

        //
        return result;
    }

    /**
     * Gets the contact URL.
     *
     * @return the contact URL
     */
    public URL getContactURL()
    {
        URL result;

        result = getURL("federation.contact.url");

        //
        return result;
    }

    /**
     * Gets the crawl date.
     *
     * @return the crawl date
     */
    public LocalDateTime getCrawlDate()
    {
        LocalDateTime result;

        result = LocalDateTime.parse(get("crawl.datetime"));

        //
        return result;
    }

    /**
     * Gets the crawled date.
     *
     * @return the crawled date
     */
    public LocalDateTime getCrawledDate()
    {
        LocalDateTime result;

        result = LocalDateTime.parse(get("crawl.file.datetime"));

        if (result.getYear() == 1970)
        {
            result = getCrawlDate();
        }

        //
        return result;
    }

    /**
     * Gets the crawl journal.
     *
     * @return the crawl journal
     */
    public CrawlJournal getCrawlJournal()
    {
        return this.crawlJournal;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        String result;

        result = get("federation.description");

        //
        return result;
    }

    /**
     * Gets the idle organizations.
     *
     * @return the idle organizations
     */
    public Organizations getIdleOrganizations()
    {
        Organizations result;

        result = this.organizations.getIdles();

        //
        return result;
    }

    /**
     * Gets the input checks.
     *
     * @return the input checks
     */
    public PropertyChecks getInputChecks()
    {
        return this.inputChecks;
    }

    /**
     * Gets the input checks all.
     *
     * @return the input checks all
     */
    public PropertyChecks getInputChecksAll()
    {
        PropertyChecks result;

        result = new PropertyChecks();

        result.addAll(this.getInputChecks());

        for (Organization organization : this.organizations)
        {
            result.addAll(organization.getInputChecksAll());
        }

        //
        return result;
    }

    /**
     * Gets the input file.
     *
     * @return the input file
     */
    public File getInputFile()
    {
        return this.inputFile;
    }

    /**
     * Gets the input URL.
     *
     * @return the input URL
     */
    public URL getInputURL()
    {
        return this.inputURL;
    }

    /**
     * Gets the legal website.
     *
     * @return the legal website
     */
    public URL getLegalURL()
    {
        URL result;

        String value = get("federation.legal.url", "federation.legal");

        result = URLUtils.of(value);

        //
        return result;
    }

    /**
     * Gets the local file base name.
     *
     * @return the local file base name
     */
    public String getLocalFileBaseName()
    {
        String result;

        result = getTechnicalName();

        //
        return result;
    }

    /**
     * Gets the local file name.
     *
     * @return the local file name
     */
    public String getLocalFileName()
    {
        String result;

        result = getTechnicalName() + ".properties";

        //
        return result;
    }

    /**
     * Gets the logo file name.
     *
     * @return the logo file name
     */
    public String getLogoFileName()
    {
        return this.logoFileName;
    }

    /**
     * Gets the logo URL.
     *
     * @return the logo URL
     */
    public URL getLogoURL()
    {
        URL result;

        try
        {
            String path = get("federation.logo");
            if ((StringUtils.isBlank(path)) || (!StringUtils.startsWith(path, "http")))
            {
                result = null;
            }
            else
            {
                result = new URL(path);
            }
        }
        catch (MalformedURLException exception)
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Gets the member organizations.
     *
     * @return the member organizations
     */
    public Organizations getMemberOrganizations()
    {
        Organizations result;

        result = this.organizations.filterMemberFor(getTechnicalName());

        //
        return result;
    }

    /**
     * Gets the member organizations.
     *
     * @param year
     *            the year
     * @return the member organizations
     */
    public Organizations getMemberOrganizations(final Year year)
    {
        Organizations result;

        result = this.organizations.filterMemberFor(year);

        //
        return result;
    }

    /**
     * Gets the metric month values all.
     *
     * @param path
     *            the path
     * @return the metric month values all
     */
    public MonthValues getMetricMonthValuesAll(final String path)
    {
        MonthValues result;

        result = getMetricMonthValues(path);

        for (Organization organization : getOrganizations())
        {
            result.addAll(organization.getMetricMonthValuesAll(path));
        }

        //
        return result;
    }

    /**
     * Gets the metric week values all.
     *
     * @param path
     *            the path
     * @return the metric week values all
     */
    public WeekValues getMetricWeekValuesAll(final String path)
    {
        WeekValues result;

        result = getMetricWeekValues(path);

        for (Organization organization : getOrganizations())
        {
            result.addAll(organization.getMetricWeekValuesAll(path));
        }

        //
        return result;
    }

    /**
     * Gets the metric year values all.
     *
     * @param path
     *            the path
     * @return the metric year values all
     */
    public YearValues getMetricYearValuesAll(final String path)
    {
        YearValues result;

        result = getMetricYearValues(path);

        for (Organization organization : getOrganizations())
        {
            result.addAll(organization.getMetricYearValuesAll(path));
        }

        //
        return result;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        String result;

        result = get("federation.name");

        //
        return result;
    }

    /**
     * Gets the organiation count.
     *
     * @return the organiation count
     */
    public int getOrganiationCount()
    {
        int result;

        result = this.organizations.size();

        //
        return result;
    }

    /**
     * Gets the organizations.
     *
     * @return the organizations
     */
    public Organizations getOrganizations()
    {
        return this.organizations;
    }

    /**
     * Gets the service count.
     *
     * @return the service count
     */
    public int getServiceCount()
    {
        int result;

        result = 0;
        for (Organization organization : getActiveOrganizations())
        {
            result += organization.getServiceCount();
        }

        //
        return result;
    }

    /**
     * Gets the service count by.
     *
     * @param year
     *            the year
     * @return the service count by
     */
    public long getServiceCountBy(final Year year)
    {
        long result;

        Organizations organizations = this.organizations.filterMemberFor(year);

        result = 0;
        for (Organization organization : organizations)
        {
            for (Service service : organization.getServices().getBy(year))
            {
                Year memberStart = organization.getMemberStartYear();
                Year memberEnd = organization.getMemberEndYear();
                Year serviceStart = service.getStartYear();
                Year serviceEnd = service.getEndYear();

                if (StatoolInfosUtils.overlapp(memberStart, memberEnd, serviceStart, serviceEnd))
                {
                    result += 1;
                }
            }
        }

        //
        return result;
    }

    /**
     * Gets the all services.
     *
     * @return the all services
     */
    public Services getServices()
    {
        Services result;

        result = new Services();

        for (Organization organization : getActiveOrganizations())
        {
            result.addAll(organization.getServices());
        }

        //
        return result;
    }

    /**
     * Gets the all services.
     *
     * @return the all services
     */
    public Services getServicesAll()
    {
        Services result;

        result = new Services();

        for (Organization organization : this.organizations)
        {
            result.addAll(organization.getServices());
        }

        //
        return result;
    }

    /**
     * Gets the software catalog.
     *
     * @return the software catalog
     */
    public Softwares getSoftwares()
    {
        Softwares result;

        result = new Softwares();

        for (Service service : getServices())
        {
            if (StringUtils.isNotBlank(service.getSoftwareName()))
            {
                Software software = result.get(service.getSoftwareName());

                if (software == null)
                {
                    software = new Software(service.getSoftwareName(), service.getSoftwareDescription());
                    result.put(software);
                }
            }
        }

        //
        return result;
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public LocalDate getStartDate()
    {
        LocalDate result;

        result = StatoolInfosUtils.parseDate(getStartDateValue());

        //
        return result;
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public String getStartDateValue()
    {
        String result;

        result = get("federation.startdate");

        //
        return result;
    }

    /**
     * Gets the start year.
     *
     * @return the start year
     */
    public Year getStartYear()
    {
        Year result;

        LocalDate date = getStartDate();
        if (date == null)
        {
            result = null;
        }
        else
        {
            result = Year.from(date);
        }

        //
        return result;
    }

    /**
     * Gets the technical doc website.
     *
     * @return the technical doc website
     */
    public URL getTechnicalGuideURL()
    {
        URL result;

        String value = get("federation.guide.technical", "federation.guide.technical.url");

        result = URLUtils.of(value);

        //
        return result;
    }

    /**
     * Gets the technical name.
     *
     * @return the technical name
     */
    public String getTechnicalName()
    {
        String result;

        result = StatoolInfosUtils.toTechnicalName(getName());

        //
        return result;
    }

    /**
     * Gets the URL active all.
     *
     * @return the URL active all
     */
    public URLSet getURLActiveAll()
    {
        URLSet result;

        result = new URLSet();

        //
        result.add(getContactURL());
        result.add(getLegalURL());
        result.add(getLogoURL());
        result.add(getTechnicalGuideURL());
        result.add(getUserGuideURL());
        result.add(getWebsiteURL());

        //
        for (Organization organization : getOrganizations())
        {
            result.addAll(organization.getURLActiveAll());
        }

        //
        return result;
    }

    /**
     * Gets the URL all.
     *
     * @return the URL all
     */
    public URLSet getURLAll()
    {
        URLSet result;

        result = new URLSet();

        //
        result.add(getContactURL());
        result.add(getLegalURL());
        result.add(getLogoURL());
        result.add(getTechnicalGuideURL());
        result.add(getUserGuideURL());
        result.add(getWebsiteURL());

        //
        for (Organization organization : getOrganizations())
        {
            result.addAll(organization.getURLAll());
        }

        //
        return result;
    }

    /**
     * Gets the user doc website.
     *
     * @return the user doc website
     */
    public URL getUserGuideURL()
    {
        URL result;

        String value = get("federation.documentation",
                "federation.documentation.url",
                "federation.documentation.user",
                "federation.documentation.user.url",
                "service.documentation.tutorial",
                "federation.documentation.tutorial.url",
                "federation.guide.user",
                "federation.guide.user.url");

        result = URLUtils.of(value);

        //
        return result;
    }

    /**
     * Gets the website.
     *
     * @return the website
     */
    public URL getWebsiteURL()
    {
        URL result;

        result = getURL("federation.website");

        //
        return result;
    }

    /**
     * Sets the input file.
     *
     * @param inputFile
     *            the new input file
     */
    public void setInputFile(final File inputFile)
    {
        this.inputFile = inputFile;
    }

    /**
     * Sets the input URL.
     *
     * @param inputURL
     *            the new input URL
     */
    public void setInputURL(final URL inputURL)
    {
        this.inputURL = inputURL;
    }

    /**
     * Sets the logo file name.
     *
     * @param logoFileName
     *            the new logo file name
     */
    public void setLogoFileName(final String logoFileName)
    {
        this.logoFileName = logoFileName;
    }
}
