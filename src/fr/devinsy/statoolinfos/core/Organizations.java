/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import java.time.Year;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class Organizations.
 */
public class Organizations extends ArrayList<Organization>
{
    private static final long serialVersionUID = 2092235357983955170L;

    /**
     * Instantiates a new organizations.
     */
    public Organizations()
    {
        super();
    }

    /**
     * Instantiates a new organizations.
     *
     * @param organization
     *            the organization
     */
    public Organizations(final Organization organization)
    {
        super();

        if (organization != null)
        {
            add(organization);
        }
    }

    /**
     * Filter active.
     *
     * @return the organizations
     */
    public Organizations filterActive()
    {
        Organizations result;

        result = new Organizations();

        for (Organization organization : this)
        {
            if (organization.isActive())
            {
                result.add(organization);
            }
        }

        //
        return result;
    }

    /**
     * Filter member of.
     *
     * @param entityName
     *            the entity name
     * @return the organizations
     */
    public Organizations filterActiveFor(final String entityName)
    {
        Organizations result;

        result = new Organizations();

        for (Organization organization : this)
        {
            if ((organization.isActive()) && (!organization.isAwayFor(entityName)))
            {
                result.add(organization);
            }
        }

        //
        return result;
    }

    /**
     * Filter away for.
     *
     * @param entityName
     *            the entity name
     * @return the organizations
     */
    public Organizations filterAwayFor(final String entityName)
    {
        Organizations result;

        result = new Organizations();

        for (Organization organization : this)
        {
            if ((organization.isAway()) || (organization.isAwayFor(entityName)))
            {
                result.add(organization);
            }
        }

        //
        return result;
    }

    /**
     * Filter by social network.
     *
     * @param value
     *            the value
     * @return the organizations
     */
    public Organizations filterBySocialNetwork(final SocialNetworks value)
    {
        Organizations result;

        result = new Organizations();

        for (Organization organization : this)
        {
            if (organization.hasSocialNetwork(value))
            {
                result.add(organization);
            }
        }

        //
        return result;
    }

    /**
     * Filter by social network.
     *
     * @return the organizations
     */
    public Organizations filterBySocialNetworks()
    {
        Organizations result;

        result = new Organizations();

        for (Organization organization : this)
        {
            if (organization.hasSocialNetwork())
            {
                result.add(organization);
            }
        }

        //
        return result;
    }

    /**
     * Filter member for.
     *
     * @param entityName
     *            the entity name
     * @return the organizations
     */
    public Organizations filterMemberFor(final String entityName)
    {
        Organizations result;

        result = new Organizations();

        for (Organization organization : this)
        {
            if ((!organization.isAway()) && (!organization.isAwayFor(entityName)))
            {
                result.add(organization);
            }
        }

        //
        return result;
    }

    /**
     * Filter member for.
     *
     * @param year
     *            the year
     * @return the organizations
     */
    public Organizations filterMemberFor(final Year year)
    {
        Organizations result;

        result = new Organizations();

        for (Organization organization : this)
        {
            if (organization.isMember(year))
            {
                result.add(organization);
            }
        }

        //
        return result;
    }

    /**
     * Gets the active service count.
     *
     * @return the active service count
     */
    public int getActiveServiceCount()
    {
        int result;

        result = 0;
        for (Organization organization : this)
        {
            result += organization.getActiveServiceCount();
        }

        //
        return result;
    }

    /**
     * Gets the by technical name.
     *
     * @param technicalName
     *            the technical name
     * @return the by technical name
     */
    public Organization getByTechnicalName(final String technicalName)
    {
        Organization result;

        Iterator<Organization> iterator = this.iterator();
        boolean ended = false;
        result = null;
        while (!ended)
        {
            if (iterator.hasNext())
            {
                Organization organization = iterator.next();

                if (StringUtils.equals(organization.getTechnicalName(), technicalName))
                {
                    ended = true;
                    result = organization;
                }
            }
            else
            {
                ended = true;
                result = null;
            }
        }

        //
        return result;
    }

    /**
     * Gets the idles.
     *
     * @return the idles
     */
    public Organizations getIdles()
    {
        Organizations result;

        result = new Organizations();

        for (Organization organization : this)
        {
            if (organization.isIdle())
            {
                result.add(organization);
            }
        }

        //
        return result;
    }

    /**
     * Gets the service count.
     *
     * @return the service count
     */
    public int getServiceCount()
    {
        int result;

        result = 0;
        for (Organization organization : this)
        {
            result += organization.getServiceCount();
        }

        //
        return result;
    }

    /**
     * Reverse.
     *
     * @return the services
     */
    public Organizations reverse()
    {
        Organizations result;

        Collections.reverse(this);

        result = this;

        //
        return result;
    }

    /**
     * Sort.
     *
     * @param sorting
     *            the sorting
     * @return the issues
     */
    public Organizations sort(final OrganizationComparator.Sorting sorting)
    {
        Organizations result;

        sort(new OrganizationComparator(sorting));

        result = this;

        //
        return result;
    }

    /**
     * Sort by name.
     *
     * @return the services
     */
    public Organizations sortByName()
    {
        Organizations result;

        result = sort(OrganizationComparator.Sorting.NAME);

        //
        return result;
    }

    /**
     * Sort by reverse service count.
     *
     * @return the organizations
     */
    public Organizations sortByReverseServiceCount()
    {
        Organizations result;

        result = sort(OrganizationComparator.Sorting.REVERSE_SERVICE_COUNT);

        //
        return result;
    }

    /**
     * Sort by service count.
     *
     * @return the organizations
     */
    public Organizations sortByServiceCount()
    {
        Organizations result;

        result = sort(OrganizationComparator.Sorting.SERVICE_COUNT);

        //
        return result;
    }
}
