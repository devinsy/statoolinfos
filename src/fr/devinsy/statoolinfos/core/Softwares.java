/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import java.util.HashMap;

/**
 * The Class Softwares.
 */
public class Softwares extends HashMap<String, Software>
{
    private static final long serialVersionUID = 4780901718767657693L;

    /**
     * Instantiates a new softwares.
     */
    public Softwares()
    {
        super();
    }

    /**
     * Builds the key.
     *
     * @param source
     *            the source
     * @return the string
     */
    private String buildKey(final String source)
    {
        String result;

        result = StatoolInfosUtils.toTechnicalName(source);

        //
        return result;
    }

    /**
     * Gets the.
     *
     * @param key
     *            the key
     * @return the software
     */
    public Software get(final String key)
    {
        Software result;

        result = super.get(buildKey(key));

        //
        return result;
    }

    /**
     * Put.
     *
     * @param software
     *            the software
     */
    public void put(final Software software)
    {
        put(buildKey(software.getName()), software);

        for (String alias : software.getAliases())
        {
            put(buildKey(alias), software);
        }
    }
}
