/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import java.io.File;
import java.net.URL;
import java.time.LocalDateTime;

import org.apache.commons.io.FilenameUtils;

import fr.devinsy.statoolinfos.checker.PropertyChecks;
import fr.devinsy.statoolinfos.properties.PathProperties;
import fr.devinsy.statoolinfos.properties.PathPropertyList;

/**
 * The Class Metrics.
 */
public class Metrics extends PathPropertyList
{
    private static final long serialVersionUID = -1608084706095266037L;

    private String localFileNamePrefix;
    private File inputFile;
    private URL inputURL;
    private PropertyChecks inputChecks;

    /**
     * Instantiates a new service.
     */
    public Metrics()
    {
        this(null);
    }

    /**
     * Instantiates a new service.
     *
     * @param properties
     *            the properties
     */
    public Metrics(final PathProperties properties)
    {
        super(properties);
        this.inputChecks = new PropertyChecks();
    }

    /**
     * Gets the crawl date.
     *
     * @return the crawl date
     */
    public LocalDateTime getCrawlDate()
    {
        LocalDateTime result;

        result = LocalDateTime.parse(get("crawl.datetime"));

        //
        return result;
    }

    /**
     * Gets the crawled date.
     *
     * @return the crawled date
     */
    public LocalDateTime getCrawledDate()
    {
        LocalDateTime result;

        result = LocalDateTime.parse(get("crawl.file.datetime"));

        if (result.getYear() == 1970)
        {
            result = getCrawlDate();
        }

        //
        return result;
    }

    /**
     * Gets the input checks.
     *
     * @return the input checks
     */
    public PropertyChecks getInputChecks()
    {
        return this.inputChecks;
    }

    /**
     * Gets the input file.
     *
     * @return the input file
     */
    public File getInputFile()
    {
        return this.inputFile;
    }

    /**
     * Gets the input URL.
     *
     * @return the input URL
     */
    public URL getInputURL()
    {
        return this.inputURL;
    }

    /**
     * Gets the local file base name.
     *
     * @return the local file base name
     */
    public String getLocalFileBaseName()
    {
        String result;

        result = this.localFileNamePrefix + "-" + getTechnicalName();

        //
        return result;
    }

    /**
     * Gets the local name.
     *
     * @return the local name
     */
    public String getLocalFileName()
    {
        String result;

        result = getLocalFileBaseName() + ".properties";

        //
        return result;
    }

    /**
     * Gets the local file name prefix.
     *
     * @return the local file name prefix
     */
    public String getLocalFileNamePrefix()
    {
        return this.localFileNamePrefix;
    }

    /**
     * Gets the technical name.
     *
     * @return the technical name
     */
    public String getTechnicalName()
    {
        String result;

        String name = FilenameUtils.getBaseName(this.inputURL.getPath());
        result = StatoolInfosUtils.toTechnicalName(name);

        //
        return result;
    }

    /**
     * Sets the input file.
     *
     * @param inputFile
     *            the new input file
     */
    public void setInputFile(final File inputFile)
    {
        this.inputFile = inputFile;
    }

    /**
     * Sets the input URL.
     *
     * @param inputURL
     *            the new input URL
     */
    public void setInputURL(final URL inputURL)
    {
        this.inputURL = inputURL;
    }

    /**
     * Sets the local file name prefix.
     *
     * @param localFileNamePrefix
     *            the new local file name prefix
     */
    public void setLocalFileNamePrefix(final String localFileNamePrefix)
    {
        this.localFileNamePrefix = localFileNamePrefix;
    }
}
