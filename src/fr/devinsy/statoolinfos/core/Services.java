/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.statoolinfos.core.Service.Status;

/**
 * The Class Services.
 */
public class Services extends ArrayList<Service>
{
    private static final long serialVersionUID = -6288956439933064467L;

    /**
     * Instantiates a new services.
     */
    public Services()
    {
        super();
    }

    /**
     * Instantiates a new services.
     *
     * @param service
     *            the service
     */
    public Services(final Service service)
    {
        super();

        if (service != null)
        {
            add(service);
        }
    }

    /**
     * Count by year.
     *
     * @param year
     *            the year
     * @return the long
     */
    public long countBy(final Year year)
    {
        long result;

        result = 0;
        Year now = Year.now();
        for (Service service : this)
        {
            Year start = service.getStartYear();
            Year end = service.getEndYear();

            if (start != null)
            {
                if (end == null)
                {
                    end = now;
                }

                if ((!start.isAfter(year) && (!end.isBefore(year))))
                {
                    result += 1;
                }
            }
        }

        //
        return result;
    }

    /**
     * Gets the by.
     *
     * @param category
     *            the category
     * @return the by
     */
    public Services getBy(final Category category)
    {
        Services result;

        result = new Services();

        for (Service service : this)
        {
            if (category.matches(service))
            {
                result.add(service);
            }
        }

        //
        return result;
    }

    /**
     * Gets the by.
     *
     * @param software
     *            the software
     * @return the by
     */
    public Services getBy(final Software software)
    {
        Services result;

        result = new Services();

        for (Service service : this)
        {
            String serviceSoftwareName = StatoolInfosUtils.toTechnicalName(service.getSoftwareName());
            String softwareName = StatoolInfosUtils.toTechnicalName(software.getName());
            if (StringUtils.equals(serviceSoftwareName, softwareName))
            {
                result.add(service);
            }
        }

        //
        return result;
    }

    /**
     * Gets the by.
     *
     * @param status
     *            the category
     * @return the by
     */
    public Services getBy(final Status status)
    {
        Services result;

        result = new Services();

        for (Service service : this)
        {
            if (service.getStatus() == status)
            {
                result.add(service);
            }
        }

        //
        return result;
    }

    /**
     * Gets the by.
     *
     * @param year
     *            the year
     * @return the by
     */
    public Services getBy(final Year year)
    {
        Services result;

        result = new Services();

        if (year != null)
        {
            for (Service service : this)
            {
                Year startYear = service.getStartYear();
                Year endYear = service.getEndYear();
                if (endYear == null)
                {
                    endYear = Year.now();
                }

                if ((startYear != null) && (!year.isBefore(startYear)) && (!year.isAfter(endYear)))
                {
                    result.add(service);
                }
            }
        }

        //
        return result;
    }

    /**
     * Gets the by technical name.
     *
     * @param technicalName
     *            the technical name
     * @return the by technical name
     */
    public Service getByTechnicalName(final String technicalName)
    {
        Service result;

        Iterator<Service> iterator = this.iterator();
        boolean ended = false;
        result = null;
        while (!ended)
        {
            if (iterator.hasNext())
            {
                Service service = iterator.next();

                if (StringUtils.equals(service.getTechnicalName(), technicalName))
                {
                    ended = true;
                    result = service;
                }
            }
            else
            {
                ended = true;
                result = null;
            }
        }

        //
        return result;
    }

    /**
     * Gets the older.
     *
     * @return the older
     */
    public Service getOldestService()
    {
        Service result;

        result = null;
        LocalDate oldestDate = null;
        for (Service current : this)
        {
            LocalDate date = current.getStartDate();

            if (date != null)
            {
                LocalDate currentDate = current.getStartDate();
                if ((result == null) || (currentDate.isBefore(oldestDate)))
                {
                    result = current;
                    oldestDate = currentDate;
                }
            }
        }

        //
        return result;
    }

    /**
     * Gets the oldest year.
     *
     * @return the oldest year
     */
    public Year getOldestStartYear()
    {
        Year result;

        Service oldestService = getOldestService();

        if (oldestService == null)
        {
            result = null;
        }
        else
        {
            result = oldestService.getStartYear();
        }

        //
        return result;
    }

    /**
     * Reverse.
     *
     * @return the services
     */
    public Services reverse()
    {
        Services result;

        Collections.reverse(this);

        result = this;

        //
        return result;
    }

    /**
     * Sort.
     *
     * @param sorting
     *            the sorting
     * @return the issues
     */
    public Services sort(final ServiceComparator.Sorting sorting)
    {
        Services result;

        sort(new ServiceComparator(sorting));

        result = this;

        //
        return result;
    }

    /**
     * Sort by name.
     *
     * @return the services
     */
    public Services sortByName()
    {
        Services result;

        result = sort(ServiceComparator.Sorting.NAME);

        //
        return result;
    }

    /**
     * Of.
     *
     * @param service
     *            the service
     * @return the services
     */
    public static Services of(final Service service)
    {
        Services result;

        result = new Services();

        if (service != null)
        {
            result.add(service);
        }

        //
        return result;
    }
}
