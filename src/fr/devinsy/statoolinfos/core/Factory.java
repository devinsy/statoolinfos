/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.statoolinfos.checker.PropertyChecker;
import fr.devinsy.statoolinfos.checker.PropertyChecks;
import fr.devinsy.statoolinfos.crawl.CrawlCache;
import fr.devinsy.statoolinfos.crawl.CrawlJournal;
import fr.devinsy.statoolinfos.properties.PathProperties;
import fr.devinsy.statoolinfos.properties.PathProperty;
import fr.devinsy.statoolinfos.properties.PathPropertyList;
import fr.devinsy.statoolinfos.properties.PathPropertyUtils;
import fr.devinsy.statoolinfos.properties.PropertyClassType;
import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringSet;

/**
 * The Class PathProperty.
 */
public class Factory
{
    private static Logger logger = LoggerFactory.getLogger(Factory.class);

    /**
     * Instantiates a new factory.
     */
    private Factory()
    {
    }

    /**
     * Load categories.
     *
     * @param source
     *            the source
     * @return the categories
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static Categories loadCategories(final File source) throws IOException
    {
        Categories result;

        PathProperties properties = PathPropertyUtils.load(source);

        result = loadCategories(properties);

        //
        return result;
    }

    /**
     * Load categories.
     *
     * @param source
     *            the source
     * @param federation
     *            the federation
     * @return the categories
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static Categories loadCategories(final File source, final Federation federation) throws IOException
    {
        Categories result;

        result = loadCategories(source);

        Category other = new Category("Autres", "Qui ne rentre pas dans une catégorie existante.");
        result.add(other);

        for (Software software : federation.getSoftwares().values())
        {
            if (!result.matches(software.getName()))
            {
                other.getSoftwares().add(software.getName());
            }
        }

        //
        return result;
    }

    /**
     * Load categories.
     *
     * @param properties
     *            the properties
     * @return the categories
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static Categories loadCategories(final PathProperties properties) throws IOException
    {
        Categories result;

        result = new Categories();

        StringSet prefixes = properties.getSubPrefixes();
        for (String prefix : prefixes)
        {
            String name = properties.get(prefix + ".name");
            String description = properties.get(prefix + ".description");
            String softwares = properties.get(prefix + ".softwares");
            String logoPath = properties.get(prefix + ".logo");

            StringList softwareList = new StringList();
            if (StringUtils.isNotBlank(softwares))
            {
                for (String string : softwares.split("[;,]"))
                {
                    softwareList.add(string.trim());
                }
            }

            Category category = new Category(name, description, softwareList);
            category.setLogoPath(logoPath);
            result.add(category);
        }

        result.sortByName();

        //
        return result;
    }

    /**
     * Load categories.
     *
     * @param source
     *            the source
     * @return the categories
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static Categories loadCategories(final URL source) throws IOException
    {
        Categories result;

        PathProperties properties = PathPropertyUtils.load(source);

        result = loadCategories(properties);

        //
        return result;
    }

    /**
     * Load categories.
     *
     * @param source
     *            the source
     * @param federation
     *            the federation
     * @return the categories
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static Categories loadCategories(final URL source, final Federation federation) throws IOException
    {
        Categories result;

        result = loadCategories(source);

        Category other = new Category("Autres", "Qui ne rentre pas dans une catégorie existante.");
        result.add(other);

        for (Software software : federation.getSoftwares().values())
        {
            if (!result.matches(software.getName()))
            {
                other.getSoftwares().add(software.getName());
            }
        }

        //
        return result;
    }

    /**
     * Load configuration.
     *
     * @param configurationFile
     *            the configuration file
     * @return the configuration
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public static Configuration loadConfiguration(final File configurationFile) throws StatoolInfosException
    {
        Configuration result;

        try
        {
            PathProperties properties = PathPropertyUtils.load(configurationFile);
            result = new Configuration(properties);
        }
        catch (IOException exception)
        {
            throw new StatoolInfosException("Error reading configuration file: " + configurationFile, exception);
        }

        //
        return result;
    }

    /**
     * Load federation.
     *
     * @param inputURL
     *            the input URL
     * @param cache
     *            the cache
     * @return the federation
     * @throws StatoolInfosException
     *             the statool infos exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static Federation loadFederation(final URL inputURL, final CrawlCache cache) throws StatoolInfosException, IOException
    {
        Federation result;

        System.out.println("Loading federation " + inputURL);

        if (inputURL == null)
        {
            throw new IllegalArgumentException("Null input URL.");
        }
        else if (cache == null)
        {
            throw new IllegalArgumentException("Null cache URL.");
        }
        else
        {
            File federationFile = cache.restoreFile(inputURL);
            if (federationFile == null)
            {
                throw new IllegalArgumentException("Htmlize input file undefined.");
            }
            else if (!federationFile.exists())
            {
                throw new IllegalArgumentException("Htmlize input file is missing.");
            }
            else if (federationFile.isDirectory())
            {
                throw new IllegalArgumentException("Htmlize input file is a directory.");
            }
            else
            {
                PathProperties properties = PathPropertyUtils.load(federationFile);
                result = new Federation(properties);
                result.setInputURL(inputURL);
                result.setInputFile(federationFile);
                result.setLogoFileName("logo" + StringUtils.defaultIfBlank(cache.getExtension(result.getLogoURL()), ".png"));

                PropertyChecker checker = new PropertyChecker();
                PropertyChecks checks = checker.checkFederation(result.getInputFile());
                result.getInputChecks().addAll(checks);
                result.getInputChecks().setFileName(result.getLocalFileName());

                PathProperties subs = result.getByPrefix("subs");
                for (PathProperty property : subs)
                {
                    if (StringUtils.startsWith(property.getValue(), "http"))
                    {
                        URL subInputURL = new URL(property.getValue());
                        Organization organization = loadOrganization(subInputURL, cache);
                        if (organization != null)
                        {
                            organization.setFederation(result);
                            result.getOrganizations().add(organization);
                        }
                    }
                }

                //
                result.getCrawlJournal().addAll(cache.restoreCrawlJournal());
            }
        }

        //
        return result;
    }

    /**
     * Load metrics.
     *
     * @param inputURL
     *            the input URL
     * @param cache
     *            the cache
     * @param prefixPath
     *            the prefix path
     * @return the metrics
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static Metrics loadMetrics(final URL inputURL, final CrawlCache cache, final String prefixPath) throws IOException
    {
        Metrics result;

        System.out.println("Loading metrics " + inputURL);

        File inputFile = cache.restoreFile(inputURL);
        if (inputFile == null)
        {
            logger.warn("WARNING: metrics not found in cache [{}]", inputURL);
            result = null;
        }
        else
        {
            PathProperties properties = PathPropertyUtils.load(inputFile);
            result = new Metrics(properties);
            result.setLocalFileNamePrefix(prefixPath);
            result.setInputFile(inputFile);
            result.setInputURL(inputURL);

            //
            PropertyChecker checker = new PropertyChecker();
            PropertyChecks checks = checker.checkMetrics(result.getInputFile());
            result.getInputChecks().addAll(checks);
            result.getInputChecks().setFileName(result.getLocalFileName());
        }

        //
        return result;
    }

    /**
     * Load organization.
     *
     * @param inputURL
     *            the input
     * @param cache
     *            the cache
     * @return the organization
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static Organization loadOrganization(final URL inputURL, final CrawlCache cache) throws IOException
    {
        Organization result;

        System.out.println("Loading organization " + inputURL);

        File inputFile = cache.restoreFile(inputURL);

        if (inputFile == null)
        {
            result = null;
            logger.warn("WARNING: organization not found in cache [{}]", inputURL);
        }
        else
        {
            PathProperties properties = PathPropertyUtils.load(inputFile);
            result = new Organization(properties);
            if (result.isValid())
            {
                result.setInputFile(inputFile);
                result.setInputURL(inputURL);
                result.setLogoFileName("logo" + StringUtils.defaultIfBlank(cache.getExtension(result.getLogoURL()), ".png"));

                //
                PropertyChecker checker = new PropertyChecker();
                PropertyChecks checks = checker.checkOrganization(result.getInputFile());
                checks.setFileName(result.getLocalFileName());
                result.getInputChecks().addAll(checks);

                //
                PathProperties subs = result.getByPrefix("subs");
                for (PathProperty property : subs)
                {
                    if (StringUtils.startsWith(property.getValue(), "http"))
                    {
                        URL serviceInputURL = new URL(property.getValue());
                        Service service = loadService(serviceInputURL, cache, result);
                        if (service != null)
                        {
                            result.getServices().add(service);
                        }
                    }
                }

                //
                CrawlJournal journal = cache.restoreCrawlJournal();
                result.getCrawlJournal().add(journal.getByUrl(inputURL));
                result.getCrawlJournal().addAll(journal.searchByParent(result.getInputURL()));
                for (Service service : result.getServices())
                {
                    result.getCrawlJournal().addAll(journal.searchByParent(service.getInputURL()));
                }
            }
            else
            {
                result = null;
                logger.warn("WARNING: organization has invalid file [{}]", inputURL);
            }
        }

        //
        return result;
    }

    /**
     * Load service.
     *
     * @param inputURL
     *            the input URL
     * @param cache
     *            the cache
     * @param organization
     *            the organization
     * @return the service
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static Service loadService(final URL inputURL, final CrawlCache cache, final Organization organization) throws IOException
    {
        Service result;

        System.out.println("Loading service " + inputURL);

        File inputFile = cache.restoreFile(inputURL);

        if (inputFile == null)
        {
            logger.warn("WARNING: service not found in cache [{}]", inputURL);
            result = null;
        }
        else
        {
            PathPropertyList properties = PathPropertyUtils.load(inputFile);
            if (properties.getClassType() != PropertyClassType.SERVICE)
            {
                logger.warn("WARNING: not file class service [{}]", inputURL);
                result = null;
            }
            else
            {
                result = new Service(properties);
                result.setOrganization(organization);
                result.setInputFile(inputFile);
                result.setInputURL(inputURL);
                result.setLogoFileName("logo" + StringUtils.defaultIfBlank(cache.getExtension(result.getLogoURL()), ".png"));

                //
                PathProperties subs = result.getByPrefix("subs");
                for (PathProperty property : subs)
                {
                    if (StringUtils.startsWith(property.getValue(), "http"))
                    {
                        URL metricsInputURL = new URL(property.getValue());
                        Metrics metrics = loadMetrics(metricsInputURL, cache, organization.getTechnicalName() + "-" + result.getTechnicalName());
                        if (metrics != null)
                        {
                            result.getMetrics().add(metrics);
                        }
                    }
                }

                //
                PropertyChecker checker = new PropertyChecker();
                PropertyChecks checks = checker.checkService(result.getInputFile());
                checks.setFileName(result.getLocalFileName());
                result.getInputChecks().addAll(checks);

                //
                CrawlJournal journal = cache.restoreCrawlJournal();
                result.getCrawlJournal().add(journal.getByUrl(inputURL));
                result.getCrawlJournal().addAll(journal.searchByParent(result.getInputURL()));

                //
                for (Metrics metrics : result.getMetrics())
                {
                    result.addAll(metrics.getByPrefix("metrics."));
                }
            }
        }

        //
        return result;
    }
}
