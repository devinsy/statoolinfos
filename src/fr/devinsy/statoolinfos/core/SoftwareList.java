/*
 * Copyright (C) 2020 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import java.util.ArrayList;
import java.util.Collections;

/**
 * The Class Categories.
 */
public class SoftwareList extends ArrayList<Software>
{
    private static final long serialVersionUID = -3197394621123996060L;

    /**
     * Instantiates a new software list.
     */
    public SoftwareList()
    {
        super();
    }

    /**
     * Reverse.
     *
     * @return the software list
     */
    public SoftwareList reverse()
    {
        SoftwareList result;

        Collections.reverse(this);

        result = this;

        //
        return result;
    }

    /**
     * Sort.
     *
     * @param sorting
     *            the sorting
     * @return the issues
     */
    public SoftwareList sort(final SoftwareComparator.Sorting sorting)
    {
        SoftwareList result;

        sort(new SoftwareComparator(sorting));

        result = this;

        //
        return result;
    }

    /**
     * Sort by name.
     *
     * @return the services
     */
    public SoftwareList sortByName()
    {
        SoftwareList result;

        result = sort(SoftwareComparator.Sorting.NAME);

        //
        return result;
    }
}
