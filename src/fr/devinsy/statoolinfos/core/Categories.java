/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.strings.StringList;

/**
 * The Class Categories.
 */
public class Categories extends ArrayList<Category>
{
    private static final long serialVersionUID = -221887756635386650L;

    /**
     * Instantiates a new categories.
     */
    public Categories()
    {
        super();
    }

    /**
     * Find by software.
     *
     * @param softwareName
     *            the software name
     * @return the category
     */
    public Categories findBySoftware(final String softwareName)
    {
        Categories result;

        result = new Categories();

        for (Category category : this)
        {
            if (category.matchesSoftware(softwareName))
            {
                result.add(category);
            }
        }

        //
        return result;
    }

    /**
     * Gets the by technical name.
     *
     * @param technicalName
     *            the technical name
     * @return the by technical name
     */
    public Category getByTechnicalName(final String technicalName)
    {
        Category result;

        Iterator<Category> iterator = this.iterator();
        boolean ended = false;
        result = null;
        while (!ended)
        {
            if (iterator.hasNext())
            {
                Category category = iterator.next();

                if (StringUtils.equals(category.getTechnicalName(), technicalName))
                {
                    ended = true;
                    result = category;
                }
            }
            else
            {
                ended = true;
                result = null;
            }
        }

        //
        return result;
    }

    /**
     * Matches.
     *
     * @param softwareName
     *            the software name
     * @return true, if successful
     */
    public boolean matches(final String softwareName)
    {
        boolean result;

        boolean ended = false;
        Iterator<Category> iterator = this.iterator();
        result = false;
        while (!ended)
        {
            if (iterator.hasNext())
            {
                Category category = iterator.next();

                if (category.matchesSoftware(softwareName))
                {
                    ended = true;
                    result = true;
                }
            }
            else
            {
                ended = true;
                result = false;
            }
        }

        //
        return result;
    }

    /**
     * Reverse.
     *
     * @return the categories
     */
    public Categories reverse()
    {
        Categories result;

        Collections.reverse(this);

        result = this;

        //
        return result;
    }

    /**
     * Sort.
     *
     * @param sorting
     *            the sorting
     * @return the issues
     */
    public Categories sort(final CategoryComparator.Sorting sorting)
    {
        Categories result;

        sort(new CategoryComparator(sorting));

        result = this;

        //
        return result;
    }

    /**
     * Sort by name.
     *
     * @return the services
     */
    public Categories sortByName()
    {
        Categories result;

        result = sort(CategoryComparator.Sorting.NAME);

        //
        return result;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString()
    {
        String result;

        StringList buffer = new StringList();

        for (Category category : this)
        {
            buffer.append(category.getName()).append(",");
        }
        if (!buffer.isEmpty())
        {
            buffer.removeLast();
        }

        result = buffer.toString();

        //
        return result;
    }
}
