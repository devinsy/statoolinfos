/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import fr.devinsy.statoolinfos.checker.PropertyChecks;
import fr.devinsy.statoolinfos.crawl.CrawlJournal;
import fr.devinsy.statoolinfos.properties.MonthValues;
import fr.devinsy.statoolinfos.properties.PathProperties;
import fr.devinsy.statoolinfos.properties.PathPropertyList;
import fr.devinsy.statoolinfos.properties.WeekValues;
import fr.devinsy.statoolinfos.properties.YearValues;
import fr.devinsy.statoolinfos.uptime.URLSet;
import fr.devinsy.statoolinfos.util.URLUtils;

/**
 * The Class PathProperty.
 */
public class Organization extends PathPropertyList
{
    private static final long serialVersionUID = -2709210934548224213L;

    public enum Status
    {
        ACTIVE,
        IDLE,
        AWAY
    }

    public enum Type
    {
        ASSOCIATION,
        COMPANY,
        COOPERATIVE,
        INDIVIDUAL,
        INFORMAL,
        MICROCOMPANY,
        OTHER
    }

    private Federation federation;
    private Services services;
    private File inputFile;
    private URL inputURL;
    private String logoFileName;
    private PropertyChecks inputChecks;
    private CrawlJournal crawlJournal;

    /**
     * Instantiates a new organization.
     */
    public Organization()
    {
        super();
        this.inputChecks = new PropertyChecks();
        this.services = new Services();
        this.crawlJournal = new CrawlJournal();
    }

    /**
     * Instantiates a new organization.
     *
     * @param properties
     *            the properties
     */
    public Organization(final PathProperties properties)
    {
        super(properties);
        this.inputChecks = new PropertyChecks();
        this.services = new Services();
        this.crawlJournal = new CrawlJournal();
    }

    /**
     * Gets the service active count.
     *
     * @return the service active count
     */
    public int getActiveServiceCount()
    {
        int result;

        result = 0;
        for (Service service : this.services)
        {
            if (service.isActive())
            {
                result += 1;
            }
        }

        //
        return result;
    }

    /**
     * Gets the active services.
     *
     * @return the active services
     */
    public Services getActiveServices()
    {
        Services result;

        result = new Services();

        for (Service service : this.services)
        {
            if (service.isActive())
            {
                result.add(service);
            }
        }

        //
        return result;
    }

    /**
     * Gets the age.
     *
     * @return the age
     */
    public String getAge()
    {
        String result;

        result = StatoolInfosUtils.toHumanDuration(getStartDate(), getEndDate());

        //
        return result;
    }

    /**
     * Gets the contact email.
     *
     * @return the contact email
     */
    public String getContactEmail()
    {
        String result;

        result = get("organization.contact.email");

        //
        return result;
    }

    /**
     * Gets the contact website.
     *
     * @return the contact website
     */
    public URL getContactURL()
    {
        URL result;

        result = getURL("organization.contact.url");

        //
        return result;
    }

    /**
     * Gets the country code.
     *
     * @return the country code
     */
    public String getCountryCode()
    {
        String result;

        result = StringUtils.toRootUpperCase(get("organization.country.code"));

        //
        return result;
    }

    /**
     * Gets the crawl date.
     *
     * @return the crawl date
     */
    public LocalDateTime getCrawlDate()
    {
        LocalDateTime result;

        result = LocalDateTime.parse(get("crawl.datetime"));

        //
        return result;
    }

    /**
     * Gets the crawled date.
     *
     * @return the crawled date
     */
    public LocalDateTime getCrawledDate()
    {
        LocalDateTime result;

        result = LocalDateTime.parse(get("crawl.file.datetime"));

        if (result.getYear() == 1970)
        {
            result = getCrawlDate();
        }

        //
        return result;
    }

    public CrawlJournal getCrawlJournal()
    {
        return this.crawlJournal;
    }

    public String getDescription()
    {
        String result;

        result = get("organization.description");

        //
        return result;
    }

    /**
     * Gets the diaspora page.
     *
     * @return the diaspora page
     */
    public String getDiasporaWebpage()
    {
        String result;

        result = get("organization.socialnetworks.diaspora", "organization.socialnetworks.diaspora.url");

        //
        return result;
    }

    /**
     * Gets the end date.
     *
     * @return the end date
     */
    public LocalDate getEndDate()
    {
        LocalDate result;

        result = StatoolInfosUtils.parseDate(getEndDateValue());

        //
        return result;
    }

    /**
     * Gets the end date.
     *
     * @return the end date
     */
    public String getEndDateValue()
    {
        String result;

        result = get("organization.enddate");

        //
        return result;
    }

    /**
     * Gets the end year.
     *
     * @return the end year
     */
    public Year getEndYear()
    {
        Year result;

        LocalDate date = getEndDate();

        if (date == null)
        {
            result = null;
        }
        else
        {
            result = Year.from(date);
        }

        //
        return result;
    }

    public Federation getFederation()
    {
        return this.federation;
    }

    /**
     * Gets the funkwhale webpage.
     *
     * @return the funkwhale webpage
     */
    public String getFunkwhaleWebpage()
    {
        String result;

        result = get("organization.socialnetworks.funkwhale", "organization.socialnetworks.funkwhale.url");

        //
        return result;
    }

    public PropertyChecks getInputChecks()
    {
        return this.inputChecks;
    }

    /**
     * Gets the input checks all.
     *
     * @return the input checks all
     */
    public PropertyChecks getInputChecksAll()
    {
        PropertyChecks result;

        result = new PropertyChecks();

        result.addAll(this.inputChecks);
        for (Service service : this.services)
        {
            result.addAll(service.getInputChecksAll());
        }

        //
        return result;
    }

    public File getInputFile()
    {
        return this.inputFile;
    }

    /**
     * Gets the crawled input URL.
     *
     * @return the crawled input URL
     */
    public URL getInputURL()
    {
        URL result;

        result = this.inputURL;

        //
        return result;
    }

    /**
     * Gets the legal website.
     *
     * @return the legal website
     */
    public URL getLegalURL()
    {
        URL result;

        String value = get("organization.legal.url", "organization.legal");

        result = URLUtils.of(value);

        //
        return result;
    }

    /**
     * Gets the local file base name.
     *
     * @return the local file base name
     */
    public String getLocalFileBaseName()
    {
        String result;

        result = getTechnicalName();

        //
        return result;
    }

    /**
     * Gets the local file name.
     *
     * @return the local file name
     */
    public String getLocalFileName()
    {
        String result;

        result = getLocalFileBaseName() + ".properties";

        //
        return result;
    }

    /**
     * Gets the logo file name.
     *
     * @return the logo file name
     */
    public String getLogoFileName()
    {
        String result;

        result = this.logoFileName;

        //
        return result;
    }

    /**
     * Gets the logo URL.
     *
     * @return the logo URL
     */
    public URL getLogoURL()
    {
        URL result;

        result = URLUtils.of(getLogoURLValue());

        //
        return result;
    }

    /**
     * Gets the logo URL value.
     *
     * @return the logo URL value
     */
    public String getLogoURLValue()
    {
        String result;

        result = get("organization.logo");

        //
        return result;
    }

    /**
     * Gets the mastodon page.
     *
     * @return the mastodon page
     */
    public String getMastodonWebpage()
    {
        String result;

        result = get("organization.socialnetworks.mastodon", "organization.socialnetworks.mastodon.url");

        //
        return result;
    }

    /**
     * Gets the member age.
     *
     * @return the member age
     */
    public String getMemberAge()
    {
        String result;

        result = StatoolInfosUtils.toHumanDuration(getMemberStartDate(), getMemberEndDate());

        //
        return result;
    }

    /**
     * Gets the member end date.
     *
     * @return the member end date
     */
    public LocalDate getMemberEndDate()
    {
        LocalDate result;

        result = getDate("organization.memberof." + this.federation.getTechnicalName() + ".enddate");

        //
        return result;
    }

    /**
     * Gets the member end date.
     *
     * @param entityName
     *            the entity name
     * @return the member end date
     */
    public LocalDate getMemberEndDate(final String entityName)
    {
        LocalDate result;

        result = getDate("organization.memberof." + entityName + ".enddate");

        //
        return result;
    }

    /**
     * Gets the member end date.
     *
     * @return the member end date
     */
    public String getMemberEndDateValue()
    {
        String result;

        result = get("organization.memberof." + this.federation.getTechnicalName() + ".enddate");

        //
        return result;
    }

    /**
     * Gets the member end year.
     *
     * @return the member end year
     */
    public Year getMemberEndYear()
    {
        Year result;

        LocalDate date = getMemberEndDate();
        if (date == null)
        {
            result = null;
        }
        else
        {
            result = Year.from(date);
        }

        //
        return result;
    }

    /**
     * Gets the member start date.
     *
     * @return the member start date
     */
    public LocalDate getMemberStartDate()
    {
        LocalDate result;

        result = getDate("organization.memberof." + this.federation.getTechnicalName() + ".startdate");

        //
        return result;
    }

    /**
     * Gets the member start date.
     *
     * @param entityName
     *            the entity name
     * @return the member start date
     */
    public LocalDate getMemberStartDate(final String entityName)
    {
        LocalDate result;

        result = getDate("organization.memberof." + entityName + ".startdate");

        //
        return result;
    }

    /**
     * Gets the member start date.
     *
     * @return the member start date
     */
    public String getMemberStartDateValue()
    {
        String result;

        result = get("organization.memberof." + this.federation.getTechnicalName() + ".startdate");

        //
        return result;
    }

    /**
     * Gets the member start year.
     *
     * @return the member start year
     */
    public Year getMemberStartYear()
    {
        Year result;

        LocalDate date = getMemberStartDate();
        if (date == null)
        {
            result = null;
        }
        else
        {
            result = Year.from(date);
        }

        //
        return result;
    }

    /**
     * Gets the status member of.
     *
     * @param entityName
     *            the entity name
     * @return the status member of
     */
    public Status getMemberStatusOf(final String entityName)
    {
        Status result;

        String value = get("organization.memberof." + entityName + ".status");

        if (StringUtils.isBlank(value))
        {
            result = null;
        }
        else if (StringUtils.equalsIgnoreCase(value, "ACTIVE"))
        {
            result = Status.ACTIVE;
        }
        else if (StringUtils.equalsIgnoreCase(value, "IDLE"))
        {
            result = Status.IDLE;
        }
        else if (StringUtils.equalsIgnoreCase(value, "AWAY"))
        {
            result = Status.AWAY;
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Gets the metric month values all.
     *
     * @param path
     *            the path
     * @return the metric month values all
     */
    public MonthValues getMetricMonthValuesAll(final String path)
    {
        MonthValues result;

        result = getMetricMonthValues(path);

        for (Service service : getServices())
        {
            result.addAll(service.getMetricMonthValues(path));
        }

        //
        return result;
    }

    /**
     * Gets the metric week values all.
     *
     * @param path
     *            the path
     * @return the metric week values all
     */
    public WeekValues getMetricWeekValuesAll(final String path)
    {
        WeekValues result;

        result = getMetricWeekValues(path);

        for (Service service : getServices())
        {
            result.addAll(service.getMetricWeekValues(path));
        }

        //
        return result;
    }

    /**
     * Gets the metric year values all.
     *
     * @param path
     *            the path
     * @return the metric year values all
     */
    public YearValues getMetricYearValuesAll(final String path)
    {
        YearValues result;

        result = getMetricYearValues(path);

        for (Service service : getServices())
        {
            result.addAll(service.getMetricYearValues(path));
        }

        //
        return result;
    }

    /**
     * Gets the mobilizon webpage.
     *
     * @return the mobilizon webpage
     */
    public String getMobilizonWebpage()
    {
        String result;

        result = get("organization.socialnetworks.mobilizon", "organization.socialnetworks.mobilizon.url");

        //
        return result;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        String result;

        result = get("organization.name");

        if (StringUtils.isBlank(result))
        {
            URL url = getInputURL();
            if (url == null)
            {
                result = "anonymous";
            }
            else
            {
                result = DigestUtils.md5Hex(url.toString()).substring(0, 8);
            }
        }

        //
        return result;
    }

    /**
     * Gets the peertube webpage.
     *
     * @return the peertube webpage
     */
    public String getPeertubeWebpage()
    {
        String result;

        result = get("organization.socialnetworks.peertube", "organization.socialnetworks.peertube.url");

        //
        return result;
    }

    /**
     * Gets the pixelfed webpage.
     *
     * @return the pixelfed webpage
     */
    public String getPixelfedWebpage()
    {
        String result;

        result = get("organization.socialnetworks.pixelfed", "organization.socialnetworks.pixelfed.url");

        //
        return result;
    }

    /**
     * Gets the previous month user count.
     *
     * @return the previous month user count
     */
    public long getPreviousMonthUserCount()
    {
        long result;

        result = 0;
        for (Service service : this.services)
        {
            result += service.getPreviousMonthUserCount();
        }

        //
        return result;
    }

    /**
     * Gets the previous month visit count.
     *
     * @return the previous month visit count
     */
    public long getPreviousMonthVisitCount()
    {
        long result;

        result = 0;
        for (Service service : this.services)
        {
            result += service.getPreviousMonthVisitCount();
        }

        //
        return result;
    }

    /**
     * Gets the service count.
     *
     * @return the service count
     */
    public int getServiceCount()
    {
        int result;

        result = this.services.size();

        //
        return result;
    }

    public Services getServices()
    {
        return this.services;
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public LocalDate getStartDate()
    {
        LocalDate result;

        result = StatoolInfosUtils.parseDate(getStartDateValue());

        //
        return result;
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public String getStartDateValue()
    {
        String result;

        result = get("organization.startdate");

        //
        return result;
    }

    /**
     * Gets the start year.
     *
     * @return the start year
     */
    public Year getStartYear()
    {
        Year result;

        LocalDate date = getStartDate();

        if (date == null)
        {
            result = null;
        }
        else
        {
            result = Year.from(date);
        }

        //
        return result;
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    public Status getStatus()
    {
        Status result;

        String value = get("organization.status.level", "organization.status");

        if (StringUtils.isBlank(value))
        {
            result = null;
        }
        else if (StringUtils.equalsIgnoreCase(value, "ACTIVE"))
        {
            result = Status.ACTIVE;
        }
        else if (StringUtils.equalsIgnoreCase(value, "IDLE"))
        {
            result = Status.IDLE;
        }
        else if (StringUtils.equalsIgnoreCase(value, "AWAY"))
        {
            result = Status.AWAY;
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Gets the technical guide website.
     *
     * @return the technical guide website
     */
    public URL getTechnicalGuideURL()
    {
        URL result;

        String value = get("organization.guide.technical", "organization.guide.technical.url");

        result = URLUtils.of(value);

        //
        return result;
    }

    /**
     * Gets the technical name.
     *
     * @return the technical name
     */
    public String getTechnicalName()
    {
        String result;

        result = StatoolInfosUtils.toTechnicalName(getName());

        //
        return result;
    }

    public Type getType()
    {
        Type result;

        String value = get("organization.type");

        if (StringUtils.isBlank(value))
        {
            result = null;
        }
        else if (StringUtils.equalsIgnoreCase(value, "ASSOCIATION"))
        {
            result = Type.ASSOCIATION;
        }
        else if (StringUtils.equalsIgnoreCase(value, "COOPERATIVE"))
        {
            result = Type.COOPERATIVE;
        }
        else if (StringUtils.equalsIgnoreCase(value, "MICROCOMPANY"))
        {
            result = Type.MICROCOMPANY;
        }
        else if (StringUtils.equalsIgnoreCase(value, "COMPANY"))
        {
            result = Type.COMPANY;
        }
        else if (StringUtils.equalsIgnoreCase(value, "INDIVIDUAL"))
        {
            result = Type.INDIVIDUAL;
        }
        else if (StringUtils.equalsIgnoreCase(value, "OTHER"))
        {
            result = Type.OTHER;
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Gets the URL all.
     *
     * @return the URL all
     */
    public URLSet getURLActiveAll()
    {
        URLSet result;

        result = new URLSet();

        result.add(getContactURL());
        result.add(getLegalURL());
        result.add(getLogoURL());
        result.add(getTechnicalGuideURL());
        result.add(getUserGuideURL());
        result.add(getWebsiteURL());

        //
        for (Service service : getServices().getBy(Service.Status.OK))
        {
            result.addAll(service.getURLAll());
        }

        //
        return result;
    }

    /**
     * Gets the URL all.
     *
     * @return the URL all
     */
    public URLSet getURLAll()
    {
        URLSet result;

        result = new URLSet();

        result.add(getContactURL());
        result.add(getLegalURL());
        result.add(getLogoURL());
        result.add(getTechnicalGuideURL());
        result.add(getUserGuideURL());
        result.add(getWebsiteURL());

        //
        for (Service service : getServices())
        {
            result.addAll(service.getURLAll());
        }

        //
        return result;
    }

    /**
     * Gets the user count.
     *
     * @return the user count
     */
    public int getUserCount()
    {
        int result;

        result = 0;

        //
        return result;
    }

    /**
     * Gets the user guide website.
     *
     * @return the user guide website
     */
    public URL getUserGuideURL()
    {
        URL result;

        String value = get("organization.guide.user", "organization.guide.user.url");

        result = URLUtils.of(value);

        //
        return result;
    }

    /**
     * Gets the website.
     *
     * @return the website
     */
    public URL getWebsiteURL()
    {
        URL result;

        result = getURL("organization.website");

        //
        return result;
    }

    /**
     * Checks for social network.
     *
     * @return true, if successful
     */
    public boolean hasSocialNetwork()
    {
        boolean result;

        if (StringUtils.isAllBlank(getDiasporaWebpage(), getMastodonWebpage()))
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /**
     * Checks for social network.
     *
     * @param value
     *            the value
     * @return true, if successful
     */
    public boolean hasSocialNetwork(final SocialNetworks value)
    {
        boolean result;

        String link;
        switch (value)
        {
            case DIASPORA:
                link = getDiasporaWebpage();
            break;
            case MASTODON:
                link = getMastodonWebpage();
            break;
            default:
                link = null;
        }

        if (StringUtils.isBlank(link))
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /**
     * Checks if is active.
     *
     * @return true, if is active
     */
    public boolean isActive()
    {
        boolean result;

        if (getStatus() == Status.ACTIVE)
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Checks if is away.
     *
     * @return true, if is away
     */
    public boolean isAway()
    {
        boolean result;

        Status status = getStatus();

        if (status == Status.AWAY)
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Checks if is away for.
     *
     * @param entityName
     *            the entity name
     * @return true, if is away for
     */
    public boolean isAwayFor(final String entityName)
    {
        boolean result;

        Status memberStatus = getMemberStatusOf(entityName);
        LocalDate endDate = getDate("organization.memberof." + entityName + ".enddate");

        if (memberStatus == Status.AWAY)
        {
            result = true;
        }
        else if ((endDate == null) || (endDate.isAfter(LocalDate.now())))
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /**
     * Checks if is default.
     *
     * @return true, if is default
     */
    public boolean isDefault()
    {
        boolean result;

        if ((getServiceCount() == 0) && (getLogoURLValue() == null) && (getStartDateValue() == null))
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Checks if is idle.
     *
     * @return true, if is idle
     */
    public boolean isIdle()
    {
        boolean result;

        Status status = getStatus();
        if ((status == null) || (status == Status.IDLE))
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Checks if is member.
     *
     * @param year
     *            the year
     * @return true, if is member
     */
    public boolean isMember(final Year year)
    {
        boolean result;

        if (year == null)
        {
            result = false;
        }
        else
        {
            Year startYear = getMemberStartYear();
            if (startYear == null)
            {
                result = false;
            }
            else
            {
                Year endYear = getMemberEndYear();
                if (endYear == null)
                {
                    endYear = Year.now();
                }

                if ((year.isBefore(startYear)) || (year.isAfter(endYear)))
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
        }

        //
        return result;
    }

    /**
     * Checks if is valid.
     *
     * @return true, if is valid
     */
    public boolean isValid()
    {
        boolean result;

        // if (StringUtils.equalsIgnoreCase(get("file.class"), "organization"))
        if (isEmpty())
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /**
     * Sets the federation.
     *
     * @param federation
     *            the new federation
     */
    public void setFederation(final Federation federation)
    {
        this.federation = federation;
    }

    /**
     * Sets the input file.
     *
     * @param inputFile
     *            the new input file
     */
    public void setInputFile(final File inputFile)
    {
        this.inputFile = inputFile;
    }

    /**
     * Sets the input URL.
     *
     * @param inputURL
     *            the new input URL
     */
    public void setInputURL(final URL inputURL)
    {
        this.inputURL = inputURL;
    }

    /**
     * Sets the logo file name.
     *
     * @param logoFileName
     *            the new logo file name
     */
    public void setLogoFileName(final String logoFileName)
    {
        this.logoFileName = logoFileName;
    }
}
