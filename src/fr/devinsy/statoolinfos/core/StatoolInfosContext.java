/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

import fr.devinsy.statoolinfos.crawl.CrawlCache;
import fr.devinsy.statoolinfos.uptime.UptimeJournal;

/**
 * The Class StatoolInfosContext.
 */
public class StatoolInfosContext
{
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(StatoolInfosContext.class);

    private LocalDateTime initDateTime;
    private Configuration configuration;
    private Federation federation;
    private Categories categories;
    private CrawlCache cache;
    private UptimeJournal uptimeJournal;

    /**
     * Instantiates a new statool infos context.
     *
     * @param configurationFile
     *            the configuration file
     * @throws StatoolInfosException
     *             the statool infos exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public StatoolInfosContext(final File configurationFile) throws StatoolInfosException, IOException
    {
        configure(configurationFile);
    }

    /**
     * Configure.
     *
     * @param configurationFile
     *            the configuration file
     * @throws StatoolInfosException
     *             the statool infos exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void configure(final File configurationFile) throws StatoolInfosException, IOException
    {
        this.configuration = Factory.loadConfiguration(configurationFile);

        logger.info("Cache setting: {}", this.configuration.getCrawlCachePath());
        logger.info("Cache input setting: {}", this.configuration.getCrawlInputURL());

        this.cache = new CrawlCache(this.configuration.getCrawlCacheDirectory());

        if (this.configuration.isFederation())
        {
            long startTime = System.currentTimeMillis();
            System.out.println("Loading configuration file.");
            this.federation = Factory.loadFederation(this.configuration.getCrawlInputURL(), this.cache);
            if (this.configuration.isCategoryFileDefined())
            {
                this.categories = Factory.loadCategories(this.configuration.getCategoryFile(), this.federation);
            }
            else
            {
                URL categoriesURL = StatoolInfosContext.class.getResource("/fr/devinsy/statoolinfos/core/categories.properties");
                this.categories = Factory.loadCategories(categoriesURL, this.federation);
            }
            this.uptimeJournal = this.cache.restoreUptimeJournal();
            this.initDateTime = LocalDateTime.now();
            System.out.println("Loaded configuration context in " + (System.currentTimeMillis() - startTime) + " ms.");
        }
        else
        {
            throw new IllegalArgumentException("Not a federation configuration.");
        }
    }

    /**
     * Gets the cache.
     *
     * @return the cache
     */
    public CrawlCache getCache()
    {
        CrawlCache result;

        result = this.cache;

        //
        return result;
    }

    /**
     * Gets the categories.
     *
     * @return the categories
     */
    public Categories getCategories()
    {
        Categories result;

        result = this.categories;

        //
        return result;
    }

    /**
     * Gets the configuration.
     *
     * @return the configuration
     */
    public Configuration getConfiguration()
    {
        Configuration result;

        result = this.configuration;

        //
        return result;
    }

    /**
     * Gets the federation.
     *
     * @return the federation
     */
    public Federation getFederation()
    {
        Federation result;

        result = this.federation;

        //
        return result;
    }

    /**
     * Gets the uptime journal.
     *
     * @return the uptime journal
     */
    public UptimeJournal getUptimeJournal()
    {
        return this.uptimeJournal;
    }

    /**
     * Checks if is expired.
     *
     * @return true, if is expired
     */
    public boolean isExpired()
    {
        boolean result;

        getCache();

        long cacheTime = this.getCache().getDirectory().lastModified();
        LocalDateTime cacheDateTime = LocalDateTime.ofEpochSecond(cacheTime / 1000, 0, OffsetDateTime.now().getOffset());

        if (cacheDateTime.isAfter(this.initDateTime))
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }
}
