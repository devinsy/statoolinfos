/*
 * 
 */
package fr.devinsy.statoolinfos.core;

import java.util.Comparator;

import fr.devinsy.statoolinfos.util.CompareUtils;

/**
 * The Class OrganizationComparator.
 */
public class OrganizationComparator implements Comparator<Organization>
{
    public enum Sorting
    {
        NAME,
        SERVICE_COUNT,
        REVERSE_SERVICE_COUNT,
        USER_COUNT
    }

    private Sorting sorting;

    /**
     * Instantiates a new organization comparator.
     *
     * @param sorting
     *            the sorting
     */
    public OrganizationComparator(final Sorting sorting)
    {
        this.sorting = sorting;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    @Override
    public int compare(final Organization alpha, final Organization bravo)
    {
        int result;

        result = compare(alpha, bravo, this.sorting);

        //
        return result;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @param sorting
     *            the sorting
     * @return the int
     */
    public static int compare(final Organization alpha, final Organization bravo, final Sorting sorting)
    {
        int result;

        if (sorting == null)
        {
            result = 0;
        }
        else
        {
            switch (sorting)
            {
                default:
                case NAME:
                    result = CompareUtils.compareIgnoreCase(getName(alpha), getName(bravo));
                break;

                case SERVICE_COUNT:
                    result = CompareUtils.compare(getServiceCount(alpha), getServiceCount(bravo));
                    if (result == 0)
                    {
                        result = CompareUtils.compareIgnoreCase(getName(alpha), getName(bravo));
                    }
                break;

                case REVERSE_SERVICE_COUNT:
                    result = -CompareUtils.compare(getServiceCount(alpha), getServiceCount(bravo));
                    if (result == 0)
                    {
                        result = CompareUtils.compareIgnoreCase(getName(alpha), getName(bravo));
                    }
                break;

                case USER_COUNT:
                    result = CompareUtils.compare(getUserCount(alpha), getUserCount(bravo));
                break;
            }
        }

        //
        return result;
    }

    /**
     * Gets the name.
     *
     * @param source
     *            the source
     * @return the name
     */
    public static String getName(final Organization source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.getName();
        }

        //
        return result;
    }

    /**
     * Gets the service count.
     *
     * @param source
     *            the source
     * @return the service count
     */
    public static Long getServiceCount(final Organization source)
    {
        Long result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = (long) source.getServiceCount();
        }

        //
        return result;
    }

    /**
     * Gets the user count.
     *
     * @param source
     *            the source
     * @return the user count
     */
    public static Long getUserCount(final Organization source)
    {
        Long result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = (long) source.getUserCount();
        }

        //
        return result;
    }

}
