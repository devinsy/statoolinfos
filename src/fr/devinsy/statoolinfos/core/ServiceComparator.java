/*
 * Copyright (C) 2020-2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.core;

import java.util.Comparator;

import fr.devinsy.statoolinfos.util.CompareUtils;

/**
 * The Class ServiceComparator.
 */
public class ServiceComparator implements Comparator<Service>
{
    public enum Sorting
    {
        NAME,
        USER_COUNT,
        REVERSE_USER_COUNT
    }

    private Sorting sorting;

    /**
     * Instantiates a new category stat comparator.
     *
     * @param sorting
     *            the sorting
     */
    public ServiceComparator(final Sorting sorting)
    {
        this.sorting = sorting;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    @Override
    public int compare(final Service alpha, final Service bravo)
    {
        int result;

        result = compare(alpha, bravo, this.sorting);

        //
        return result;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @param sorting
     *            the sorting
     * @return the int
     */
    public static int compare(final Service alpha, final Service bravo, final Sorting sorting)
    {
        int result;

        if (sorting == null)
        {
            result = 0;
        }
        else
        {
            switch (sorting)
            {
                default:
                case NAME:
                    result = CompareUtils.compareIgnoreCase(getName(alpha), getName(bravo));
                break;

                case USER_COUNT:
                    result = CompareUtils.compare(getUserCount(alpha), getUserCount(bravo));
                    if (result == 0)
                    {
                        result = CompareUtils.compareIgnoreCase(getName(alpha), getName(bravo));
                    }
                break;

                case REVERSE_USER_COUNT:
                    result = -CompareUtils.compare(getUserCount(alpha), getUserCount(bravo));
                    if (result == 0)
                    {
                        result = CompareUtils.compareIgnoreCase(getName(alpha), getName(bravo));
                    }
                break;
            }
        }

        //
        return result;
    }

    /**
     * Gets the name.
     *
     * @param source
     *            the source
     * @return the name
     */
    public static String getName(final Service source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.getName();
        }

        //
        return result;
    }

    /**
     * Gets the user count.
     *
     * @param source
     *            the source
     * @return the user count
     */
    public static Long getUserCount(final Service source)
    {
        Long result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = (long) source.getUserCount();
        }

        //
        return result;
    }
}
