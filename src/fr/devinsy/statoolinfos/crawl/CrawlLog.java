/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.crawl;

import java.net.URL;

/**
 * The Class CrawlLog.
 */
public class CrawlLog
{
    private URL url;
    private URL parentUrl;
    private CrawlStatus status;

    /**
     * Instantiates a new crawl log.
     *
     * @param url
     *            the url
     * @param parentUrl
     *            the parent url
     * @param status
     *            the status
     */
    public CrawlLog(final URL url, final URL parentUrl, final CrawlStatus status)
    {
        this.url = url;
        this.parentUrl = parentUrl;
        this.status = status;
    }

    /**
     * Gets the parent url.
     *
     * @return the parent url
     */
    public URL getParentUrl()
    {
        return this.parentUrl;
    }

    /**
     * Gets the parent url value.
     *
     * @return the parent url value
     */
    public String getParentUrlValue()
    {
        String result;

        if (this.parentUrl == null)
        {
            result = null;
        }
        else
        {
            result = this.parentUrl.toString();
        }

        //
        return result;
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    public CrawlStatus getStatus()
    {
        return this.status;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public URL getUrl()
    {
        return this.url;
    }
}