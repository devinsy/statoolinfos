/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.crawl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.core.StatoolInfosUtils;
import fr.devinsy.statoolinfos.properties.PathProperties;
import fr.devinsy.statoolinfos.properties.PathProperty;
import fr.devinsy.statoolinfos.properties.PathPropertyList;
import fr.devinsy.statoolinfos.properties.PathPropertyUtils;
import fr.devinsy.statoolinfos.properties.PropertyClassType;

/**
 * The Class Crawler.
 */
public class Crawler
{
    private static Logger logger = LoggerFactory.getLogger(Crawler.class);

    private CrawlCache cache;
    private CrawlJournal journal;

    /**
     * Instantiates a new crawler.
     *
     * @param rootDirectory
     *            the root directory
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public Crawler(final File rootDirectory) throws StatoolInfosException
    {
        logger.info("Crawl cache setting: {}", rootDirectory);
        this.cache = new CrawlCache(rootDirectory);
        this.journal = new CrawlJournal();
    }

    /**
     * Clear.
     *
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public void clear() throws StatoolInfosException
    {
        this.cache.clear();
    }

    /**
     * Crawl.
     *
     * @param url
     *            the input url
     * @throws StatoolInfosException
     *             the statool infos exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void crawl(final URL url) throws StatoolInfosException, IOException
    {
        crawl(url, null, null);
    }

    /**
     * Crawl.
     *
     * @param url
     *            the url
     * @param parentURL
     *            the parent URL
     * @param parent
     *            the parent
     */
    public void crawl(final URL url, final URL parentURL, final PropertyClassType parent)
    {
        logger.info("Crawling {}", url);

        File downloadFile = null;
        try
        {
            try
            {
                downloadFile = download(url);
            }
            catch (java.net.ConnectException exception)
            {
                logger.error("ERROR: crawl failed for [{}]: {}", url.toString(), exception.getMessage());
                this.journal.add(url, parentURL, CrawlStatus.CONNECTERROR);
                downloadFile = null;
            }
            catch (java.net.SocketException exception)
            {
                logger.error("ERROR: crawl failed for [{}]: {}", url.toString(), exception.getMessage());
                this.journal.add(url, parentURL, CrawlStatus.CONNECTERROR);
                downloadFile = null;
            }
            catch (FileNotFoundException exception)
            {
                logger.error("ERROR: crawl failed for [{}]: {}", url.toString(), exception.getMessage());
                this.journal.add(url, parentURL, CrawlStatus.URLNOTFOUND);
                downloadFile = null;
            }
            catch (IOException exception)
            {
                logger.error("ERROR: crawl failed for [{}]: {}", url.toString(), exception.getMessage());
                this.journal.add(url, parentURL, CrawlStatus.DOWNLOADERROR);
                downloadFile = null;
            }

            if (downloadFile != null)
            {
                if (!downloadFile.exists())
                {
                    logger.error("ERROR: download missing.");
                    this.journal.add(url, parentURL, CrawlStatus.MISSING);
                }
                else if (downloadFile.length() == 0)
                {
                    logger.error("ERROR: download empty.");
                    this.journal.add(url, parentURL, CrawlStatus.EMPTY);
                }
                else
                {
                    PathPropertyList downloadProperties = PathPropertyUtils.load(downloadFile);
                    PropertyClassType downloadClass = downloadProperties.getClassType();

                    if ((downloadClass == null) || (!downloadClass.isChildOf(parent)))
                    {
                        logger.error("ERROR: bad child class [{}][{}].", downloadClass, parent);
                        this.journal.add(url, parentURL, CrawlStatus.BADCHILDCLASS);
                        downloadFile.delete();
                    }
                    else
                    {
                        File storedFile = this.cache.restoreFile(url);
                        String storedSha;
                        if (storedFile == null)
                        {
                            storedSha = null;
                        }
                        else
                        {
                            PathProperties storedProperties = PathPropertyUtils.load(storedFile);
                            storedSha = storedProperties.get("crawl.file.sha1");
                        }

                        String downloadSha = StatoolInfosUtils.sha1sum(downloadFile);
                        if (StringUtils.equals(downloadSha, storedSha))
                        {
                            this.journal.add(url, parentURL, CrawlStatus.SUCCESS);
                            downloadFile.delete();
                        }
                        else
                        {
                            // Build crawl data.
                            PathProperties crawlSection = new PathPropertyList();
                            crawlSection.put("crawl.crawler", "StatoolInfos");
                            crawlSection.put("crawl.datetime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));
                            crawlSection.put("crawl.url", url.toString());
                            crawlSection.put("crawl.file.size", FileUtils.sizeOf(downloadFile));
                            crawlSection.put("crawl.file.datetime", StatoolInfosUtils.urlLastModified(url).toString());
                            crawlSection.put("crawl.file.sha1", downloadSha);
                            String crawlSectionLines = crawlSection.toStringListFormatted().toStringSeparatedBy('\n');

                            // Add crawl data in crawled file.
                            String downloadExtendedLines = FileUtils.readFileToString(downloadFile, StandardCharsets.UTF_8) + "\n" + crawlSectionLines;
                            FileUtils.write(downloadFile, downloadExtendedLines, StandardCharsets.UTF_8);

                            // Store in cache.
                            this.cache.store(url, downloadFile);
                            downloadFile.delete();

                            //
                            this.journal.add(url, parentURL, CrawlStatus.UPDATED);
                        }

                        // Cache another resources.
                        crawlLogo(downloadProperties.getURL("federation.logo"), url);
                        crawlLogo(downloadProperties.getURL("organization.logo"), url);
                        crawlLogo(downloadProperties.getURL("service.logo"), url);

                        // Do subs.
                        PathProperties subs = downloadProperties.getByPrefix("subs");
                        for (PathProperty property : subs)
                        {
                            if (StringUtils.isNotBlank(property.getValue()))
                            {
                                try
                                {
                                    URL subUrl = new URL(property.getValue());
                                    crawl(subUrl, url, downloadClass);
                                }
                                catch (java.net.MalformedURLException exception)
                                {
                                    logger.error("ERROR: subcrawl failed for [{}][{}][{}]: {}", url.toString(), property.getPath(), property.getValue(), exception.getMessage());
                                    this.journal.add(url, parentURL, CrawlStatus.BADURLFORMAT);
                                    exception.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (IOException exception)
        {
            this.journal.add(url, parentURL, CrawlStatus.IOERROR);
        }
        finally
        {
            if ((downloadFile != null) && (downloadFile.exists()))
            {
                downloadFile.delete();
            }
        }
    }

    /**
     * Crawl logo.
     *
     * @param url
     *            the url
     * @param parentURL
     *            the parent URL
     * @return the file
     */
    public File crawlLogo(final URL url, final URL parentURL)
    {
        File result;

        try
        {
            if ((url == null) || (!StringUtils.startsWithIgnoreCase(url.getProtocol(), "http")))
            {
                result = null;
            }
            else
            {
                logger.info("Crawling {}", url);

                File logoFile;
                try
                {
                    logoFile = download(url);
                }
                catch (java.net.ConnectException exception)
                {
                    logger.error("ERROR: crawl failed (1) for [{}]: {}", url.toString(), exception.getMessage());
                    this.journal.add(url, parentURL, CrawlStatus.CONNECTERROR);
                    logoFile = null;
                }
                catch (FileNotFoundException exception)
                {
                    logger.error("ERROR: crawl failed (2) for [{}]: {}", url.toString(), exception.getMessage());
                    this.journal.add(url, parentURL, CrawlStatus.URLNOTFOUND);
                    logoFile = null;
                }
                catch (IOException exception)
                {
                    logger.error("ERROR: crawl failed (3) for [{}]: {}", url.toString(), exception.getMessage());
                    this.journal.add(url, parentURL, CrawlStatus.DOWNLOADERROR);
                    logoFile = null;
                }

                if (logoFile == null)
                {
                    result = null;
                }
                else
                {
                    result = this.cache.store(url, logoFile);
                    this.journal.add(url, parentURL, CrawlStatus.SUCCESS);
                    logoFile.delete();
                }
            }
        }
        catch (IOException exception)
        {
            logger.info("Store failed for {}: {}", url, exception.getMessage());
            result = null;
        }

        //
        return result;
    }

    /**
     * Download.
     *
     * @param url
     *            the url
     * @return the file
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public File download(final URL url) throws IOException
    {
        File result;

        if (!StringUtils.startsWith(url.getProtocol(), "http"))
        {
            logger.warn("WARNING: crawl failed because bad http+ protocol for [{}]", url);
            result = null;
        }
        else
        {
            final int TIMEOUT = 5000;
            result = Files.createTempFile("tmp-", ".statoolsinfos").toFile();
            result.deleteOnExit();
            FileUtils.copyURLToFile(url, result, TIMEOUT, TIMEOUT);
        }

        //
        return result;
    }

    /**
     * Restore journal.
     *
     * @return the crawl journal
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public CrawlJournal restoreJournal() throws IOException
    {
        CrawlJournal result;

        logger.info("Restoring crawl journal.");

        result = this.cache.restoreCrawlJournal();

        //
        return result;
    }

    /**
     * Store journal.
     */
    public void storeJournal()
    {
        logger.info("Storing crawl journal.");
        this.cache.storeCrawlJournal(this.journal);
    }
}
