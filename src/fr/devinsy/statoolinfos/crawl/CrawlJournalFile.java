/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.crawl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class CrawlFile.
 */
public class CrawlJournalFile
{
    private static Logger logger = LoggerFactory.getLogger(CrawlJournalFile.class);

    /**
     * Instantiates a new crawl file.
     */
    private CrawlJournalFile()
    {
        super();
    }

    /**
     * Load.
     *
     * @param file
     *            the file
     * @return the path property list
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static CrawlJournal load(final File file) throws IOException
    {
        CrawlJournal result;

        result = load(file, StandardCharsets.UTF_8);

        //
        return result;
    }

    /**
     * Load.
     *
     * @param file
     *            the file
     * @param charset
     *            the charset name
     * @return the path properties
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static CrawlJournal load(final File file, final Charset charset) throws IOException
    {
        CrawlJournal result;

        if (file == null)
        {
            throw new IllegalArgumentException("File parameter is null.");
        }
        else
        {
            BufferedReader in = null;
            try
            {
                in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charset));
                result = read(in);
            }
            finally
            {
                IOUtils.closeQuietly(in);
            }

            result.setDatetime(LocalDateTime.ofEpochSecond(file.lastModified() / 1000, 0, ZoneOffset.UTC));
        }

        //
        return result;
    }

    /**
     * Read.
     *
     * @param in
     *            the in
     * @return the crawl logs
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static CrawlJournal read(final BufferedReader in) throws IOException
    {
        CrawlJournal result;

        result = new CrawlJournal();

        boolean ended = false;
        while (!ended)
        {
            String line = in.readLine();

            if (line == null)
            {
                ended = true;
            }
            else
            {
                CrawlLog log = valueOf(line);
                result.add(log);
            }
        }

        //
        return result;
    }

    /**
     * Save.
     *
     * @param file
     *            the file
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void save(final File file, final CrawlJournal source) throws IOException
    {
        PrintWriter out = null;
        try
        {
            out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8));
            write(out, source);
        }
        finally
        {
            //
            IOUtils.closeQuietly(out);
        }
    }

    /**
     * Value of.
     *
     * @param line
     *            the line
     * @return the path property
     */
    public static CrawlLog valueOf(final String line)
    {
        CrawlLog result;

        if (line == null)
        {
            result = null;
        }
        else
        {
            String[] tokens = line.split(" ", 3);

            CrawlStatus status = CrawlStatus.valueOf(tokens[0].toUpperCase());

            URL parentURL;
            try
            {
                if (StringUtils.equals(tokens[1], "null"))
                {
                    parentURL = null;
                }
                else
                {
                    parentURL = new URL(tokens[1].trim());
                }
            }
            catch (MalformedURLException exception)
            {
                logger.error("Error valuing [{}]", line);
                exception.printStackTrace();
                parentURL = null;
            }

            URL url;
            try
            {
                url = new URL(tokens[2].trim());
            }
            catch (MalformedURLException exception)
            {
                logger.error("Error valuing [{}]", line);
                exception.printStackTrace();
                url = null;
            }

            result = new CrawlLog(url, parentURL, status);
        }

        //
        return result;
    }

    /**
     * Write.
     *
     * @param out
     *            the out
     * @param journal
     *            the journal
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void write(final PrintWriter out, final CrawlJournal journal) throws IOException
    {
        if (journal != null)
        {
            for (CrawlLog log : journal)
            {
                String line = String.format("%s %s %s", log.getStatus(), log.getParentUrl(), log.getUrl());
                out.write(line);
                out.write("\n");
            }
        }
    }

}