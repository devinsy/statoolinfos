/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.crawl;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.statoolinfos.util.URLUtils;

/**
 * The Class CrawlLogs.
 */
public class CrawlLogs extends ArrayList<CrawlLog>
{
    private static final long serialVersionUID = -8749217049690008582L;

    /**
     * Instantiates a new crawl logs.
     */
    public CrawlLogs()
    {
        super();
    }

    /* (non-Javadoc)
     * @see java.util.ArrayList#add(java.lang.Object)
     */
    @Override
    public boolean add(final CrawlLog log)
    {
        boolean result;

        if (log == null)
        {
            result = false;
        }
        else
        {
            result = super.add(log);
        }

        //
        return result;
    }

    /**
     * Adds the.
     *
     * @param url
     *            the url
     * @param parentUrl
     *            the parent url
     * @param status
     *            the status
     */
    public void add(final URL url, final URL parentUrl, final CrawlStatus status)
    {
        this.add(new CrawlLog(url, parentUrl, status));
    }

    /**
     * Find by url.
     *
     * @param url
     *            the url
     * @return the crawl logs
     */
    public CrawlLogs findByUrl(final URL url)
    {
        CrawlLogs result;

        result = new CrawlLogs();

        for (CrawlLog log : this)
        {
            if (StringUtils.equals(log.getUrl().toString(), url.toString()))
            {
                result.add(log);
            }
        }

        //
        return result;
    }

    /**
     * Gets the by url.
     *
     * @param url
     *            the url
     * @return the by url
     */
    public CrawlLog getByUrl(final URL url)
    {
        CrawlLog result;

        boolean ended = false;
        Iterator<CrawlLog> iterator = iterator();
        result = null;
        while (!ended)
        {
            if (iterator.hasNext())
            {
                CrawlLog log = iterator.next();

                if (URLUtils.equals(log.getUrl(), url))
                {
                    ended = true;
                    result = log;
                }
            }
            else
            {
                ended = true;
            }
        }

        //
        return result;
    }

    /**
     * Gets the errors.
     *
     * @return the errors
     */
    public CrawlLogs getErrors()
    {
        CrawlLogs result;

        result = new CrawlLogs();

        for (CrawlLog log : this)
        {
            if (log.getStatus().isError())
            {
                result.add(log);
            }
        }

        //
        return result;
    }

    /**
     * Gets the success.
     *
     * @return the success
     */
    public CrawlLogs getSuccess()
    {
        CrawlLogs result;

        result = new CrawlLogs();

        for (CrawlLog log : this)
        {
            if (!log.getStatus().isError())
            {
                result.add(log);
            }
        }

        //
        return result;
    }

    /**
     * Reverse.
     *
     * @return the categories
     */
    public CrawlLogs reverse()
    {
        CrawlLogs result;

        Collections.reverse(this);

        result = this;

        //
        return result;
    }

    /**
     * Gets the by parent.
     *
     * @param parentURL
     *            the parent URL
     * @return the by parent
     */
    public CrawlLogs searchByParent(final URL parentURL)
    {
        CrawlLogs result;

        result = new CrawlLogs();

        for (CrawlLog log : this)
        {
            if (URLUtils.equals(log.getParentUrl(), parentURL))
            {
                result.add(log);
            }
        }

        //
        return result;
    }
}
