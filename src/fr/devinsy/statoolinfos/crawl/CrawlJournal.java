/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.crawl;

import java.time.LocalDateTime;

/**
 * The Class CrawlJournal.
 */
public class CrawlJournal extends CrawlLogs
{
    private static final long serialVersionUID = -7855320365496351766L;

    private LocalDateTime datetime;

    /**
     * Instantiates a new crawl journal.
     */
    public CrawlJournal()
    {
        super();
        this.datetime = LocalDateTime.now();
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public LocalDateTime getDatetime()
    {
        return this.datetime;
    }

    /**
     * Sets the date.
     *
     * @param date
     *            the new date
     */
    public void setDatetime(final LocalDateTime date)
    {
        this.datetime = date;
    }

}