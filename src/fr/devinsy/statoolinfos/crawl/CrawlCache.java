/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.crawl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.catgenerator.core.BirdGenerator;
import fr.devinsy.catgenerator.core.CatGenerator;
import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.core.StatoolInfosUtils;
import fr.devinsy.statoolinfos.properties.PathProperties;
import fr.devinsy.statoolinfos.properties.PathPropertyList;
import fr.devinsy.statoolinfos.properties.PathPropertyUtils;
import fr.devinsy.statoolinfos.uptime.UptimeJournal;
import fr.devinsy.statoolinfos.uptime.UptimeJournalFile;
import fr.devinsy.statoolinfos.util.URLUtils;

/**
 * The Class CrawlCache.
 */
public class CrawlCache
{
    private static Logger logger = LoggerFactory.getLogger(CrawlCache.class);

    public static enum DefaultLogoGenerator
    {
        CAT,
        BIRD
    }

    private File directory;

    /**
     * Instantiates a new crawl cache.
     *
     * @param directory
     *            the directory
     * @throws StatoolInfosException
     *             the statool infos exception
     */
    public CrawlCache(final File directory) throws StatoolInfosException
    {
        if (directory == null)
        {
            throw new IllegalArgumentException("Null parameter.");
        }
        else if (StringUtils.isBlank(directory.getName()))
        {
            throw new IllegalArgumentException("Crawl cache directory undefined.");
        }
        else if (!directory.exists())
        {
            throw new IllegalArgumentException("Crawl cache directory does not exist.");
        }
        else
        {
            this.directory = directory;
        }
    }

    /**
     * Builds the file.
     *
     * @param key
     *            the key
     * @return the file
     */
    private File buildFile(final URL url)
    {
        File result;

        result = new File(this.directory, DigestUtils.md5Hex(url.toString()));

        //
        return result;
    }

    /**
     * Clear file in cache. Only files ending with ".property" are deleted.
     */
    public void clear()
    {
        for (File file : this.directory.listFiles())
        {
            if ((file.isFile()) && ((file.getName().endsWith(".properties")) || file.getName().length() == 32))
            {
                logger.info("Deleting " + file.getName());
                file.delete();
            }
        }
    }

    /**
     * Gets the directory.
     *
     * @return the directory
     */
    public File getDirectory()
    {
        return this.directory;
    }

    /**
     * Gets the extension.
     *
     * @param url
     *            the url
     * @return the extension
     */
    public String getExtension(final URL url)
    {
        String result;

        File file = restoreFile(url);
        if (file == null)
        {
            result = null;
        }
        else
        {
            result = StatoolInfosUtils.findExtension(file);
        }

        //
        return result;
    }

    /**
     * Restore crawl journal.
     *
     * @return the crawl journal
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public CrawlJournal restoreCrawlJournal() throws IOException
    {
        CrawlJournal result;

        File journalFile = restoreFile(getCrawlJournalURL());

        result = CrawlJournalFile.load(journalFile);

        //
        return result;
    }

    /**
     * Restore file.
     *
     * @param url
     *            the key
     * @return the file
     */
    public File restoreFile(final URL url)
    {
        File result;

        if (url == null)
        {
            result = null;
        }
        else
        {
            result = buildFile(url);
            if (!result.exists())
            {
                result = null;
            }
        }

        //
        return result;
    }

    /**
     * Restore file to.
     *
     * @param url
     *            the url
     * @param target
     *            the target
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void restoreFileTo(final URL url, final File target) throws IOException
    {
        File source = restoreFile(url);
        if (source != null)
        {
            FileUtils.copyFile(source, target);
        }
    }

    /**
     * Restore logo to.
     *
     * @param url
     *            the url
     * @param target
     *            the target
     * @param seed
     *            the seed
     * @param generator
     *            the generator
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void restoreLogoTo(final URL url, final File target, final String seed, final DefaultLogoGenerator generator) throws IOException
    {
        if ((target == null) || (seed == null))
        {
            throw new IllegalArgumentException("Null parameter.");
        }
        else
        {
            logger.info("Restoring logo from {}", url);
            File logoFile = restoreFile(url);
            if (logoFile == null)
            {
                try
                {
                    logger.info("CatGeneratoring cat avatar: {}", target.getAbsoluteFile());
                    if ((generator == null) || (generator == DefaultLogoGenerator.CAT))
                    {
                        CatGenerator.buildAvatarTo(seed, target);
                    }
                    else
                    {
                        BirdGenerator.buildAvatarTo(seed, target);
                    }
                }
                catch (IOException exception)
                {
                    logger.warn("CatGeneratoring failed for {}: {}", seed, exception.getMessage());
                    URLUtils.copyResource("/fr/devinsy/statoolinfos/htmlize/stuff/default-organization-logo.png", target);
                }
            }
            else
            {
                FileUtils.copyFile(logoFile, target);
            }
        }
    }

    /**
     * Restore to properties.
     *
     * @param url
     *            the url
     * @return the path properties
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public PathProperties restoreToProperties(final URL url) throws IOException
    {
        PathProperties result;

        if (url == null)
        {
            result = new PathPropertyList();
        }
        else
        {
            File file = buildFile(url);
            result = PathPropertyUtils.load(file);
        }

        //
        return result;
    }

    /**
     * Restore uptime journal.
     *
     * @return the crawl journal
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public UptimeJournal restoreUptimeJournal() throws IOException
    {
        UptimeJournal result;

        File journalFile = restoreFile(getUptimeJournalURL());

        if (journalFile == null)
        {
            result = new UptimeJournal();
        }
        else
        {
            result = UptimeJournalFile.load(journalFile);
        }

        //
        return result;
    }

    /**
     * Store.
     *
     * @param url
     *            the url
     * @param source
     *            the source
     * @return the file
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public File store(final URL url, final File source) throws IOException
    {
        File result;

        if ((url == null) || (!StringUtils.startsWith(url.getProtocol(), "http")))
        {
            result = null;
        }
        else
        {
            result = buildFile(url);
            FileUtils.copyFile(source, result);
        }

        //
        return result;
    }

    /**
     * Store crawl journal.
     *
     * @param journal
     *            the journal
     */
    public void storeCrawlJournal(final CrawlJournal journal)
    {
        try
        {
            File file = Files.createTempFile("tmp-", ".statoolsinfos").toFile();
            CrawlJournalFile.save(file, journal);
            store(getCrawlJournalURL(), file);
            file.delete();
        }
        catch (IOException exception)
        {
            exception.printStackTrace();
        }
    }

    /**
     * Store.
     *
     * @param url
     *            the url
     * @param properties
     *            the properties
     * @return the file
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public File storeProperties(final URL url, final PathProperties properties) throws IOException
    {
        File result;

        if ((url == null) || (!StringUtils.startsWith(url.getProtocol(), "http")))
        {
            result = null;
        }
        else
        {
            result = buildFile(url);

            PathPropertyUtils.save(result, properties);
        }

        //
        return result;
    }

    /**
     * Store quietly.
     *
     * @param url
     *            the url
     * @return the file
     */
    public File storeQuietly2(final URL url)
    {
        File result;

        try
        {
            if ((url == null) || (!StringUtils.startsWithIgnoreCase(url.getProtocol(), "http")))
            {
                result = null;
            }
            else
            {
                final int TIMEOUT = 5000;
                result = buildFile(url);
                FileUtils.copyURLToFile(url, result, TIMEOUT, TIMEOUT);
                logger.info("Crawled {}", url);
            }
        }
        catch (IOException exception)
        {
            logger.info("Store failed for {}: {}", url, exception.getMessage());
            result = null;
        }

        //
        return result;
    }

    /**
     * Store journal.
     *
     * @param journal
     *            the journal
     */
    public void storeUptimeJournal(final UptimeJournal journal)
    {
        try
        {
            File file = Files.createTempFile("tmp-", ".statoolsinfos").toFile();
            UptimeJournalFile.save(file, journal);
            store(getUptimeJournalURL(), file);
            file.delete();
        }
        catch (IOException exception)
        {
            exception.printStackTrace();
        }
    }

    /**
     * Gets the crawl journal URL.
     *
     * @return the crawl journal URL
     * @throws MalformedURLException
     *             the malformed URL exception
     */
    public static URL getCrawlJournalURL() throws MalformedURLException
    {
        URL result;

        result = new URL("http://localhost/crawl.journal");

        //
        return result;
    }

    /**
     * Gets the uptimes journal URL.
     *
     * @return the uptimes journal URL
     * @throws MalformedURLException
     *             the malformed URL exception
     */
    public static URL getUptimeJournalURL() throws MalformedURLException
    {
        URL result;

        result = new URL("http://localhost/uptimes.journal");

        //
        return result;
    }
}
