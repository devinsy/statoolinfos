/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.cli;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class CLIUtils.
 */
public final class CLIUtils
{
    /**
     * Instantiates a new statool infos CLI.
     */
    private CLIUtils()
    {
    }

    /**
     * Checks if is matching.
     *
     * @param args
     *            the args
     * @param regexps
     *            the regexps
     * @return true, if is matching
     */
    public static boolean isMatching(final String[] args, final String... regexps)
    {
        boolean result;

        if ((args.length == 0) && (regexps == null))
        {
            result = true;
        }
        else if ((args.length != 0) && (regexps == null))
        {
            result = false;
        }
        else if (args.length != regexps.length)
        {
            result = false;
        }
        else
        {
            boolean ended = false;
            int index = 0;
            result = false;
            while (!ended)
            {
                if (index < args.length)
                {
                    String arg = StringUtils.trim(args[index]);
                    String regexp = regexps[index];

                    if (arg.matches(regexp))
                    {
                        index += 1;
                    }
                    else
                    {
                        ended = true;
                        result = false;
                    }
                }
                else
                {
                    ended = true;
                    result = true;
                }
            }
        }

        //
        return result;
    }

    /**
     * Checks if is matching ellipsis.
     *
     * @param args
     *            the args
     * @param regexps
     *            the regexps
     * @return true, if is matching ellipsis
     */
    public static boolean isMatchingEllipsis(final String[] args, final String... regexps)
    {
        boolean result;

        if ((args.length == 0) && (regexps == null))
        {
            result = true;
        }
        else if ((args.length != 0) && (regexps == null))
        {
            result = false;
        }
        else if (args.length < regexps.length)
        {
            result = false;
        }
        else
        {
            boolean ended = false;
            int index = 0;
            result = false;
            while (!ended)
            {
                if (index < args.length)
                {
                    String arg = StringUtils.trim(args[index]);
                    String regexp;
                    if (index < regexps.length)
                    {
                        regexp = regexps[index];
                    }
                    else
                    {
                        regexp = regexps[regexps.length - 1];
                    }

                    if (arg.matches(regexp))
                    {
                        index += 1;
                    }
                    else
                    {
                        ended = true;
                        result = false;
                    }
                }
                else
                {
                    ended = true;
                    result = true;
                }
            }
        }

        //
        return result;
    }
}
