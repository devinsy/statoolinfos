/*
 * Copyright (C) 2020 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.cli;

import fr.devinsy.statoolinfos.core.StatoolInfosException;

/**
 * The Class StatoolInfosCLIException.
 */
public class StatoolInfosCLIException extends StatoolInfosException
{
    private static final long serialVersionUID = 2986878456227891377L;

    /**
     * Instantiates a new statool infos CLI exception.
     */
    public StatoolInfosCLIException()
    {
        super();
    }

    /**
     * Instantiates a new statool infos CLI exception.
     *
     * @param message
     *            the message
     */
    public StatoolInfosCLIException(final String message)
    {
        super(message);
    }

    /**
     * Instantiates a new statool infos CLI exception.
     *
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public StatoolInfosCLIException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Instantiates a new statool infos CLI exception.
     *
     * @param cause
     *            the cause
     */
    public StatoolInfosCLIException(final Throwable cause)
    {
        super(cause);
    }
}