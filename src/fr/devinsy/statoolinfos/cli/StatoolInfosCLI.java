/*
 * Copyright (C) 2020-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple service statistics tool.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.cli;

import java.io.File;
import java.time.LocalDateTime;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.statoolinfos.app.StatoolInfosApp;
import fr.devinsy.statoolinfos.core.BotFilter;
import fr.devinsy.statoolinfos.util.BuildInformation;
import fr.devinsy.statoolinfos.util.Chrono;
import fr.devinsy.statoolinfos.util.Files;
import fr.devinsy.statoolinfos.util.FilesUtils;
import fr.devinsy.strings.StringList;

/**
 * The Class <code>StatoolInfosCLI</code> manages a Command Line Interface for
 * StatoolInfos.
 * 
 */
public final class StatoolInfosCLI
{
    private static Logger logger = LoggerFactory.getLogger(StatoolInfosCLI.class);

    /**
     * Instantiates a new statool infos CLI.
     */
    private StatoolInfosCLI()
    {
    }

    /**
     * Convert path.
     *
     * @param path
     *            the path
     * @return the files
     */
    public static Files convertPath(final String path)
    {
        Files result;

        result = new Files();

        if (StringUtils.isNotBlank(path))
        {
            File input = new File(path);

            if (input.exists())
            {
                if (input.isFile())
                {
                    result.add(input);
                }
                else
                {
                    for (File file : input.listFiles())
                    {
                        if ((file.isFile()) && (file.getName().endsWith(".conf")))
                        {
                            result.add(file);
                        }
                    }
                }
            }
            else
            {
                result.add(input);
            }
        }

        //
        return result;
    }

    /**
     * Display help.
     */
    public static void displayHelp()
    {
        StringList message = new StringList();

        message.append("StatoolInfos CLI version ").appendln(BuildInformation.instance().version());
        message.appendln("Usage:");
        message.appendln("    statoolinfos [ -h | -help | --help ]");
        message.appendln("    statoolinfos [ -v | -version | --version ]");
        message.appendln();
        message.appendln("    statoolinfos build                       <configurationfile>     build property files from conf and input");
        message.appendln("    statoolinfos clear                       <configurationfile>     remove property files from conf");
        message.appendln("    statoolinfos crawl                       <configurationfile>     crawl all file from conf and input");
        message.appendln("    statoolinfos format                      <fileordirectory>       format property files in tiny way");
        message.appendln("    s̶t̶a̶t̶o̶o̶l̶i̶n̶f̶o̶s̶ ̶h̶t̶m̶l̶i̶z̶e̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶<̶c̶o̶n̶f̶i̶g̶u̶r̶a̶t̶i̶o̶n̶f̶i̶l̶e̶>     REMOVED since splitweb");
        message.appendln("    statoolinfos probe   OPTION [<directory>|<configurationfile>]    generate metrics files from conf");
        message.appendln("           OPTION = [-full|-today|-previousday|-NN] with NN a day count");
        message.appendln("    statoolinfos tagdate                     <fileordirectory>       update the file.datetime file");
        message.appendln("    statoolinfos uptime                      <configurationfile>     update uptime journal");
        message.appendln();
        message.appendln("    statoolinfos list file                   <logfilesorconfigfile>  display http access log files");
        message.appendln("    statoolinfos list log      [-bot|-nobot] <logfilesorconfigfile>  display http access log lines");
        message.appendln("    statoolinfos list ip       [-bot|-nobot] <logfilesorconfigfile>  generate ip list from http log file");
        message.appendln("    statoolinfos list ua       [-bot|-nobot] <logfilesorconfigfile>  generate user agent list from http log file");
        message.appendln("    statoolinfos list visitor  [-bot|-nobot] <logfilesorconfigfile>  generate visitors (ip+ua) list from http log file");
        message.appendln("    statoolinfos stat ip       [-bot|-nobot] <logfilesorconfigfile>  generate stats about ip from http log file");
        message.appendln("    statoolinfos stat ua       [-bot|-nobot] <logfilesorconfigfile>  generate stats about user agent from http log file");
        message.appendln("    statoolinfos stat visitor  [-bot|-nobot] <logfilesorconfigfile>  generate stats about visitor (ip+ua) from http log file");
        message.appendln("    statoolinfos list errfile                <logfilesorconfigfile>  display http error log files");
        message.appendln("    statoolinfos list errlog                 <logfilesorconfigfile>  display http error log lines");

        System.out.print(message.toString());
    }

    /**
     * Display version.
     */
    public static void displayVersion()
    {
        StringList message = new StringList();

        message.appendln(BuildInformation.instance().version());

        System.out.print(message.toString());
    }

    /**
     * Normalize bash parameter.
     *
     * @param value
     *            the value
     * @return the string
     */
    private static String normalizeBashParameter(final String value)
    {
        String result;

        if (value == null)
        {
            result = null;
        }
        else
        {
            result = StringUtils.trim(value).replace("\\*", "*").replace("\\?", "?");
        }

        //
        return result;
    }

    /**
     * Parses the log filter option.
     *
     * @param source
     *            the source
     * @return the log filter
     */
    private static BotFilter parseLogFilterOption(final String source)
    {
        BotFilter result;

        if (StringUtils.equals(source, "-all"))
        {
            result = BotFilter.ALL;
        }
        else if (StringUtils.equals(source, "-bot"))
        {
            result = BotFilter.BOT;
        }
        else if (StringUtils.equals(source, "-nobot"))
        {
            result = BotFilter.NOBOT;
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * 
     * This method launch CLI.
     * 
     * @param args
     *            necessary arguments
     */
    public static void run(final String[] args)
    {
        // Set default catch.
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException(final Thread thread, final Throwable exception)
            {
                String message;
                if (exception instanceof OutOfMemoryError)
                {
                    message = "Java ran out of memory!\n\n";
                }
                else
                {
                    message = String.format("An error occured: %1s(%2s)", exception.getClass(), exception.getMessage());
                }

                logger.error("uncaughtException ", exception);
                logger.error(message);
                logger.info("Oups, an unexpected error occured. Please try again.");
            }
        });

        logger.debug("{} StatoolInfos call: {}", LocalDateTime.now(), new StringList(args).toStringSeparatedBy(" "));

        if (CLIUtils.isMatching(args))
        {
            logger.info("No parameter.");
            displayHelp();
        }
        else if (CLIUtils.isMatching(args, "(-h|--h|--help)"))
        {
            displayHelp();
        }
        else if (CLIUtils.isMatching(args, "(-v|-version|--version)"))
        {
            displayVersion();
        }
        else if (CLIUtils.isMatching(args, "build", ".+\\.conf"))
        {
            File configurationFile = new File(StringUtils.trim(args[1]));
            try
            {
                StatoolInfosApp.build(configurationFile);
            }
            catch (Exception exception)
            {
                logger.error("Error with [{}]: {}", configurationFile.getAbsoluteFile(), exception.getMessage());
            }
        }
        else if (CLIUtils.isMatching(args, "clear", ".+\\.conf*"))
        {
            File configurationFile = new File(StringUtils.trim(args[1]));
            try
            {
                StatoolInfosApp.clear(configurationFile);
            }
            catch (Exception exception)
            {
                logger.error("Error with [{}]: {}", configurationFile.getAbsoluteFile(), exception.getMessage());
            }
        }
        else if (CLIUtils.isMatching(args, "crawl", ".+\\.conf"))
        {
            Chrono chrono = new Chrono().start();

            File configurationFile = new File(StringUtils.trim(args[1]));
            try
            {
                StatoolInfosApp.crawl(configurationFile);
            }
            catch (Exception exception)
            {
                logger.error("Error with [{}]: {}", configurationFile.getAbsoluteFile(), exception.getMessage());
                exception.printStackTrace();
            }
            System.out.println(chrono.format());
        }
        else if (CLIUtils.isMatching(args, "format", ".+"))
        {
            Files inputs = FilesUtils.searchEndingWith(new File(args[1]), ".properties");
            for (File input : inputs)
            {
                try
                {
                    StatoolInfosApp.format(input);
                }
                catch (Exception exception)
                {
                    logger.error("Error with [{}]: {}", input.getAbsoluteFile(), exception.getMessage());
                    exception.printStackTrace();
                }
            }
        }
        else if (CLIUtils.isMatching(args, "list", "ip", "(-all|-bot|-nobot)", ".+\\.conf*"))
        {
            BotFilter filter = parseLogFilterOption(args[2]);
            File configurationFile = new File(StringUtils.trim(args[3]));

            StatoolInfosApp.listIps(configurationFile, filter);
        }
        else if (CLIUtils.isMatching(args, "list", "ip", ".+\\.conf"))
        {
            File configurationFile = new File(StringUtils.trim(args[2]));

            StatoolInfosApp.listIps(configurationFile, BotFilter.ALL);
        }
        else if (CLIUtils.isMatchingEllipsis(args, "list", "ip", "(-all|-bot|-nobot)", ".+"))
        {
            BotFilter filter = parseLogFilterOption(args[2]);
            Files source = new Files();
            for (int index = 3; index < args.length; index++)
            {
                source.add(new File(args[index]));
            }

            StatoolInfosApp.listIps(source, filter);
        }
        else if (CLIUtils.isMatchingEllipsis(args, "list", "ip", ".+"))
        {
            Files files = new Files();
            for (int source = 2; source < args.length; source++)
            {
                files.add(new File(args[source]));
            }

            StatoolInfosApp.listIps(files, BotFilter.ALL);
        }
        else if (CLIUtils.isMatching(args, "list", "(file|files)", ".+\\.conf"))
        {
            File configurationFile = new File(StringUtils.trim(args[2]));

            StatoolInfosApp.listAccessFiles(configurationFile);
        }
        else if (CLIUtils.isMatchingEllipsis(args, "list", "(file|files)", ".+"))
        {
            Files source = new Files();
            for (int index = 2; index < args.length; index++)
            {
                source.add(new File(args[index]));
            }

            StatoolInfosApp.listAccessFiles(source);
        }
        else if (CLIUtils.isMatching(args, "list", "(errfile|errfiles)", ".+\\.conf"))
        {
            File configurationFile = new File(StringUtils.trim(args[2]));

            StatoolInfosApp.listErrorFiles(configurationFile);
        }
        else if (CLIUtils.isMatchingEllipsis(args, "list", "(errfile|errfiles)", ".+"))
        {
            Files source = new Files();
            for (int index = 2; index < args.length; index++)
            {
                source.add(new File(args[index]));
            }

            StatoolInfosApp.listErrorFiles(source);
        }
        else if (CLIUtils.isMatching(args, "list", "(log|logs)", "(-all|-bot|-nobot)", ".+\\.conf*"))
        {
            BotFilter filter = parseLogFilterOption(args[2]);
            File configurationFile = new File(StringUtils.trim(args[3]));

            StatoolInfosApp.listLogs(configurationFile, filter);
        }
        else if (CLIUtils.isMatching(args, "list", "(log|logs)", ".+\\.conf"))
        {
            File configurationFile = new File(StringUtils.trim(args[2]));

            StatoolInfosApp.listLogs(configurationFile, BotFilter.ALL);
        }
        else if (CLIUtils.isMatchingEllipsis(args, "list", "(log|logs)", "(-all|-bot|-nobot)", ".+"))
        {
            BotFilter filter = parseLogFilterOption(args[2]);
            Files source = new Files();
            for (int index = 3; index < args.length; index++)
            {
                source.add(new File(args[index]));
            }

            StatoolInfosApp.listLogs(source, filter);
        }
        else if (CLIUtils.isMatchingEllipsis(args, "list", "(log|logs)", ".+"))
        {
            Files source = new Files();
            for (int index = 2; index < args.length; index++)
            {
                source.add(new File(args[index]));
            }

            StatoolInfosApp.listLogs(source, BotFilter.ALL);
        }
        else if (CLIUtils.isMatching(args, "list", "(errlog|errlogs)", ".+\\.conf"))
        {
            File configurationFile = new File(StringUtils.trim(args[2]));

            StatoolInfosApp.listErrorLogs(configurationFile);
        }
        else if (CLIUtils.isMatchingEllipsis(args, "list", "(errlog|errlogs)", ".+"))
        {
            Files source = new Files();
            for (int index = 2; index < args.length; index++)
            {
                source.add(new File(args[index]));
            }

            StatoolInfosApp.listErrorLogs(source);
        }
        else if (CLIUtils.isMatching(args, "list", "(useragent|ua)", "(-all|-bot|-nobot)", ".+\\.conf*"))
        {
            BotFilter filter = parseLogFilterOption(args[2]);
            File configurationFile = new File(StringUtils.trim(args[3]));

            StatoolInfosApp.listUserAgents(configurationFile, filter);
        }
        else if (CLIUtils.isMatching(args, "list", "(useragent|ua)", ".+\\.conf"))
        {
            File configurationFile = new File(StringUtils.trim(args[2]));

            StatoolInfosApp.listUserAgents(configurationFile, BotFilter.ALL);
        }
        else if (CLIUtils.isMatchingEllipsis(args, "list", "(useragent|ua)", "(-all|-bot|-nobot)", ".+"))
        {
            BotFilter filter = parseLogFilterOption(args[2]);
            Files source = new Files();
            for (int index = 3; index < args.length; index++)
            {
                source.add(new File(args[index]));
            }

            StatoolInfosApp.listUserAgents(source, filter);
        }
        else if (CLIUtils.isMatchingEllipsis(args, "list", "(useragent|ua)", ".+"))
        {
            Files source = new Files();
            for (int index = 2; index < args.length; index++)
            {
                source.add(new File(args[index]));
            }

            StatoolInfosApp.listUserAgents(source, BotFilter.ALL);
        }
        else if (CLIUtils.isMatching(args, "list", "(visitor|visitors)", "(-all|-bot|-nobot)", ".+\\.conf*"))
        {
            BotFilter filter = parseLogFilterOption(args[2]);
            File configurationFile = new File(StringUtils.trim(args[3]));

            StatoolInfosApp.listVisitors(configurationFile, filter);
        }
        else if (CLIUtils.isMatching(args, "list", "(visitor|visitors)", ".+\\.conf"))
        {
            File configurationFile = new File(StringUtils.trim(args[2]));

            StatoolInfosApp.listVisitors(configurationFile, BotFilter.ALL);
        }
        else if (CLIUtils.isMatchingEllipsis(args, "list", "(visitor|visitors)", "(-all|-bot|-nobot)", ".+"))
        {
            BotFilter filter = parseLogFilterOption(args[2]);
            Files source = new Files();
            for (int index = 3; index < args.length; index++)
            {
                source.add(new File(args[index]));
            }

            StatoolInfosApp.listVisitors(source, filter);
        }
        else if (CLIUtils.isMatchingEllipsis(args, "list", "(visitor|visitors)", ".+"))
        {
            Files source = new Files();
            for (int index = 2; index < args.length; index++)
            {
                source.add(new File(args[index]));
            }

            StatoolInfosApp.listVisitors(source, BotFilter.ALL);
        }
        else if (CLIUtils.isMatching(args, "probe", "(-full|-today|-previousday|-\\d+)", ".+"))
        {
            File configurationFile = new File(StringUtils.trim(args[2]));

            String filter = StringUtils.trim(args[1]);
            int dayCountFilter;
            if (filter.equals("-full"))
            {
                dayCountFilter = -1;
            }
            else if (filter.equals("-today"))
            {
                dayCountFilter = 0;
            }
            else if (filter.equals("-previousday"))
            {
                dayCountFilter = 1;
            }
            else
            {
                dayCountFilter = -1 * NumberUtils.toInt(filter);
            }

            try
            {
                Files inputs = FilesUtils.searchEndingWith(configurationFile, ".conf");
                if (inputs.isEmpty())
                {
                    System.out.println("No configuration file found.");
                }
                else
                {
                    for (File input : inputs)
                    {
                        try
                        {
                            System.out.println("Probing [" + input + "] with day count filter " + dayCountFilter);
                            StatoolInfosApp.probe(input, dayCountFilter);
                        }
                        catch (Exception exception)
                        {
                            logger.error("Error with [{}]: {}", input.getAbsoluteFile(), exception.getMessage());
                            exception.printStackTrace();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                logger.error("Error with [{}]: {}", configurationFile.getAbsoluteFile(), exception.getMessage());
                exception.printStackTrace();
            }
        }
        else if (CLIUtils.isMatching(args, "stat", "ip", "(-all|-bot|-nobot)", ".+\\.conf*"))
        {
            BotFilter filter = parseLogFilterOption(args[2]);
            File configurationFile = new File(StringUtils.trim(args[3]));

            StatoolInfosApp.statIps(configurationFile, filter);
        }
        else if (CLIUtils.isMatching(args, "stat", "ip", ".+\\.conf"))
        {
            File configurationFile = new File(StringUtils.trim(args[2]));

            StatoolInfosApp.statIps(configurationFile, BotFilter.ALL);
        }
        else if (CLIUtils.isMatchingEllipsis(args, "stat", "ip", "(-all|-bot|-nobot)", ".+"))
        {
            BotFilter filter = parseLogFilterOption(args[2]);
            Files source = new Files();
            for (int index = 3; index < args.length; index++)
            {
                source.add(new File(args[index]));
            }

            StatoolInfosApp.statIps(source, filter);
        }
        else if (CLIUtils.isMatchingEllipsis(args, "stat", "ip", ".+"))
        {
            Files source = new Files();
            for (int index = 2; index < args.length; index++)
            {
                source.add(new File(args[index]));
            }

            StatoolInfosApp.statIps(source, BotFilter.ALL);
        }
        else if (CLIUtils.isMatching(args, "stat", "(useragent|ua)", "(-all|-bot|-nobot)", ".+\\.conf*"))
        {
            BotFilter filter = parseLogFilterOption(args[2]);
            File configurationFile = new File(StringUtils.trim(args[3]));

            StatoolInfosApp.statUserAgents(configurationFile, filter);
        }
        else if (CLIUtils.isMatching(args, "stat", "(useragent|ua)", ".+\\.conf"))
        {
            File configurationFile = new File(StringUtils.trim(args[2]));

            StatoolInfosApp.statUserAgents(configurationFile, BotFilter.ALL);
        }
        else if (CLIUtils.isMatchingEllipsis(args, "stat", "(useragent|ua)", "(-all|-bot|-nobot)", ".+"))
        {
            BotFilter filter = parseLogFilterOption(args[2]);
            Files source = new Files();
            for (int index = 3; index < args.length; index++)
            {
                source.add(new File(args[index]));
            }

            StatoolInfosApp.statUserAgents(source, filter);
        }
        else if (CLIUtils.isMatchingEllipsis(args, "stat", "(useragent|ua)", ".+"))
        {
            Files source = new Files();
            for (int index = 2; index < args.length; index++)
            {
                source.add(new File(args[index]));
            }

            StatoolInfosApp.statUserAgents(source, BotFilter.ALL);
        }
        else if (CLIUtils.isMatching(args, "stat", "(visitor|visitors)", "(-all|-bot|-nobot)", ".+\\.conf*"))
        {
            BotFilter filter = parseLogFilterOption(args[2]);
            File configurationFile = new File(StringUtils.trim(args[3]));

            StatoolInfosApp.statVisitors(configurationFile, filter);
        }
        else if (CLIUtils.isMatching(args, "stat", "(visitor|visitors)", ".+\\.conf"))
        {
            File configurationFile = new File(StringUtils.trim(args[2]));

            StatoolInfosApp.statVisitors(configurationFile, BotFilter.ALL);
        }
        else if (CLIUtils.isMatchingEllipsis(args, "stat", "(visitor|visitors)", "(-all|-bot|-nobot)", ".+"))
        {
            BotFilter filter = parseLogFilterOption(args[2]);
            Files source = new Files();
            for (int index = 3; index < args.length; index++)
            {
                source.add(new File(args[index]));
            }

            StatoolInfosApp.statVisitors(source, filter);
        }
        else if (CLIUtils.isMatchingEllipsis(args, "stat", "(visitor|visitors)", ".+"))
        {
            Files source = new Files();
            for (int index = 2; index < args.length; index++)
            {
                source.add(new File(args[index]));
            }

            StatoolInfosApp.statVisitors(source, BotFilter.ALL);
        }
        else if (CLIUtils.isMatching(args, "tagdate", ".+"))
        {
            Files inputs = FilesUtils.searchEndingWith(new File(args[1]), ".properties");
            for (File input : inputs)
            {
                try
                {
                    StatoolInfosApp.tagDate(input);
                }
                catch (Exception exception)
                {
                    logger.error("Error with [{}]: {}", input.getAbsoluteFile(), exception.getMessage());
                    exception.printStackTrace();
                }
            }
        }
        else if (CLIUtils.isMatching(args, "uptime", ".+\\.conf"))
        {
            Chrono chrono = new Chrono().start();

            File configurationFile = new File(StringUtils.trim(args[1]));
            try
            {
                StatoolInfosApp.uptime(configurationFile);
            }
            catch (Exception exception)
            {
                logger.error("Error with [{}]: {}", configurationFile.getAbsoluteFile(), exception.getMessage());
                exception.printStackTrace();
            }
            System.out.println(chrono.format());
        }
        else
        {
            System.out.println("Bad usage.");
            displayHelp();
        }
    }
}
