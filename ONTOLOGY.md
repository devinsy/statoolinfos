# Section « File »

Cette section regroupe des informations sur le fichier *properties*.

```
# [File]

# Classe du fichier (valeur parmi Federation/Organization/Service/Device, obligatoire).
file.class = federation

# Version de l'ontologie utilisée utilisé (type STRING, recommandé).
file.protocol = ChatonsInfos-0.5

# Date et horaire de génération du fichier (type DATETIME, recommandé).
file.datetime = 2020-07-06T14:23:20

# Nom du générateur du fichier (type STRING, recommandé).
file.generator = Christian avec ses doigts

```

# Section « Federation »

Cette section regroupe des informations sur la fédération.
```
# [Federation]

# Nom de la fédération (type STRING, obligatoire).
federation.name= CHATONS

# Description de la fédération (type STRING, recommandé). 
federation.description = CHATONS est le Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires.

# Lien du site web de la fédération (type URL, recommandé).
federation.website = https://www.chatons.org/

# Lien du logo de la fédération (type URL, recommandé).
federation.logo = https://chatons.org/logo_chatons_v2.png

# Lien du favicon de la fédération (type URL, recommandé).
federation.favicon = https://chatons.org/sites/default/files/chatons_logo_tiny.png

# Lien de la page web de contact de la fédération (type URL, recommandé).
federation.contact.url = https://www.chatons.org/contact

# Courriel de contact de la fédération (type EMAIL, recommandé).
federation.contact.email = contact@chatons.org

# Lien de la page web des mentions légales de la fédération (type URL, recommandé).
federation.legal.url = https://www.chatons.org/page/mentions-l%C3%A9gales

# Lien de la documentation web de la fédération (type URL, recommandé).
federation.documentation.url = https://wiki.chatons.org/

# Lien de la documentation technique de la fédération (type URL, recommandé).
federation.documentation.technical.url = 

# Lien des tutoriels web de la fédération (type URL, recommandé).
federation.documentation.tutorial.url = 

# Date de naissance de la fédération (type DATE, recommandé).
federation.birthdate = 09/02/2016
```

# Section « Organisation »

Cette section regroupe des informations sur l'organisation.

```
# [Organisation]

# Nom de l'organisation (type STRING, obligatoire).
organization.name = Chapril

# Description de l'organisation (type STRING, recommandé).
organization.description = Chapril est le chaton de l'April

# Lien du site web de l'organisation (type URL, recommandé).
organization.website = httsp://www.chapril.org/

# Lien du logo de l'organisation (type URL, recommandé).
organization.logo = https://date.chapril.org/Chapril-banner/v1/chapril-logo-small.png |

# Lien du favicon de l'organisation (type URL, recommandé).
organization.favicon = https://date.chapril.org/favicon.png

# Nome du propriétaire de l'organisation (type STRING, optionnel).
organization.owner.name = April

# Lien du site web du propriétaire de l'organisation (type URL, optionnel).
organization.owner.website = https://www.april.org/

# Lien du logo du propriétaire de l'organisation (type URL, optionnel).
organization.owner.logo = https://www.april.org/sites/default/themes/zen_april/logo.png

# Lien du favicon du propriétaire de l'organisation (type URL, optionnel).
organization.owner.favicon = https://www.april.org/sites/default/themes/zen_april/favicon.ico

# Lien de la page web de contact de l'organisation (type URL, recommandé).
organization.contact.url = https://www.chapril.org/contact.html

# Courriel de contact de l'organisation (type EMAIL, recommandé).
organization.contact.email = contact@chapril.org

# Lien de la page des mentions légales de l'organisation (type URL, recommandé).
organization.legal.url = https://www.chapril.org/cgu.html

# Lien de la documentation technique de l'organisation (type URL, recommandé).
organization.documentation.technical.url = https://admin.chapril.org/

# Date de création de l'organisation (type DATE, recommandé).
organization.birthdate = 08/11/2018

# Date de fermeture de l'organisation (type DATE, recommandé).
organization.deathdate = 08/11/2018

```

# Section « service »

Cette section regroupe des informations sur un service.


```
# [Service]

# Nom du service (type STRING, obligatoire).
service.name = MumbleChaprilOrg

# Description du service (type STRING, recommandé).
service.description = Service libre de conférence audio

# Lien du site web du service (type URL, recommandé).
service.website = https://mumble.chapril.org/

# Lien de la page web des mentions légales du service (type URL, recommandé).
service.legal.url = https://www.chapril.org/cgu.html

# Lien de la documentation web du service (type URL, recommandé).
service.documentation.technical.url = https://admin.chapril.org/doku.php?id=admin:services:mumble.chapril.org

# Lien des aides web pour le service (type URL, recommandé).
service.documentation.tutorial = https://www.chapril.org/Mumble.html

# Lien de la page de support du service (type URL, recommandé).
service.contact.url = https://www.chapril.org/contact.html

# Courriel du support du service (type EMAIL, recommandé).
service.contact.email = mumble-support@chapril.org

# Date d'ouverture du service (type DATE, recommandé).
service.birthdate = 20/03/2020 |

# Date de fermture du service (type DATE, optionnel).
service.deathdate = 

# Statut du service (type STATUS, obligatoire).
service.status = ON

# Inscriptions requises pour utiliser le service (parmi None;Free;Member;Client, obligatoire).
service.registration = Free,Member
```


# Section « Software »

Cette section regroupe des information sur le logiciel utilisé par un service.

```
# [Software]

# Nom du logiciel (type STRING, obligatoire).
software.name = Firefox Send

# Lien du site web du logiciel (type URL, recommandé).
software.website = https://send.firefox.com/

# Lien web vers la licence du logiciel (type URL, obligatoire).
software.license.url = https://forge.april.org/Chapril/drop.chapril.org-firefoxsend/src/branch/chapril-v3.0.21/LICENSE

# Nom de la licence du logiciel (type STRING, obligatoire).
software.license.name = Mozilla Public License Version 2.0

# Version du logiciel (type STRING, recommandé).
software.version = Chapril-3.0.21

# Lien web vers les sources du logiciel (type URL, recommandé).
software.source.url = https://forge.april.org/Chapril/drop.chapril.org-firefoxsend/
```

# Section « Host » (expérimental)

Cette section regroupe des informations sur l'hébergeur.

```
# [Host]

# Nom de l'hébergeur (type STRING, obligatoire).
host.name = Foo

#
host.provider = 

# Desription de l'hébergeur (type STRING, recommandé).
host.description = 

# Type d'hébergement (un parmi VPS / CLOUD / LOCATEDSERVER / HOSTSERVER / HOMESERVER / RASPERRY, recommandé).
host.type = 

# Pays de l'hébergeur (type STRING, recommandé).
host.country.name = France

# Code pays de l'hébergeur (type STRING, recommandé).
host.country.code = FR
```

# Section « Subs »

Cette section regroupe des liens vers des entités complémentaires.

Chaque chemin doit être différent mais pas nécessairement signifiant.

```
# [Subs]

# Un lien vers un fichier properties complémentaire (type URL, optionnel).
subs.foo = https://date.chapril.org/.well-known/datechaprilorg.properties
```

Exemple :
```
subs.01 = https://date.chapril.org/.well-known/datechaprilorg.properties
subs.07 = https://paste.chapril.org/.well-known/pastechaprilorg.properties
subs.dropchaprilorg = https://drop.chapril.org/.well-known/dropchaprilorg.properties
```


# Section « Metrics »

Cette section regroupe des informations de métrologie.

```
# [Metrics]

# Nom du métrique (type STRING, recommandé).
metrics.http.total.name = Nombre total de requêtes HTTP

# Description du métriques (type STRING, recommandé).
metrics.http.total.description = Somme des requêtes HTTP ipv4 et ipv6.

# Métrique à valeur anuelle (type NUMERIC, optionnel).
metrics.http.total.2020 = 23232

# Métrique à valeur mensuelle (type MONTHS, optionnel).
metrics.http.total.2020.months=119417,122403,133108,148109,286762,334892

# Métrique à valeur hebdomadaire (type WEEKS, optionnel).
metrics.http.total.2020.months=1194,1224,1331,1481,2867,3348

# Métrique à valeur quotidienne (type DAYS, optionnel).
metrics.http.total.2020.days=11,12,13,14,28,33
```

Exemple :
```
metrics.visitors.ipv4.2020=123
metrics.visitors.ipv6.2020=123
metrics.visitors.total.2020=246

metrics.visitors.ipv4.2020.months=12;34;56;
metrics.visitors.ipv6.2020.months=12;34;56;
metrics.visitors.total.2020.months=24;68;112;

metrics.visitors.ipv4.2020.weeks=123;456;
metrics.visitors.ipv6.2020.weeks=123;456;
metrics.visitors.total.2020.weeks=246;912;

metrics.visitors.ipv4.2020.days=123;456;
metrics.visitors.ipv6.2020.days=123;456;
metrics.visitors.total.2020.days=246;912;
```
