/*
 * Copyright (C) 2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple key value database.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.checker;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.devinsy.statoolinfos.core.StatoolInfosException;

/**
 * The Class PropertyCheckerTest.
 */
public class PropertyCheckerTest
{
    // private static org.slf4j.Logger logger =
    // org.slf4j.LoggerFactory.getLogger(PropertyCheckerTest.class);

    /**
     *
     */
    @Test
    public void PropertyCheckerIsCommentTest01()
    {
        Assert.assertFalse(PropertyChecker.isComment(null));
        Assert.assertFalse(PropertyChecker.isComment(""));
        Assert.assertFalse(PropertyChecker.isComment("    "));
        Assert.assertFalse(PropertyChecker.isComment("foo"));
        Assert.assertFalse(PropertyChecker.isComment("   foo   "));

        Assert.assertTrue(PropertyChecker.isComment("#"));
        Assert.assertTrue(PropertyChecker.isComment("# "));
        Assert.assertTrue(PropertyChecker.isComment("# foo"));
    }

    /**
    *
    */
    @Test
    public void PropertyCheckerIsPropertyTest01()
    {
        Assert.assertFalse(PropertyChecker.isProperty(null));
        Assert.assertFalse(PropertyChecker.isProperty(""));
        Assert.assertFalse(PropertyChecker.isProperty("    "));
        Assert.assertFalse(PropertyChecker.isProperty("   foo   "));
        Assert.assertFalse(PropertyChecker.isProperty("foo"));
        Assert.assertFalse(PropertyChecker.isProperty("#"));
        Assert.assertFalse(PropertyChecker.isProperty("# "));
        Assert.assertFalse(PropertyChecker.isProperty("# foo"));
        Assert.assertFalse(PropertyChecker.isProperty("#foo=bar"));

        Assert.assertTrue(PropertyChecker.isProperty("foo="));
        Assert.assertTrue(PropertyChecker.isProperty("foo=    "));
        Assert.assertTrue(PropertyChecker.isProperty("foo=bar"));
        Assert.assertTrue(PropertyChecker.isProperty("foo=bar   "));
        Assert.assertTrue(PropertyChecker.isProperty("   foo="));
        Assert.assertTrue(PropertyChecker.isProperty("   foo=   "));
        Assert.assertTrue(PropertyChecker.isProperty("   foo=bar"));
        Assert.assertTrue(PropertyChecker.isProperty("   foo=bar   "));
        Assert.assertTrue(PropertyChecker.isProperty("   foo   =   bar"));
        Assert.assertTrue(PropertyChecker.isProperty("   foo   =   bar   "));
    }

    /**
     * After class.
     * 
     * @throws StatoolInfosException
     *             the Juga exception
     */
    @AfterClass
    public static void afterClass() throws StatoolInfosException
    {
    }

    /**
     * Before class.
     * 
     * @throws StatoolInfosException
     *             the Juga exception
     */
    @BeforeClass
    public static void beforeClass() throws StatoolInfosException
    {
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.DEBUG);
    }
}
