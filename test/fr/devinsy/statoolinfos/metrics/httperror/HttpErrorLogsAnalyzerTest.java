/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple key value database.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httperror;

import java.io.File;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.devinsy.statoolinfos.core.Configuration;
import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.statoolinfos.metrics.Prober;
import fr.devinsy.statoolinfos.metrics.httperrorlog.HttpErrorLogAnalyzer;
import fr.devinsy.statoolinfos.metrics.httperrorlog.HttpErrorLogs;
import fr.devinsy.statoolinfos.util.Files;
import fr.devinsy.statoolinfos.util.FilesUtils;

/**
 * The Class HttpErrorLogsTest.
 */
public class HttpErrorLogsAnalyzerTest
{
    @Test
    public void test01() throws Exception
    {
        System.out.println(System.getProperty("user.dir"));

        String source = "./test/fr/devinsy/statoolinfos/metrics/httperror/data/paste.libre-service.eu/paste*";
        Files files = FilesUtils.searchByWildcard(source);
        for (File file : files)
        {
            System.out.println(file);
        }
        HttpErrorLogs logs = new HttpErrorLogs(files);

        PathCounters counters = HttpErrorLogAnalyzer.probe(logs);

        for (String prefix : counters.getPrefixes())
        {
            System.out.println(prefix);

        }

        System.out.println("Prefix count: " + counters.getPrefixes().size());
        System.out.println("metrics.http.errors.2023.months=" + counters.get("metrics.http.errors", "2023-08").getCounter());

        Assert.assertEquals(2, counters.getPrefixes().size());
        Assert.assertEquals(25, counters.getCount("metrics.http.errors", "2023-08"));
    }

    @Test
    public void test02() throws Exception
    {
        System.out.println(System.getProperty("user.dir"));

        Configuration configuration = new Configuration();
        configuration.add("conf.probe.types", "HttpErrorLog");
        configuration.add("conf.probe.httperrorlog.file", "./test/fr/devinsy/statoolinfos/metrics/httperror/data/paste.libre-service.eu/paste*");
        // configuration.add("conf.probe.httperrorlog.pattern", "");
        // configuration.add("conf.probe.httperrorlog.dateTimePattern", "");

        PathCounters counters = Prober.probeHttpErrorLog(configuration);

        // for (String counter : counters.keySet())
        // {
        // System.out.println(counter + " " +
        // counters.get(counter).getCounter());
        // }

        Assert.assertEquals(660, counters.size());

        //
        Assert.assertEquals(210, counters.getCount("metrics.http.errors", "2023"));
        Assert.assertEquals(16, counters.getCount("metrics.http.errors", "2023-10"));
        Assert.assertEquals(7, counters.getCount("metrics.http.errors", "2023-W31"));
        Assert.assertEquals(1, counters.getCount("metrics.http.errors", "2023-05-20"));
        Assert.assertEquals(",21,19,23,26,23,22,25,10,16,9,16", counters.getMonthsValuesLine("metrics.http.errors", "2023"));
        Assert.assertEquals(",,,,3,4,4,9,4,5,4,4,5,5,3,6,7,8,4,7,4,5,5,6,5,5,6,6,4,6,7,6,6,5,1,4,3,2,2,2,6,4,2,3,3,2,,4,3,5,2,4", counters.getWeeksValuesLine("metrics.http.errors", "2023"));

        Assert.assertEquals(190, counters.getCount("metrics.http.errors.php", "2023"));
        Assert.assertEquals(7, counters.getCount("metrics.http.errors", "2023-W31"));
        Assert.assertEquals(22, counters.getCount("metrics.http.errors.php", "2023-07"));
        Assert.assertEquals(",17,19,22,20,22,22,23,10,15,8,12", counters.getMonthsValuesLine("metrics.http.errors.php", "2023"));
        Assert.assertEquals(",,,,3,3,4,6,4,5,4,4,5,5,3,6,6,4,3,7,3,5,5,6,4,5,6,6,4,6,6,5,6,5,1,4,3,2,2,2,5,4,2,3,3,1,,3,3,2,2,4", counters.getWeeksValuesLine("metrics.http.errors.php", "2023"));

        //
        Assert.assertEquals(105, counters.getCount("metrics.http.errors", "2024"));
        Assert.assertEquals(19, counters.getCount("metrics.http.errors", "2024-02"));
        Assert.assertEquals(7, counters.getCount("metrics.http.errors", "2024-W17"));
        Assert.assertEquals(2, counters.getCount("metrics.http.errors", "2024-03-15"));

        Assert.assertEquals("9,19,18,23,13,17,6", counters.getMonthsValuesLine("metrics.http.errors", "2024"));
    }

    @Test
    public void test03() throws Exception
    {
        System.out.println(System.getProperty("user.dir"));

        Configuration configuration = new Configuration();
        configuration.add("conf.probe.types", "HttpErrorLog");
        configuration.add("conf.probe.httperrorlog.file", "/home/cpm/Projets/StatoolInfos/EnvTest/logs/pastechaprilorg-error.log.gz");
        // configuration.add("conf.probe.httperrorlog.pattern", "TODO");
        // configuration.add("conf.probe.httperrorlog.dateTimePattern", "TODO");

        PathCounters counters = Prober.probeHttpErrorLog(configuration);

        Assert.assertEquals(84, counters.size());
    }

    /**
     * After class.
     * 
     * @throws StatoolInfosException
     *             the Juga exception
     */
    @AfterClass
    public static void afterClass() throws StatoolInfosException
    {
    }

    /**
     * Before class.
     * 
     * @throws StatoolInfosException
     *             the Juga exception
     */
    @BeforeClass
    public static void beforeClass() throws StatoolInfosException
    {
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.DEBUG);
    }
}
