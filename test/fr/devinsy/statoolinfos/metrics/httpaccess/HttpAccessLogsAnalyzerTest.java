/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple key value database.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.devinsy.statoolinfos.core.Configuration;
import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.metrics.PathCounters;
import fr.devinsy.statoolinfos.metrics.Prober;
import fr.devinsy.statoolinfos.util.Files;
import fr.devinsy.statoolinfos.util.FilesUtils;

/**
 * The Class HttpAccessLogsAnalyzerTest.
 */
public class HttpAccessLogsAnalyzerTest
{
    /**
     * Checks if is page test 01.
     */
    @Test
    public void isPageTest01()
    {
        Assert.assertFalse(HttpAccessLogAnalyzer.isPage(null));
        Assert.assertFalse(HttpAccessLogAnalyzer.isPage(""));
        Assert.assertFalse(HttpAccessLogAnalyzer.isPage("     "));

        Assert.assertTrue(HttpAccessLogAnalyzer.isPage("GET /foo"));
        Assert.assertTrue(HttpAccessLogAnalyzer.isPage("GET /foo42"));
        Assert.assertTrue(HttpAccessLogAnalyzer.isPage("GET /foo.html"));
        Assert.assertTrue(HttpAccessLogAnalyzer.isPage("GET /foo.HTML"));
        Assert.assertTrue(HttpAccessLogAnalyzer.isPage("GET /foo.HtMl"));
        Assert.assertTrue(HttpAccessLogAnalyzer.isPage("GET /foo.HtMl"));

        Assert.assertTrue(HttpAccessLogAnalyzer.isPage("GET /foo/bar/foo.php"));
        Assert.assertFalse(HttpAccessLogAnalyzer.isPage("GET /foo.jpg"));
    }

    /**
     * @throws Exception
     */
    @Test
    public void test01() throws Exception
    {
        // System.out.println(System.getProperty("user.dir"));

        Files files = FilesUtils.searchByWildcard("./test/fr/devinsy/statoolinfos/metrics/httpaccess/data/lseu/www*");
        // for (File file : files)
        // {
        // System.out.println(file);
        // }
        HttpAccessLogs logs = new HttpAccessLogs(files);

        PathCounters counters = HttpAccessLogAnalyzer.probe(logs);

        // for (String prefix : counters.getPrefixes())
        // {
        // System.out.println(prefix);
        // }

        System.out.println("Prefix count: " + counters.getPrefixes().size());

        Assert.assertEquals(41, counters.getPrefixes().size());
        Assert.assertEquals(1035, counters.getCount("metrics.http.hits", "2022-11"));
        Assert.assertEquals(1031, counters.getCount("metrics.http.hits", "2022-W44"));
        Assert.assertEquals(964, counters.getCount("metrics.http.hits", "2022-11-01"));
        Assert.assertEquals(1016, counters.getCount("metrics.http.hits", "2023-01"));
    }

    /**
     * @throws Exception
     */
    @Test
    public void test02() throws Exception
    {
        // System.out.println(System.getProperty("user.dir"));

        Configuration configuration = new Configuration();
        configuration.add("conf.probe.types", "HttpAccessLog");
        configuration.add("conf.probe.httpaccesslog.file", "./test/fr/devinsy/statoolinfos/metrics/httpaccess/data/lseu/www*");
        // configuration.add("conf.probe.httperrorlog.pattern", "");
        // configuration.add("conf.probe.httperrorlog.dateTimePattern", "");

        PathCounters counters = Prober.probeHttpAccessLog(configuration);

        // for (String counter : counters.keySet())
        // {
        // System.out.println(counter + " " +
        // counters.get(counter).getCounter());
        // }

        Assert.assertEquals(983, counters.size());

        //
        Assert.assertEquals(2056, counters.getCount("metrics.http.hits", "2022"));
        Assert.assertEquals(1035, counters.getCount("metrics.http.hits", "2022-11"));
        Assert.assertEquals(1031, counters.getCount("metrics.http.hits", "2022-W44"));
        Assert.assertEquals(964, counters.getCount("metrics.http.hits", "2022-11-01"));

        Assert.assertEquals(",,,,,,,,,,1035,1021", counters.getMonthsValuesLine("metrics.http.hits", "2022"));
        Assert.assertEquals(",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,1031,,4,,1021,,,,1014", counters.getWeeksValuesLine("metrics.http.hits", "2022"));
    }

    /**
     * After class.
     * 
     * @throws StatoolInfosException
     *             the Juga exception
     */
    @AfterClass
    public static void afterClass() throws StatoolInfosException
    {
    }

    /**
     * Before class.
     * 
     * @throws StatoolInfosException
     *             the Juga exception
     */
    @BeforeClass
    public static void beforeClass() throws StatoolInfosException
    {
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.DEBUG);
    }
}
