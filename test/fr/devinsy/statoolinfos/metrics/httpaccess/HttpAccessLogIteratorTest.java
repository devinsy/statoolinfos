/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple key value database.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

import java.io.File;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.util.Files;
import fr.devinsy.statoolinfos.util.FilesUtils;

/**
 * The Class HttpAccessLogIteratorTest.
 */
public class HttpAccessLogIteratorTest
{
    /**
     * Test 01.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void test01() throws Exception
    {
        System.out.println(System.getProperty("user.dir"));

        String source = "./test/fr/devinsy/statoolinfos/metrics/httpaccess/data/lseu/www*";
        Files files = FilesUtils.searchByWildcard(source);
        for (File file : files)
        {
            System.out.println(file);
        }
        HttpAccessLogs logs = new HttpAccessLogs(files);
        HttpAccessLogIterator iterator = (HttpAccessLogIterator) logs.iterator();
        while (iterator.hasNext())
        {
            // HttpAccessLog log =
            iterator.next();
            // System.out.println(log.toStringLog());
        }

        System.out.println(iterator.getLogCount());
        System.out.println(iterator.getFailedLogCount());

        Assert.assertEquals(7146, iterator.getLogCount());
        Assert.assertEquals(0, iterator.getFailedLogCount());
    }

    /**
     * After class.
     * 
     * @throws StatoolInfosException
     *             the Juga exception
     */
    @AfterClass
    public static void afterClass() throws StatoolInfosException
    {
    }

    /**
     * Before class.
     * 
     * @throws StatoolInfosException
     *             the Juga exception
     */
    @BeforeClass
    public static void beforeClass() throws StatoolInfosException
    {
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.DEBUG);
    }
}
