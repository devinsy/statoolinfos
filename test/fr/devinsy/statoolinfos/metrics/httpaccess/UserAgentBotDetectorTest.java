/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple key value database.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.devinsy.statoolinfos.core.StatoolInfosException;

/**
 * The Class UserAgentBotDetectorTest.
 */
public class UserAgentBotDetectorTest
{
    // private static org.slf4j.Logger logger =
    // org.slf4j.LoggerFactory.getLogger(UserAgentBotDetectorTest.class);

    /**
     * Test 01.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void test01() throws Exception
    {
        Assert.assertTrue(UserAgentBotDetector.isBot(""));
        Assert.assertTrue(UserAgentBotDetector.isBot("-"));
        Assert.assertTrue(UserAgentBotDetector.isBot("mobilizon.zapashcanon.fr - Mobilizon 1.1.2"));
        Assert.assertTrue(UserAgentBotDetector.isBot("Pleroma 2.3.50-242-g8e9f032f-develop; https://soc.abcdefg.club <ary@abcdefg.club>"));

        Assert.assertFalse(UserAgentBotDetector.isBot("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36"));
    }

    /**
     * After class.
     * 
     * @throws StatoolInfosException
     *             the Juga exception
     */
    @AfterClass
    public static void afterClass() throws StatoolInfosException
    {
    }

    /**
     * Before class.
     * 
     * @throws StatoolInfosException
     *             the Juga exception
     */
    @BeforeClass
    public static void beforeClass() throws StatoolInfosException
    {
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.DEBUG);
    }
}
