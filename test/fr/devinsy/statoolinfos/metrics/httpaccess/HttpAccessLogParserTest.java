/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple key value database.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.devinsy.statoolinfos.core.StatoolInfosException;

/**
 * The Class HttpAccessLogParserTest.
 */
public class HttpAccessLogParserTest
{
    /**
     * Test 01.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testParseLog01() throws Exception
    {
        // Nominal line.
        String line = "172.104.137.47 - - [30/Apr/2023:23:25:15 +0200] \"GET /default.jsp HTTP/1.1\" 302 534 \"-\" \"curl/7.54.0\"";
        Assert.assertNotNull(HttpAccessLogParser.parseLog(line));

        // Line with a \" in request string.
        line = "172.104.137.47 - - [30/Apr/2023:23:25:15 +0200] \"GET /def\\\"ault.jsp HTTP/1.1\" 302 534 \"-\" \"curl/7.54.0\"";
        Assert.assertNotNull(HttpAccessLogParser.parseLog(line));

        // Line with many \".
        line = "10.141.57.46 - - [28/Apr/2023:13:46:34 +0200] \"GET /?\\\"<?=print(9347655345-4954366)?>\\\" HTTP/1.1\" 302 5143 \"https://www.gaagle.com/\\\"<?=print(9347655345-4954366);?>\\\"\" \"Mozilliqa\\\"<?=print(9347655345-4954366);?>\\\"\"";
        Assert.assertNotNull(HttpAccessLogParser.parseLog(line));

        line = "10.48.83.91 - - [30/May/2024:00:41:04 +0200] \"GET /user.php?act=login HTTP/1.0\" 302 3383 \"554fcae493e564ee0dc75bdf2ebf94caads|a:2:{s:3:\\\"num\\\";s:107:\\\"*/SELECT 1,0x2d312720554e494f4e2f2a,2,4,5,6,7,8,0x7b24617364275d3b706870696e666f0928293b2f2f7d787878,10-- -\\\";s:2:\\\"id\\\";s:11:\\\"-1' UNION/*\\\";}554fcae493e564ee0dc75bdf2ebf94ca\" \"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.3319.102 Safari/537.36\"";
        Assert.assertNotNull(HttpAccessLogParser.parseLog(line));

        line = "10.9.74.87 - - [03/Aug/2023:21:11:59 +0200] \"GET /plus/flink.php?dopost=save&c=cat%20/etc/passwd HTTP/1.1\" 302 5438 \"<?php \\\"system\\\"($c);die;/*ref\" \"Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/44.0.2403.155 Safari/537.36\"";
        Assert.assertNotNull(HttpAccessLogParser.parseLog(line));

        line = "41.80.118.70 - - [04/Jan/2024:00:38:58 +0100] \"GET /services/audio.xhtml?url=http://go.a.gle.email.2.%5C%5Cn1@sarahjohttonw.estbrooxxertrew.e.r@hu.fe.ng.k.ua.ngniu.bi..uk41@www.zaxxle@silxxa.wooxx.o.r.t.h@ba.tt.le9.578@jxd.1.4.7m.nb.v.3.6.9.cx.z.9"
                + "51.4@ex.p.lo.si.v.edhq.g@sixxia.wooxx.o.r.t.h@r.eces.si.v.e.x.g.z@leaxxa.lanxxon@vi.rt.u.ali.rd.j@h.att.ie.m.c.d.o.w.e.ll2.56.6.3@buxxon.rene@fullxxuestickyriddl.edyxxmi.c.t.r.a@johndf.gfjhfgjf.ghfdjfhjhjhjfdgh@sybbr%3Er.eces.si.v.e.x.g.z@lexxna.l"
                + "axxton@c.o.nne.c.t.tn.tu@go.a.gle.email.2.%5C%5C%5C%5C%5C%5C%5C%5Cn1@saxxhjohxxonw.estbxxokbertrew.e.r@hu.fe.ng.k.ua.nxxiu.bi..uk41@www.zxxele@sixxia.woxxw.o.r.t.h@fullgluxxtickyriddl.edyxxmi.c.t.r.a@johndf.gfjhfgjf.ghfdjfhjhjhjfdgh@sybbr%3Er.eces"
                + ".si.v.e.x.g.z@lexxna.lxxgton@c.o.nne.c.t.tn.tu@go.a.gle.email.2.%5C%5C%5C%5C%5C%5C%5C%5Cn1@sarahxxnsonw.estbrookbxxtrew.e.r@hu.fe.ng.k.ua.nxxiu.bi..uk41@www.zaxxle@silxxa.wooxx.o.r.t.h@p.a.r.a.ju.mp.e.r.sj.a.s.s.en20.14@maxxalena.tuxx@h.att.ie.m."
                + "c.d.o.w.e.ll2.56.6.3buxxon.rene@c.o.nne.c.t.tn.tu@go.a.gle.email.2.%5C%5Cn1@saxxhjohnsonw.estbrooxxertrew.e.r@hu.fe.ng.k.ua.nxxiu.bi..uk41@www.zxxele@silvxx.wooxx.o.r.t.h@winkxxr-sanxxini.it/info/mwst01i.pdf/rk=0/rs=fzqfiq9omocv.7bggtuxxnthpge-%3F"
                + "a%5B%5D=%3Ca+href%3Dhttps%3A%2F%2Fwww.rssxxg.com%2Frequest.php%3Freq%3Dfurl%26i%3D3759037%26r%3D17559%26url%3Dhttp%253A%252F%252Fbuxxssayuk.us%3xxuy+essays%3C%2Fa%3E%3Cmeta+http-equiv%3Drefresh+content%3D0%3Burl%3Dhttps%3A%2F%2Fwww.spxxtsbook.ag%2"
                + "Fctr%2Facctmgt%2Fpl%2FopxxLink.ctr%3FctrPage%3Dhttps%3A%2F%2Fbuxxssayuk.us%2F+%2F%3E HTTP/1.0\" 200 4521 \"https://www.toxoyz.com/t/spip_cookie.php?url=http://go.a.gle.email.2.%5C%5Cn1@sarxxxohnsonw.estbrxxkbertrew.e.r@hu.fe.ng.k.ua.nxxiu.bi..uk"
                + "41@www.zaxxle@silvia.wooxx.o.r.t.h@ba.tt.le9.578@jxd.1.4.7m.nb.v.3.6.9.cx.z.951.4@ex.p.lo.si.v.edhq.g@sixxia.wooxx.o.r.t.h@r.eces.si.v.e.x.g.z@xxnna.lxxxton@vi.rt.u.ali.rd.j@h.att.ie.m.c.d.o.w.e.ll2.56.6.3@burton.rene@fullgxxestickyriddl.exxxami."
                + "c.t.r.a@johndf.gfjhfgjf.ghfdjfhjhjhjfdgh@sybbr%3Er.eces.si.v.e.x.g.z@leaxxa.laxxton@c.o.nne.c.t.tn.tu@go.a.gle.email.2.%5C%5C%5C%5C%5C%5C%5C%5Cn1@sarxxjohnxxnw.estbroxxbertrew.e.r@hu.fe.ng.k.ua.ngxxu.bi..uk41@www.zxxele@silxxa.woxxw.o.r.t.h@fullgl"
                + "uesxxckyriddl.exxnami.c.t.r.a@johxxf.gfjhfgjf.ghfdjfhjhjhjfdgh@sybbr%3Er.eces.si.v.e.x.g.z@lexxna.laxxton@c.o.nne.c.t.tn.tu@go.a.gle.email.2.%5C%5C%5C%5C%5C%5C%5C%5Cn1@sarxxjohnxxnw.estbxxokbertrew.e.r@hu.fe.ng.k.ua.ngniu.bi..ux41@www.zaxxle@sxxvi"
                + "a.wooxx.o.r.t.h@p.a.r.a.ju.mp.e.r.sj.a.s.s.en20.14@magxxlena.txxn@h.att.ie.m.c.d.o.w.e.ll2.56.6.3burxxn.rene@c.o.nne.c.t.tn.tu@go.a.gle.email.2.%5C%5Cn1@sarahjohxxonw.estbrookxxrtrew.e.r@hu.fe.ng.k.ua.nxxiu.bi..ux41@www.zxxele@sixxia.woxxw.o.r.t.h"
                + "@winxxer-sanxxini.it/info/mwst01i.pdf/rk=0/rs=fzqfixxomocv.7bggtuxxnthpge-%3Fa%5B%5D=%3Ca+href%3Dhttps%3A%2F%2Fwww.rsxxng.com%2Frequest.php%3Freq%3Dfurl%26i%3D3759037%26r%3D17559%26url%3Dhttp%253A%252F%252Fbuyexxayuk.us%3Ebuy+essays%3C%2Fa%3E%3Cme"
                + "ta+http-equiv%3Drefresh+content%3D0%3Burl%3Dhttps%3A%2F%2Fwww.sportxxook.ag%2Fctr%2Facctmgt%2Fpl%2Fopenxxnk.ctr%3FctrPage%3Dhttps%3A%2F%2Fbuxxssayuk.us%2F+%2F%3E\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Ch"
                + "rome/114.0.0.0 Safari/537.36\"";
        Assert.assertNotNull(HttpAccessLogParser.parseLog(line));
    }

    /**
     * After class.
     * 
     * @throws StatoolInfosException
     *             the Juga exception
     */
    @AfterClass
    public static void afterClass() throws StatoolInfosException
    {
    }

    /**
     * Before class.
     * 
     * @throws StatoolInfosException
     *             the Juga exception
     */
    @BeforeClass
    public static void beforeClass() throws StatoolInfosException
    {
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.DEBUG);
    }
}
