/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of StatoolInfos, simple key value database.
 * 
 * StatoolInfos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * StatoolInfos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with StatoolInfos.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.statoolinfos.metrics.httpaccess;

import java.time.LocalDateTime;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.devinsy.statoolinfos.core.StatoolInfosException;
import fr.devinsy.statoolinfos.metrics.PathCounter;
import fr.devinsy.statoolinfos.metrics.PathCounters;

/**
 * The Class VisitCountersTest.
 */
public class VisitCountersTest
{
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(VisitCountersTest.class);

    /**
     * Test 01.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void test01() throws Exception
    {
        VisitCounters visitCounters = new VisitCounters();

        //
        HttpAccessLog log = new HttpAccessLog();
        log.setIp("192.168.1.1");
        log.setTime(LocalDateTime.of(2020, 10, 20, 23, 0));
        log.setUserAgent(new UserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36"));
        visitCounters.putVisit(log);

        //
        visitCounters.storeTimeMarks("2020-10");
        PathCounters counters = visitCounters.getCounters("metrics.aaa");

        for (PathCounter counter : counters.values())
        {
            logger.info(counter.getPath() + " " + counter.getTimeMark() + " " + counter.getCounter());
        }

        Assert.assertEquals(1, counters.size());
        Assert.assertEquals(1, counters.get("metrics.aaa", "2020-10").getCounter());
    }

    /**
     * Test 02.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void test02() throws Exception
    {
        VisitCounters visitCounters = new VisitCounters();

        //
        {
            HttpAccessLog log = new HttpAccessLog();
            log.setIp("192.168.1.1");
            log.setUserAgent(new UserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36"));
            log.setTime(LocalDateTime.of(2020, 10, 20, 12, 0));
            visitCounters.putVisit(log);
        }
        {
            HttpAccessLog log = new HttpAccessLog();
            log.setIp("192.168.1.1");
            log.setUserAgent(new UserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36"));
            log.setTime(LocalDateTime.of(2020, 10, 20, 12, 20));
            visitCounters.putVisit(log);
        }

        //
        visitCounters.storeTimeMarks("2020-10");
        PathCounters counters = visitCounters.getCounters("metrics.aaa");

        for (PathCounter counter : counters.values())
        {
            logger.info(counter.getPath() + " " + counter.getTimeMark() + " " + counter.getCounter());
        }

        Assert.assertEquals(1, counters.size());
        Assert.assertEquals(1, counters.get("metrics.aaa", "2020-10").getCounter());
    }

    /**
     * Test 03.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void test03() throws Exception
    {
        VisitCounters visitCounters = new VisitCounters();

        //
        {
            HttpAccessLog log = new HttpAccessLog();
            log.setIp("192.168.1.1");
            log.setUserAgent(new UserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36"));
            log.setTime(LocalDateTime.of(2020, 10, 20, 12, 0));
            visitCounters.putVisit(log);
        }
        {
            HttpAccessLog log = new HttpAccessLog();
            log.setIp("192.168.1.1");
            log.setUserAgent(new UserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36"));
            log.setTime(LocalDateTime.of(2020, 10, 20, 12, 20));
            visitCounters.putVisit(log);
        }
        {
            HttpAccessLog log = new HttpAccessLog();
            log.setIp("192.168.1.1");
            log.setUserAgent(new UserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36"));
            log.setTime(LocalDateTime.of(2020, 10, 20, 12, 55));
            visitCounters.putVisit(log);
        }

        //
        visitCounters.storeTimeMarks("2020-10");
        PathCounters counters = visitCounters.getCounters("metrics.aaa");

        for (PathCounter counter : counters.values())
        {
            logger.info(counter.getPath() + " " + counter.getTimeMark() + " " + counter.getCounter());
        }

        Assert.assertEquals(1, counters.size());
        Assert.assertEquals(2, counters.get("metrics.aaa", "2020-10").getCounter());
    }

    /**
     * Test 04.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void test04() throws Exception
    {
        VisitCounters visitCounters = new VisitCounters();

        //
        {
            HttpAccessLog log = new HttpAccessLog();
            log.setIp("192.168.1.1");
            log.setUserAgent(new UserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36"));
            log.setTime(LocalDateTime.of(2020, 10, 20, 12, 0));
            visitCounters.putVisit(log);
        }
        {
            HttpAccessLog log = new HttpAccessLog();
            log.setIp("192.168.1.1");
            log.setUserAgent(new UserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36"));
            log.setTime(LocalDateTime.of(2020, 10, 20, 12, 55));
            visitCounters.putVisit(log);
        }
        {
            HttpAccessLog log = new HttpAccessLog();
            log.setIp("192.168.1.1");
            log.setUserAgent(new UserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36"));
            log.setTime(LocalDateTime.of(2020, 10, 20, 12, 20));
            visitCounters.putVisit(log);
        }

        //
        visitCounters.storeTimeMarks("2020-10");
        PathCounters counters = visitCounters.getCounters("metrics.aaa");

        for (PathCounter counter : counters.values())
        {
            logger.info(counter.getPath() + " " + counter.getTimeMark() + " " + counter.getCounter());
        }

        Assert.assertEquals(1, counters.size());
        Assert.assertEquals(2, counters.get("metrics.aaa", "2020-10").getCounter());
    }

    /**
     * Test 05.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void test05() throws Exception
    {
        VisitCounters visitCounters = new VisitCounters();

        //
        {
            HttpAccessLog log = new HttpAccessLog();
            log.setIp("192.168.1.1");
            log.setUserAgent(new UserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36"));
            log.setTime(LocalDateTime.of(2020, 10, 20, 12, 0));
            visitCounters.putVisit(log);
        }
        {
            HttpAccessLog log = new HttpAccessLog();
            log.setIp("192.168.1.1");
            log.setUserAgent(new UserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36"));
            log.setTime(LocalDateTime.of(2020, 10, 20, 12, 35));
            visitCounters.putVisit(log);
        }
        {
            HttpAccessLog log = new HttpAccessLog();
            log.setIp("192.168.1.1");
            log.setUserAgent(new UserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36"));
            log.setTime(LocalDateTime.of(2020, 10, 20, 12, 20));
            visitCounters.putVisit(log);
        }

        //
        visitCounters.storeTimeMarks("2020-10");
        PathCounters counters = visitCounters.getCounters("metrics.aaa");

        for (PathCounter counter : counters.values())
        {
            logger.info(counter.getPath() + " " + counter.getTimeMark() + " " + counter.getCounter());
        }

        Assert.assertEquals(1, counters.size());
        Assert.assertEquals(1, counters.get("metrics.aaa", "2020-10").getCounter());
    }

    /**
     * After class.
     * 
     * @throws StatoolInfosException
     *             the Juga exception
     */
    @AfterClass
    public static void afterClass() throws StatoolInfosException
    {
    }

    /**
     * Before class.
     * 
     * @throws StatoolInfosException
     *             the Juga exception
     */
    @BeforeClass
    public static void beforeClass() throws StatoolInfosException
    {
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.DEBUG);
    }
}
