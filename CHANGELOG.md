# StatoolInfos Changelog

All notable changes of StatoolInfos will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

- …

## [0.6.1] - 2024-09-23

- Fixed bad log init with no configuration file.
- Fixed StackOverflowError case in AccessLog regex.
- Improved Javadoc and added test for isPage method.
- Fixed Java version in build script.
- Fixed label in script build.sh.
- Upgraded log4j libs from 1.7.32 to 2.23.1.
- Improved main README and resources files.
- Improved documentation.
- Improved build script.


## [0.6.0] - 2024-08-17
Change major number because of split web event.

Changes for metrics:
- Deleted no longer targeted GSL service.
- Made Javadoc review and header review.
- Increased version after the splitweb step.
- Added CrawlCache expired management feature.
- Updated the README text.
- Added categories file from CHATONS stats workgroup with Angie from Framasoft, MrFlo and Cpm.
- Improved the federation time load (> x200).
- Huge optimization in the property check.
- Removed htmlize target directory conf.
- Migrated from Java 11 to Java 17.
- Split htmlize command code to statoolinfosweb project.
- Added new error log parameter description for configuration file.
- Fixed property label.
- Fixed parsing of \" in "" for http access logs. Less unrecognized http logs.
- Refactored http error log metrics.
- Added list errfile and list errlog CLI commands.
- Improved tests.
- Improved package naming.
- Added configuration file for list and stats CLI commands.
- Added HttpAccessLogDateTimePattern management (#sleto).
- Improved user agent bot list.


Changes for web building:
- SPLITWEB: no more web building! See the StatoolInfosWeb project.

## [0.5.1] – 2023-05-09

Changes for metrics:
- Added multi sources for Http log file search.
- Added wrong password metrics for Minetest.
- Fixed property label in Privatebin patch probe.
- Added CLI commands to test HttpAccessLog files and lines.
- Moved HttpErrorLog analyzer in dedicated package.
- Refactored prober call from CLI.
- Added new metrics analyzers.
- Added pathFilter parameter in httpAccessLog read.
- Fixed HTTP access log pattern.
- Fixed HTTP pattern management from metric conf file.
- Added Kuma in Bot detection file.
- Added not authorized HTTP status code.
- Added chat metrics for Minetest.
- Set 401 uptime case as OK.
- Improved bot pattern list with TinyStatus.
- Added metrics for GSL.
- Improved documentation (README)

Changes for web building:
- Improved property stats page with only active organizations.
- Improved PropertyStats page with only active services.
- Fixed hosting properties file management.
- Added Z support in DATETIME property check.
- Added optional parameter IPV6ONLY in statoolinfos.sh.
- Changed metrics menu entry from 2020 to 2022.
- Fixed bad category list building.
- Removed debug trace.
- Fixed categories compare.
- Fixed wording.
- Fixed separate case characters in property stats.
- Improved federation service count.
- Improved date management.
- Improved startDate and endDate management for services.
- Improved startDate and endDate management for organizations.
- Improved Federation getStartDate management.
- Added provider hypervisor charts for service stats.
- Added type chart and provider hypervisor charts for organization stats.
- Added federation organization type chart. Improved federation charts.
- Refactored federation stats. Added provider hypervisor chart.
- Added organization.type check entry.
- Added host.provider.hypervisor check entry.
- Changed color of ServiceCountYearChart.
- Fixed bad display of htmlizeHostNamePieChart.
- Swap columns in htmlizeOrganizationInOutChart.
- Fixed colors.


## [0.5.0] – 2022-01-23

This release contains many improvments:
- Extended http access log regex.
- Restricted federation stats to active organizations only.
- Improved status management in organizations and services display.
- Added Let's encrypt detection for bot filtering.
- Added date tips in header column with monthly label.
- Replaced 0 value with emoji in organizations view and services view.
- Added visits.ipv4 and visits.ipv6 management.
- Added metrics configuration templates in README.
- Changed visit and visitor definition.
- Fixed bug in error nginx time parsing.
- Fixed label display in 2 data graph.
- Added bot pattern for mod_jk dummy connection in http logs.
- Added asterisk month in organization list view.
- Added asterisk month in service list view.
- Fixed DataTables paths.
- Fixed former member display.
- Improved userAgent analyze.
- Fixed empty http log file bug.
- Added first veresion of Mumble probing (see README.md).
- Fixed bug in TimeMark code.
- Added first version of PrivateBin probing (see README.md).
- Migrated to Log4j2 (with Log4shell fix).
- Made code improvments.

Warning: visit and visitors definition has changed. Now it is more restricted: humans + method GET + page + status SUCCESS.

So some metrics have to be generate again (with -full) and some properties have to deleted:
- Added
  -  metrics.http.requesters : nombre de requêteurs.
  -  metrics.http.requesters.ipv4 : nombre de requêteurs IPv4.
  -  metrics.http.requesters.ipv6 : nombre de requêteurs IPv6.
  -  metrics.http.requesters.humans : nombre de requêteurs humains.
  -  metrics.http.requesters.bots : nombre de requêteurs bots.
  -  metrics.http.requesters.bots.walks : nombre de passages de requêteurs bots.
  -  metrics.http.metrics.methods.XXX : nombre de requête avec la méthode XXX.
  -  metrics.http.visits.ipv4 : nombre de visites en IPv4.
  -  metrics.http.visits.ipv6 : nombre de visites en IPv6.
- Changed
  - metrics.http.visits : définition plus restrictive (humains + GET + page + SUCCESS).
  - metrics.http.visitors : définition de visiteur plus restrictive.
  - metrics.http.visitors.ipv4 : définition de visiteur plus restrictive.
  - metrics.http.visitors.ipv6 : définition de visiteur plus restrictive.
- Removed
  - metrics.http.visitors.humans : remplacé par metrics.http.visitors (nouvelle définition d'une visite).
  - metrics.http.visitors.bots : supprimé (nouvelle définition d'une visite).
  - metrics.https.visits.bots : supprimé (nouvelle définition d'une visite).
 
## [0.4.1] – 2021-12-23

This release contains small improvments:
- Fixed DataTables (CVE-2021-23445).
- Improved wildcard feature in probe command (#1 thx Kepon).
- Improved sample config file.
- Added beta Minetest specific probing feature.

## [0.4.0] – 2021-12-15

This release contains probe feature to generate metrics files.
