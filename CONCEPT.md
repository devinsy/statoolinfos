# Concepts de StatoolInfos

## Principe générale

StatoolInfos s'appuie sur StatoolInfos qui utilise des fichiers publics disponibles via des liens web :

- 1 fichier web public pour décrire la fédération ;
- 1 fichier web public par organisation membre de la fédération ;
- 1 fichier web public par service des organisations.

Chaque fichier web public étant acessible via un lien web, chaque fichier public peut se trouver sur un site web différent.

Chaque fichier web public contient des informations descriptives et potentiellement des métriques d'activité.

Il suffit de récupérer tous les fichiers web publics afin de pouvoir générer des pages web, des statistiques, des graphiques…


## Format des fichiers web publics

Dans StatoolInfos, ce sont des fichiers **properties**, c'est à dire ne contenant que des lignes de texte au format `chemin=valeur`.

Un **chemin** consiste en une suite de mots séparés par des points : `organization.name=Chapril`.

Les lignes vides et les lignes commençant par un `#` sont ignorées.

Exemple :

```
# [Organisation]
organization.name=Chapril
organization.description=Chapril est le chaton de l\'April.
organization.website=https://www.chapril.org/
organization.logo=
organization.owner.name=April
organization.owner.website=https://www.april.org/
organization.owner.logo=
organization.geography=
organization.contact.url=https://www.chapril.org/contact.xhtml
organization.contact.email=contact@chapril.org
organization.legal.url=
organization.technical.url=
organization.startDate=
organization.endDate=
```

## Notion de section

Le premier mot d'un chemin est appelé la **section**. Donc chaque fichier web public est une liste de sections.

Suivant qu'il décrive la fédération, une organisation ou un service, le fichier web public ne contient pas les mêmes sections :

- Fédération = file + federation + subs + metrics
- Organisation = file + organization + subs + metrics
- Service = file + service + host + software + metrics

La liste des chemins de chaque section est normalisée dans une ontologie.


## Des données simples

Dans lignes de **properties**, les valeurs doivent être le plus simple possible.

Voici une liste de types simples :


| Type | Description | Modèle | Exemple |
| ------ | ------ | ------ | ------ |
| DATE | Une date au format ISO | ISO_8601 | 2020-07-06 |
| DATETIME | Une date horaire au format ISO | ISO_8601 | 2020-07-06T14:23:20 |
| DAYS | Une liste de 366 valeurs numériques avec séparateurs | STU | 1;2;3;4;5;…;365 |
| EMAIL | Un courriel. | ^.+@.+$ | christian@momon.org |
| MONTHS | Une liste de 12 valeurs numériques avec séparateurs | STU | 1;2;3;4;5;6;7;9;10;11;12 |
| NUMERIC | Une valeur numérique | STU | -1234.567 |
| NUMERICS | Une liste de valeurs numériques avec séparateurs | STU | 12.34;56.78;9 |
| SEPARATOR | Un caractère séparateur | `;` ou `,` | Foo;foo;;foo |
| STATUS | Un statut. |  ON/MAINTENANCE/DOWN/OFF/DRAFT | ON |
| STRING | Une chaîne de caractères quelconques. Peut contenir des `<br/>` et des `\n`. | ^.*$ | Foo is foo. |
| URL | Un lien web | ^https?://.+\..+/+$ | https://foo.foo/ |
| VALUE | Une chaîne de caractère sans séparateur | Foo |
| VALUES | Liste de valeurs de type VALUE avec séparateurs | STU | foo1;foo2;foo3 |
| WEEKS | Liste de 52 valeurs numériques avec séparateurs | STU | 1;2;3;4;5;…;51;52 |

Note for list:
* une valeur vide ou null est autorisée ;
* si une liste de valeurs contient moins de valeurs qu'attendu alors les valeurs manquantes sont considérées comme vides ou nulles.
