# StatoolInfos

StatoolInfos is…

Data are stored in property files. Each entry line uses key defined by convention.

## Primitive Types

| Type | Description | Pattern | Example |
| ------ | ------ | ------ | ------ |
| DATE | A date in ISO format | ISO_DATE  | 2020-07-06 |
| DATETIME | A date and time in ISO format | ISO_DATETIME  | 2020-07-06T14:23:20 |
| DAYS | List of 366 NUMERIC separated by SEPARATOR | STU | 1;2;3;4;5;…;365 |
| DECIMAL_DEGREE | Longitude or latitidue value | ^[+-]?\\d+[,\\.]\\d{1,8}$ | -12,3456 |
| EMAIL | An electronic address. | ^.+@.+$ | christian@momon.org |
| MONTHS | List of 12 NUMERIC separated by SEPARATOR | STU | 1;2;3;4;5;6;7;9;10;11;12 |
| NUMERIC | A numeric value | STU | -1234.567 |
| NUMERICS | List of NUMERIC separated by SEPARATOR | STU | 12.34;56.78;9 |
| SEPARATOR | The default separator character | `;` | Foo;foo;;foo |
| STATUS | A status. |  ON/MAINTENANCE/DOWN/OFF/DRAFT | ON |
| STRING | Foo string. With `<br/>` or `\n`. | ^.*$ | Foo is foo. |
| URL | A web link | ^https?://.+\..+/+$ | https://foo.foo/ |
| VALUE | String not containg SEPARATOR character | Foo |
| VALUES | List of VALUE separated by SEPARATOR | STU | foo1;foo2;foo3 |
| WEEKS | List of 52 NUMERIC separated by SEPARATOR | STU | 1;2;3;4;5;…;51;52 |

Note for list:
* empty value or null value are available;
* shorter list designs empty values.



## Sections

### Federation section

| Field | Type | Mandatory |  Description | Example |
| ------ | ------ | ------| ------ | ------ |
| federation.name | STRING | Yes | Name of federation | CHATONS |
| federation.description | STRING | Yes | Desciption of the federation | Collectif CHATONS |
| federation.website | URL | Yes | Official website URL of the federation | https://www.chatons.org/ |
| federation.logo | URL  | Wished | URL of the organization logo | https://chatons.org/logo_chatons_v2.png |
| federation.favicon | URL  | Wished | URL of the organization favicon | https://chatons.org/sites/default/files/chatons_logo_tiny.png |
| federation.contact.url | URL | No | Contact webpage of the federation | https://www.chatons.org/contact |
| federation.contact.email | EMAIL | Yes | Contact email of the federation | contact@chatons.org | 
| federation.legal.url | URL | No | Legal webpage of the federation | https://www.chatons.org/page/mentions-l%C3%A9gales |
| federation.documentation.url | URL | No | Documentation webpage of the federation | https://wiki.chatons.org/ |
| federation.documentation.technical.url | URL | No | Technical documentation webpage of the federation | |
| federation.documentation.tutorial.url | URL | No | Tutorial documentation webpage of the federation | |
| federation.birthdate | DATE | No | Date of birth of the federation | 09/02/2016 |


### File section

| Field | Type | Mandatory |  Description | Example |
| ------ | ------ | ------ | ------ | ------ |
| file.class | Federation/Organization/Service/Device | Yes | Class of the content | Service |
| file.protocol | STRING | Yes | Version of the StatoolInfos schema | StatoolInfos-0.1 |
| file.datetime | DATETIME | Yes | Date and time of the file build. | 2020-07-06T14:23:20 |
| file.generator | STRING | Yes | Generator of the file. | Cpm hands |
| file.url | URL | Yes | Origin URL of the file. | Cpm hands |


### Host section (dratf)

| Field | Type | Mandatory |  Description | Example |
| ------ | ------ | ------ | ------ | ------ |
| host.name | STRING | Yes | Name of the host | foo |
| host.provider | STRING | Yes | Provider of the host | foo |
| host.description | STRING | Wished | Description of the host | The virtual machine called foo |
| host.type | VPS / CLOUD / LOCATEDSERVER / HOSTSERVER / HOMESERVER / RASPERRY | Wished | Type of the host | HOMESERVER |
| host.country.name | STRING | Wished | Country name of the host | France |
| host.country.code | STRING | Wished | ISO country code of the host | FR |


### Metrics section

| Field | Type | Mandatory |  Description | Example |
| ------ | ------ | ------ | ------ | ------ |
| metrics.foo1.foo2.name | STRING | Wished | Name of metric | visitor Ip |
| metrics.foo1.foo2.description | STRING | Wished | Description of metric | Nombre d'ip ayant visitées |
| metrics.foo1.foo2.2020 | NUMERIC | No | Count of foo1.foo2 | 123 |
| metrics.foo1.foo2.2020.months | MONTHS | No | Month count of foo1.foo2 for year 2020 | 100;200;300;;;; |
| metrics.foo1.foo2.2020.weeks | WEEKS | No | Week count of foo1.foo2 for year 2020 | 100;200;300;;;; |
| metrics.foo1.foo2.2020.days | DAYS | No | Day count of foo1.foo2 for year 2020 | 100;200;300;;;; |

Examples:
```
metrics.visitors.ipv4.2020=123
metrics.visitors.ipv6.2020=123
metrics.visitors.total.2020=246

metrics.visitors.ipv4.2020.months=12;34;56;
metrics.visitors.ipv6.2020.months=12;34;56;
metrics.visitors.total.2020.months=24;68;112;

metrics.visitors.ipv4.2020.weeks=123;456;
metrics.visitors.ipv6.2020.weeks=123;456;
metrics.visitors.total.2020.weeks=246;912;

metrics.visitors.ipv4.2020.days=123;456;
metrics.visitors.ipv6.2020.days=123;456;
metrics.visitors.total.2020.days=246;912;
```


### Organization section

| Field | Type | Mandatory |  Description | Example |
| ------ | ------ | ------ | ------ | ------ |
| organization.name | STRING | Yes | Name of the organization | Chapril |
| organization.description | STRING | Wished | Description of the organization | Chapril est le chaton de l'April |
| organization.website | URL | Y | Official website of the organization | httsp://www.chapril.org/ |
| organization.logo | URL  | Wished | Logo URL of the organization | https://date.chapril.org/Chapril-banner/v1/chapril-logo-small.png |
| organization.favicon | URL  | Wished | Favicon URL of the organization | https://date.chapril.org/favicon.png |
| organization.owner.name | STRING | No | Name of the organization owner | April |
| organization.owner.website | URL | No | Official website of the organization owner | https://www.april.org/ |
| organization.owner.logo | URL | No | Logo URL of the organization owner | https://www.april.org/sites/default/themes/zen_april/logo.png |
| organization.owner.favicon | URL | No | Favicon URL of the organization owner | https://www.april.org/sites/default/themes/zen_april/favicon.ico |
| organization.contact.url | URL | Wished | Contact webpage of the organization | https://www.chapril.org/contact.html |
| organization.contact.email | EMAIL | Contact email of the organization | contact@chapril.org |
| organization.legal.url | URL | Wished | Legal webpage of the organization | https://www.chapril.org/cgu.html |
| organization.documentation.technical.url | URL | Wished | Technical documentation webpage of the organization | https://admin.chapril.org/ |
| organization.birthdate | DATE | Wished | Birth date of the organization | 08/11/2018 |


### Service section

| Field | Type | Mandatory |  Description | Example |
| ------ | ------ | ------ | ------ | ------ |
| service.name | STRING | Yes | Service name. | MumbleChaprilOrg |
| service.description | STRING | Yes | Service description. | Service libre de conférence audio |
| service.organization.name | STRING | Yes | Organization name. | Chapril |
| service.website.url | URL | Wished | Website of the service | https://mumble.chapril.org/ |
| service.legal.url | URL | Wished | URL | Legal webpage of the organization | https://www.chapril.org/cgu.html |
| service.documentation.technical.url | URL | No | Technical documentation webpage of the service | https://admin.chapril.org/doku.php?id=admin:services:mumble.chapril.org |
| service.documentation.tutorial.url | URL | No | Tutorial documentation webpage of the service | https://www.chapril.org/Mumble.html |
| service.contact.url | URL | Wished | Contact webpage of the service | contact@chapril.org |
| service.contact.email | EMAIL | Wished | Contact email of the service | mumble-support@chapril.org |
| service.birthdate | DATETIME | Wished | Birth date of the service | 20/03/2020 |
| service.deathdate | DATETIME | No | Death date of the service |  |
| service.status | STATUS | Yes | Service status. | ON |
| service.registration | None;Free;Member;Client | Yes | Registration requirement | Free; Member|

### Subs section

| Field | Type | Mandatory |  Description | Example |
| ------ | ------ | ------ | ------ | ------ |
| subs.foo | URL | No | URL of a sub node. | Firefox Send |

Example:
```
subs.datechaprilorg=https://date.chapril.org/.well-known/datechaprilorg.properties
subs.pastechaprilorg=https://paste.chapril.org/.well-known/pastechaprilorg.properties
subs.dropchaprilorg=https://drop.chapril.org/.well-known/dropchaprilorg.properties
```



### Software section

| Field | Type | Mandatory |  Description | Example |
| ------ | ------ | ------ | ------ | ------ |
| software.name | STRING | Yes | Name of the software | Firefox Send |
| software.website | URL | Yes | Offical website of the software | https://send.firefox.com/ |
| software.license.url | URL | Yes | Webpage of the software license | https://forge.april.org/Chapril/drop.chapril.org-firefoxsend/src/branch/chapril-v3.0.21/LICENSE |
| software.license.name | STRING | Yes | Name of the license | Mozilla Public License Version 2.0 |
| software.version | STRING | Yes | Version of the software | Chapril-3.0.21 |
| software.source.url | URL | Yes | URL of the software source | https://forge.april.org/Chapril/drop.chapril.org-firefoxsend/ |





## Classes

### Federation class

Federation = file + federation + subs + metrics

### Organization class

Organization = file + organization + subs + metrics

### Service class

Service = file + service + host + software + metrics + extras

### Device class

Device = file + device + system



# Draft

| Field | Type | Mandatory |  Description |
| ------ | ------ | ------ | ------ |
| services.* | URL | Y | URL of the Statool file of the service |

| Field | Type | Mandatory |  Description |
| ------ | ------ | ------ | ------ |
| services.urls.* | URL | Y | URL of services |
