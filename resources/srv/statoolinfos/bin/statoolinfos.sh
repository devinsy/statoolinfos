#!/bin/bash

# Optional environment settings:
#   export LOG4J_CONFIGURATION_FILE="toto.properties"
#   export LOG4J_LEVEL=ERROR
# https://logging.apache.org/log4j/log4j-2.11.2/manual/configuration.html#System_Properties

# Java check.
javaCheck=`which java`
if [[ "$javaCheck" =~ ^/.* ]]; then
    #echo "Java requirement............... OK"
    
    # Optional system properties:
    #   LOGFILE="-Dlog4j2.configurationFile=../../log4j2.properties"
    #   LOGLEVEL="-Dlog4j2.level=ERROR"
    #   IPV6ONLY="-Djava.net.preferIPv4Stack=false -Djava.net.preferIPv6Addresses=true"
    # https://logging.apache.org/log4j/log4j-2.11.2/manual/configuration.html#System_Properties

    java -Djava.awt.headless=true $IPV6ONLY $LOGFILE $LOGLEVEL -jar  "$(dirname "$0")"/statoolinfos.jar $@
else
    echo "Java requirement............... MISSING"
fi
